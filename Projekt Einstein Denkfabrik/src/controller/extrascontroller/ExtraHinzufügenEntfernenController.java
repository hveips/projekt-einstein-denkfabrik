package controller.extrascontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;
import view.extrasview.ExtraHinzufügenEntfernenGUI;

public class ExtraHinzufügenEntfernenController 
{
	private DatabaseManager db;
	private ExtraHinzufügenEntfernenGUI gui;
	
	/**
	 * Der Konstruktor erzeugt den ExtraHinzufügenEntfernenController.
	 * @param gui
	 */
	public ExtraHinzufügenEntfernenController(ExtraHinzufügenEntfernenGUI gui)
	{
		this.gui = gui;
	}
	
	/**
	 * Die Methode führt die beiden Methoden istExtraInBuex() und istExtraInResex() aus und vergleicht die beiden zurückgegebenen ArrayListen.
	 * Doppelt vorkommende Bezeichnungen werden gelöscht.
	 * @return ArrayList<String> bezeichnungen
	 */
	public ArrayList<String> istExtraInBuexResex()
	{
		ArrayList<String> bezeichnungen = new ArrayList<String>();
		ArrayList<String> bezeichnungen2 = new ArrayList<String>();
		bezeichnungen = istExtraInBuex();
		bezeichnungen2 = istExtraInResex();
		bezeichnungen.removeAll(bezeichnungen2);
		bezeichnungen.addAll(bezeichnungen2);
		return bezeichnungen;
	}
	
	/**
	 * Die Methode greift über den SELECT-Befehl auf die Datenbank zu und holt sich alle Bezeichnungen eines Extras, die in allen Buchung vorkommen, einmalig heraus.
	 * Das Attribut bezeichung wird als String ausgelesen, zwischengespeichert und in einer Liste gespeichert.
	 * @return ArrayList<String> bezeichnungen
	 */
	public ArrayList<String> istExtraInBuex()
	{
		ArrayList<String> bezeichnungen = new ArrayList<String>();
		String sqlStatement = "SELECT DISTINCT bezeichnung FROM buex";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String bezeichnung = rs.getString("bezeichnung");
				bezeichnungen.add(bezeichnung);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return bezeichnungen;
	}
	
	/**
	 * Die Methode greift über den SELECT-Befehl auf die Datenbank zu und holt sich alle Bezeichnungen eines Extras, die in allen Reservierungen vorkommen, einmalig heraus.
	 * Das Attribut bezeichung wird als String ausgelesen, zwischengespeichert und in einer Liste gespeichert.
	 * @return ArrayList<String> bezeichnungen2
	 */
	public ArrayList<String> istExtraInResex()
	{
		ArrayList<String> bezeichnungen2 = new ArrayList<String>();
		String sqlStatement2 = "SELECT DISTINCT bezeichnung FROM resex";
		try
		{
			ResultSet rs2 = db.getConnection().select(sqlStatement2);
			while(rs2.next())
			{
				String bezeichnung2 = rs2.getString("bezeichnung");
				bezeichnungen2.add(bezeichnung2);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return bezeichnungen2;
	}
	
	/**
	 * Die Methode entscheidet ob noch ein Extra hinzugefügt werden darf.
	 * @return boolean hinzufügen
	 */
	public boolean hinzufügenEntscheidung()
	{
		boolean hinzufügen = true;
		if(getAnzahlEssenGetraenke()[0] == 6 && getAnzahlEssenGetraenke()[1] == 4)
		{
			hinzufügen = false;
		}
		return hinzufügen;
	}
	
	/**
	 * Die Methode greift über den UPDATE-Befehl mit dem Parameter bezeichnung auf die Datenbank zu, löscht ein Extra
	 * und aktualisiert die Tabelle in der ExtraHinzufügenEntfernenGUI.
	 * Der Nutzer wird vorher gefragt, ob er/sie sich sicher ist, dass das Extra gelöscht werden soll.
	 * @param bezeichnung
	 */
	public void deleteExtra(String bezeichnung)
	{
		String sqlStatement = "DELETE FROM Extra WHERE bezeichnung = '" + bezeichnung + "'";
		int option = JOptionPane.showConfirmDialog(gui, "Sind Sie sicher, dass Sie  das Extra " + gui.getTable().getValueAt(gui.getTable().getSelectedRow(), 0) + " löschen wollen?", "Löschen bestätigen", JOptionPane.YES_NO_OPTION);
		if(option == JOptionPane.YES_OPTION)
		{
			try
			{
				db.getConnection().update(sqlStatement);
				gui.getUpdatedTableModel();
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Fehler bei Datenbank-Verbindung!");
			}
		}
	}
	
	/**
	 * Die Methode greift auf die Tabelle der ExtraHinzufügenEntfernenGUI zu und holt sich aus jeder Zeile die Information, ob ein "Essen" oder ein "Getränk" vorliegt.
	 * Für beide Sachen werden zwei Integer-Zahlen hochgezählt, je nachdem was vorliegt und am Ende in einem Array gespeichert.
	 * @return int[] anzahl
	 */
	public int[] getAnzahlEssenGetraenke()
	{
		int a = 0;
		int b = 0;
		for(int i = 0;i < gui.getTable().getRowCount();i++)
		{
			if(gui.getTable().getValueAt(i, 2).equals("Essen"))
			{
				a++;
			}
			else
			{
				b++;
			}
		}
		int[] anzahl = {a, b};
		return anzahl;
	}
	
	/**
	 * Die Methode greift über den INSERT-Befehl mit dem Parameter bezeichnung und den Variablen prize und a auf die Datenbank zu.
	 * Sie speichert das Extra, führt den COMMIT-Befehl aus und gibt aus, ob dies erfolgreich war oder nicht. 
	 * @param bezeichnung
	 * @param preis
	 * @param art
	 * @return boolean success
	 */
	public boolean addExtra(String bezeichnung, String preis, String art)
	{
		boolean success = false;
		String prize = preis.replace(".", ",");
		int a;
		if(art.equals("Essen"))
		{
			a = 1;
		}
		else
		{
			a = 0;
		}
		String sqlStatement = "INSERT INTO Extra(bezeichnung, preis_pro_person, essen) VALUES ('" + bezeichnung + "', '" + prize + "', '" + a + "')";
		try
		{
			db.getConnection().insert2(sqlStatement);
			db.getConnection().commit();
			success = true;
		}
		catch(SQLException e)
		{
			success = false;
		}
		return success;
	}
}