package controller.extrascontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;
import model.Extra;

public class ExtraController 
{
	private DatabaseManager db;
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich jedes Extra.
	 * F�r jedes Extra werden die Attribute bezeichnung und preis_pro_preson zwischengespeichert, ein Extra erzeugt 
	 * und in einer Liste gepseichert, die zur�ckgegeben wird.
	 * @return ArrayList<Extra> extras
	 */
	public ArrayList<Extra> ladeExtrasPreise()
	{
		ArrayList<Extra> extras = new ArrayList<Extra>();
		String sqlStatement = "SELECT * FROM EXTRA";
		String bezeichnung;
		String preis_pro_person;
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				bezeichnung = rs.getString("bezeichnung");
				String prize = rs.getString("preis_pro_person");
				preis_pro_person = prize.replace(".", ",");
				
				Extra extra = new Extra(bezeichnung, preis_pro_person);
				extras.add(extra);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return extras;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich jedes Extra.
	 * F�r jedes Extra wird jedes Attribut zwischengespeichert, ein Extra erzeugt und in einer Liste gepseichert, die zur�ckgegeben wird.
	 * @return ArrayList<Extra> extras
	 */
	public ArrayList<Extra> ladeExtras()
	{
		ArrayList<Extra> extras = new ArrayList<Extra>();
		String sqlStatement = "SELECT * FROM EXTRA";
		String bezeichnung;
		String preis_pro_person;
		String art;
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				bezeichnung = rs.getString("bezeichnung");
				String prize = rs.getString("preis_pro_person");
				preis_pro_person = prize.replace(".", ",");
				if(rs.getInt("essen") == 1)
				{
					art = "Essen";
				}
				else
				{
					art = "Getr�nk";
				}
				Extra extra = new Extra(bezeichnung, preis_pro_person, art);
				extras.add(extra);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return extras;
	}
}