package controller.extrascontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.SQLException;

import controller.DatabaseManager;
import view.extrasview.PreisExtra�ndernGUI;

public class PreisExtra�ndernController 
{
	private PreisExtra�ndernGUI guiChange;
	private DatabaseManager db;

	/**
	 * Der Konstruktor erzeugt den PreisExtra�ndernKontroller.
	 * @param guiChange
	 */
	public PreisExtra�ndernController(PreisExtra�ndernGUI guiChange)
	{
		this.guiChange = guiChange;
	}
	
	/**
	 * Die Methode greift �ber den UPDATE-Befehl auf die Datenbank zu und setzt den Preis aus dem Eingabefeld der PreisExtra�ndernGUI zum passenden Extra.
	 * Der Erfolg wird zur�ckgegeben.
	 * @return boolean success
	 */
	public boolean change()
	{
		String bezeichnung = guiChange.getBezeichnungInput();
		String preis = guiChange.getPreisInput();
		boolean success = false;
		
		String sqlStatement = "UPDATE Extra SET preis_pro_person = '" + preis + "' WHERE bezeichnung = '" + bezeichnung + "'";
		try
		{
			db.getConnection().update2(sqlStatement);
			db.getConnection().commit();
			success = true;
		}
		catch(SQLException e)
		{
			success = false;
		}
		return success;
	}
}