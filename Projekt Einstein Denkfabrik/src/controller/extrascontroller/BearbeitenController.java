package controller.extrascontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;

public class BearbeitenController 
{
	private DatabaseManager db;
	private int anzahlEssen = 0;
	private int anzahlGetraenke = 0;
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich alle Bezeichnungen der Getr�nke (WHERE essen = 0).
	 * Sie z�hlt au�erdem mit jeder Bezeichnung die Instanzvariable anzahlGetraenke um einen nach oben.
	 * @return ArrayList<String> getraenke
	 */
	public ArrayList<String> ladeExtraGetraenkeBezeichnungen()
	{
		ArrayList<String> getraenke = new ArrayList<String>();
		String sqlStatement = "SELECT bezeichnung FROM Extra WHERE essen = 0 ORDER BY bezeichnung ASC";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String getraenk = rs.getString("bezeichnung");
				getraenke.add(getraenk);
				anzahlGetraenke++;
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return getraenke;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich alle Bezeichnungen vom Essen (WHERE essen = 1).
	 * Sie z�hlt au�erdem mit jeder Bezeichnung die Instanzvariable anzahlEssen um einen nach oben.
	 * @return ArrayList<String> essen
	 */
	public ArrayList<String> ladeExtraEssenBezeichnungen()
	{
		ArrayList<String> essen = new ArrayList<String>();
		String sqlStatement = "SELECT bezeichnung FROM Extra WHERE essen = 1 ORDER BY bezeichnung ASC";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String esse = rs.getString("bezeichnung");
				essen.add(esse);
				anzahlEssen++;
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return essen;
	}
	
	/**
	 * Die Methode gibt die Instanzvariable anzahlGetraenke zur�ck.
	 * @return int anzahlGetraenke
	 */
	public int getAnzahlGetraenke()
	{
		return anzahlGetraenke;
	}
	
	/**
	 * Die Methode gibt die Instanzvariable anzhalEssen zur�ck.
	 * @return int anzahlEssen
	 */
	public int getAnzahlEssen()
	{
		return anzahlEssen;
	}
}