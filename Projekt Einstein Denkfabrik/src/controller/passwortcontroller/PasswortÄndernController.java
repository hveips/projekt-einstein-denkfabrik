package controller.passwortcontroller;

/**
 * @author Alexander Stavski
 * Diese Controller-Klasse dient der View "Passwort�ndernGUI" und erm�glicht es ihr
 * das Passwort zu �ndern
 */

import javax.swing.JOptionPane;

import controller.LoginController;
import view.passwortview.Passwort�ndernGUI;

public class Passwort�ndernController
{

	private Passwort�ndernGUI gui;
	private PasswortVergessenController passwortVergessenController = new PasswortVergessenController();
	private LoginController loginController = new LoginController();
	private boolean samePassword;
	private boolean save2;

	public Passwort�ndernController(Passwort�ndernGUI gui)
	{
		this.gui = gui;
	}

	/**
	 * Die Methode ist f�r das �ndern des Passwortes verantwortlich
	 * @return boolean-Wert, ob das Passwort erfolgreich ge�ndert wurde oder nicht
	 */
	public boolean passwort�ndern()
	{
		boolean passwortGe�ndert = false;
		char[] eingabePasswort = gui.getNewPassword().getPassword();
		char[] eingabeWiederholePasswort = gui.getRepeatPassword().getPassword();
		if (eingabePasswort.length == 0 || eingabeWiederholePasswort.length == 0)
		{
			JOptionPane.showMessageDialog(null, "Kein Passwort gesetzt!");
		} else
		{
			{
				samePassword = passwortVergessenController.pruefeGleichheit(eingabePasswort, eingabeWiederholePasswort);
				if (samePassword == true)
				{
					String passwort = String.valueOf(eingabePasswort);
					save2 = loginController.speicherePasswort(passwort, gui.getBenutzername());
					if (save2 == true)
					{
						JOptionPane.showMessageDialog(null, "Passwort wurde ge�ndert");
						passwortGe�ndert = true;
					}
				} else if (samePassword == false)
				{
					JOptionPane.showMessageDialog(null, "Passwort stimmt nicht �berein!");
				}
			}
		}
		return passwortGe�ndert;
	}
}
