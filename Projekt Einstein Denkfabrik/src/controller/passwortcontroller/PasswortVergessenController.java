package controller.passwortcontroller;

/**
 * @author Hendrik Veips
 */

public class PasswortVergessenController 
{
	private boolean same = false;
	
	/**
	 * Die Methode pr�ft die Gleichheit der beiden Parameter eingabe1 und eingabe2 und gibt das Ergebnis zur�ck.
	 * @param eingabe1
	 * @param eingabe2
	 * @return boolean same
	 */
	public boolean pruefeGleichheit(char[] eingabe1, char[] eingabe2)
	{
		if(eingabe1.length == eingabe2.length)
		{
			int i = 0;
			while(i < eingabe1.length)
			{
				if(eingabe1[i] == eingabe2[i])
				{
					if(i == eingabe1.length - 1)
					{
						same = true;
						i++;
					}
					else
					{
						i++;
					}
				}
				else
				{
					same = false;
					i = eingabe1.length;
				}
			}
		}
		else
		{
			same = false;
		}
		return same;
	}
}