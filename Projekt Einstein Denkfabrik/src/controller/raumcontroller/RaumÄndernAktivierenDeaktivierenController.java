package controller.raumcontroller;

/**
 * @author Niklas Nebeling, Hendrik Veips
 */
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Niklas Nebeling, Hendrik Veips
 */

import java.util.ArrayList;

import model.Ausstattung;
import model.Raum;
import view.raumview.AusstattungAnzeigenVerändernGUI;
import view.raumview.RaumÄndernAktivierenDeaktivierenGUI;
import view.raumview.RaumÄndernGUI;

public class RaumÄndernAktivierenDeaktivierenController 
{	
	private RaumController DBRaum;
	private RaumÄndernAktivierenDeaktivierenGUI gui;
	private Raum raum;
	private ArrayList<Ausstattung> ausstattung;	

	/**
	 * Kontruktor der den RaumController initialisiert und die gui setzt.
	 * @param gui Instanzvariable RaumÄndernAktivierenDeaktivierenGUI
	 */
	public RaumÄndernAktivierenDeaktivierenController(RaumÄndernAktivierenDeaktivierenGUI gui) 
	{
		DBRaum = new RaumController();
		this.gui = gui;
	}
	
	/**
	 * Die Methode entscheidet ob man einen Raum aktivieren oder deaktivieren kann. Wenn der Raum bereits aktiv ist, kann er nicht erneut aktiviert werden
	 * und wenn der Raum inaktiv ist, kann er nicht erneut deaktiviert werden. Dafür werden die Buttons active oder deactive auf enabled = false oder
	 * enabled = true gesetzt.
	 * @author Hendrik Veips
	 */
	public void aktivierenDeaktivierenUnterscheidung()
	{
		gui.getTable().addMouseListener(new MouseAdapter()
		{
			public void mouseReleased(MouseEvent me)
			{
				if(gui.getTable().getValueAt(gui.getTable().getSelectedRow(), 3).equals("aktiv"))
				{
					gui.getActiveButton().setEnabled(false);
				}
				else
				{
					gui.getActiveButton().setEnabled(true);
				}
				if(gui.getTable().getValueAt(gui.getTable().getSelectedRow(), 3).equals("inaktiv"))
				{
					gui.getDeactiveButton().setEnabled(false);
				}
				else
				{
					gui.getDeactiveButton().setEnabled(true);
				}
			}
		});
	}
	
	/**
	 * Wird von der RaumÄndernAktivierenDeaktivierenGUI aufgerufen um den in der Tabelle
	 * ausgewählten zeilenindex zu übergeben. Setzt den Raum und lädt seine Aussstattung aus der DB.
	 * @param row Die Selektierte Zeile der Tabelle
	 */
	public void übergebeReihe(int row) 
	{
		raum = gui.getTableModel().getData(row);
		try
		{
			ausstattung = DBRaum.ladeAusstattung(raum.getRaumNr());
		}
		catch (Exception e)
		{
			System.out.println("Ausstattung konnte nicht geladen werden.");
		}
		
	}
	
	/**
	 * Tabellenupdate
	 */
	public void update() 
	{
		gui.getTableModel().databaseUpdated();
	}
	
	/**
	 * Aktiviert den gesetzten Raum.
	 */
	public void raumAktivieren()
	{
		DBRaum.raumAktivieren(raum.getRaumNr());
	}
	
	/**
	 * Deaktiviert den gesetzten Raum.
	 */
	public void raumDeaktivieren() 
	{
		DBRaum.raumDeaktivieren(raum.getRaumNr());
	}
	
	/**
	 * Erzeugt eine RaumÄndernGUI und übergibt ihr die RaumÄndernAktivierenDeaktivierenGUI, 
	 * this als controller und die Sitzplätze des Raumes.
	 */
	public void erzeugeRaumÄndernGUI() 
	{
		new RaumÄndernGUI(gui, this,raum.getSitzplätze());
	}
	
	/**
	 * Erzeugt eine AusstattungAnzeigenVerändernGUI.
	 */
	public void erzeugeAusstattungVerändernGUI() 
	{
		new AusstattungAnzeigenVerändernGUI(this, ausstattung, true);
	}
	
	public void raumÄndern(int sitzplätze) 
	{
		String kategorie;
		int rnr = raum.getRaumNr();
		
		if(sitzplätze > 0 && sitzplätze < 25) 
		{
			kategorie = "D";
		}
		else if(sitzplätze >= 25 && sitzplätze < 50) 
		{
			kategorie = "C";
		}
		else if(sitzplätze >= 50 && sitzplätze <150 ) 
		{
			kategorie = "B";
		}
		else if(sitzplätze >= 150 && sitzplätze <= 9999) 
		{
			kategorie = "A";
		}
		else 
		{			//negative Sitzplätze werden nich akzeptiert
			return;
		}
		
		Raum raum = new Raum(rnr, sitzplätze, kategorie, ausstattung);
		DBRaum.raumÄndern(raum);	
		ausstattung.clear();
	}
	
	/**
	 * Leert die Ausstattungsliste
	 */
	public void clearAusstattung() 
	{
		ausstattung.clear();
	}
	
	/**
	 * Fügt ein Ausstattungsobjekt der Ausstattungsliste hinzu
	 * @param bezeichnung Die Bezeichnung des Ausstattungsobjekts
	 * @param anzahl Die Anzahl des Ausstattungsobjektes
	 */
	public void ausstattungErzeugen(String bezeichnung, int anzahl) 
	{
		ausstattung.add(new Ausstattung(bezeichnung, anzahl));
	}
}