package controller.raumcontroller;

/**
 * @author Niklas Nebeling
 */

import java.util.ArrayList;

import model.Ausstattung;
import model.Raum;

public class RaumHinzuf�genController 
{	
	private RaumController DBRaum;
	ArrayList<Ausstattung> ausstattung = new ArrayList<Ausstattung>();	
	
	/**
	 * Im Kontruktor wird der RaumController initialisiert
	 */
	public RaumHinzuf�genController() 
	{
		DBRaum = new RaumController();
	}
	
	/**
	 * Wird von der RaumHinzuf�genGUI aufgerufen, berechnet die Kategorie des 
	 * Raums und ruft die  Datenbankmethode auf
	 * @param rnr Die Raumnummer des neuen Raumes
	 * @param sitzpl�tze Die Sitzplatzanzahl des neuen RaumesS
	 */
	public void raumAnlegen(int rnr, int sitzpl�tze) 
	{
		String kategorie;
		
		if(sitzpl�tze > 0 && sitzpl�tze < 25) 
		{
			kategorie = "D";
		}
		else if(sitzpl�tze >= 25 && sitzpl�tze < 50)
		{
			kategorie = "C";
		}
		else if(sitzpl�tze >= 50 && sitzpl�tze <150 ) 
		{
			kategorie = "B";
		}
		else if(sitzpl�tze >= 150 && sitzpl�tze <= 9999) 
		{
			kategorie = "A";
		}
		else 
		{			//negative Sitzpl�tze werden nich akzeptiert
			return;
		}
		
		Raum raum = new Raum(rnr, sitzpl�tze, kategorie, ausstattung);
		DBRaum.raumHinzufuegen(raum);	
		ausstattung.clear();
	}
	
	/**
	 * Leert die Ausstattungsliste.
	 */
	public void clearAusstattung() 
	{
		ausstattung.clear();
	}
	
	/**
	 * Erzeugt ein Ausstattungsobjekt und f�gt es der Ausstattungsliste hinzu.
	 * @param bezeichnung
	 * @param anzahl
	 */
	public void ausstattungErzeugen(String bezeichnung, int anzahl) 
	{
		ausstattung.add(new Ausstattung(bezeichnung, anzahl));
	}
}