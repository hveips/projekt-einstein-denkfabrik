package controller.raumcontroller;

/**
 * @author Niklas Nebeling
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import controller.DatabaseManager;
import model.Ausstattung;
import model.Raum;

public class RaumController 
{	

	/**
	 * Diese Methode erzeugt SQL-Strings zur Erzeugung eines neuen Raumobjektes 
	 * in der Datenbank und f�hrt diese dann aus, gibt Erfolgsmeldung dar�ber aus.
	 * @param neuerRaum Der hinzuzuf�gende Raum
	 */
	public void raumHinzufuegen(Raum neuerRaum) 
	{
		ArrayList<String> Statements = new ArrayList<String>();
		String sqlStatement = "INSERT INTO Raum(rnr, sitzpl�tze, k�rzel, inbenutzung) VALUES(";
		sqlStatement += neuerRaum.getRaumNr() + ", ";
		sqlStatement += neuerRaum.getSitzpl�tze() + ", ";
		sqlStatement += "'" + neuerRaum.getK�rzel() + "', ";
		sqlStatement += 1 + ")";
		Statements.add(sqlStatement);
		
		ArrayList<Ausstattung> aust = neuerRaum.getAusstattung();
		for(int i=0; i<aust.size(); i++) 
		{
			sqlStatement = "INSERT INTO AURA(bezeichnung, rnr, menge) VALUES(";
			sqlStatement += "'" + aust.get(i).getBezeichnung() + "', ";
			sqlStatement += neuerRaum.getRaumNr() + ", ";
			sqlStatement += aust.get(i).getMenge() + ")";
			Statements.add(sqlStatement);
		}	
		
		try 
		{
			for(int i=0; i<Statements.size(); i++) 
			{
				DatabaseManager.getConnection().insert2(Statements.get(i));
			}
			DatabaseManager.getConnection().commit();
			JOptionPane.showMessageDialog(null, "Raum wurde erfolgreich angelegt.");
		} 
		catch (Exception e) 
		{
			DatabaseManager.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 
	}
	
	/**
	 * L�dt die R�ume aus der Datenbank und gibt sie f�r das tablemodel in einer List zur�ck.
	 * @return raumliste Die Liste mit den R�umen.
	 */
	public List<Raum> ladeRaumListe() 
	{
		List<Raum> raumliste = new ArrayList<Raum>();
		String sqlStatement = "SELECT * FROM RAUM ORDER BY rnr ASC";
		int rnr;
		String k�rzel;
		int sitzpl�tze;
		String inBenutzung;
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			while(rs.next()) 
			{
				rnr = rs.getInt("rnr");
				k�rzel = rs.getString("k�rzel");
				sitzpl�tze = rs.getInt("Sitzpl�tze");
				int a = rs.getInt("inBenutzung");
				if(a == 0)
				{
					inBenutzung = "inaktiv";
				}
				else
				{
					inBenutzung = "aktiv";
				}
				
				Raum raum = new Raum(rnr, sitzpl�tze, k�rzel, inBenutzung);
				raumliste.add(raum);
			}	
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return raumliste;
	}
	
	/**
	 * L�dt f�r die Tabelle beim Raum buchen/reservieren eine Liste der zwischen und an den 
	 * angegebenen Tages/Uhrzeiten freien R�ume und gibt diese in einer List zur�ck.
	 * @param anfangsdatum
	 * @param enddatum
	 * @param eint�gig true=eint�gig, false=mehrt�gig
	 * @return raumliste Die Liste mit den R�umen.
	 */
	public List<Raum> ladeRaumListeNachDatum(String anfangsdatum, String enddatum, boolean eint�gig) 
	{
		List<Raum> raumliste = new ArrayList<Raum>();
		Raum[] r�ume;
		String sqlStatement = "";
		if(eint�gig) 
		{
			sqlStatement = sqlStringEint�gig(anfangsdatum, enddatum);
		}
		else 
		{
			sqlStatement = sqlStringMehrt�gig(anfangsdatum, enddatum);
		}
		int rnr;
		String k�rzel;
		int sitzpl�tze;
		int rows = 0;
		
		try 
		{
			rows = DatabaseManager.getConnection().select2(sqlStatement);
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			r�ume = new Raum[rows];
			
			for(int i=0; i<rows; i++) 
			{
				rs.next();
				rnr = rs.getInt("rnr");
				k�rzel = rs.getString("k�rzel");
				sitzpl�tze = rs.getInt("Sitzpl�tze");
				r�ume[i] = new Raum(rnr,sitzpl�tze,k�rzel, new ArrayList<Ausstattung>());			
			}
			for(int j=0; j<rows; j++) 
			{
				rnr = r�ume[j].getRaumNr();
				ArrayList<Ausstattung> ausstattung = ladeAusstattung(rnr);
				r�ume[j].setAusstattung(ausstattung);
				raumliste.add(r�ume[j]);
			}		
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return raumliste;
	}

	/**
	 * Erezugt f�r die Methode ladeRaumListeNachDatum bei eint�gigem Buchungsvorgang einen SQL-String.
	 * Sucht mit einer Unteranfrage die Raumnummern raus, welche nicht mit Buchungen oder Reservierungen 
	 * belegt sind zu der Zeit zwischen anfangs- und enddatum und gibt dann diese R�ume zur�ck.
	 * @param anfangsdatum
	 * @param enddatum
	 * @return Der String mit dem SQL-Statement
	 */
	private String sqlStringEint�gig(String anfangsdatum, String enddatum) 
	{
		String[] anfangsdatumParts = anfangsdatum.split(" ");
		String anfangsdatum21 = anfangsdatumParts[0] + " 21:00";	// da SQL Daten auf die Sekunde(minute) genau verleicht, wird die uhrzeit zum vergleichen auf 21:00 (Ende der Gesch�ftszeit) gesetzt
		
		String sqlStatement = "SELECT rnr, k�rzel, sitzpl�tze FROM raum WHERE inBenutzung = 1 AND rnr NOT IN (SELECT buchung.rnr FROM buchung " + 
																		//das "-1/24" (1 ist ein tag in SQL, 1/24 = eine Stunde) dient dazu um zwischen zwei Buchungen mindestens eine Stunde Zeit zu lassen f�r Aufr�umen und sauber machen etc.
				"WHERE ( datum <= to_date('" + anfangsdatum21 + "', 'dd-mm-yy HH24:MI') AND enddatum > to_date('" + anfangsdatum + "', 'dd-mm-yy HH24:MI')-1/24 AND storniert = 0 )) " + 
				"AND rnr NOT IN (SELECT reservierung.rnr FROM reservierung " + 
				"WHERE ( datum <= to_date('" + anfangsdatum21 + "', 'dd-mm-yy HH24:MI') AND enddatum > to_date('" + anfangsdatum + "', 'dd-mm-yy HH24:MI')-1/24)) ORDER BY rnr ASC";
		return sqlStatement;
	}
	
	/**
	 * Erezugt f�r die Methode ladeRaumListeNachDatum bei mehrt�gigem Buchungsvorgang einen SQL-String.
	 * Sucht mit einer Unteranfrage die Raumnummern raus, welche nicht mit Buchungen oder Reservierungen 
	 * belegt sind zu der Zeit zwischen anfangs- und enddatum und gibt dann diese R�ume zur�ck.
	 * @param anfangsdatum
	 * @param enddatum
	 * @return Der String mit dem SQL-Statement
	 */
	private String sqlStringMehrt�gig(String anfangsdatum, String enddatum) 
	{
		String[] anfangsdatumParts = anfangsdatum.split(" ");
		String anfangsdatum21 = anfangsdatumParts[0] + " 21:00";	// Da SQL Daten auf die Sekunde(minute) genau verleicht, wird die uhrzeit zum vergleichen auf 21:00 (Ende der Gesch�ftszeit) gesetzt
		
		String sqlStatement = "SELECT rnr, k�rzel, sitzpl�tze FROM raum WHERE inBenutzung = 1 AND rnr NOT IN (SELECT buchung.rnr FROM buchung " + 
				"WHERE ( (datum <= to_date('" + anfangsdatum21 + "', 'dd-mm-yy HH24:MI') AND enddatum >= to_date('" + anfangsdatum + "', 'dd-mm-yy HH24:MI')  ) " + 
				"OR (datum > to_date('" + anfangsdatum21 + "', 'dd-mm-yy HH24:MI') AND datum < to_date('" + enddatum + "', 'dd-mm-yy HH24:MI') ) ) AND storniert = 0) " + 
				"AND rnr NOT IN (SELECT reservierung.rnr FROM reservierung " + 
				"WHERE ( (datum <= to_date('" + anfangsdatum21 + "', 'dd-mm-yy HH24:MI') AND enddatum >= to_date('" + anfangsdatum + "', 'dd-mm-yy HH24:MI')  ) " + 
				"OR (datum > to_date('" + anfangsdatum21 + "', 'dd-mm-yy HH24:MI') AND datum < to_date('" + enddatum + "', 'dd-mm-yy HH24:MI') ))) ORDER BY rnr ASC";
		return sqlStatement;
	}

	/**
	 * L�dt f�r einen Raum die Ausstattung aus der Datenbank und gibt sie in einer ArrayList zur�ck.
	 * @param rnr Die Raumnummer des Raumes
	 * @return Eine ArrayList mit den Ausstattungsobjekten
	 * @throws SQLException
	 */
	public ArrayList<Ausstattung> ladeAusstattung(int rnr) throws SQLException 
	{
		ArrayList<Ausstattung> ausstattung = new ArrayList<Ausstattung>();
		Ausstattung ausst;
		String sqlSubStatement = "SELECT bezeichnung, menge FROM AURA WHERE rnr =" + rnr;
		
		ResultSet subRs = DatabaseManager.getConnection().select(sqlSubStatement);
		while(subRs.next()) 
		{
			ausst = new Ausstattung(subRs.getString("bezeichnung"),subRs.getInt("menge"));
			ausstattung.add(ausst);
		}
		subRs.close();
		return ausstattung;
	}	
	
	/**
	 * Erezugt SQL-Strings zum �ndern eines Raums in der Datenbank und f�hrt diese aus, gibt Erfolgsmeldung dar�ber aus.
	 * @param raum Der zu �ndernde Raum
	 */
	public void raum�ndern(Raum raum) 
	{
		int rnr = raum.getRaumNr();
		int sitzpl�tze = raum.getSitzpl�tze();
		String kategorie = raum.getK�rzel();
		ArrayList<String> Statements = new ArrayList<String>();
		String sqlStatement = "UPDATE Raum SET sitzpl�tze = " + sitzpl�tze + ", k�rzel = '" + kategorie + "' where rnr = " + rnr;       	//sitzpl�tze und kategorie werden geupdated
		Statements.add(sqlStatement);
		
		sqlStatement = "DELETE FROM Aura WHERE rnr =" + rnr;							//erst werden alle Datens�tze mit der rnr aus AURA gel�scht
		Statements.add(sqlStatement);
		
		ArrayList<Ausstattung> ausstattung =raum.getAusstattung();
		for(int i=0; i<ausstattung.size() ;i++) 
		{
			sqlStatement = " INSERT INTO Aura(rnr, bezeichnung, menge) VALUES(";		//Dann werden die neuen eingef�gt
			sqlStatement += rnr + ", ";
			sqlStatement += "'"+ ausstattung.get(i).getBezeichnung() +"', ";
			sqlStatement += ausstattung.get(i).getMenge() + ")";
			Statements.add(sqlStatement);
		}
		
		try 
		{
			for(int i=0; i<Statements.size(); i++) 
			{
				DatabaseManager.getConnection().insert(Statements.get(i));
			}
			DatabaseManager.getConnection().commit();
			JOptionPane.showMessageDialog(null, "Raum wurde erfolgreich ge�ndert.");

		} 
		catch (Exception e) 
		{
			DatabaseManager.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}

	/**
	 * Setzt den Status eines Raumes in der Datenbank auf aktiviert (aktiviert=1).
	 * @param rnr Die Raumnummer des Raumes
	 */
	public void raumAktivieren(int rnr)
	{
		String sqlStatement = "UPDATE Raum SET inBenutzung = 1 WHERE rnr = " + rnr;
		try
		{
			DatabaseManager.getConnection().insert(sqlStatement);
			JOptionPane.showMessageDialog(null, "Raum f�r die Gesch�ftst�tigkeit aktiviert.");
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
	
	/**
	 * Setzt den Status eines Raumes in der Datenbank auf deaktiviert (aktiviert=0).
	 * @param rnr Die Raumnummer des Raumes
	 */
	public void raumDeaktivieren(int rnr) 
	{
		String sqlStatement = "UPDATE Raum SET inBenutzung = 0 where rnr = "+ rnr;	
		try 
		{
			DatabaseManager.getConnection().insert(sqlStatement);
			JOptionPane.showMessageDialog(null, "Raum f�r die Gesch�ftst�tigkeit deaktiviert.");
		} 
		catch(Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
	
	/**
	 * L�dt die Sitzplatzanzahl eines Raumes aus der Datenbank
	 * @param rnr Die Raumnummer des Raumes
	 * @return Die Sitzplatzanzahl
	 */
	public int getSitzpl�tze(int rnr)
	{
		String sqlStatement = "SELECT sitzpl�tze FROM raum WHERE rnr = "+ rnr;
		ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
		int sitzpl�tze = 0;
		
		try
		{
			rs.next();
			sitzpl�tze = rs.getInt("sitzpl�tze");
		}
		catch (Exception ex)
		{
			JOptionPane.showMessageDialog(null,"Sitzpl�tze konnten nicht geladen werden.");
		}
		return sitzpl�tze;
		
	}
}