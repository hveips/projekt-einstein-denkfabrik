package controller.raumcontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.SQLException;

import controller.DatabaseManager;
import view.raumview.PreisKategorie�ndernGUI;

public class PreisKategorie�ndernController 
{
	private PreisKategorie�ndernGUI guiChange;
	private DatabaseManager db;
	
	/**
	 * Der Konstruktor erzeugt den PreisKategorie�ndernController.
	 * @param guiChange
	 */
	public PreisKategorie�ndernController(PreisKategorie�ndernGUI guiChange)
	{
		this.guiChange = guiChange;
	}
	
	/**
	 * Die Methode greift �ber den UPDATE-Befehl auf die Datenbank zu und setzt die Preise aus den Eingabefeldern der PreisKategorie�ndernGUI zur passenden Kategorie.
	 * Der Erfolg wird zur�ckgegeben.
	 * @return boolean success
	 */
	public boolean change()
	{
		String kategorie = guiChange.getKategorieInput();
		String preis_h = guiChange.getPreis_HInput();
		String tagespreis = guiChange.getTagespreisInput();
		boolean success = false;
		
		String sqlStatement = "UPDATE Kategorie SET preis_h = '" + preis_h + "', tagespreis = '" + tagespreis + "' WHERE k�rzel = '" + kategorie + "'";
		try
		{
			db.getConnection().update2(sqlStatement);
			db.getConnection().commit();
			success = true;
		}
		catch(SQLException e)
		{
			success = false;
		}
		return success;
	}
}