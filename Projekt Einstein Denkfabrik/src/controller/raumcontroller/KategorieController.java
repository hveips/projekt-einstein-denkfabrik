package controller.raumcontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;
import model.RaumKategorie;

public class KategorieController 
{
	private DatabaseManager db;
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich alle Kategorien.
	 * Die Atrribute k�rzel, preis_h und tagespreis werden zwischengespeichert, als RaumKategorie erzeugt und in einer Liste gespeichert.
	 * @return ArrayList<RaumKategorie> raumKategListe
	 */
	public ArrayList<RaumKategorie> ladeKategorie()
	{
		ArrayList<RaumKategorie> raumKategListe = new ArrayList<RaumKategorie>();
		String sqlStatement = "SELECT * FROM KATEGORIE ORDER BY k�rzel ASC";
		String kuerzel;
		int preis_h;
		int tagespreis;
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				kuerzel = rs.getString("k�rzel");
				preis_h = rs.getInt("preis_h");
				tagespreis = rs.getInt("tagespreis");
				
				RaumKategorie raumKateg = new RaumKategorie(kuerzel, preis_h, tagespreis);
				raumKategListe.add(raumKateg);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return raumKategListe;
	}
}