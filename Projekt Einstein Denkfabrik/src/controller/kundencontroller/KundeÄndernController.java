package controller.kundencontroller;
  
/**
 * @author Marvin Plepis, Alexander Stavski
 */

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;

import model.Kunde;
import view.kundenview.Kunde�ndernEntfernenStatistikGUI;
import view.kundenview.Kunde�ndernGUI;

public class Kunde�ndernController
{
	private Kunde�ndernGUI gui�ndern;
	private Kunde�ndernEntfernenStatistikGUI guiTabelle;
	private KundeController newOp;
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	private static final String EMAIL_PATTERN =  "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public Kunde�ndernController(Kunde�ndernGUI gui, Kunde�ndernEntfernenStatistikGUI guiTabelle ) 
	{
		this.gui�ndern = gui;
		this.guiTabelle = guiTabelle;
		this.newOp = new KundeController();
	}
	/**
	 * Diese Methode liest die Textfelder der Kunden�ndernGUI, bei einer �nderung werden diese zwischengespeichert.
	 * @return Kunden Objekt mit der Eingabe
	 * @author Marvin Plepis
	 */
	public Kunde updateKunde() 
	{
		int kdid = Integer.parseInt(gui�ndern.getIDInput().getText());
		String anrede = (String) gui�ndern.getAnredeInput().getSelectedItem();
		String vorname = gui�ndern.getVornameInput().getText().trim();
		String name = gui�ndern.getNachnameInput().getText().trim();
		int plz = Integer.parseInt(gui�ndern.getPlzInput().getText().trim());
		String ort = gui�ndern.getOrtInput().getText().trim();
		String stra�e = gui�ndern.getStrasseInput().getText();
		String hausNr = gui�ndern.getHnrInput().getText().trim();
		String email = gui�ndern.geteMailInput().getText().trim();
		String arbeitsplatz = gui�ndern.getArbeitsplatzInput().getText().trim();

		Kunde neuerKunde = new Kunde(kdid, anrede, vorname, name, plz, ort, stra�e, hausNr, email, arbeitsplatz);
		return neuerKunde;
	}
	/**
	 * Diese Methode l�dt die ausgew�hlten Daten aus dem Kundentable und gibt Daten in die Kunden�ndernGUI
	 * @author Marvin Plepis
	 */
	public void ladeDatenVonAusgew�hlterZeile() 
	{
		if (guiTabelle.getTable().getSelectedRow() >= 0) 
		{
			int id = (int) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 0);
			int plz = (int) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 3);
			String anrede = guiTabelle.getAnredeRow();

			gui�ndern.getIDInput().setText(Integer.toString(id));
			gui�ndern.getAnredeInput().setSelectedItem(anrede);
			gui�ndern.getNachnameInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 1));
			gui�ndern.getVornameInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 2));
			gui�ndern.getPlzInput().setText(Integer.toString(plz));
			gui�ndern.getOrtInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 4));
			gui�ndern.getStrasseInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 5));
			gui�ndern.getHnrInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 6));
			gui�ndern.geteMailInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 7));
			gui�ndern.getArbeitsplatzInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 8));
		}
	}
	/**
	 * F�hrt das speichern in die Datenbank des ge�nderten Kunden aus
	 * @author Marvin Plepis
	 */
	public void changeAction()
	{
		try 
		{
			Kunde kunde = updateKunde();
			newOp.kundeAendern(kunde);
			JOptionPane.showMessageDialog(null, "Daten wurden ge�ndert und gespeichert");
			guiTabelle.loadTable();
			gui�ndern.getJDialog().dispose();
		}
		catch (Exception e) 
		{
			setBorderLine();
		}
	}
	/**
	 * Durch dr�cken des Knopfes Speichern wird die Methode aufgerufen und 
	 * If Anweisung �berpr�ft ob im TextFeld etwas steht, wenn es nicht leer ist, geht er weiter zum �ndern der Kunden in der DB(changeAction())
	 * @author Marvin Plepis
	 */
	public void changeButtonAction()
	{
		if (gui�ndern.getVornameInput().getText().equals("") || gui�ndern.getNachnameInput().getText().equals("")
				|| gui�ndern.getOrtInput().getText().equals("") || gui�ndern.getStrasseInput().getText().equals("")
				|| gui�ndern.geteMailInput().getText().equals("") || !gui�ndern.geteMailInput().getText().matches(EMAIL_PATTERN) 
				|| gui�ndern.getHnrInput().getText().equals("")
				|| gui�ndern.getArbeitsplatzInput().getText().equals(""))
		{
			setBorderLine();
		} 
		else
		{
			changeAction();
		}
	}
	
	/**
	 * @author Alexander Stavski
	 */
	public void setBorderLine()
	{
		if (gui�ndern.getVornameInput().getText().equals(""))
		{
			gui�ndern.getVornameInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getVornameInput().setBorder(border3);
		}

		if (gui�ndern.getNachnameInput().getText().equals(""))
		{
			gui�ndern.getNachnameInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getNachnameInput().setBorder(border3);
		}

		if (gui�ndern.getPlzInput().getText().equals("") || gui�ndern.getPlzInput().getText().length() < 5)
		{
			gui�ndern.getPlzInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getPlzInput().setBorder(border3);
		}
		if (gui�ndern.getOrtInput().getText().equals(""))
		{
			gui�ndern.getOrtInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getOrtInput().setBorder(border3);
		}

		if (gui�ndern.getStrasseInput().getText().equals(""))
		{
			gui�ndern.getStrasseInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getStrasseInput().setBorder(border3);
		}
		if (gui�ndern.getHnrInput().getText().equals(""))
		{
			gui�ndern.getHnrInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getHnrInput().setBorder(border3);
		}

		if (gui�ndern.geteMailInput().getText().equals("") || !gui�ndern.geteMailInput().getText().matches(EMAIL_PATTERN))
		{
			gui�ndern.geteMailInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.geteMailInput().setBorder(border3);
		}
		
		if (gui�ndern.getArbeitsplatzInput().getText().equals(""))
		{
			gui�ndern.getArbeitsplatzInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getArbeitsplatzInput().setBorder(border3);
		}
	}
}