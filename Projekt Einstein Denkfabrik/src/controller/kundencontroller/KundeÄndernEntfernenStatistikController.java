package controller.kundencontroller;
  
/**
 * @author Marvin Plepis
 */

import javax.swing.JOptionPane;
import controller.DatabaseManager;
import view.kundenview.Kunde�ndernEntfernenStatistikGUI;
import view.kundenview.Kunde�ndernGUI;

public class Kunde�ndernEntfernenStatistikController
{
	private Kunde�ndernEntfernenStatistikGUI gui;
	private KundeController newOp;
	DatabaseManager conn;

	
	public Kunde�ndernEntfernenStatistikController(Kunde�ndernEntfernenStatistikGUI gui)
	{
		this.gui = gui;
		this.newOp = new KundeController();
	}
    /**
     * Abfrage ob Kunde ausgew�hlt wurde. 
     * Wenn ein Kunde ausgew�hlt �ffnet sich Kunde�ndernGUI()
     */
	public void aendernButtonAction()
	{
		int selectedRow = gui.getTable().getSelectedRow();
		if (selectedRow == -1)
		{
			JOptionPane.showMessageDialog(gui, "Bitte w�hlen Sie einen Kunden aus, der ge�ndert werden soll.", "Fehler", JOptionPane.ERROR_MESSAGE);
		} 
		else
		{
			new Kunde�ndernGUI(gui);
		}
	}
	/**
	 * Abfrage ob Kunde ausgew�hlt wurde. Wenn ein Kunde ausgew�hlt wurde kommt eine Sicherheitsabfrage ob wirklich gel�scht werden soll
	 * Datenbank-Befehl um Kunde zu l�schen wird ausgef�hrt
	 * Tabelle wird neu geladen
	 */
	public void loeschenButtonAction()
	{
		int selectedRow = gui.getTable().getSelectedRow();
		if (selectedRow == -1)
		{
			JOptionPane.showMessageDialog(gui, "Bitte w�hlen Sie einen Kunden aus, der entfernt werden soll.", "Fehler", JOptionPane.ERROR_MESSAGE);
		} 
		else
		{
			int option = JOptionPane.showConfirmDialog(gui, "Sind Sie sicher, dass Sie " + gui.getVornameRow() + " "
					+ gui.getNachnameRow() + " l�schen wollen?", "L�schen best�tigen", JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION)
			{
				try
				{
					newOp.kundenLoeschen(gui.getKundeRow());
					gui.loadTable();
				} 
				catch (Exception ex)
				{
					JOptionPane.showMessageDialog(null, "Fehler:" + ex.getMessage());
				}
			}
		}
	}
}