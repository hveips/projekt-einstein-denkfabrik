package controller.kundencontroller;
  
/**
 * @author Marvin Plepis, Alexander Stavski
 */

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;

import model.Kunde;
import view.kundenview.KundeHinzufügenGUI;

public class KundeHinzufügenController
{
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	private KundeHinzufügenGUI gui;
	private KundeController newOp;
	private static final String EMAIL_PATTERN =  "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
												+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * @param gui - GUI wird übergeben
	 * @author Marvin Plepis
	 */
	public KundeHinzufügenController(KundeHinzufügenGUI gui)
	{
		this.gui = gui;
		newOp = new KundeController();
	}

	/**
	 * Gibt Kunden die nächste freie ID
	 * Sucht letzte ID und zählt kdid++
	 * @author Marvin Plepis
	 */
	public void setNewKundenID()
	{
		// ID aus DB holen und draufzählen + im Tf anzeigen
		int kdid = newOp.getLastKundenID();
		kdid++;
		gui.getIDInput().setText(Integer.toString(kdid));
		if (kdid == 1)
		{
			kdid = 10000;
			gui.getIDInput().setText(Integer.toString(kdid));
		}
	}
    /**
     * Ließt werde aus GUI und erstellt daraus neuen Kunden
     * @return neuerKunde - Kunden Objekt
     * @author Marvin Plepis
     */
	public Kunde getNeuenKunden()
	{
		int kdid = Integer.parseInt(gui.getIDInput().getText());
		String anrede = (String) gui.getAnredeInput().getSelectedItem();
		String vorname = gui.getVornameInput().getText().trim();
		String name = gui.getNachnameInput().getText().trim();
		int plz = Integer.parseInt(gui.getPlzInput().getText().trim());
		String ort = gui.getOrtInput().getText().trim();
		String straße = gui.getStrasseInput().getText();
		String hausNr = gui.getHnrInput().getText().trim();
		String email = gui.geteMailInput().getText().trim();
		String arbeitsplatz = gui.getArbeitsplatzInput().getText().trim();

		Kunde neuerKunde = new Kunde(kdid, anrede, vorname, name, plz, ort, straße, hausNr, email, arbeitsplatz);
		return neuerKunde;
	}
/**
 * Methode die dafür sorgt das Kunde hinzugefügt wird, wird an View Übergeben
 */
	public void kundeHinzufügenAction()
	{
		try
		{
			Kunde ku = getNeuenKunden();
			setBorderLine();
			if (gui.geteMailInput().getText().matches(EMAIL_PATTERN))
			{
				newOp.kundeHinzufuegen(ku);
				clearTextFields();
			} 
			else
			{
				gui.geteMailInput().setBorder(border2);
			}
		} 
		catch (Exception ex)
		{
			setBorderLine();
		}
	}

	/** Textfelder nach erfolgreichem Anlegen zurücksetzen und ID aktualisieren
	 *  @author Marvin Plepis
	 */
	public void clearTextFields()
	{
		if (gui.getVornameInput().getText().equals("") || gui.getNachnameInput().getText().equals("")
				|| gui.getOrtInput().getText().equals("") || gui.getStrasseInput().getText().equals("")
				|| gui.geteMailInput().getText().equals("") || gui.getHnrInput().getText().equals("")
				|| gui.getArbeitsplatzInput().getText().equals(""))
		{
			// do nothing
		} 
		else
		{
			setNewKundenID();
			gui.getVornameInput().setText("");
			gui.getNachnameInput().setText("");
			gui.getPlzInput().setText("");
			gui.getOrtInput().setText("");
			gui.getStrasseInput().setText("");
			gui.getHnrInput().setText("");
			gui.geteMailInput().setText("");
			gui.getArbeitsplatzInput().setText("");
			JOptionPane.showMessageDialog(null, "Kunde wurde erfolgreich angelegt");
		}
	}

	/**
	 * @author Alexander Stavski
	 */
	public void setBorderLine()
	{
		if (gui.getVornameInput().getText().equals(""))
		{
			gui.getVornameInput().setBorder(border2);
		} else
		{
			gui.getVornameInput().setBorder(border3);
		}

		if (gui.getNachnameInput().getText().equals(""))
		{
			gui.getNachnameInput().setBorder(border2);
		} else
		{
			gui.getNachnameInput().setBorder(border3);
		}

		if (gui.getPlzInput().getText().equals("") || gui.getPlzInput().getText().length() < 5)
		{
			gui.getPlzInput().setBorder(border2);
		} else
		{
			gui.getPlzInput().setBorder(border3);
		}
		if (gui.getOrtInput().getText().equals(""))
		{
			gui.getOrtInput().setBorder(border2);
		} else
		{
			gui.getOrtInput().setBorder(border3);
		}

		if (gui.getStrasseInput().getText().equals(""))
		{
			gui.getStrasseInput().setBorder(border2);
		} else
		{
			gui.getStrasseInput().setBorder(border3);
		}
		if (gui.getHnrInput().getText().equals(""))
		{
			gui.getHnrInput().setBorder(border2);
		} else
		{
			gui.getHnrInput().setBorder(border3);
		}

		if (gui.geteMailInput().getText().equals("") || !gui.geteMailInput().getText().matches(EMAIL_PATTERN))
		{
			gui.geteMailInput().setBorder(border2);
		} else
		{
			gui.geteMailInput().setBorder(border3);
		}

		if (gui.getArbeitsplatzInput().getText().equals(""))
		{
			gui.getArbeitsplatzInput().setBorder(border2);
		} else
		{
			gui.getArbeitsplatzInput().setBorder(border3);
		}
	}
}
