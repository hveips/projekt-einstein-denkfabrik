package controller.kundencontroller;
  
/**
 * @author Marvin Plepis
 */

import java.util.*;
import javax.swing.JOptionPane;
import controller.DatabaseManager;
import java.sql.*;
import model.Kunde;


public class KundeController 
{	
	DatabaseManager conn;
	
	/**
	 * Methode, um Kundendaten aus Datenbank zu laden mit allen angegeben Attributen. 
	 * @return ArrayListe aus Kunden Objekten
	 */
	public ArrayList<Kunde> kundenTabelleLaden()
	{
		ArrayList<Kunde> kdList = new ArrayList<Kunde>();
		String sqlStatement = "SELECT * FROM Kunde ORDER BY kdid ASC";
		try 
		{
			ResultSet rs = conn.getConnection().select(sqlStatement);
			
			while (rs.next()) 
			{
				int kdid = rs.getInt("kdid");
				String anrede = rs.getString("anrede");
				String vorname = rs.getString("vorname");
				String name = rs.getString("nachname");
				int plz = rs.getInt("plz");
				String ort = rs.getString("ort");
				String stra�e = rs.getString("strasse");
				String hausNr = rs.getString("hnr");
				String email = rs.getString("mail");
				String arbeitsplatz = rs.getString("arbeitsplatz");
				
				Kunde kd = new Kunde(kdid, anrede, vorname, name, plz, ort, stra�e, hausNr, email, arbeitsplatz);
				
				kd.setKundenID(kdid);
				kd.setAnrede(anrede);
				kd.setVorname(vorname);
				kd.setName(name);
				kd.setPlz(plz);
				kd.setOrt(ort);
				kd.setStra�e(stra�e);
				kd.setHausNr(hausNr);
				kd.setEmail(email);
				kd.setArbeitsplatz(arbeitsplatz);
				
				kdList.add(kd);
			}
		}
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return kdList;	
	}
	/**
	 * SQL-Befehl um Kunden aus der DB zu L�schen 
	 * @param kd - Kunde �bergeben der in der Tabelle ausgew�hlt wurde
	 * �bergabe im Kunde�ndernEntfernenController
	 */
	public void kundenLoeschen(Kunde kd) 
	{
		String sqlStatement = "DELETE FROM Kunde WHERE kdid = " + kd.getKundenID();	
		conn.getConnection().update(sqlStatement);
	}
	/**
	 * SQL-Befehl, um Kunden in die DB hinzuzuf�gen
	 * @param neu - Neuer Kunde mit angelegten Daten aus KundeHinzuf�genGUI
	 * �bergabe im KundeHinzuf�genController
	 */
	public void kundeHinzufuegen(Kunde neu) 
	{
		String sqlStatement = "INSERT INTO Kunde(kdid, anrede, vorname, nachname, plz, ort, strasse, hnr, mail, arbeitsplatz)";
		sqlStatement += "VALUES (" + neu.getKundenID() + ",'";
		sqlStatement += neu.getAnrede() + "','";
		sqlStatement += neu.getVorname() + "','";
		sqlStatement += neu.getName() + "',";
		sqlStatement += neu.getPlz() + ",'";
		sqlStatement += neu.getOrt() + "','";
		sqlStatement += neu.getStra�e() + "','";
		sqlStatement += neu.getHausNr() + "','";
		sqlStatement += neu.getEmail() + "','";
		sqlStatement += neu.getArbeitsplatz() + "')";	
		
		try 
		{
			conn.getConnection().insert(sqlStatement);
			conn.getConnection().commit();
		} 
		catch (Exception e) 
		{
			conn.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		} 
	}
	/**
	 * SQL-Befehl, um Kunden in der DB zu �ndern
	 * @param kd - Ausgew�hlter Kunde aus Tabelle
	 */
	public void kundeAendern(Kunde kd) 
	{
		String sqlStatement = "UPDATE Kunde SET " + "anrede = '" + kd.getAnrede() + "', " + "vorname = '" + kd.getVorname()
		+ "', " + "nachname = '" + kd.getName() + "', " + "plz = " + kd.getPlz() + ", " + "ort = '" + kd.getOrt()
		+ "', " + "strasse = '" + kd.getStra�e() + "', " + "hnr = '" + kd.getHausNr() + "', " + "mail = '"
		+ kd.getEmail() + "', " + "arbeitsplatz = '" + kd.getArbeitsplatz() + "' WHERE kdid = " + kd.getKundenID() + "";
		conn.getConnection().update(sqlStatement);
	}
	/**
	 * Methode, um die zurzeit h�chste KundenID aus der DB abzufragen
	 * @return id - "letzte" KundenID
	 */
	public int getLastKundenID() 
	{
		String sqlStatement = "SELECT kdid FROM Kunde WHERE ROWNUM <=1 ORDER BY kdid DESC";
		int id = 0;
		try 
		{
			ResultSet rs = conn.getConnection().select(sqlStatement);
			while (rs.next()) 
			{
				int lastID = rs.getInt("kdid");
				id = lastID;
			}
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 
		return id;
	}
    /**
     * Methode, um Kunden zu suchen der f�r Catering aus Extras zust�ndig ist
     * @return email - Gibt Email des zust�ndigen Kunden f�r Catering
     */
	public String getCateringEmail() {
		String email = null;
		String sqlStatement = "SELECT mail FROM Kunde WHERE Arbeitsplatz = 'Catering'";
		try {
			ResultSet rs = conn.getConnection().select(sqlStatement);
			while (rs.next()) {
				email = rs.getString("mail");

			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			conn.getConnection().closeAllConnection();
		}
		return email;
	}
}