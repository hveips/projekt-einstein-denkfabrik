package controller;

/**
 * @author Christoph Hoppe, Alexander Stavski, Niklas Nebeling
 * Die Klasse MailController steuert das Versenden von E-Mails mit und ohne Anh�ngen an Kunden.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import controller.kundencontroller.KundeController;
import model.Extra;

public class MailController {
	
	/**
	 * Definition von Instanzvariablen zum bekanntmachen anderer Klassen und
	 * Zugangsdaten des MailAccounts
	 */
	private KundeController ku;
	private String username = "EinsteinDenkfabrik@gmx.de";
	private String password = "Einstein";

	Properties props = Mailserverproperties();
	Session session = createSession(username, password);
	
	
	public MailController() {
		this.ku = new KundeController();
	}

	/**
	 * Methode �bernimmt Parameter aus LoginGUI() und LoginController(), versendet
	 * Sicherheitscode an Nutzer
	 * @param anrede
	 * @param nam
	 * @param mailadresse
	 * @param security
	 */
	public void sendMailSecurityQuery(String anrede, String nam, String mailadresse, String security) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailadresse));
			message.setSubject("Passwort zur�cksetzen");
			message.setText(" Sehr geehrte/r " + anrede + " " + nam
					+ ",\n \n ihr Passwort wurde erfolgreich zur�ckgesetzt! \n Bitte geben sie den Zugangscode: " + "'"
					+ security + "'"
					+ " in das daf�r vorgesehene Feld ein. \n \n Mit freundlichen Gr��en \n Die Einstein Denkfabrik");

			Transport.send(message);
		} catch (MessagingException e) {
			JOptionPane.showMessageDialog(null, "Email-Fehler: Zugangscode konnte nicht verschickt werden " + e.getMessage());
		}
	}

	/**
	 * Die Methode wird bei einer Stornierungsanfrage im
	 * ReservierungStornierenController und BuchungStornierenController aufgerufen
	 * und �bernimmt Paramter der ReservierungsStornierenGUI bzw.
	 * BuchungBearbeitenStornierenGUI.
	 * @param email
	 * @param resBnr
	 * @param name
	 */
	public void sendStornierung(String email, int resBnr, String name) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Reservierung/Buchung storniert");
			message.setText(" Sehr geehrte/r Frau/Herr " + name + ",\n \n ihre Reservierung/Buchung mit der Nummer "
					+ resBnr + " wurde erfolgreich storniert!\n Mit freundlichen Gr��en \n Die Einstein Denkfabrik");

			Transport.send(message);
			JOptionPane.showMessageDialog(null,
					"Eine Best�tigung der Stornierung wurde dem Kunden per E-Mail zugeschickt");
		} catch (MessagingException e) {
			JOptionPane.showMessageDialog(null, "Email-Fehler: E-Mail konnte nicht versendet werden" + e.getMessage());
		}
	}

	/**
	 * Die Methode sendet ein Initialpasswort f�r den ersten Login. 
	 * boolean pr�ft, ob die E-Mail (Konvention) g�ltig ist.
	 * @param anrede
	 * @param name
	 * @param mailadresse
	 * @param passwort
	 * @return
	 */
	public boolean sendPassword(String anrede, String name, String mailadresse, String passwort) {
		boolean g�ltigeEmail;
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailadresse));
			message.setSubject("Initialpasswort");
			message.setText(" Sehr geehrte/r " + anrede + " " + name + ",\n ihr neues Passwort lautet: " + "'"
					+ passwort + "'" + "\n Mit freundlichen Gr��en \n Die Einstein Denkfabrik");

			Transport.send(message);
			g�ltigeEmail = true;
		} catch (MessagingException e) {
			//JOptionPane.showMessageDialog(null, "Email-Fehler: Initialpasswort konnte nicht versendet werden");
			g�ltigeEmail = false;
		}
		return g�ltigeEmail;
	}

	/**
	 * 
	 * Sendet eine E-Mail an den  Catering Kooperationspartner mit allen ben�tigten Informationen
	 * @param vorname
	 * @param name
	 * @param kuID
	 * @param anfangsdatum
	 * @param enddatum
	 * @param personenanzahl
	 * @param extras
	 * @param sonderextra
	 * @param �ndern true=Buchung/Resrvierung wurde ge�ndert, false= neue Buchung/Reservierung
	 * @author Alexander Stavski, Christoph Hoppe, Niklas Nebeling
	 */
	public void sendCatering(String vorname, String name, int kuID, String anfangsdatum , String enddatum, int personenanzahl, 
			ArrayList<Extra> extras, String sonderextra, boolean �ndern) {
		String catering;
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(ku.getCateringEmail()));
			if(�ndern) {
				message.setSubject("Catering wurde ge�ndert");
			}
			else {
				message.setSubject("Catering wurde gebucht");		
			}
			//Wenn es eine Buchungs�nderung ist und alle normalen Extras rausgenommen wurden
			if(�ndern && extras.isEmpty()) {
				//Wenn au�erdem kein Sonderwunsch vorliegt
				if(sonderextra == null) {
					catering = "Catering wurde storniert";
				}
				//Sonst nur Sonderwunsch
				else {
					catering = "Sonderwunsch: " + sonderextra;
				}
			}
			else {
				catering = erzeugeCateringString(extras, sonderextra);			
			}
			message.setText("INFO: Der Kunde: " + vorname + "  " + name
					+ " (KundenID: " + kuID + ") " 
					+ " hat f�r den Zeitraum \n " + anfangsdatum + " - " + enddatum +
					" f�r " + personenanzahl + " Personen folgendes Catering gebucht: \n" + catering
					+ "\n \n Mit freundlichen Gr��en \n Die Einstein Denkfabrik");
	
			Transport.send(message);
		} catch (MessagingException e) {
			JOptionPane.showMessageDialog(null, "Email-Fehler: Cateringauftrag konnte nicht verschickt werden.");
		}
	}
	
	
	/**
	 * 
	 * Erzeugt einen String mit dem Inhalt der Catering Bestellung f�r die Email an den Catering-Dienstleister
	 * @param extras
	 * @param sonderextra
	 * @return catering - der String mit dem Inhalt
	 * @author Niklas Nebeling
	 */
	private String erzeugeCateringString(ArrayList<Extra> extras, String sonderextra) {
		String catering = "";
		catering += extras.get(0).getBezeichnung();
		for(int i = 1; i<extras.size(); i++)
		{
			catering +=  ", " + extras.get(i).getBezeichnung();
		}
		if(sonderextra != null)
		{
			catering +=". Sonderwunsch:  '" + sonderextra + "'.";					
		}
		else 
		{
			catering +=".";
		}
		return catering;
	}

	/**
	 * Die Methode sendet eine Best�tigung f�r Buchungen und Reservierungen an die
	 * E-Mail Adresse des Kunden. Sie wird im BuchenReservierenController aufgerufen
	 * und �bernimmt die Parameter von dort.
	 * @param buresnr
	 * @param anfangsdatum
	 * @param enddatum
	 * @param raumnr
	 * @param email
	 * @param name
	 * @param extras
	 * @param sonderextra
	 * @param personenanzahl
	 * @param buchen - Hat entweder den wert "Buchung" oder "Reservierung"
	 * @param �ndern true=Buchung/Resrvierung wurde ge�ndert, false= neue Buchung/Reservierung
	 * @author Christoph Hoppe, Niklas Nebeling
	 */
	public void sendBest�tigung(int buresnr, String anfangsdatum, String enddatum, int raumnr, String email, String name, 
			ArrayList<Extra> extras, String sonderextra, int personenanzahl, String buchen, boolean �ndern) {

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			if(�ndern) {
				message.setSubject("�nderung ihrer " + buchen + " " + buresnr + "");
			}
			else {
				message.setSubject(buchen + "sbest�tigung " + buresnr + "");
			}	
			String catering = "";
			//Wenn nur Sonderwunsch bestellt wurde
			if(extras.isEmpty() && sonderextra != null) {
				catering = "\nSie haben f�r " + personenanzahl + " Personen folgenden Sonderwunsch bestellt: " + sonderextra;
			}
			//Wenn Extras bestellt wurden
			else if(!extras.isEmpty()) {
				catering = "\nSie haben f�r " + personenanzahl + " Personen folgendes Catering bestellt: ";
				catering += erzeugeCateringString(extras, sonderextra);
			}
			message.setText("Sehr geehrte/r Frau/Herr " + name + ","
					+ " \n \n \nhiermit best�tigen wir Ihre " + buchen +" mit der Nummer " + buresnr
					+ " vom " + anfangsdatum  + " bis zum " + enddatum
					+ " f�r folgenden Raum: " + raumnr
					+"\n" + catering
					+ " \n \n Mit freundlichen Gr��en \n Die Einstein Denkfabrik");
			Transport.send(message);
			JOptionPane.showMessageDialog(null, "Eine " + buchen + "sbest�tigung wurde per E-Mail versandt.");
		} catch (MessagingException e) {
			JOptionPane.showMessageDialog(null, "Email-Fehler: " +buchen + "sbest�tigung konnte nicht verschickt werden.");
		}
	}

	/**
	 * Die Methode versendet eine E-Mail mit der Rechnung im Anhang an den Kunden.
	 * Sie �bernimmt die Parameter aus der RechnungPDF Klasse und wird dort aufgerufen.
	 * @param filename
	 * @param email
	 * @param anrede
	 * @param name
	 * @param bunr
	 */
	public void sendRechnung(String filename, String email, String anrede, String name, int bunr) {

		String rechnungPDF = filename;

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("EinsteinDenkfabrik@gmx.de"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Aktuelle Rechnung " + bunr);

			MimeMultipart mimeMultipart = new MimeMultipart();
			MimeBodyPart text = new MimeBodyPart();
			text.setText("Sehr geehrte/r " + anrede + " " + name
					+ ",\nim Anhang erhalten Sie die Rechnung zu der Buchung " + bunr + ".\nMit freundlichen Gr��en\nDie Einstein Denkfabrik");
			text.setDisposition(MimeBodyPart.INLINE);

			File file = new File(rechnungPDF);
			MimeBodyPart attachement = new MimeBodyPart();
			attachement.setDataHandler(new DataHandler(new FileDataSource(file)));
			attachement.setFileName(rechnungPDF);
			attachement.setDisposition(MimeBodyPart.ATTACHMENT);
			mimeMultipart.addBodyPart(text);
			mimeMultipart.addBodyPart(attachement);

			message.setContent(mimeMultipart);

			Transport.send(message);

		} catch (MessagingException e) {
			JOptionPane.showMessageDialog(null, "Email-Fehler: Rechnung konnte nicht verschickt werden");

		}
	}

	/**
	 * Definiert Ausgangshost,Port und Protokoll vom GMX-Mailserver
	 * @return Gibt props an Klasse zur�ck
	 */
	public Properties Mailserverproperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "mail.gmx.net");
		props.put("mail.smtp.port", "587");
		return props;
	}

	/**
	 * Session Objekt �bernimmt username,passowort und props aus den
	 * Instanzvariablen, erzeugt PasswordAuthenticator.
	 * 
	 * @param username
	 * @param password
	 * @return session an Klasse
	 */
	public Session createSession(String username, String password) {
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		return session;
	}
}