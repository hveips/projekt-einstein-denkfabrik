package controller.mitarbeitercontroller;

/**
 * @author Alexander Stavski
 */

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;

import model.Mitarbeiter;
import view.mitarbeiterview.MitarbeiterHinzufügenGUI;

public class MitarbeiterHinzufügenController
{

	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);

	private MitarbeiterHinzufügenGUI gui;
	private MitarbeiterController newOp;

	public MitarbeiterHinzufügenController(MitarbeiterHinzufügenGUI gui)
	{
		this.gui = gui;
		this.newOp = new MitarbeiterController();
	}

	/**
	 * Es wird die letzte ID aus der Datenbank ausgelesen und um 1 addiert
	 * Gibt es noch keine Mitarbeiter in der DB, so ist der Anfangswert für die ID 10.000
	 */
	public void setNewMitarbeiterID()
	{
		// ID aus DB holen und draufzählen + im Tf anzeigen
		int maid = newOp.getLastMitarbeiterID();
		maid++;
		gui.getIDInput().setText(Integer.toString(maid));
		if (maid == 1)
		{
			maid = 10000;
			gui.getIDInput().setText(Integer.toString(maid));
		}
	}

	/**
	 * Liest die eingegeben Werte aus der View aus und erzeugt daraus einen Mitarbeiter
	 * @return Mitarbeiter Objekt
	 */
	public Mitarbeiter getNeuenMitarbeiter()
	{
		int maid = Integer.parseInt(gui.getIDInput().getText());
		String anrede = (String) gui.getAnredeInput().getSelectedItem();
		String vorname = gui.getVornameInput().getText().trim();
		String name = gui.getNachnameInput().getText().trim();
		int plz = Integer.parseInt(gui.getPlzInput().getText().trim());
		String ort = gui.getOrtInput().getText().trim();
		String straße = gui.getStrasseInput().getText();
		String hausNr = gui.getHnrInput().getText().trim();
		String email = gui.geteMailInput().getText().trim();
		String rolle = (String) gui.getRolleInput().getSelectedItem();

		Mitarbeiter neuerMitarbeiter = new Mitarbeiter(maid, anrede, vorname, name, plz, ort, straße, hausNr, email, rolle);
		return neuerMitarbeiter;
	}

	/**
	 * Methode für das Hinzufügen von Mitarbeitern, wird an die View übergeben
	 */
	public void mitarbeiterHinzufügenAction()
	{
		try
		{
			Mitarbeiter ma = getNeuenMitarbeiter();
			setBorderLine();
			if (newOp.mitarbeiterHinzufügen(ma) == true)
			{
				clearTextFields();
			} else
			{
				gui.geteMailInput().setBorder(border2);
			}
		} catch (Exception ex)
		{
			setBorderLine();
		}
	}

	/**
	 * Textfelder nach erfolgreichem Anlegen zurücksetzen und ID aktualisieren
	 */
	public void clearTextFields()
	{
		if (gui.getVornameInput().getText().equals("") || gui.getNachnameInput().getText().equals("")
				|| gui.getOrtInput().getText().equals("") ||  gui.getPlzInput().getText().length() < 5 || gui.getStrasseInput().getText().equals("")
				|| gui.geteMailInput().getText().equals("") || gui.getHnrInput().getText().equals(""))
		{
			setBorderLine();
		} else
		{
			setNewMitarbeiterID();
			gui.getVornameInput().setText("");
			gui.getNachnameInput().setText("");
			gui.getPlzInput().setText("");
			gui.getOrtInput().setText("");
			gui.getStrasseInput().setText("");
			gui.getHnrInput().setText("");
			gui.geteMailInput().setText("");
			JOptionPane.showMessageDialog(null, "Mitarbeiter wurde erfolgreich angelegt");
		}
	}

	/**
	 * Bei keiner oder fehlerhaften Eingabe werden die betroffenen Textfelder rot umrandet
	 */
	public void setBorderLine()
	{
		if (gui.getVornameInput().getText().equals(""))
		{
			gui.getVornameInput().setBorder(border2);
		} else
		{
			gui.getVornameInput().setBorder(border3);
		}

		if (gui.getNachnameInput().getText().equals(""))
		{
			gui.getNachnameInput().setBorder(border2);
		} else
		{
			gui.getNachnameInput().setBorder(border3);
		}

		if (gui.getPlzInput().getText().equals("") || gui.getPlzInput().getText().length() < 5)
		{
			gui.getPlzInput().setBorder(border2);
		} else
		{
			gui.getPlzInput().setBorder(border3);
		}
		if (gui.getOrtInput().getText().equals(""))
		{
			gui.getOrtInput().setBorder(border2);
		} else
		{
			gui.getOrtInput().setBorder(border3);
		}

		if (gui.getStrasseInput().getText().equals(""))
		{
			gui.getStrasseInput().setBorder(border2);
		} else
		{
			gui.getStrasseInput().setBorder(border3);
		}
		if (gui.getHnrInput().getText().equals(""))
		{
			gui.getHnrInput().setBorder(border2);
		} else
		{
			gui.getHnrInput().setBorder(border3);
		}

		if (gui.geteMailInput().getText().equals("") || !gui.geteMailInput().getText().contains("@"))
		{
			gui.geteMailInput().setBorder(border2);
		} else
		{
			gui.geteMailInput().setBorder(border3);
		}
	}
}
