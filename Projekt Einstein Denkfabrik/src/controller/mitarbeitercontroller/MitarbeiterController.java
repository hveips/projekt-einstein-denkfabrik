package controller.mitarbeitercontroller;

/**
 * @author Alexander Stavski
 * Diese Controller-Klasse ist zust�ndig f�r alle ben�tigten Datenbank-Operationen, die
 * f�r die Mitarbeiterverwaltung n�tig sind
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;
import controller.MailController;
import model.Mitarbeiter;

public class MitarbeiterController {

	private DatabaseManager verbindung;
	private MailController sendPasswordController = new MailController();

	/**
	 * Durch diese Methode werden Mitarbeiter in DB gespeichert
	 * Das Attribut "eingelogt" ist immer beim Anlegen 0 und das Passwort, ist das zuf�llig generierte
	 * @param neuerMitarbeiter - ein Mitarbeiter-Objekt, dass durch die Eingabe in den Textfeldern (MitarbeiterHinzuf�genGUI)
	 *  erzeugt wird und an diese Stelle �bergeben wird, um die ben�tigten Werte auszulesen und in die DB einzutragen
	 * @return boolean - ob das Hinzuf�gen erfolgreich war oder nicht
	 */
	public boolean mitarbeiterHinzuf�gen(Mitarbeiter neuerMitarbeiter) 
	{	
		boolean hinzugefuegt = false;
		String sqlStatement = "INSERT INTO Mitarbeiter(maid, anrede, vorname, nachname, plz, ort, strasse, hnr, mail, rolle, passwort, eingelogt)";
		sqlStatement += "VALUES (" + neuerMitarbeiter.getMitarbeiterID() + ",'";
		sqlStatement += neuerMitarbeiter.getAnrede() + "','";
		sqlStatement += neuerMitarbeiter.getVorname() + "','";
		sqlStatement += neuerMitarbeiter.getName() + "',";
		sqlStatement += neuerMitarbeiter.getPlz() + ",'";
		sqlStatement += neuerMitarbeiter.getOrt() + "','";
		sqlStatement += neuerMitarbeiter.getStra�e() + "','";
		sqlStatement += neuerMitarbeiter.getHausNr() + "','";
		sqlStatement += neuerMitarbeiter.getEmail() + "','";
		sqlStatement += neuerMitarbeiter.getRolle() + "','";
		sqlStatement += neuerMitarbeiter.getPasswort() + "',";
		sqlStatement += neuerMitarbeiter.getEingelogt() + ")";
		
		try 
		{
			
			if(sendPasswordController.sendPassword(neuerMitarbeiter.getAnrede(), neuerMitarbeiter.getName(), neuerMitarbeiter.getEmail(), neuerMitarbeiter.getPasswort()) 
					== true && checkEmail(neuerMitarbeiter.getMitarbeiterID(), neuerMitarbeiter.getEmail()) == false) 
			{
			verbindung.getConnection().insert(sqlStatement);
			verbindung.getConnection().commit();
			hinzugefuegt = true;
			} else if(checkEmail(neuerMitarbeiter.getMitarbeiterID(), neuerMitarbeiter.getEmail()) == true)
			{
				JOptionPane.showMessageDialog(null, "Die E-Mail ist bereits in der DB registriert");
			}
		} 
		catch (Exception e) 
		{
			verbindung.getConnection().rollback();
		} 
		return hinzugefuegt;
	}
	
	/**
	 * �berpr�ft, ob die eingegebene E-Mail eines Mitarbeiters bereits in der Datenbank existiert (1)
	 * @param email
	 * @return int-Wert 0 oder 1 als "Flag"
	 */
	public boolean checkEmail(int id, String email) {
		String sqlStatement = "SELECT maid, mail FROM Mitarbeiter WHERE mail = '" + email + "'";
		boolean result = false;
		
		try
		{
			ResultSet rs = verbindung.getConnection().select(sqlStatement);
			if (rs.next()) 
			{
				result = true;
			}
			if(rs.getInt("maid") == id)
			{
				//Email geh�rt zum Mitarbeiter der ge�ndert wird und ist daher vorhanden
				result = false;
			}
		} catch (SQLException e)
		{
			result = false;
		}
		return result;
	}
	
	/**
	 * Methode um einen ausgew�hlten Mitarbeiter aus der DB zu l�schen
	 * @param ma - Es wird der Mitarbeiter �bergeben, der in der Tabelle ausgew�hlt wurde (Zeile)
	 * �bergabe findet im Mitarbeiter�ndernEntfernenController statt
	 */
	public void mitarbeiterLoeschen(Mitarbeiter ma) 
	{
		String sqlStatement = "DELETE FROM Mitarbeiter WHERE maid = " + ma.getMitarbeiterID() + "";
		verbindung.getConnection().update(sqlStatement);
	}

	/**
	 * Methode um die Eigenschaften/Daten eines Mitarbeiters zu �ndern
	 * @param ma - �bergabe des ausgew�hlten Mitarbeites in der Tabelle
	 */
	public boolean mitarbeiterAendern(Mitarbeiter ma) 
	{
		boolean aendernErfolg = false;
		String sqlStatement = "UPDATE Mitarbeiter SET " + "anrede = '" + ma.getAnrede() + "', " + "vorname = '"
				+ ma.getVorname() + "', " + "nachname = '" + ma.getName() + "', " + "plz = " + ma.getPlz() + ", "
				+ "ort = '" + ma.getOrt() + "', " + "strasse = '" + ma.getStra�e() + "', " + "hnr = '" + ma.getHausNr()
				+ "', " + "mail = '" + ma.getEmail() + "', " + "rolle = '" + ma.getRolle() + "' WHERE maid = "
				+ ma.getMitarbeiterID() + "";
		
		
		
		if(checkEmail(ma.getMitarbeiterID(), ma.getEmail()) == false)
		{
			verbindung.getConnection().update(sqlStatement);
			aendernErfolg = true;
			
		} else if(checkEmail(ma.getMitarbeiterID(), ma.getEmail()) == true)
		{
			JOptionPane.showMessageDialog(null, "Die E-Mail ist bereits in der DB registriert");
		}
		return aendernErfolg;
	}
	
	/**
	 * Methode um die zurzeit h�chste MitarbeiterID aus der DB abzufragen
	 * @return - "letzte" MitarbeiterID
	 */
	public int getLastMitarbeiterID() 
	{
		String sqlStatement = "SELECT maid FROM Mitarbeiter WHERE ROWNUM <=1 ORDER BY maid DESC";
		int id = 0;
		try 
		{
			ResultSet rs = verbindung.getConnection().select(sqlStatement);
			while (rs.next()) 
			{
				int lastID = rs.getInt("maid");
				id = lastID;
			}
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 
		return id;
	}

	/**
	 * Methode, um alle Mitarbeiter aus der DB zu laden mit den ben�tigten Attributen (nicht alles wird geladen z.B. Passwort)
	 * @return - ArrayListe aus Mitarbeiter Objekten
	 */
	public ArrayList<Mitarbeiter> mitarbeiterTabelleLaden() 
	{
		ArrayList<Mitarbeiter> maList = new ArrayList<Mitarbeiter>();
		String sqlStatement = "SELECT * FROM Mitarbeiter ORDER BY maid ASC";

		try 
		{
			ResultSet rs = verbindung.getConnection().select(sqlStatement);

			while (rs.next()) 
			{
				int maid = rs.getInt("maid");
				String anrede = rs.getString("anrede");
				String vorname = rs.getString("vorname");
				String nachname = rs.getString("nachname");
				int plz = rs.getInt("plz");
				String ort = rs.getString("ort");
				String stra�e = rs.getString("strasse");
				String hnr = rs.getString("hnr");
				String email = rs.getString("mail");
				String rolle = rs.getString("rolle");

				Mitarbeiter ma = new Mitarbeiter(maid, anrede, vorname, nachname, plz, ort, stra�e, hnr, email, rolle);

				ma.setMitarbeiterID(maid);
				ma.setAnrede(anrede);
				ma.setVorname(vorname);
				ma.setName(nachname);
				ma.setPlz(plz);
				ma.setOrt(ort);
				ma.setStra�e(stra�e);
				ma.setHausNr(hnr);
				ma.setEmail(email);
				ma.setRolle(rolle);

				maList.add(ma);
			}
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return maList;
	}
}