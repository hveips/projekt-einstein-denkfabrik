package controller.mitarbeitercontroller;

/**
 * @author Alexander Stavski
 */

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;

import view.mitarbeiterview.MitarbeiterÄndernEntfernenGUI;
import view.mitarbeiterview.MitarbeiterÄndernGUI;

public class MitarbeiterÄndernEntfernenController
{

	private MitarbeiterÄndernEntfernenGUI gui;
	private MitarbeiterController newOp;

	public MitarbeiterÄndernEntfernenController(MitarbeiterÄndernEntfernenGUI gui)
	{
		this.gui = gui;
		this.newOp = new MitarbeiterController();
	}

	/**
	 * Rollenunterscheidung in der Tabelle
	 * Überprüfung der Rolle vom Eingeloggten und der Rolle der ausgewählten Zeile
	 */
	public void rollenUnterscheidung()
	{
		gui.getTable().addMouseListener(new MouseAdapter()
		{
			public void mouseReleased(MouseEvent me)
			{
				String rolle = (String) gui.getTable().getValueAt(gui.getTable().getSelectedRow(), 8);
				if (gui.getRolleEingelogt() == 'G' && rolle.equals("A"))
				{
					gui.getDeleteButton().setEnabled(false);
					gui.getChangeButton().setEnabled(false);
				} 
				else
				{
					gui.getDeleteButton().setEnabled(true);
					gui.getChangeButton().setEnabled(true);
				}
				if (gui.getRolleEingelogt() == 'G' && rolle.equals("G"))
				{
					gui.getDeleteButton().setEnabled(false);
				}
			}
		});
	}

	/**
	 * Ruft die MitarbeiterÄndern-View auf und übergibt dabei die Rolle des Eingeloggten
	 */
	public void aendernButtonAction()
	{
		int selectedRow = gui.getTable().getSelectedRow();
		if (selectedRow == -1)
		{
			JOptionPane.showMessageDialog(gui, "Kein Mitarbeiter ausgewählt.", "Fehler", JOptionPane.ERROR_MESSAGE);
		} 
		else
		{
			new MitarbeiterÄndernGUI(gui, gui.getRolleEingelogt());
		}
	}

	/**
	 * Methode zum Löschen von Mitarbeitern
	 */
	public void loeschenButtonAction()
	{
		int selectedRow = gui.getTable().getSelectedRow();
		if (selectedRow == -1)
		{
			JOptionPane.showMessageDialog(gui, "Kein Mitarbeiter ausgewählt.", "Fehler", JOptionPane.ERROR_MESSAGE);
		} 
		else
		{
			int option = JOptionPane.showConfirmDialog(gui, "Sind Sie sicher, dass Sie " + gui.getVornameRow() + " "
					+ gui.getNachnameRow() + " löschen wollen?", "Löschen bestätigen", JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION)
			{
				try
				{
					newOp.mitarbeiterLoeschen(gui.getMitarbeiterRow());
					gui.getUpdatedTableModel();
				} 
				catch (Exception ex)
				{
					JOptionPane.showMessageDialog(null, "Fehler:" + ex.getMessage());
				}
			}
		}
	}
}