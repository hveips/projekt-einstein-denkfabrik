package controller.mitarbeitercontroller;

/**
 * @author Alexander Stavski
 * Eine Controller-Klasse f�r die Mitarbeiter�ndernGUI-Klasse
 */

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;

import model.Mitarbeiter;
import view.mitarbeiterview.Mitarbeiter�ndernEntfernenGUI;
import view.mitarbeiterview.Mitarbeiter�ndernGUI;

public class Mitarbeiter�ndernController 
{
	private Mitarbeiter�ndernGUI gui�ndern;
	private Mitarbeiter�ndernEntfernenGUI guiTabelle;
	private MitarbeiterController newOp;
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	private static final String EMAIL_PATTERN = 
		    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public Mitarbeiter�ndernController(Mitarbeiter�ndernGUI gui, Mitarbeiter�ndernEntfernenGUI guiTabelle) 
	{
		this.gui�ndern = gui;
		this.guiTabelle = guiTabelle;
		this.newOp = new MitarbeiterController();
	}
	
	/**
	 * In der Methode findet das Auslesen der Textfelder des Fensters Mitarbeiter�ndernGUI statt
	 * Bei einer �nderung werden die Daten f�r den Mitarbeiter zwischengespeichert
	 * @return - Mitarbeiter-Objekt aus der Eingabe
	 */
	public Mitarbeiter updateMitarbeiter() 
	{
		int maid = Integer.parseInt(gui�ndern.getIDInput().getText());
		String anrede = (String) gui�ndern.getAnredeInput().getSelectedItem();
		String vorname = gui�ndern.getVornameInput().getText().trim();
		String name = gui�ndern.getNachnameInput().getText().trim();
		int plz = Integer.parseInt(gui�ndern.getPlzInput().getText().trim());
		String ort = gui�ndern.getOrtInput().getText().trim();
		String stra�e = gui�ndern.getStrasseInput().getText();
		String hausNr = gui�ndern.getHnrInput().getText().trim();
		String email = gui�ndern.geteMailInput().getText().trim();
		String rolle = (String) gui�ndern.getRolleInput().getSelectedItem();

		Mitarbeiter neuerMitarbeiter = new Mitarbeiter(maid, anrede, vorname, name, plz, ort, stra�e, hausNr, email, rolle);
		return neuerMitarbeiter;
	}
	/**
	 * Die Methode l�dt die Daten des ausgew�hlten Mitarbeiters in der Tabelle in das Fenster Mitarbeiter�ndernGUI
	 */
	public void ladeDatenVonAusgew�hlterZeile() 
	{
		if (guiTabelle.getTable().getSelectedRow() >= 0) 
		{
			int id = (int) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 0);
			int plz = (int) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 3);
			String anrede = guiTabelle.getAnredeRow();

			gui�ndern.getIDInput().setText(Integer.toString(id));
			gui�ndern.getAnredeInput().setSelectedItem(anrede);
			gui�ndern.getNachnameInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 1));
			gui�ndern.getVornameInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 2));
			gui�ndern.getPlzInput().setText(Integer.toString(plz));
			gui�ndern.getOrtInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 4));
			gui�ndern.getStrasseInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 5));
			gui�ndern.getHnrInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 6));
			gui�ndern.geteMailInput().setText((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 7));
			gui�ndern.getRolleInput().setSelectedItem((String) guiTabelle.getTable().getValueAt(guiTabelle.getTable().getSelectedRow(), 8));
		}
	}
	
	/**
	 * Diese Methode ist f�r die erfolgreiche �nderung der Daten eines Mitarbeiters in die DB verantwortlich
	 * (ruft die SQL-Update Methode im MitarbeiterController auf)
	 */
	public void changeAction()
	{
		try 
		{
			Mitarbeiter ma = updateMitarbeiter();
			if(newOp.mitarbeiterAendern(ma) == true)
			{
				JOptionPane.showMessageDialog(null, "Daten wurden ge�ndert und gespeichert");
				guiTabelle.getUpdatedTableModel();
				gui�ndern.getJDialog().dispose();
			} else 
			{
				gui�ndern.geteMailInput().setBorder(border2);
			}
		}
		catch (Exception e) 
		{
			setBorderLine();
		}
	}
	
	/**
	 * Methode wird beim Klick des "Speichern" - Buttons ausgef�hrt
	 * �berpr�fung, ob Textfelder leer sind, wenn nicht wird Methode "changeAction()" aufgerufen, die
	 * f�r die �nderung des Mitarbeiters veranwortlich ist
	 */
	public void changeButtonAction()
	{
		if (gui�ndern.getVornameInput().getText().equals("") || gui�ndern.getNachnameInput().getText().equals("")
				|| gui�ndern.getOrtInput().getText().equals("") || gui�ndern.getStrasseInput().getText().equals("")
				|| gui�ndern.geteMailInput().getText().equals("") || !gui�ndern.geteMailInput().getText().matches(EMAIL_PATTERN) 
				|| gui�ndern.getHnrInput().getText().equals(""))
		{
			setBorderLine();
		} 
		else
		{
			changeAction();
		}
	}
	
	/**
	 * �berpr�ft, ob die Textfelder leer sind und passt dementsprechend die R�nder der Textfelder an
	 * Zus�tzliche �berpr�fung auf eine L�nge der Postleitzahl und ein @-Zeichen in der E-Mail
	 */
	public void setBorderLine()
	{
		if (gui�ndern.getVornameInput().getText().equals(""))
		{
			gui�ndern.getVornameInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getVornameInput().setBorder(border3);
		}

		if (gui�ndern.getNachnameInput().getText().equals(""))
		{
			gui�ndern.getNachnameInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getNachnameInput().setBorder(border3);
		}

		if (gui�ndern.getPlzInput().getText().equals("") || gui�ndern.getPlzInput().getText().length() < 5)
		{
			gui�ndern.getPlzInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getPlzInput().setBorder(border3);
		}
		if (gui�ndern.getOrtInput().getText().equals(""))
		{
			gui�ndern.getOrtInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getOrtInput().setBorder(border3);
		}

		if (gui�ndern.getStrasseInput().getText().equals(""))
		{
			gui�ndern.getStrasseInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getStrasseInput().setBorder(border3);
		}
		if (gui�ndern.getHnrInput().getText().equals(""))
		{
			gui�ndern.getHnrInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.getHnrInput().setBorder(border3);
		}

		if (gui�ndern.geteMailInput().getText().equals("") || !gui�ndern.geteMailInput().getText().matches(EMAIL_PATTERN))
		{
			gui�ndern.geteMailInput().setBorder(border2);
		} 
		else
		{
			gui�ndern.geteMailInput().setBorder(border3);
		}
	}
}