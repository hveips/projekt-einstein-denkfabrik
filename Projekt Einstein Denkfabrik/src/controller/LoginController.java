package controller;

/**
 * @author Hendrik Veips
 */

import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.reservierungcontroller.ReservierungBuchungUmwandlungController;
import controller.reservierungcontroller.ReservierungController;
import model.Reservierung;
import model.Extra;

public class LoginController 
{
	private DatabaseManager db;
	private int a;
	private char[] charArray;
	private boolean validLogin = false;
	private boolean samePassword = false;
	private boolean save = false;
	private char rolle;
	private String name;
	private String anrede;
	private int eingelogt;
	private String security;
	
	private ArrayList<Reservierung> reservierungsliste = new ArrayList<Reservierung>();
	private SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
	private SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
	
	private ReservierungController reservierungController = new ReservierungController();
	
	private ReservierungBuchungUmwandlungController reservierungBuchungUmwandlungController = new ReservierungBuchungUmwandlungController();
	
	/**
	 * Die Methode greift �ber zwei SELECT-Befehle und dem Parameter benutzername auf die Datenbank zu und holt sich die E-Mail Adresse und das Passwort.
	 * In den n�chsten Schritten wird gepr�ft, ob die eingegebene E-Mail Adresse im System existiert und ob das Passwort mit dem gesetzten Passwort
	 * aus der Datenbank �bereinstimmt. Der Erfolg der Methode wird zur�ckgegeben.
	 * @param benutzername
	 * @param password
	 * @return boolean validLogin
	 */
	public boolean pruefeLoginDaten(String benutzername, char[] password)
	{
		String sqlStatement = "SELECT mail FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		String sqlStatement2 = "SELECT passwort FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		try
		{
			a = db.getConnection().select2(sqlStatement);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		if(a == 1)
		{
			try
			{
				ResultSet rs = db.getConnection().select(sqlStatement2);
				while(rs.next())
				{	
					String pw = rs.getString("passwort");
					charArray = pw.toCharArray();
				}
			}
			catch(SQLException e)
			{
				JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
			}
			int i = 0;
			if(password.length == charArray.length)
			{
				while(i < password.length)
				{
					if(password[i] == charArray[i])
					{
						if(i == password.length - 1)
						{
							validLogin = true;
							i++;
						}
						else
						{
							i++;
						}
					}
					else
					{
						validLogin = false;
						i = password.length;
					}
				}
			}
			else
			{
				validLogin = false;
			}
		}
		else
		{
			validLogin = false;
		}
		return validLogin;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl mit dem Parameter benutzername auf die Datenbank zu und holt sich die Rolle des Nutzers.
	 * @param benutzername
	 * @return
	 */
	public char getRolle(String benutzername)
	{
		String sqlStatement = "SELECT rolle FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String role = rs.getString("rolle");
				rolle = role.charAt(0);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return rolle;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl mit dem Parameter benutzername auf die Datenbank zu und holt sich den Nachnamen des Nutzers.
	 * @param benutzername
	 * @return
	 */
	public String getName(String benutzername)
	{
		String sqlStatement = "SELECT nachname FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				name = rs.getString("nachname");
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return name;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl mit dem Parameter benutzername auf die Datenbank zu und holt sich die Anrede des Nutzers.
	 * @param benutzername
	 * @return String anrede
	 */
	public String getAnrede(String benutzername)
	{
		String sqlStatement = "SELECT anrede FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				anrede = rs.getString("anrede");
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return anrede;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl mit dem Parameter benutzername auf die Datenbank zu und holt sich heraus, ob der Nutzer schon mal angemeldet war. 
	 * @param benutzername
	 * @return int eingelogt
	 */
	public int getEingelogt(String benutzername)
	{
		String sqlStatement = "SELECT eingelogt FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				eingelogt = rs.getInt("eingelogt");
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return eingelogt;
	}
	
	/**
	 * Die Methode greift �ber den UPDATE-Befehl mit den Parametern passwort und benutzername auf die Datenbank zu und speichert das Passwort f�r den Nutzer.
	 * Es gibt den Erfolg der Methode zur�ck.
	 * @param passwort
	 * @param benutzername
	 * @return boolean save
	 */
	public boolean speicherePasswort(String passwort, String benutzername)
	{
		String sqlStatement = "UPDATE Mitarbeiter SET passwort = '" + passwort + "', eingelogt = 1 WHERE mail = '" + benutzername + "'";
		try
		{
			db.getConnection().update(sqlStatement);
			db.getConnection().commit();
			save = true;
		}
		catch(Exception e)
		{
			save = false;
		}
		return save;
	}
	
	/**
	 * Die Methode kontrolliert die beiden Parameter charArray und charArray2 auf Gleichheit und gibt ein Ergebnis zur�ck.
	 * @param charArray
	 * @param charArray2
	 * @return boolean samePassword
	 */
	public boolean pruefePasswoerter(char[] charArray, char[] charArray2)
	{
		if(charArray.length == charArray2.length)
		{
			int i = 0;
			while(i < charArray.length)
			{
				if(charArray[i] == charArray2[i])
				{
					if(i == charArray2.length - 1)
					{
						samePassword = true;
						i++;
					}
					else
					{
						i++;
					}
				}
				else
				{
					samePassword = false;
					i = charArray.length;
				}
			}
		}
		else
		{
			samePassword = false;
		}
		return samePassword;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl mit dem Parameter benutzername auf die Datenbank und kontrolliert, ob der Benutzername vorhanden ist.
	 * @param benutzername
	 * @return int a
	 */
	public int pruefeUsername(String benutzername)
	{
		String sqlStatement = "SELECT mail FROM mitarbeiter WHERE mail like ('" + benutzername + "')";
		try
		{
			a = db.getConnection().select2(sqlStatement);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return a;
	}
	
	/**
	 * Die Methode erzeugt eine zuf�llige f�nfstellige Reihefolge an Zahlen und Buchstaben und gibt diese zur�ck.
	 * @return String security
	 */
	public String setZufallsSicherheitsAbfrage() 
	{
		int laenge = 5;
		String zeichen = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder passwort = new StringBuilder(laenge);
		SecureRandom random = new SecureRandom();
		for (int i = 1; i <= laenge; i++) 
		{
			passwort.append(zeichen.charAt(random.nextInt(zeichen.length())));
		}
		security = String.valueOf(passwort);
		return security;
	}
	
	/**
	 * Die Methode greift zuerst �ber den SELECT-Befehl und den Parametern tag, monat und jahr auf die Datenbank zu
	 * und holt sich die Reservierungen die kleiner als das entstehende Datum sind; alle Attribute werden ausgelesen, zwischengespeichert, als Reservierung erzeugt
	 * und in einer Liste gespeichert. Im n�chsten Schritt wird die Liste durchgegangen und die dazu geh�rigen Extras werden mit in der Liste gespeichert.
	 * Dann werden mit Hilfe des ReservierungBuchungUmwandlungControllers die Reservierungen in Buchungen umgewandelt 
	 * und die Reservierungen �ber den ReservierungController gel�scht.
	 * @param tag
	 * @param monat
	 * @param jahr
	 */
	public void wandleReservierungInBuchung(String tag, String monat, String jahr)
	{
		String sqlStatement = "SELECT * FROM RESERVIERUNG WHERE datum < '" + tag + "." + monat + "." + jahr + "'";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				int resnr = rs.getInt("resnr");
				String datum = format1.format(rs.getDate("datum")) + " " + format2.format(rs.getTime("datum"));
				double gesamtbetrag = rs.getDouble("gesamtbetrag");
				int kdid = rs.getInt("kdid");
				int anzTage = rs.getInt("anztage");
				int stunden = rs.getInt("stunden");
				int rnr = rs.getInt("rnr");
				String enddatum = format1.format(rs.getDate("enddatum")) + " " + format2.format(rs.getTime("enddatum"));
				int pAnzahl = rs.getInt("personenanzahl");
				
				Reservierung reservierung = new Reservierung(resnr, datum, enddatum, anzTage, stunden, rnr, kdid, new ArrayList<Extra>(), pAnzahl, gesamtbetrag);
				reservierungsliste.add(reservierung);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		for(int i = 0;i < reservierungsliste.size();i++)
		{
			ArrayList<Extra> extras = new ArrayList<Extra>();
			extras = reservierungBuchungUmwandlungController.ladeExtrasAusReservierung(reservierungsliste.get(i).getReservierungsNr());
			reservierungsliste.get(i).setExtras(extras);
		}
		for(int i = 0;i < reservierungsliste.size();i++)
		{
			reservierungBuchungUmwandlungController.wandleReservierungInBuchung(reservierungsliste.get(i));
		}
		for(int i = 0;i < reservierungsliste.size();i++)
		{
			reservierungController.reservierungStornieren(reservierungsliste.get(i).getReservierungsNr());
		}
	}
}