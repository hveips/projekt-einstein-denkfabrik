package controller;

/**
 * @author Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class CalendarController 
{	
	private SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
	private SimpleDateFormat format2 = new SimpleDateFormat("HH:mm'Uhr'");
	private DecimalFormat format3 = new DecimalFormat("00");
	
	private String tag;
	private String monat;
	private String buchung;
	private String reservierung;
	private String day2;
	private String month2;
	private String year2;
	
	private DatabaseManager db;
	
	private DatumController datumController = new DatumController();
	
	/**
	 * Die Methode holt sich mit Hilfe des DatumControllers und den Parametern day, month und year das Datum des n�chsten Tages und formatiert sich dies.
	 * Danach greift sie �ber den SELECT-Befehl auf die Datenbank zu und holt sich mit den Variablen tag, monat, year, day2, month2 und year2 die Buchungen vom heutigen Tag.
	 * Je nach Mietdauer wird ein String erzeugt und in einer Liste gespeichert.
	 * @param day
	 * @param month
	 * @param year
	 * @return ArrayList<String> buchungen
	 */
	public ArrayList<String> getBuchungen(int day, int month, int year)
	{
		ArrayList<String> buchungen = new ArrayList<String>();
		day2 = datumController.getNaechstenTag(day, month, year);
		month2 = datumController.getNaechstenMonat(day2, month);
		year2 = datumController.getNaechstesJahr(day, month, year);
		tag = format3.format(day);
		monat = format3.format(month);
		String sqlStatement = "SELECT datum, rnr, anztage, stunden FROM buchung WHERE datum >= '" + tag + "." + monat + "." + year + "' AND datum < '" + day2 + "." + month2 + "." + year2 + "' AND storniert = 0 ORDER BY datum ASC";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String datum = format1.format(rs.getDate("datum"));
				String uhrzeit = format2.format(rs.getTime("datum"));
				String raumnummer = rs.getString("rnr");
				if(rs.getInt("anzTage") > 1)
				{
					buchung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("anzTage") + " Tage";
				}
				else if(rs.getInt("anzTage") == 1)
				{
					buchung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("anztage") + " Tag";
				}
				else if(rs.getInt("stunden") > 1)
				{
					buchung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("stunden") + " Stunden";
				}
				else
				{
					buchung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("stunden") + " Stunde";
				}
				buchungen.add(buchung);
				buchung = null;
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return buchungen;
	}
	
	/**
	 * Die Methode holt sich mit Hilfe des DatumControllers und den Parametern day, month und year das Datum des n�chsten Tages und formatiert sich dies.
	 * Danach greift sie �ber den SELECT-Befehl auf die Datenbank zu und holt sich mit den Variablen tag, monat, year, day2, month2 und year2 die Reservierungen vom heutigen Tag.
	 * Je nach Mietdauer wird ein String erzeugt und in einer Liste gespeichert.
	 * @param day
	 * @param month
	 * @param year
	 * @return ArrayList<String> reservierungen
	 */
	public ArrayList<String> getReservierungen(int day, int month, int year)
	{
		ArrayList<String> reservierungen = new ArrayList<String>();
		day2 = datumController.getNaechstenTag(day, month, year);
		month2 = datumController.getNaechstenMonat(day2, month);
		year2 = datumController.getNaechstesJahr(day, month, year);
		tag = format3.format(day);
		monat = format3.format(month);
		String sqlStatement = "SELECT datum, rnr, anztage, stunden FROM reservierung WHERE datum >= '" + tag + "." + monat + "." + year + "' AND datum < '" + day2 + "." + month2 + "." + year2 + "' ORDER BY datum ASC";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String datum = format1.format(rs.getDate("datum"));
				String uhrzeit = format2.format(rs.getTime("datum"));
				String raumnummer = rs.getString("rnr");
				if(rs.getInt("anzTage") > 1)
				{
					reservierung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("anzTage") + " Tage";
				}
				else if(rs.getInt("anzTage") == 1)
				{
					reservierung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("anztage") + " Tag";
				}
				else if(rs.getInt("stunden") > 1)
				{
					reservierung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("stunden") + " Stunden";
				}
				else
				{
					reservierung = datum + ", " + uhrzeit + " Raum: " + raumnummer + " Mietdauer: " + rs.getString("stunden") + " Stunde";
				}
				reservierungen.add(reservierung);
				reservierung = null;
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return reservierungen;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich mit den Variablen tag und monat und dem Parameter year die Buchungen,
	 * bei denen der heutige Tag in die Zeitspanne der Buchung f�llt. Es wird ein String erzeugt und in einer Liste gespeichert.
	 * @param day
	 * @param month
	 * @param year
	 * @return ArrayList<String> buchungen
	 */
	public ArrayList<String> getBuchungen2(int day, int month, int year)
	{
		ArrayList<String> buchungen = new ArrayList<String>();
		tag = format3.format(day);
		monat = format3.format(month);
		String sqlStatement = "SELECT datum, rnr, enddatum FROM buchung WHERE datum < '" + tag + "." + monat + "." + year + "' AND enddatum >= '" + tag + "." + monat + "." + year + "' ORDER BY datum ASC";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String datum = format1.format(rs.getDate("datum"));
				String uhrzeit = format2.format(rs.getTime("datum"));
				String endDatum = format1.format(rs.getDate("enddatum"));
				String endUhrzeit = format2.format(rs.getTime("enddatum"));
				String raumnummer = rs.getString("rnr");
				buchung = "vom " + datum + ", " + uhrzeit + " bis zum " + endDatum + ", " + endUhrzeit + " Raum: " + raumnummer;
				buchungen.add(buchung);
				buchung = null;
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return buchungen;
	}
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich mit den Variablen tag und monat und dem Parameter year die Reservierungen,
	 * bei denen der heutige Tag in die Zeitspanne der Reservierung f�llt. Es wird ein String erzeugt und in einer Liste gespeichert.
	 * @param day
	 * @param month
	 * @param year
	 * @return ArrayList<String> reservierungen
	 */
	public ArrayList<String> getReservierungen2(int day, int month, int year)
	{
		ArrayList<String> reservierungen = new ArrayList<String>();
		tag = format3.format(day);
		monat = format3.format(month);
		String sqlStatement = "SELECT datum, rnr, enddatum FROM reservierung WHERE datum < '" + tag + "." + monat + "." + year + "' AND enddatum >= '" + tag + "." + monat + "." + year + "' ORDER BY datum ASC";
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				String datum = format1.format(rs.getDate("datum"));
				String uhrzeit = format2.format(rs.getTime("datum"));
				String endDatum = format1.format(rs.getDate("enddatum"));
				String endUhrzeit = format2.format(rs.getTime("enddatum"));
				String raumnummer = rs.getString("rnr");
				reservierung = "vom " + datum + ", " + uhrzeit + " bis zum " + endDatum + ", " + endUhrzeit + " Raum: " + raumnummer;
				reservierungen.add(reservierung);
				reservierung = null;
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return reservierungen;
	}
}