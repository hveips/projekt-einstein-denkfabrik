package controller.buchungcontroller;

/**
 * @author Niklas Nebeling
 */

import java.util.ArrayList;
import java.util.HashMap;

import controller.MailController;
import model.Buchung;
import model.Extra;
import view.buchung_reservierungview.BuchungBearbeitenStornierenGUI;

public class BuchungBearbeitenController 
{
	private Buchung buchung;
	private BuchungController DBBuchung;
	private int personenanzahl;
	private ArrayList<Extra> extras = new ArrayList<Extra>();
	private String sonderExtra;
	private MailController mailcontroller;
	
	/**
	 * Der Kontruktor initialisiert den BuchungController und den MailController
	 */
	public BuchungBearbeitenController() 
	{
		DBBuchung = new BuchungController();
		mailcontroller = new MailController();
	}
	
	/**
	 * Zum übergeben der Buchung aus der BearbeitenGUI.
	 * @param buchung
	 */
	public void übergebeBuchung(Buchung buchung)
	{
		this.buchung = buchung;
	}

	/**
	 * Diese Methode updated Extras, Personenanzahl, Gesamtbetrag
	 * und ruft dann die Änderungsmethode aus dem BuchungController auf.
	 * Schickt außerdem Änderungs-Emails an den Catering-Dienstleiser und an den Kunden.
	 */
	public void buchen() 
	{
		buchung.setExtras(extras);
		buchung.setPersonenanzahl(personenanzahl);
		if(buchung.getAnzTage() == 0) 
		{
			berechneGesamtbetragEintägig();
		}
		else 
		{
			berechneGesamtbetragMehrtägig();
		}
		DBBuchung.buchungAendern(buchung);
		mailcontroller.sendCatering(buchung.getVorname(), buchung.getName(), buchung.getKuID(), buchung.getAnfangsdatum(), 
				buchung.getEnddatum(), personenanzahl, extras, sonderExtra, true);
		
		mailcontroller.sendBestätigung(buchung.getBuchungsNr(), buchung.getAnfangsdatum(), buchung.getEnddatum(), buchung.getRaumNr(), 
				buchung.getEmail(), buchung.getName(), extras, sonderExtra, personenanzahl, "Buchung", true);
		clearExtras();
	}
	
	/**
	 * Berechnet und setzt den Gesamtbetrag bei eintägiger Buchung.
	 */
	private void berechneGesamtbetragEintägig() {
		HashMap<String,Double> preise = DBBuchung.ladeExtraPreise();
		double stundenpreis = DBBuchung.getRaumPreis_h(buchung.getRaumNr());
		double gesamtbetrag = 0;
		gesamtbetrag = stundenpreis*buchung.getStunden();
		if(!extras.isEmpty())
		{
			for(int i=0; i<extras.size(); i++) 
			{
				gesamtbetrag += preise.get(extras.get(i).getBezeichnung())*personenanzahl;
			}
		}
		buchung.setGesamtbetrag(gesamtbetrag);
	}
	
	/**
	 * Berechnet und setzt den Gesamtbetrag bei mehrtägiger Buchung.
	 */
	private void berechneGesamtbetragMehrtägig() {
		HashMap<String,Double> preise = DBBuchung.ladeExtraPreise();
		double tagespreis = DBBuchung.getRaumTagespreis(buchung.getRaumNr());
		double gesamtbetrag = 0;
		gesamtbetrag = tagespreis*buchung.getAnzTage();
		if(!extras.isEmpty())
		{
			for(int i=0; i<extras.size(); i++) 
			{
				gesamtbetrag += preise.get(extras.get(i).getBezeichnung())*personenanzahl*buchung.getAnzTage();
			}
		}
		buchung.setGesamtbetrag(gesamtbetrag);
	}
	
	/**
	 * Wird von der ExtrasGUI aufgerufen um ausgewählte Extras 
	 * zu setzen.
	 * @param bezeichnung Die Bezeichnung des extras
	 */
	public void erzeugeExtra(String bezeichnung) 
	{
		extras.add(new Extra(bezeichnung));
	}
	
	/**
	 * Wird von der ExtrasGUI aufgerufen um das gewünschte 
	 * Sonderextra zu setzen.
	 * @param bezeichnung Die Bezeichnung des extras
	 */
	public void sonderExtra(String bezeichnung) 
	{
		this.sonderExtra = bezeichnung;
	}
	
	/**
	 * Wird von der ExtrasGUI aufgerufen um die Personenanzahl zu setzen
	 * @param anzahl Die Anzahl der Personen.
	 */
	public void übergebePersonenAnzahl(int anzahl) {
		personenanzahl = anzahl;
	}
	
	/**
	 * Leert die Extras ArrayList für die nächste Buchung/Reservierung.
	 */
	private void clearExtras() {
		extras.clear();
	}
	
	/**
	 * Wird von der ErstelleRechnungGUI aufgerufen um eine
	 * Rechnung zu erstellen.
	 * @param buchung Buchung aus der ErstelleRechnungGUI
	 * @param rabatt Rabatt aus der ErstelleRechnungGUI
	 */
	public void erstelleRechnung(Buchung buchung, int rabatt) 
	{
		DBBuchung.erstelleRechnung(buchung, 100-rabatt);
	}
}
