package controller.buchungcontroller;

/**
 * @author Niklas Nebeling
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import controller.MailController;
import controller.reservierungcontroller.ReservierungController;
import model.Buchung;
import model.Extra;
import model.Raum;
import model.Reservierung;
import view.buchung_reservierungview.BucheReserviereRaumGUI;
import view.buchung_reservierungview.BucheReserviereRaumWeiterGUI;

public class BuchenReservierenController 
{
	private BuchungController DBBuchung;
	private ReservierungController DBReservierung;
	private int kuID;
	private Raum raum;
	private String vorname, name;
	private String anfangsdatum;
	private String enddatum;
	private int stunden;
	private int anzTage;
	private int personenanzahl;
	private ArrayList<Extra> extras = new ArrayList<Extra>();
	private double gesamtbetrag;
	private String sonderExtra;
	boolean eint�gig;
	private MailController mailcontroller;
	private BucheReserviereRaumGUI gui;
	SimpleDateFormat sf = new SimpleDateFormat("dd.MM.yy");

	/**
	 * Konstruktor der die ben�tigten Controller-Klassen initialisiert und die Gui setzt.
	 * @param gui Instanzvariable BucheReserviereRaumGUI wird gesetzt
	 */
	public BuchenReservierenController(BucheReserviereRaumGUI gui) 
	{
		DBBuchung = new BuchungController();
		DBReservierung = new ReservierungController();
		mailcontroller = new MailController();
		this.gui = gui;
	}
	
	/**
	 * Diese Methode wird von der BucheReserviereRaumGUI aufgerufen. Sie bereitet den weiteren
	 * Buchungs-/Reservierungsprozess vor und erzeugt die BucheReserviereRaumWeiterGUI.
	 * @param eint�gig true=eint�gig, false=mehrt�gig
	 * @param buchen true=Buchung, false=Reservierung
	 */
	public void weiter(boolean eint�gig, boolean buchen) 
	{
		this.vorname = gui.getVornameRow();
		this.name = gui.getNameRow();
		this.kuID = gui.getKundenIDRow();
		this.eint�gig = eint�gig;
		
		if(eint�gig) 
		{
			eint�gig(buchen);	
		}
		else 
		{
			mehrt�gig(buchen);
		}
	}

	/**
	 * Diese Methode �berpr�ft Eingaben f�r eine eint�gige Buchung/Reservierung und 
	 * setzt bei korrekter Eingabe Anfangsdatum, Enddatum und Stundenanzahl, bei
	 * falscher Eingabe werden Fehlermeldungen ausgegeben.
	 * @param buchen true=Buchung, false=Reservierung
	 */
	private void eint�gig(boolean buchen) {
		if(gui.getDatumVonInput() == null) 
		{
			JOptionPane.showMessageDialog(null, "Bitte ein Datum ausw�hlen.\n");
			return;
		}
		//Wenn die Zeit des Calendars des DateChoosers, welcher die Systemzeit bei erstmaligem anklicken des DateChoosers  als Uhrzeit hat, plus 24h in ms(86400000), 
		//kleiner ist als die Systemzeit, dann w�rde das gew�hlte Datum in der Vergangenheit liegen
		if((gui.getDatumVonInput().getTime()+86400000)<System.currentTimeMillis())
		{
			JOptionPane.showMessageDialog(null, "Bitte zuk�nftige Daten ausw�hlen.\n");
		}
		else if(buchen == false && gui.getDatumVonInput().getTime()<System.currentTimeMillis()+259200000)
		{
			JOptionPane.showMessageDialog(null, "Reservierungen m�ssen mindestens 4 Tage in der Zukunft beginnen.\n");
		}
		else 
		{
			if(checkUhrzeit())
			{
				this.anfangsdatum = sf.format(gui.getDatumVonInput()) + " " + gui.getUhrzeitVonInput();
				this.enddatum = sf.format(gui.getDatumVonInput()) + " " + gui.getUhrzeitBisInput();
				berechneStunden();
				new BucheReserviereRaumWeiterGUI(gui, buchen, this, anfangsdatum, enddatum, true);						
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Ung�ltige Uhrzeit(en) angegeben.\nGesch�ftszeiten: 06:00 - 21:00 Uhr.\nMindestdauer: 1 Stunde. \n", "Fehler!", JOptionPane.ERROR_MESSAGE);						
			}
		}
	}
	
	/**
	 * Diese Methode �berpr�ft Eingaben f�r eine mehrt�gige Buchung/Reservierung und 
	 * setzt bei korrekter Eingabe Anfangsdatum, Enddatum und Tagesanzahl, bei
	 * falscher Eingabe werden Fehlermeldungen ausgegeben.
	 * @param buchen true=Buchung, false=Reservierung
	 */
	private void mehrt�gig(boolean buchen) {
		if(gui.getDatumVonInput() == null || gui.getDatumBisInput() == null) 
		{
			JOptionPane.showMessageDialog(null, "Bitte Daten ausw�hlen.\n");
			return;
		}
		//Wenn die Zeit des Calendars des DateChoosers, welcher die Systemzeit bei erstmaligem anklicken des DateChoosers als Uhrzeit hat, plus 24h in ms, 
		//kleiner ist als die Systemzeit, dann w�rde das gew�hlte Datum in der Vergangenheit liegen
		//Au�erdem muss das Enddatum mindestens einen Tag nach dem Anfangsdatum liegen
		if((gui.getDatumVonInput().getTime()+86400000)<System.currentTimeMillis() || (gui.getDatumBisInput().getTime()+86400000)<System.currentTimeMillis() 
				|| gui.getDatumBisInput().getTime()<gui.getDatumVonInput().getTime()+66400000)//In der letzen Bedingung wird am Ende nur 66400000 drauf addiert, 
																							//da wenn man den zweiten DateChooser zuerst anklickt, ein Bug entsteht
																							//durch den man nicht mehr zweit�gig buchen/reservieren kann
		{
			JOptionPane.showMessageDialog(null, "Bitte zuk�nftige Daten w�hlen. Anfangsdatum muss vor dem Enddatum liegen.\n");
		}
		//Wenn die Buchung l�nger als 31 tage ist (2678400000ms sind 31 tage), long cast da int zu klein
		else if(gui.getDatumBisInput().getTime() > gui.getDatumVonInput().getTime() + ((long)26784000 * 100))
		{
			JOptionPane.showMessageDialog(null, "Maximale Buchungsdauer 31 Tage! \n", "Fehler!", JOptionPane.ERROR_MESSAGE);
		}
		//Reservierung werden 3 Tage vor Beginn automatisch in Buchugen umgewandelt, daher m�ssen sie mindestens 4 Tage in der Zukunft liegen
		else if(buchen == false && gui.getDatumVonInput().getTime()<System.currentTimeMillis()+259200000)
		{
			JOptionPane.showMessageDialog(null, "Reservierungen m�ssen mindestens 4 Tage in der Zukunft beginnen.\n");
		}
		else 
		{
			this.anfangsdatum = sf.format(gui.getDatumVonInput()) + " 06:00";
			this.enddatum = sf.format(gui.getDatumBisInput()) + " 21:00";
			berechneTage();
			new BucheReserviereRaumWeiterGUI(gui, buchen, this, anfangsdatum, enddatum, false);
		}
	}
	
	/**
	 * Diese Methode �berpr�ft die Uhrzeitangaben f�r eine eint�gige Buchung.
	 * @return true bei korrekten Uhrzeiten, sonst false
	 */
	private boolean checkUhrzeit() {
		String von = gui.getUhrzeitVonInput();
		String bis = gui.getUhrzeitBisInput();
		
		//richtige L�nge
		if(von.length() < 5 || bis.length() < 5) 
		{
			return false;
		}
		// an der 3. stelle muss ein : stehen
		if(von.charAt(2) != ':' || bis.charAt(2) != ':') 
		{
			return false;
		}
		
		//jeweils aufsplitten in Stunden und Minuten
		String[] vonparts = von.split(":");
		String[] bisparts = bis.split(":");
		int vonStunden = Integer.parseInt(vonparts[0]);
		int vonMinuten = Integer.parseInt(vonparts[1]);
		int bisStunden = Integer.parseInt(bisparts[0]);
		int bisMinuten = Integer.parseInt(bisparts[1]);
		
		//Beginn Gesch�ftszeiten 06:00
		if(vonStunden < 6 || bisStunden < 7 )
		{
			return false;
		}
		//Ende Gesch�ftszeiten 21:00
		if(vonStunden > 20 || bisStunden > 21 || (bisStunden == 21 && bisMinuten != 0))
		{
			return false;
		}
		//Minuten gehen nur bis 59
		if(vonMinuten > 59 || bisMinuten > 59)
		{
			return false;
		}
		//Wenn bis vor von liegt
		if(bisStunden <= vonStunden)
		{
			return false;
		}
		//Mindestdauer eine Stunde
		if(bisStunden == vonStunden+1)
		{
			if(bisMinuten < vonMinuten)
			{
				return false;
			}
		}
		return true;	
	}
	
	/**
	 * Diese Methode wird von der BucheReserviereRaumWeiterGUI genutzt, um den aus
	 * der Tabelle ausgew�hlten Raum an den Controller zu �bergeben.
	 * @param raum der ausgew�hlte Raum
	 */
	public void �bergebeRaum(Raum raum) 
	{
		this.raum = raum;
	}

	
	/**
	 * Holt sich die letze Buchungsnummer aus der DB +1, berechnet den Gesamtbetrag, schreibt die Buchung in die DB
	 * und setzt die Extra-Liste zur�ck f�r die n�chste Buchung/Reservierung.
	 */
	public void buchen() 
	{
		int bnr = DBBuchung.getLastBuchungsNr();
		if(eint�gig) 
		{
			berechneGesamtbetragEint�gig();
		}
		else 
		{
			berechneGesamtbetragMehrt�gig();
		}
		if(personenanzahl == 0)
		{
			personenanzahl = raum.getSitzpl�tze();
		}
		DBBuchung.buchungHinzufuegen(new Buchung(bnr, anfangsdatum, enddatum, anzTage, stunden, raum.getRaumNr(), kuID, extras, personenanzahl, gesamtbetrag));
		if(!extras.isEmpty())
		{
			mailcontroller.sendCatering(vorname, name, kuID, anfangsdatum, enddatum, personenanzahl, extras, sonderExtra, false);		
		}
		mailcontroller.sendBest�tigung(bnr, anfangsdatum, enddatum, raum.getRaumNr(), gui.getEmailRow(), gui.getNameRow(), extras, sonderExtra, personenanzahl, "Buchung", false);
		clearExtras();
	}
	
	/**
	 * Holt sich die letze Reservierungsnummer aus der DB +1, berechnet den Gesamtbetrag, schreibt die Reservierung in die DB
	 * und setzt die Extra-Liste zur�ck f�r die n�chste Buchung/Reservierung.
	 */
	public void reservieren() 
	{
		int resnr = DBReservierung.getLastReservierungsNr();
		if(eint�gig) 
		{
			berechneGesamtbetragEint�gig();
		}
		else 
		{
			berechneGesamtbetragMehrt�gig();
		}
		if(personenanzahl == 0)
		{
			personenanzahl = raum.getSitzpl�tze();
		}
		DBReservierung.reservierungHinzufuegen(new Reservierung(resnr, anfangsdatum, enddatum, anzTage, stunden, raum.getRaumNr(), kuID, extras, personenanzahl, gesamtbetrag));
		if(!extras.isEmpty())
		{
			mailcontroller.sendCatering(vorname, name, kuID, anfangsdatum, enddatum, personenanzahl, extras, sonderExtra, false);		
		}
		mailcontroller.sendBest�tigung(resnr,anfangsdatum, enddatum, raum.getRaumNr(), gui.getEmailRow(), gui.getNameRow(), extras, sonderExtra, personenanzahl, "Reservierung", false);
		clearExtras();
	}
	
	/**
	 * Berechnet und setzt die Tagesanzahl einer Buchung.
	 */
	public void berechneTage()
	{
		String[] datumparts = anfangsdatum.split("\\.");
		int anfangstag = Integer.parseInt(datumparts[0]);
		int anfangsmonat = Integer.parseInt(datumparts[1]);
		String[] enddatumparts = enddatum.split("\\.");
		int endtag = Integer.parseInt(enddatumparts[0]);
		int endmonat = Integer.parseInt(enddatumparts[1]);
		//Wenn Monat unterschiedlich
		if(anfangsmonat != endmonat)
		{
			//Wenn vorheriger Monat 30 Tage hatte
			if(anfangsmonat == 4 || anfangsmonat == 6 || anfangsmonat == 9 || anfangsmonat == 11)
			{
				//tage im angebrochenen Monat + Rest Tage im vorherigen Monat
				this.anzTage = endtag + 30-anfangstag +1;
			}
			//Februar
			else if(anfangsmonat == 2)
			{
				this.anzTage = endtag + 28-anfangstag +1;
			}
			//Sonst 31
			else
			{
				this.anzTage = endtag + 31-anfangstag +1;
			}
		}
		else
		{
			this.anzTage = endtag-anfangstag +1;
		}
		
		this.stunden = 0;
			
	}
	
	/**
	 * Berechnet und setzt die Stundenanzahl einer Buchung.
	 */
	public void berechneStunden()
	{
		String[] parts = anfangsdatum.split(" ");
		String[] uhrzeitparts = parts[1].split(":");
		int anfangsstunde = Integer.parseInt(uhrzeitparts[0]);
		int anfangsminute = Integer.parseInt(uhrzeitparts[1]);
		parts = enddatum.split(" ");
		uhrzeitparts = parts[1].split(":");
		int endstunde = Integer.parseInt(uhrzeitparts[0]);
		int endminute = Integer.parseInt(uhrzeitparts[1]);
		
		if(endminute > anfangsminute) {
			this.stunden = endstunde-anfangsstunde+1;
		}
		else {
			this.stunden = endstunde-anfangsstunde;
		}
		this.anzTage = 0;
	}
	
	/**
	 * Berechnet und setzt den Gesamtbetrag bei eint�giger Buchung.
	 */
	private void berechneGesamtbetragEint�gig() {
		HashMap<String,Double> preise = DBBuchung.ladeExtraPreise();
		double stundenpreis = DBBuchung.getRaumPreis_h(raum.getRaumNr());
		gesamtbetrag = stundenpreis*stunden;
		if(!extras.isEmpty())
		{
			for(int i=0; i<extras.size(); i++) 
			{
				gesamtbetrag += preise.get(extras.get(i).getBezeichnung())*personenanzahl;
			}
		}	
	}
	
	/**
	 * Berechnet und setzt den Gesamtbetrag bei mehrt�giger Buchung.
	 */
	private void berechneGesamtbetragMehrt�gig() {
		HashMap<String,Double> preise = DBBuchung.ladeExtraPreise();
		double tagespreis = DBBuchung.getRaumTagespreis(raum.getRaumNr());
		gesamtbetrag = tagespreis*anzTage;
		if(!extras.isEmpty())
		{
			for(int i=0; i<extras.size(); i++) 
			{
				gesamtbetrag += preise.get(extras.get(i).getBezeichnung())*personenanzahl*anzTage;		
			}
		}
	}
	
	/**
	 * Wird von der ExtrasGUI aufgerufen um ausgew�hlte Extras 
	 * zu setzen.
	 * @param bezeichnung Die Bezeichnung des extras
	 */
	public void erzeugeExtra(String bezeichnung) 
	{
		extras.add(new Extra(bezeichnung));
	}
	
	/**
	 * Wird von der ExtrasGUI aufgerufen um das gew�nschte 
	 * Sonderextra zu setzen.
	 * @param bezeichnung Die Bezeichnung des extras
	 */
	public void sonderExtra(String bezeichnung) 
	{
		this.sonderExtra = bezeichnung;
	}
	
	/**
	 * Wird von der ExtrasGUI aufgerufen um die Personenanzahl zu setzen
	 * @param anzahl Die Anzahl der Personen
	 */
	public void �bergebePersonenAnzahl(int anzahl) {
		personenanzahl = anzahl;
	}
	
	/**
	 * Leert die Extras ArrayList f�r die n�chste Buchung/Reservierung
	 */
	private void clearExtras() {
		extras.clear();
	}

}