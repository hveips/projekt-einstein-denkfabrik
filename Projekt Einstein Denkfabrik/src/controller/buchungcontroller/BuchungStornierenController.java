package controller.buchungcontroller;

/**
 * @author Hendrik Veips
 */

import javax.swing.JOptionPane;

import controller.MailController;
import view.buchung_reservierungview.BuchungBearbeitenStornierenGUI;

public class BuchungStornierenController 
{
	private MailController mailController;
	private BuchungController op;
	private BuchungBearbeitenStornierenGUI gui;
	
	/**
	 * Der Konstruktor erzeugt den BuchungStornierenController.
	 * @param gui
	 */
	public BuchungStornierenController(BuchungBearbeitenStornierenGUI gui)
	{
		this.gui = gui;
		this.op = new BuchungController();
		this.mailController = new MailController();
	}
	
	/**
	 * Die Methode holt sich aus der Tabelle die ausgewählte Zeile; falls keine Zeile ausgewählt wurde, wird dies dem Nutzer gemeldet.
	 * Ansonsten wird der Nutzer gefragt, ob er/sie sich sicher ist, dass die Buchung storniert wird.
	 * Bei einer Bestätigung wird mit Hilfe des BuchungsController die Buchung storniert, dann die Tabelle in der BuchungBearbeitenStronierenGUI aktualisiert
	 * und eine Bestätigung als JOptionPane an den Nutzer geschickt. Eventuelle Fehler werden abgefangen und dem Nutzer mitgeteilt.
	 */
	public void loeschenButtonAction() 
	{
		int selectedRow = gui.getTable().convertRowIndexToModel(gui.getTable().getSelectedRow());
		if (selectedRow == -1) 
		{
			JOptionPane.showMessageDialog(gui, "Keine Buchung ausgewählt.", "Fehler", JOptionPane.ERROR_MESSAGE);
		} 
		else 
		{
			int option = JOptionPane.showConfirmDialog(gui, "Sind Sie sicher, dass Sie die Buchung von " + gui.getBuchungRow().getVorname() 
					+ " " + gui.getBuchungRow().getName() + " stornieren wollen?", "Stornierung bestätigen", JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) 
			{
				try 
				{
					mailController.sendStornierung(gui.getEmailRow(), gui.getBuchungsNrRow(), gui.getNameRow());
					op.buchungStornieren(gui.getBuchungRow().getBuchungsNr());
					gui.databaseUpdated();	
				} 
				catch (Exception ex) 
				{
					JOptionPane.showMessageDialog(null, "Fehler:" + ex.getMessage());
				}
			}
		}
	}
}