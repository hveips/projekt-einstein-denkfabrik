package controller.buchungcontroller;

/**
 * @author Niklas Nebeling, Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import controller.DatabaseManager;
import model.Buchung;
import model.Extra;

public class BuchungController 
{
	private SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
	private SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
	DecimalFormat df = new DecimalFormat(".00");
	

	/**
	 * Diese Methode erzeugt SQL-Strings zur Erzeugung eines neuen Buchungsobjektes 
	 * in der Datenbank und f�hrt diese dann aus, gibt Erfolgsmeldung dar�ber aus.
	 * @param neueBuchung Die hinzuzuf�gende Buchung
	 */
	public void buchungHinzufuegen(Buchung neueBuchung) 
	{
		ArrayList<String> Statements = new ArrayList<String>();
		String sqlStatement = "INSERT INTO Buchung(bnr, datum, storniert, gesamtbetrag, kdid, anztage, rnr, stunden, enddatum, verschickt, personenanzahl) VALUES(";
		sqlStatement += neueBuchung.getBuchungsNr() + ",";
		sqlStatement += "to_date('"+ neueBuchung.getAnfangsdatum() + "','DD-MM-YY HH24:MI'),";
		sqlStatement += 0 +",";
		sqlStatement += neueBuchung.getGesamtbetrag() +",";
		sqlStatement += neueBuchung.getKuID() +",";
		sqlStatement += neueBuchung.getAnzTage() +",";
		sqlStatement += neueBuchung.getRaumNr() +",";
		sqlStatement += neueBuchung.getStunden() +",";
		sqlStatement += "to_date('"+ neueBuchung.getEnddatum() +"','DD-MM-YY HH24:MI'),";
		sqlStatement += 0 + ",";
		sqlStatement += neueBuchung.getPersonenanzahl() + ")";
		Statements.add(sqlStatement);
		
		ArrayList<Extra> extras = neueBuchung.getExtras();
		for(int i=0; i<extras.size() ;i++) 
		{
			sqlStatement = "INSERT INTO BUEX(bnr, bezeichnung) VALUES(";
			sqlStatement += neueBuchung.getBuchungsNr() + ", ";
			sqlStatement += "'"+ extras.get(i).getBezeichnung() +"')";
			Statements.add(sqlStatement);
		}
		
		try 
		{
			for(int i=0; i<Statements.size(); i++) 
			{
				DatabaseManager.getConnection().insert2(Statements.get(i));
			}
			DatabaseManager.getConnection().commit();
			JOptionPane.showMessageDialog(null, "Buchung angelegt.  Buchungsnummer: "+ neueBuchung.getBuchungsNr()+ "\nGesamtbetrag(vorl�ufig) : "+ df.format(neueBuchung.getGesamtbetrag())+"� \n");
			
		} 
		catch (Exception e) 
		{
			DatabaseManager.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 			
	}	
	
	/**
	 * Holt die Letze Buchungsnummer aus der Datenbank und z�hlt diese um 1 hoch.
	 * @return Die Letze Buchungsnummer aus der DB um 1 hochgez�hlt
	 */
	public int getLastBuchungsNr() 
	{
		String sqlStatement = "SELECT bnr FROM Buchung WHERE ROWNUM <=1 ORDER BY bnr DESC";
		int lastBnr = 0;
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			if(rs.next()) 
			{
				lastBnr = rs.getInt("bnr");
			}
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 
		return lastBnr + 1;
	}
	
	/**
	 * Storniert eine Buchung in der Datenbank (storniert = 1(true)).
	 * @param bnr Die Buchungsnummer der zu stornierenden Buchung
	 */
	public void buchungStornieren(int bnr) 
	{
		String sqlStatement = "UPDATE Buchung SET storniert = 1 WHERE bnr =" +bnr;
		DatabaseManager.getConnection().update(sqlStatement);
	}
	
	/**
	 * Erezugt SQL-Strings zum �ndern einer Buchung in der Datenbank und f�hrt diese aus, gibt Erfolgsmeldung dar�ber aus.
	 * @param buchung Die zu �ndernde Buchung
	 */
	public void buchungAendern(Buchung buchung) 
	{
		ArrayList<String> Statements = new ArrayList<String>();
		int bnr = buchung.getBuchungsNr();
		
		String sqlStatement = "UPDATE Buchung SET " + "gesamtbetrag = " + buchung.getGesamtbetrag() + ", personenanzahl = " + buchung.getPersonenanzahl() + "WHERE bnr = " + bnr;	
		Statements.add(sqlStatement);
		
		sqlStatement = "DELETE FROM BUEX WHERE bnr =" + bnr;				//Erst werden alle Datens�tze mit der bnr aus BUEX gel�scht
		Statements.add(sqlStatement);
		
		ArrayList<Extra> extras = buchung.getExtras();
		for(int i=0; i<extras.size() ;i++) 
		{
			sqlStatement = "INSERT INTO BUEX(bnr, bezeichnung) VALUES(";	//Dann werden die neuen eingef�gt
			sqlStatement += bnr + ", ";
			sqlStatement += "'"+ extras.get(i).getBezeichnung() +"')";
			Statements.add(sqlStatement);
		}
		
		try 
		{
			for(int i=0; i<Statements.size(); i++) 
			{
				DatabaseManager.getConnection().update2(Statements.get(i));
			}
			DatabaseManager.getConnection().commit();
			JOptionPane.showMessageDialog(null, "Buchung ge�ndert.  \nNeuer Gesamtbetrag(vorl�ufig) : "+ df.format(buchung.getGesamtbetrag())+"� \n");

		} 
		catch (Exception e) 
		{
			DatabaseManager.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 					
	}	
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl auf die Datenbank zu und holt sich alle aktuellen und nicht stornierten Buchungen zusammen mit ein paar Kundendaten.
	 * Sie holt sich jeweils aus dem ResultSet die einzelen Attribute und erzeugt eine Buchung, die in einer Liste gespeichert wird.
	 * @author Hendrik Veips
	 * @return ArrayList<Buchung> buchungsliste
	 */
	public ArrayList<Buchung> ladeTeildatenAusBuchungenUndKunde()
	{
		ArrayList<Buchung> buchungsliste = new ArrayList<Buchung>();
		String sqlStatement = "SELECT bnr, datum, rnr, Buchung.kdid, nachname, vorname, mail, anrede, anztage, stunden, gesamtbetrag, personenanzahl, enddatum FROM Kunde, Buchung"
				+ " WHERE Buchung.kdid = Kunde.kdid AND storniert = 0 AND datum >= ANY(SELECT to_char(datum, 'DD-MM-YYYY') datum\r\n" +
				"FROM Buchung\r\n" +
				"WHERE datum >= trunc(CURRENT_DATE)) ORDER BY datum ASC";
		int bnr;
		String datum;
		int kdid;
		int rnr;
		String nachname;
		String vorname;
		String email;
		String anrede;
		int anzTage;
		int stunden;
		String enddatum;
		int personenanzahl;
		double gesamtbetrag;
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			while (rs.next()) 
			{
				bnr = rs.getInt("bnr");
				datum = format1.format(rs.getDate("datum")) + " " + format2.format(rs.getTime("datum"));
				kdid = rs.getInt("kdid");
				rnr = rs.getInt("rnr");
				nachname = rs.getString("nachname");
				vorname = rs.getString("vorname");
				email = rs.getString("mail");
				anrede = rs.getString("anrede");
				anzTage = rs.getInt("anztage");
				stunden = rs.getInt("stunden");
				gesamtbetrag = rs.getDouble("gesamtbetrag");
				personenanzahl = rs.getInt("personenanzahl");
				enddatum = format1.format(rs.getDate("enddatum")) + " " + format2.format(rs.getTime("enddatum"));
				
				Buchung buchung = new Buchung(bnr, datum, anzTage, stunden, rnr, kdid, nachname, vorname, email, anrede, gesamtbetrag, enddatum, personenanzahl);
				buchungsliste.add(buchung);
			}
			
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return buchungsliste;
	}
	
	/**
	 * F�r die Bearbeitung. Greift auf die Datenbank zu um alle unstornierten Buchungen zu laden, deren anfangsdatum noch vor(in der Zukunft) dem aktuellen Systemdatum liegt.
	 * Es werden auch gewissen Daten aus der Kundenrelation mitgeladen f�r die Ansicht in der Tabelle.
	 * @return buchungsliste - Eine ArrayList mit den Buchungen
	 */
	public ArrayList<Buchung> ladeBuchungenZumBearbeiten()
	{
		ArrayList<Buchung> buchungsliste = new ArrayList<Buchung>();
		String sqlStatement = "SELECT bnr, datum, rnr, Buchung.kdid, nachname, vorname, mail, anrede, anztage, stunden, gesamtbetrag, personenanzahl, enddatum FROM Kunde, Buchung "
				+ "WHERE Buchung.kdid = Kunde.kdid AND storniert = 0 AND datum >= trunc(CURRENT_DATE) ORDER BY datum ASC";
		int bnr;
		String datum;
		int kdid;
		int rnr;
		String nachname;
		String vorname;
		String email;
		String anrede;
		int anzTage;
		int stunden;
		String enddatum;
		int personenanzahl;
		double gesamtbetrag;
		Buchung[] buchungen;
		int rows = getNumberOfRows();
		
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			buchungen = new Buchung[rows];
			
			for(int i=0; i<rows; i++) 
			{
				rs.next();
				bnr = rs.getInt("bnr");
				datum = format1.format(rs.getDate("datum")) + " " + format2.format(rs.getTime("datum"));
				kdid = rs.getInt("kdid");
				rnr = rs.getInt("rnr");
				nachname = rs.getString("nachname");
				vorname = rs.getString("vorname");
				email = rs.getString("mail");
				anrede = rs.getString("anrede");
				anzTage = rs.getInt("anztage");
				stunden = rs.getInt("stunden");
				gesamtbetrag = rs.getDouble("gesamtbetrag");
				personenanzahl = rs.getInt("personenanzahl");
				enddatum = format1.format(rs.getDate("enddatum")) + " " + format2.format(rs.getTime("enddatum"));
				
				buchungen[i] = new Buchung(bnr, datum, anzTage, stunden, rnr, kdid, nachname, vorname, email, anrede, gesamtbetrag, enddatum, personenanzahl);
			}
			for(int j=0; j<rows; j++) 
			{
				bnr = buchungen[j].getBuchungsNr();
				ArrayList<Extra> extras = ladeExtras(bnr);
				buchungen[j].setExtras(extras);
				buchungsliste.add(buchungen[j]);
			}		
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return buchungsliste;
	}
	
	/**
	 * F�r die Rechnungserstellung. Greift auf die Datenbank zu um alle unstornierten Buchungen, zu denen noch 
	 * keine Rechnung verschickt wurde, zu laden, deren enddatum bereits vergangen ist(relativ zur Systemzeit).
	 * Es werden auch gewissen Daten aus der Kundenrelation mitgeladen f�r die Ansicht in der Tabelle.
	 * @return buchungsliste - Eine ArrayList mit den Buchungen
	 */
	public ArrayList<Buchung> ladeBuchungenZumRechnungErstellen()
	{
		ArrayList<Buchung> buchungsliste = new ArrayList<Buchung>();
		String sqlStatement = "SELECT bnr, datum, rnr, Buchung.kdid, nachname, vorname, mail, anrede, anztage, stunden, gesamtbetrag, personenanzahl, enddatum FROM Kunde, Buchung "
				+ "WHERE Buchung.kdid = Kunde.kdid AND storniert = 0 AND verschickt = 0 AND enddatum  < trunc(CURRENT_DATE) ORDER BY datum ASC";
		int bnr;
		String datum;
		int kdid;
		int rnr;
		String nachname;
		String vorname;
		String email;
		String anrede;
		int anzTage;
		int stunden;
		String enddatum;
		int personenanzahl;
		double gesamtbetrag;
		
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			while(rs.next()) 
			{
				bnr = rs.getInt("bnr");
				datum = format1.format(rs.getDate("datum")) + " " + format2.format(rs.getTime("datum"));
				kdid = rs.getInt("kdid");
				rnr = rs.getInt("rnr");
				nachname = rs.getString("nachname");
				vorname = rs.getString("vorname");
				email = rs.getString("mail");
				anrede = rs.getString("anrede");
				anzTage = rs.getInt("anztage");
				stunden = rs.getInt("stunden");
				gesamtbetrag = rs.getDouble("gesamtbetrag");
				personenanzahl = rs.getInt("personenanzahl");
				enddatum = format1.format(rs.getDate("enddatum")) + " " + format2.format(rs.getTime("enddatum"));
				
				buchungsliste.add(new Buchung(bnr, datum, anzTage, stunden, rnr, kdid, nachname, vorname, email, anrede, gesamtbetrag, enddatum, personenanzahl));
			}
				
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return buchungsliste;
	}
	
	/**
	 * Z�hlt die Anzahl an Datens�tzen f�r die LadeBuchungenZumBearbeiten Methode.
	 * @return Die Anzahl der Datens�tze
	 */
	private int getNumberOfRows() 
	{
		int anzahl = 0;
		String sqlStatement = "SELECT COUNT(*) from buchung, kunde where Buchung.kdid = kunde.kdid and storniert = 0 and datum >= trunc(Current_date) order by datum asc";
		ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
		try 
		{
			rs.next();
			anzahl = rs.getInt(1);
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return anzahl;
	}
	
	/**
	 * Berechnet den Gesamtbetrag nach Rabatt und erstellt eine Rechnung in der Datenbank mit der Buchungsnummer als Rechnungsnummer.
	 * Setzt verschickt auf 1(true) bei der Buchung. Gibt Erfolgsmeldung mit Gesamtbetrag und Rechnungsnummer aus.
	 * @param buchung Die Buchung f�r die die Rechnung erstellt wird
	 * @param rabatt Der Rabatt als Integer zwischen 0 und 100, abgezogen von 100
	 */
	public void erstelleRechnung(Buchung buchung, int rabatt) {
		String sqlStatement_1 ="UPDATE Buchung SET verschickt = 1 WHERE bnr =" + buchung.getBuchungsNr();
		String sqlStatement_2 ="INSERT INTO RECHNUNG (recnr, datum, gesamtbetrag, rabatt, kdid, enddatum) VALUES(";
		double rabattfraction = (double)rabatt/100;
		sqlStatement_2 += buchung.getBuchungsNr() + ",";
		sqlStatement_2 += "to_date('"+ buchung.getAnfangsdatum() + "','DD-MM-YY HH24:MI'),";
		sqlStatement_2 += buchung.getGesamtbetrag()*rabattfraction + ",";
		sqlStatement_2 += 100-rabatt + ",";
		sqlStatement_2 += buchung.getKuID() + ",";
		sqlStatement_2 += "to_date('"+ buchung.getEnddatum() +"','DD-MM-YY HH24:MI'))";
		
		try 
		{
			DatabaseManager.getConnection().insert2(sqlStatement_1);
			DatabaseManager.getConnection().insert2(sqlStatement_2);
			JOptionPane.showMessageDialog(null, "Rechnung verschickt.  Rechnungsnummer: "+ buchung.getBuchungsNr()+ "\nGesamtbetrag nach Rabatt : "+ df.format(buchung.getGesamtbetrag()*rabattfraction)+"� \n");
		}
		catch (SQLException e)
		{
			DatabaseManager.getConnection().rollback();
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
	}
		
	/**
	 * L�dt die Extras einer Buchung aus der Realtion BUEX in der Datenbank.
	 * @param bnr Die Buchungsnummer der Buchung
	 * @return Eine ArrayList mit den Extras
	 * @throws SQLException
	 */
	private ArrayList<Extra> ladeExtras(int bnr) throws SQLException 
	{
		ArrayList<Extra> extras = new ArrayList<Extra>();
		String sqlSubStatement = "SELECT bezeichnung FROM BUEX WHERE bnr =" + bnr;
		
		ResultSet subRs = DatabaseManager.getConnection().select(sqlSubStatement);
		while(subRs.next()) 
		{	
			extras.add(new Extra(subRs.getString("bezeichnung")));
		}
		subRs.close();
		return extras;
	}	
	
	/**
	 * L�dt die Extras aus der Datenbank mit Bezeichnung als Key und Preis als Value in eine HashMap.
	 * @return Die Hashmap mit den Bezeichnungen und Preisen.
	 */
	public HashMap<String,Double> ladeExtraPreise() {
		HashMap<String,Double> preise = new HashMap<String,Double>();
		String sqlStatement = "SELECT * FROM Extra";
		double preis = 0;
		String bezeichnung = "";
		
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			while (rs.next()) 
			{
				bezeichnung = rs.getString("bezeichnung");
				preis = rs.getDouble("preis_pro_person");
				preise.put(bezeichnung, preis);
			}
		}catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return preise;
	}
	
	/**
	 * L�dt den Stundenpreis einer Raumkategorie aus der Datenbank.
	 * @param rnr Die Raumnummer des Raumes von welchem die Kategorie gefragt ist
	 * @return Den Stundenpreis
	 */
	public int getRaumPreis_h(int rnr) {
		int preis_h = 0;
		String sqlStatement = "SELECT preis_h FROM kategorie, raum WHERE raum.k�rzel = kategorie.k�rzel and rnr=" +rnr;
		ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
		
		try {
			if(rs.next()) {
				preis_h = rs.getInt("preis_h");
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		
		return preis_h;
	}
	
	/**
	 * L�dt den Tagespreis einer Raumkategorie aus der Datenbank.
	 * @param rnr Die Raumnummer des Raumes von welchem die Kategorie gefragt ist
	 * @return Den Tagespreis
	 */
	public int getRaumTagespreis(int rnr) {
		int tagespreis = 0;
		String sqlStatement = "SELECT tagespreis FROM kategorie, raum WHERE raum.k�rzel = kategorie.k�rzel and rnr=" +rnr;
		ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
		
		try {
			if(rs.next()) {
				tagespreis = rs.getInt("tagespreis");
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		
		return tagespreis;
	}
}