package controller.tablesearchcontroller;

/**
 * @author Niklas Nebeling
 */

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * Ein Interface f�r Gui Klassen, welche Tabellen beinhalten.
 * @author Niklas
 *
 */
public interface ITabellenContainer 
{
	JTextField getCueInput();
	JTable getTable();
	TableRowSorter<TableModel> getTableRowSorter();
}