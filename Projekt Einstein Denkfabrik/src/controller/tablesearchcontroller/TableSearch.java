package controller.tablesearchcontroller;

/**
 * @author Alexander Stavski
 * Diese Klasse erm�glicht eine dynamische Suche und Sortierung in Tabellen
 */

import java.awt.Color;
import java.util.regex.PatternSyntaxException;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class TableSearch implements DocumentListener 
{

	private ITabellenContainer gui;

	public TableSearch(ITabellenContainer gui) 
	{
		this.gui = gui;
	}

	/**
	 * Wird aufgerufen, wenn sich der Stil des Textes im Dokument �ndert
	 * z.B. ein Attribut oder eine Menge von Attributen
	 */
	@Override
	public void changedUpdate(DocumentEvent e) 
	{
		aktualisiereTabelle();
	}

	/**
	 * Wird aufgerufen, wenn ein Text in das Dokument hinzugef�gt wird
	 */
	@Override
	public void insertUpdate(DocumentEvent e) 
	{
		aktualisiereTabelle();
	}

	/**
	 * Wird aufgerufen, wenn der Text aus dem Dokument entfernt wird
	 */
	@Override
	public void removeUpdate(DocumentEvent e) 
	{
		aktualisiereTabelle();
	}

	/**
	 * erm�glicht eine Java-interne Tabellensuche/Filterung
	 */
	public void aktualisiereTabelle() 
	{
		String text = gui.getCueInput().getText();

		gui.getTable().clearSelection();
		if (text != null && text.length() > 0)
		{
			try 
			{
				gui.getTableRowSorter().setRowFilter(RowFilter.regexFilter("(?i)" + text)); // "(?i)" -> f�r lowercase
			} 
			catch (PatternSyntaxException pe) 
			{
				gui.getCueInput().setForeground(Color.BLUE);
			}
		}
		else
		{
			gui.getTableRowSorter().setRowFilter(null);
		}
	}
}