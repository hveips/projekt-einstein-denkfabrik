package controller;

/**
 * @author Hendrik Veips
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class DatabaseManager 
{
	private static DatabaseManager instance;
	
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	
	private String url = "jdbc:oracle:thin:@aix1.fh-bielefeld.de:1521:d2";
	
	private boolean result = false;
	
	/**
	 * Der Konstruktor erzeugt des DatabaseManager als Singleton.
	 */
	private DatabaseManager()
	{
		String user = "dvi739";
		String password = "fh6378";
		connectToServer(user, password);
		createStatement();
	}
	
	/**
	 * Die Methode erzeugt den DatabaseManager einmalig.
	 * @return instance DatabaseManager
	 */
	public static DatabaseManager getConnection()
	{
		try
		{
			if(instance == null)
				instance = new DatabaseManager();
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return instance;
		
	}
	
	/**
	 * Die Methode baut mit den Parametern user und password ein Verbindung zur Datenbank auf.
	 * @param user
	 * @param password
	 */
	public void connectToServer(String user, String password)
	{
		try
		{
			con = DriverManager.getConnection(url, user, password);
		}
		catch(SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
	}
	
	/**
	 * Die Methode erzeugt ein Statement auf einer bestehenden Verbindung.
	 */
	public void createStatement()
	{
		try
		{
			stmt = con.createStatement();
		}
		catch(SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
	}
	
	/**
	 * Die Methode f�hrt einen SELECT-Befehl �ber den Parameter query auf der Datenbank aus und gibt ein ResultSet zur�ck.
	 * @param query
	 * @return ResulSet rs
	 */
	public ResultSet select(String query)
	{
		try
		{
			DatabaseManager.getConnection();
			rs = stmt.executeQuery(query);
		}
		catch(SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return rs;
	}
	
	/**
	 * Die Methode f�hrt einen SELECT-Befehl �ber den Parameter query auf der Datenbak auf und gibt die Anzahl der Datens�tze zur�ck.
	 * @param query
	 * @return int a
	 */
	public int select2(String query)
	{
		int a = 0;
		try
		{
			DatabaseManager.getConnection();
			a = stmt.executeUpdate(query);
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return a;
	}
	
	/**
	 * Die Methode f�hrt einen INSERT-Befehl �ber den Parameter query auf der Datenbank aus und gibt zur�ck, ob dies erfolgreich war.
	 * @param query
	 * @return boolean result
	 */
	public boolean insert(String query)
	{
		try
		{
			DatabaseManager.getConnection();
			result = stmt.execute(query);  
		}
		catch(SQLException e)
		{
			//JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Die Methode f�hrt einen INSERT-Befehl �ber den Parameter query auf der Datenbank aus und gibt zur�ck, ob dies erfolgreich war.
	 * Sie gibt die Exception an die ausf�hrende Klasse weiter.
	 * @param query
	 * @return boolean result
	 * @throws SQLException
	 */
	public boolean insert2(String query) throws SQLException
	{
		DatabaseManager.getConnection();
		result = stmt.execute(query);
		return result;
	}
	
	/**
	 * Die Methode f�hrt einen UPDATE-Befehl �ber den Parameter query auf der Datenbank aus und gibt zur�ck, ob dies erfolgreich war.
	 * @param query
	 * @return boolean result
	 */
	public boolean update(String query)
	{
		try
		{
			DatabaseManager.getConnection();
			result = stmt.execute(query);
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Die Methode f�hrt einen UPDATE-Befehl �ber den Parameter query auf der Datenbank aus.
	 * Sie gibt die Exception an die ausf�hrende Klasse weiter.
	 * @param query
	 * @throws SQLException
	 */
	public void update2(String query) throws SQLException 
	{
		result = stmt.execute(query);
	}
	
	/**
	 * Die Methode f�hrt einen DELETE-Befehl �ber den Parameter query auf der Datenbank aus und gibt zur�ck, ob dies erfolgreich war.
	 * @param query
	 * @return boolean result
	 */
	public boolean delete(String query)
	{
		try
		{
			DatabaseManager.getConnection();
			result = stmt.execute(query);
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Die Methode schlie�t die bestehende Connection.
	 */
	public void closeAllConnection()
	{
		try
		{
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			if (con != null)
				con.close();
		}
		catch(SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
	}
	
	/**
	 * Die Methode f�hrt den COMMIT-Befehl auf der Datenbank aus.
	 */
	public void commit()
	{
		try
		{
			con.commit();
		}
		catch(SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
	}
	
	/**
	 * Die Methode f�hrt den ROLLBACK-Befehl auf der Datenbank aus.
	 */
	public void rollback()
	{
		try
		{
			con.rollback();
		}
		catch(SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
	}
}