package controller.documentcontroller;

/**
 * Dieses Dokument wird einem Textfeld angeh�ngt und akzeptiert nur 
 * eine gewisse Menge der Ziffern von 0-9 oder Ketten von diesen.
 * @author Niklas Nebeling
 */

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class IntegerOnlyDocument extends PlainDocument
{
	//Die maximale Anzahl an Ziffern die eingegeben werden kann
	private int limit;

	/**
	 * Der Konstruktor erzeugt das IntegerOnlyDocument mit dem Parameter limit.
	 * @param limit Maximale L�nge
	 */
	public IntegerOnlyDocument(int limit) 
	{
		this.limit = limit;
	}
	
	/**
	 * Die Methode die automatisch aufgerufen wird wenn etwas in das Document(eines Textfeldes) eingegeben oder eingef�gt wird.
	 * Wenn das limit nach dem Input noch nicht �berschritten w�re und es eine Ziffer ist wird der Input akzeptiert.
	 * @param str ist das einzuf�gende Zeichen oder die Zeichenkette
	 * @param offset ist die Position des Einf�gezeichens im String des Documents
	 */
	public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException 
	{
		String string = "";
		if (str == null) return;
		if(str.length() > 1)         //Wenn der input String l�nger als 1 zeichen ist (z.B. bei copy und paste, oder .setText )	
		{
			//Der string wird mit der �u�eren For-Schleife char f�r char durchgegangen und es wird f�r jeden char gepr�ft ob es eine Ziffer ist		
			for (int j=0; j<str.length(); j++)
			{
				//getLength() = aktuelle L�nge des Strings im Document(im Textfeld)
				if (getLength() < limit) 
				{
					//Es wird durch die Innere For-Schleife jedes Zeichen mit jeder Ziffer von 0-9 verglichen
					for(int i=0; i<10; i++) 
					{	
						if(Character.toString(str.charAt(j)).equals(Integer.toString(i))) 		//Character.toString(str.charAt(j)) -> Erkl�rung: die charAt Methode liefert einen char, wir ben�tigen aber einen String, deswegen Character.toString		
						{						
							string += str.charAt(j);
						}
					}
					
				}
			}
			super.insertString(offset, string, attr);
		}
		else 
		{
			if (getLength() < limit) 
			{
				//Mit der For-Schleife wird das Zeichen mit jeder Ziffer von 0-9 verglichen
				for(int i=0; i<10; i++) 
				{
					if(str.equals(Integer.toString(i))) 
					{
						super.insertString(offset, str, attr);
					}
				}	
			}
		}
	}
}