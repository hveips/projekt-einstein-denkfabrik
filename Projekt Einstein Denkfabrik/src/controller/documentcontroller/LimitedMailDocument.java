package controller.documentcontroller;

/**
 * @author Hendrik Veips
 */

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class LimitedMailDocument extends PlainDocument
{
	private int limit;

	/**
	 * Der Konstruktor erzeugt das StringLittleOnlyDocument mit dem Parameter limit.
	 * @param limit
	 */
	public LimitedMailDocument(int limit) 
	{
		this.limit = limit;
	}
	
	/**
	 * Die Methode pr�ft welche Zeichen eingegeben werden d�rfen und ob die Anzahl der Zeichen stimmt.
	 */
	public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException 
	{
		if (str == null) 
		{
			return;
		}
		int actualLength = this.getLength()-1;
		if (actualLength + str.length() < this.limit) 
		{
			super.insertString(offset, str.replaceAll("[^a-z|0-9|@|.|,|;|:|!|#|$|%|&|'|*|+|/|=|?|^|_|`|{|}|~|(|)|<|>|����������� ������� �� �|�� �� �� � ]", ""), attr);
		}
	}
}