package controller.documentcontroller;

/**
 * @author Alexander Stavski
 * Diese Klasse sorgt daf�r, dass in Textfeldern nur ausgew�hlte Strings
 * mit einem gesetzten Zeichen-Limit gesetzt werden k�nnen
 */

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class StringOnlyDocument extends PlainDocument 
{
	private int limit;

	public StringOnlyDocument(int limit) 
	{
		this.limit = limit;
	}
	
	/**
	 * Hier werden die erlaubten Zeichen festgelegt und das Limit (Anzahl) an Zeichen �berpr�ft
	 */
	public void insertString(int offset, String str, javax.swing.text.AttributeSet attr) throws BadLocationException 
	{
		if (str == null) 
		{
			return;
		}
		int actualLength = this.getLength()-1;
		if (actualLength + str.length() < this.limit) 
		{
			super.insertString(offset, str.replaceAll("[^a-z|^�A-Z|����������� ������� �� �|�� �� �� � ]", ""), attr);
		}
	}
}