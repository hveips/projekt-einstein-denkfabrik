package controller.documentcontroller;

/**
 * Dieses Dokument wird einem Textfeld angeh�ngt und akzeptiert nur 
 * eine gewisse Menge an Zeichen.
 * @author Niklas Nebeling
 */

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class LimitedDocument extends PlainDocument 
{
	//Die maximale Anzahl an Zeichen die eingegeben werden kann
	private int limit;

	/**
	 * Der Konstruktor erzeugt das LimitedDocument mit dem Parameter limit.
	 * @param limit Maximale L�nge
	 */
	public LimitedDocument(int limit) 
	{
		this.limit = limit;
	}

	/**
	 * Die Methode die automatisch aufgerufen wird wenn etwas in das Document(eines Textfeldes) eingegeben oder eingef�gt wird.
	 * Wenn das limit nach dem Input noch nicht �berschritten w�re, wird der Input akzeptiert.
	 * @param str ist das einzuf�gende Zeichen oder die Zeichenkette
	 * @param offset ist die Position des Einf�gezeichens im String des Documents
	 */
	public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException 
	{
		if (str == null)
		{
			return;			
		}
		//getLength() = aktuelle L�nge des Strings im Document(im Textfeld)
		if ((getLength() + str.length()) <= limit) 
		{
			super.insertString(offset, str, attr);
		}
	}
}