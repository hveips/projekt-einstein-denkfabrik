package controller;

/**
 * @author Hendrik Veips
 */

import java.text.DecimalFormat;

public class DatumController 
{
	private boolean schaltjahr;
	
	private DecimalFormat format3 = new DecimalFormat("00");
	
	/**
	 * Die Methode berechnet den n�chsten Tag �ber die Parameter day, month und year und beachtet dabei in welchen Monat man sich befindet und ob ein Schaltjahr vorliegt.
	 * @param day
	 * @param month
	 * @param year
	 * @return String tag
	 */
	public String getNaechstenTag(int day, int month, int year)
	{
		String tag;
		if(year % 4 == 0 && year % 400 == 0)
		{
			schaltjahr = true;
		}
		else if(year % 4 == 0 && year % 100 != 0)
		{
			schaltjahr = false;
		}
		else
		{
			schaltjahr = false;
		}
		
		if((day + 1 == 32) || (day + 1 == 31 && (month == 4 || month == 6 || month == 9 || month == 11)) || (day + 1 == 30 && month == 2) || (day + 1 == 29 && month == 2 && schaltjahr == false))
		{
			tag = format3.format(1);
		}
		else
		{
			day = day + 1;
			tag = format3.format(day);
		}
		return tag;
	}
	
	/**
	 * Die Methode berechnet �ber die Parameter den n�chsten Monat und beachtet dabei auch den m�glichen Jahrewechsel.
	 * @param day
	 * @param month
	 * @return String monat
	 */
	public String getNaechstenMonat(String day, int month)
	{
		String monat;
		if(day.equals("01") && month < 12)
		{
			month = month + 1;
			monat = format3.format(month);
		}
		else if(day.equals("01") && month == 12)
		{
			month = month - 11;
			monat = format3.format(month);
		}
		else
		{
			monat = format3.format(month);
		}
		return monat;
	}

	/**
	 * Die Methode berechnet �ber die Parameter day, month und year das n�chste Jahr.
	 * @param day
	 * @param month
	 * @param year
	 * @return String jahr
	 */
	public String getNaechstesJahr(int day, int month, int year)
	{
		String jahr;
		if(day == 1 && month == 1)
		{
			year = year + 1;
			jahr = "" + year;
		}
		else
		{
			jahr = "" + year;
		}
		return jahr;
	}
}