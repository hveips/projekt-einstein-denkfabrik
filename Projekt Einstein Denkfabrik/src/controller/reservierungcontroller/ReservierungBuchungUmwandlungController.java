package controller.reservierungcontroller;

/**
 * @author Hendrik Veips
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;
import controller.buchungcontroller.BuchungController;
import model.Extra;
import model.Reservierung;

public class ReservierungBuchungUmwandlungController 
{
	private DatabaseManager db;
	private BuchungController buchungController = new BuchungController();
	
	/**
	 * Die Methode greift �ber den SELECT-Befehl mit dem Parameter resnr auf die Datenbank zu und holt sich f�r jede Reservierung die Extras,
	 * die erzeugt werden, in einer Liste gespeichert und zur�ckgegeben werden.
	 * @param resnr
	 * @return ArrayList<Extra> extras
	 */
	public ArrayList<Extra> ladeExtrasAusReservierung(int resnr)
	{
		ArrayList<Extra> extras = new ArrayList<Extra>();
		String sqlStatement = "SELECT bezeichnung FROM RESEX WHERE resnr = " + resnr;
		try
		{
			ResultSet rs = db.getConnection().select(sqlStatement);
			while(rs.next())
			{
				Extra extra = new Extra(rs.getString("bezeichnung"));
				extras.add(extra);
			}
		}
		catch(SQLException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler: " + e.getMessage());
		}
		return extras;
	}
	
	/**
	 * Die Methode greift �ber den INSERT-BEFEHL mit den Attributen aus dem Parameter reservierung auf die Datenbank zu
	 * und speichert so eine Reservierung als Buchung.
	 * @param reservierung
	 */
	public void wandleReservierungInBuchung(Reservierung reservierung)
	{
		ArrayList<String> statements = new ArrayList<String>();
		int bnr = buchungController.getLastBuchungsNr();
		String sqlStatement = "INSERT INTO BUCHUNG(bnr, datum, storniert, gesamtbetrag, kdid, anztage, rnr, stunden, enddatum, verschickt, personenanzahl) VALUES(";
		sqlStatement += bnr + ",";
		sqlStatement += "to_date('" + reservierung.getAnfangsdatum() + "','DD-MM-YY HH24:MI:SS'),";
		sqlStatement += 0 + ",";
		sqlStatement += reservierung.getGesamtbetrag() + ",";
		sqlStatement += reservierung.getKdID() + ",";
		sqlStatement += reservierung.getAnzTage() + ",";
		sqlStatement += reservierung.getRaumNr() + ",";
		sqlStatement += reservierung.getStunden() + ",";
		sqlStatement += "to_date('" + reservierung.getEnddatum() + "','DD-MM-YY HH24:MI:SS'),";
		sqlStatement += 0 + ",";
		sqlStatement += reservierung.getPersonenanzahl() + ")";
		statements.add(sqlStatement);
		
		ArrayList<Extra> extras = reservierung.getExtras();
		for(int i = 0;i < extras.size();i++)
		{
			String sqlStatement2 = "INSERT INTO BUEX(bnr, bezeichnung) VALUES(";
			sqlStatement2 += bnr + ",";
			sqlStatement2 += "'" + extras.get(i).getBezeichnung() + "')";
			statements.add(sqlStatement2);
		}
		
		try
		{
			for(int i = 0;i < statements.size();i++)
			{
				db.getConnection().insert(statements.get(i));
			}
			db.getConnection().commit();
		}
		catch(Exception e)
		{
			db.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
}