package controller.reservierungcontroller;

/**
 * @author Niklas Nebeling, Alexander Stavski
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import controller.DatabaseManager;
import model.Extra;
import model.Reservierung;

public class ReservierungController 
{	
	private SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
	private SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
	DecimalFormat df = new DecimalFormat(".00");
	
	/**
	 * Diese Methode erzeugt SQL-Strings zur Erzeugung eines neuen Reservierungsobjektes 
	 * in der Datenbank und f�hrt diese dann aus, gibt Erfolgsmeldung dar�ber aus.
	 * @param neueReservierung Die hinzuzuf�gende Reservierung
	 */
	public void reservierungHinzufuegen(Reservierung neueReservierung) 
	{
		ArrayList<String> Statements = new ArrayList<String>();
		String sqlStatement = "INSERT INTO Reservierung(resnr, datum, gesamtbetrag, kdid, anztage, rnr, stunden, enddatum, personenanzahl) VALUES(";
		sqlStatement += neueReservierung.getResNr() + ",";
		sqlStatement += "to_date('"+ neueReservierung.getAnfangsdatum() + "','DD-MM-YY HH24:MI'),";
		sqlStatement += neueReservierung.getGesamtbetrag() +",";
		sqlStatement += neueReservierung.getKdID() +",";
		sqlStatement += neueReservierung.getAnzTage() +",";
		sqlStatement += neueReservierung.getRaumNr() +",";
		sqlStatement += neueReservierung.getStunden() +",";
		sqlStatement += "to_date('"+ neueReservierung.getEnddatum() +"','DD-MM-YY HH24:MI'),"; 
		sqlStatement += neueReservierung.getPersonenanzahl() + ")";
		Statements.add(sqlStatement);
		
		ArrayList<Extra> extras = neueReservierung.getExtras();
		for(int i=0; i<extras.size(); i++) 
		{
			sqlStatement = "INSERT INTO RESEX(resnr, bezeichnung) VALUES(";
			sqlStatement += neueReservierung.getResNr() + ", ";
			sqlStatement += "'"+ extras.get(i).getBezeichnung() +"')";
			Statements.add(sqlStatement);
		}
		
		try 
		{
			for(int i=0; i<Statements.size(); i++) 
			{
				DatabaseManager.getConnection().insert2(Statements.get(i));
			}
			DatabaseManager.getConnection().commit();
			JOptionPane.showMessageDialog(null, "Reservierung angelegt.  Reservierungsnummer: "+ neueReservierung.getResNr()+ "\nGesamtbetrag(vorl�ufig) : "+ df.format(neueReservierung.getGesamtbetrag())+"� \n");
			
		} 
		catch (Exception e) 
		{
			DatabaseManager.getConnection().rollback();
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}				
	}
	
	/**
	 * Holt die Letze Reservierungsnummer aus der Datenbank und z�hlt diese um 1 hoch.
	 * @return Die Letze Reservierungsnummer aus der DB um 1 hochgez�hlt
	 */
	public int getLastReservierungsNr() 
	{
		String sqlStatement = "SELECT resnr FROM Reservierung WHERE ROWNUM <=1 ORDER BY resnr DESC";
		int lastResnr = 0;
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			if(rs.next()) 
			{
				lastResnr = rs.getInt("resnr");
			}
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		} 
		return lastResnr+1;
	}
	
	/**
	 * L�scht eine Reservierung aus der Datenbank.
	 * @param resnr Die Reservierungsnummer der zu l�schenden Reservierung.
	 */
	public void reservierungStornieren(int resnr)  
	{
		try
		{
			String sqlStatement2 = "DELETE FROM RESEX WHERE resnr=" + resnr;
			DatabaseManager.getConnection().update(sqlStatement2);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			DatabaseManager.getConnection().rollback();
		}
		try
		{
			String sqlStatement = "DELETE FROM Reservierung WHERE resnr=" +resnr;
			DatabaseManager.getConnection().update(sqlStatement);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			DatabaseManager.getConnection().rollback();
		}
	}
	
	/**
	 * Es werden nur bestimmte Datens�tze aus Kunde und Reservierung geladen
	 * @author Alexander Stavski
	 */
	public ArrayList<Reservierung> ladeTeildatenAusReservierungenUndKunde()
	{
		ArrayList<Reservierung> reservierungsliste = new ArrayList<Reservierung>();
		String sqlStatement = "SELECT resnr, datum, rnr, Reservierung.kdid, nachname, vorname, mail, anztage, enddatum, gesamtbetrag, personenanzahl from Kunde, Reservierung"
				+ " WHERE Reservierung.kdid = Kunde.kdid AND datum >= ANY(SELECT to_char(datum, 'DD-MM-YYYY') datum\r\n" + 
				"FROM Reservierung\r\n" + 
				"WHERE datum >= trunc(CURRENT_DATE)) ORDER BY datum ASC";
		
//		Alle Reservierungen vergangene + in Zukunft
//		String sqlStatement = "SELECT resnr, datum, rnr, Reservierung.kdid, nachname, vorname, mail, anztage, enddatum from Kunde, Reservierung"
//				+ " WHERE Reservierung.kdid = Kunde.kdid ORDER BY datum ASC";
		
		int resnr;
		String datum;
		int kdid;
		int rnr;
		String nachname;
		String vorname;
		String email;
		int anzTage;
		String enddatum;
		double gesamtbetrag;
		int personenanzahl;
		try 
		{
			ResultSet rs = DatabaseManager.getConnection().select(sqlStatement);
			while (rs.next()) 
			{
				resnr = rs.getInt("resnr");
				datum = format1.format(rs.getDate("datum")) + " " + format2.format(rs.getTime("datum"));
				kdid = rs.getInt("kdid");
				rnr = rs.getInt("rnr");
				nachname = rs.getString("nachname");
				vorname = rs.getString("vorname");
				email = rs.getString("mail");
				anzTage = rs.getInt("anztage");
				enddatum = format1.format(rs.getDate("enddatum")) + " " + format2.format(rs.getTime("enddatum"));
				gesamtbetrag = rs.getDouble("gesamtbetrag");
				personenanzahl = rs.getInt("personenanzahl");
				
				Reservierung reservierung = new Reservierung(resnr, datum, rnr, kdid, nachname, vorname, email, anzTage, enddatum, personenanzahl, gesamtbetrag);
				reservierungsliste.add(reservierung);
			}
			
		} 
		catch (SQLException e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
		return reservierungsliste;
	}
}
