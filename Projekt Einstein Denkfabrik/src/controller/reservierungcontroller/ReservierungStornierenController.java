package controller.reservierungcontroller;

/**
 * @author Alexander Stavski
 */

import javax.swing.JOptionPane;

import controller.MailController;
import view.buchung_reservierungview.ReservierungStornierenGUI;

public class ReservierungStornierenController 
{
	private MailController mailController;
	private ReservierungController op;
	private ReservierungStornierenGUI gui;
	
	public ReservierungStornierenController(ReservierungStornierenGUI gui)
	{
		this.gui = gui;
		this.op = new ReservierungController();
		this.mailController = new MailController();
	}
	
	/**
	 * Methode zum l�schen/stornieren von Reservierungen
	 */
	public void loeschenButtonAction() 
	{
		int selectedRow = gui.getTable().getSelectedRow();
		if (selectedRow == -1) 
		{
			JOptionPane.showMessageDialog(gui, "Keine Reservierung ausgew�hlt.", "Fehler", JOptionPane.ERROR_MESSAGE);
		} 
		else 
		{
			int option = JOptionPane.showConfirmDialog(gui, "Sind Sie sicher, dass Sie die Reservierung von " + gui.getReservierungRow().getVorname() 
					+ " " + gui.getReservierungRow().getName() + " stornieren wollen?", "Stornierung best�tigen", JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) 
			{
				try 
				{
					mailController.sendStornierung(gui.getEmailRow(), gui.getResnrRow(), gui.getNameRow());
					op.reservierungStornieren(gui.getResnrRow());
					gui.getDatabaseUpdated();
				} 
				catch (Exception ex) 
				{
					JOptionPane.showMessageDialog(null, "Fehler:" + ex.getMessage());
				}
			}
		}
	}
}