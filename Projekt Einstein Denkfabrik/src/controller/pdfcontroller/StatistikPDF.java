package controller.pdfcontroller;  

import java.awt.Font;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import controller.DatabaseManager;

/**
 * Diese Klasse generiert eine PDF die, das Buchungs- und Umsatzverhalten eines Kunden zeigt.
 * @author Marvin
 * 
 */
public class StatistikPDF 
{
	// Variablen um ausgew�hlten Kunden zu speichern
	private int kuid;
	private String anrede;
	private String name;
	private String vorname;
	private int plz;
	private String hausNr;
	private String strasse;
	private String ort;
	private String filename;
	
	// Variablen f�r DB Abfragen
		private String summe = "0";
		private int anzahl =0;
		private double anzahlStorno = 0;
		private double anzahlGesamt = 0; 
		private double anzahlOffen = 0;
		
		private DecimalFormat df = new DecimalFormat(".00");
		
		
		// Variable um mit DatabaseManager zu kommunizieren
		DatabaseManager conn;
		
		// Schriftarten der PDF
		private com.itextpdf.text.Font font1 = FontFactory.getFont("Arial", BaseFont.IDENTITY_H, 10, Font.PLAIN);
		private com.itextpdf.text.Font font3 = FontFactory.getFont("Arial", BaseFont.IDENTITY_H, 12, Font.PLAIN);
		private com.itextpdf.text.Font font2 = FontFactory.getFont("Arial", BaseFont.IDENTITY_H, 14, Font.BOLD);
		
		/**
		 * Setzt Einstellungen der PDF Gr��e, Inhalt usw.
		 * Setzen der DB Ergebnisse in erstellte Tabelle der PDF
		 *  Die fertige Datei wird im Projektorder in Kundenstatistiken\ gespeichert.
		 */
		public void createPdf(String filename) throws DocumentException, IOException, MalformedURLException
		{
			
			//Dokument wird erzeugt
			Document document = new Document(PageSize.A4, 40, 20, 0, 0);
			
			//Pr�fen ob Ordner vorhanden ist und wenn nicht erzeugen
			Path ordner = Paths.get("Kundenstatistiken");
			if(!Files.exists(ordner))
			{	
				Files.createDirectory(ordner);
			}
			
			//lokale Ausgabe als PDF
			PdfWriter.getInstance(document, new FileOutputStream("Kundenstatistiken\\" + filename + ".pdf"));
			document.open();
			
			// Erstellen des "Layout"
			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(100);
			table.setSpacingBefore(150f);
			table.setWidths(new int[] { 2, 1, 2});
			
			// Daten des Autors/Firma
			PdfPCell cellData = new PdfPCell(new Phrase("Firma\nEinstein Denkfabrik\nInteraktion 1\nD 33619 Bielefeld\n", font1) );
			cellData.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
			cellData.setBorder(0);
			table.addCell(cellData);
			
			// �berschrift der PDF
			PdfPCell cellHeadline = new PdfPCell(new Phrase("Statistik", font2));
			cellHeadline.setBorder(0);
			cellHeadline.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
			cellHeadline.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			table.addCell(cellHeadline);
			
			// Leere Zelle zur F�llung 
			PdfPCell cellText = new PdfPCell();
			cellText.setBorder(0);
			cellText.setHorizontalAlignment(PdfPCell.RIGHT);
			table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellText);
			document.add(table);
			
			//Datumsangabe + Linie
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			StringBuilder dateBuilder = new StringBuilder(dateFormat.format(date));
			Paragraph rightDate = new Paragraph();
			rightDate.setAlignment(Element.ALIGN_RIGHT);
			rightDate.setFont(font1);
			rightDate.add("" + dateBuilder);
			document.add(new LineSeparator());
			
			document.add(rightDate);
			document.add(new Paragraph(""));
			
			//Tabelle f�r Kundendaten
			PdfPTable adressTable = new PdfPTable(2);
			adressTable.setHorizontalAlignment(PdfPTable.ALIGN_RIGHT);
			adressTable.setSpacingBefore(40f);
			adressTable.setWidthPercentage(96f);
			cellData = new PdfPCell(new Phrase(vorname + " " + name + "\n" + strasse + " " + hausNr + "\n" + plz + " " + ort + "\n", font2));
			cellData.setBorder(0);
			adressTable.addCell(cellData);
			
			// Daten die an der Seite ausgegeben werden
			Chunk chunk = new Chunk("Kundennummer: " +kuid+ "\n" + "Erstellungsdatum: " + dateBuilder, font1);
			cellData = new PdfPCell(new Phrase(chunk));
			cellData.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			cellData.setBorder(0);
			adressTable.addCell(cellData);
			document.add(adressTable);
			
			// setzt Betreff
			PdfPTable textTable = new PdfPTable(1);
			textTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
			textTable.setSpacingBefore(40f);
			textTable.setWidthPercentage(96f);
			cellData = new PdfPCell(new Phrase("Buchungs- und Umsatzverhalten " +kuid+ "", font2));
			cellData.setBorder(0);
			textTable.addCell(cellData);
			
			// erstellt die Nachricht der PDF
			cellText = new PdfPCell(new Phrase("Sehr geehrte/r " + anrede + " " + name +", \n \n" + "dies ist eine autogenerierte Statistik zu Ihrem Buchungs- und Umsatzverhalten in der Einstein Denkfabrik. \n" , font3));
			cellText.setBorder(0);
			textTable.addCell(cellText);
			document.add(textTable);

			//setzt Inhalt in Tabelle
			PdfPTable contentTable = new PdfPTable(2);
			contentTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
			contentTable.setSpacingBefore(40f);
			contentTable.setWidthPercentage(96f);
			
			
			contentTable.addCell(new Phrase("Gesamtumsatz: "));
			contentTable.addCell(new Phrase(summe+"�"));
					
			contentTable.addCell(new Phrase("Anzahl aller Buchungen: "));
			contentTable.addCell(new Phrase(Math.round(anzahlGesamt) +" Buchungen"));
			
			contentTable.addCell(new Phrase("durchgef�hrte Buchungen: "));
			contentTable.addCell(new Phrase(anzahl + " Buchungen"));
			
			contentTable.addCell(new Phrase("offene Buchungen: "));
			contentTable.addCell(new Phrase(Math.round(anzahlOffen) + " Buchungen"));
			
			contentTable.addCell(new Phrase("Stornierte Buchungen: "));
			contentTable.addCell(new Phrase(Math.round(anzahlStorno) + " Buchungen"));
			
			contentTable.addCell(new Phrase("Anteil Storniert: "));
			double verhaeltnis = Math.round(100.0 * (anzahlStorno / anzahlGesamt *100)) / 100.0;
			contentTable.addCell(new Phrase(verhaeltnis + "%"));
			
			document.add(contentTable);
			document.close();
			
				
			// Erzeugung von OptionPane zur Info �ber erfolgreich angelegte PDF
			JOptionPane.showMessageDialog(null, "Eine Kopie der Statistik wurde als PDF Datei im Projektordner hinterlegt");
		}
		/**
		 * Speichert den ausgew�hlten Kunden in die jeweiligen Variablen
		 * DB Abfragen und Speicherung in Variablen um diese Ausgeben zu lassen
		 * Generiert die PDF und setzt den Namen
		 * @param kuid
		 * @param anrede
		 * @param name
		 * @param vorname
		 * @param strasse
		 * @param hausNr
		 * @param plz
		 * @param ort
		 */
		public void getPdfContent(int kuid, String anrede, String name, String vorname, String strasse, String hausNr, int plz, String ort) 
		{
			this.kuid = kuid;
			this.anrede = anrede;
			this.name = name;
			this.vorname = vorname;
			this.strasse = strasse;
			this.ort = ort;
			this.hausNr = hausNr;
			this.plz = plz;
			
			this.filename = "Buchungs- und Umsatzverthalten von " + name + " " + vorname + " " + kuid + "";
			
			//datenbankabfragen:
			double sum =0;
			int count=0;
			int countStorno= 0;
			int countGesamt = 0;
			int countOffen = 0; 
			String sqlStatement = "select SUM(gesamtbetrag) from rechnung where kdid = " + kuid;
			String sqlStatement2 = "select count(bnr) from buchung where kdid = " + kuid + " and storniert = 0 and TRUNC(SYSDATE) > Datum";;
			String sqlStatement3 = "select count(bnr) from buchung where kdid = " + kuid + " and storniert = 1";
			String sqlStatement4 = "select count(bnr) from buchung where kdid = " + kuid;
			String sqlStatement5 = "select count(bnr) from buchung where kdid = " + kuid + "and TRUNC(SYSDATE) < Datum and storniert = 0";
			
			//Gesamtumsatz 
			try
			{
				ResultSet rs2 =conn.getConnection().select(sqlStatement);
				while(rs2.next())
				{
					summe = df.format(rs2.getDouble(1));
					
				}
			}
			catch (SQLException e)
			{
				JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			}
			//Anzahl aller durchgef�hrten Buchungen 
			try
			{
				ResultSet rs =conn.getConnection().select(sqlStatement2);
				while(rs.next())
				{
					count = rs.getInt(1);
					anzahl = count;
				}
			}
			catch (SQLException e)
			{
				JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			}
			
			//Anzahl aller stornierten Buchungen
			try
			{
				ResultSet rs =conn.getConnection().select(sqlStatement3);
				while(rs.next())
				{
					countStorno = rs.getInt(1);
					anzahlStorno = countStorno;
				}
			}
			catch (SQLException e)
			{
				JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			}
			
			// Anzahl aller Buchungen
			try
			{
				ResultSet rs =conn.getConnection().select(sqlStatement4);
				while(rs.next())
				{
					countGesamt = rs.getInt(1);
					anzahlGesamt = countGesamt;
				}
			}
			catch (SQLException e)
			{
				JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			}
			// Anzahl aller offenen Buchungen
			try
			{
				ResultSet rs =conn.getConnection().select(sqlStatement5);
				while(rs.next())
				{
					countOffen = rs.getInt(1);
					anzahlOffen = countOffen;
				}
			}
			catch (SQLException e)
			{
				JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
			}
			
			
			//erstellung pdf:
			try
			{
				createPdf(filename);
			} catch (DocumentException e) 
			{
				e.printStackTrace();
			} catch (MalformedURLException ex)
			{
				ex.printStackTrace();
			} catch (IOException ey) {
				ey.printStackTrace();
			}
			
		}
}