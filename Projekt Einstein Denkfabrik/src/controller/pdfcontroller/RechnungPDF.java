package controller.pdfcontroller;
/**
 * @author Christoph
 * Die Klasse RechnungPDF erzeugt eine individuelle Rechnung f�r einen ausgew�hlten Kunden,
 * speichert diese lokal im Projektordner ab und �bergibt sie an die sendRechnung() Methode aus dem MailController. 
 */
import java.awt.Font;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import controller.MailController;

public class RechnungPDF {
	/**
	 * Instanzvarialen 
	 */
	private int kuid;
	private int bunr;
	private String anfangsdatum;
	private String enddatum;
	private String anrede;
	private String name;
	private String vorname;
	private String gbetrag;
	private int raumnr;
	private String email;
	private String filename;
	private String rabatt;
	private MailController sendpdf = new MailController();
	private com.itextpdf.text.Font font1 = FontFactory.getFont("Arial", BaseFont.IDENTITY_H, 10, Font.PLAIN);
	private com.itextpdf.text.Font font2 = FontFactory.getFont("Arial", BaseFont.IDENTITY_H, 14, Font.BOLD);
	private com.itextpdf.text.Font font3 = FontFactory.getFont("Arial", BaseFont.IDENTITY_H, 12, Font.PLAIN);
	private DecimalFormat df = new DecimalFormat(".00");
	
	/**
	 * Die Methode getRechnungsDaten() �bernimmt Parameter aus der ErstelleRechnungGUI(),
	 * definiert einen individuellen Namen f�r die zu erzeugende PDF-Datei und ruft die Methode
	 * erstellePDF()auf.
	 * @param bunr
	 * @param anrede
	 * @param name
	 * @param vorname
	 * @param gbetrag
	 * @param kuid
	 * @param datum
	 * @param raumnr
	 * @param email
	 */
	public void getRechnungsDaten(int bunr, String anrede, String name, String vorname, double gbetrag, int kuid, String anfangsdatum, String enddatum, int raumnr, String email, String rabatt) {
		this.bunr = bunr;
		this.kuid = kuid;
		this.anrede = anrede;
		this.name = name;
		this.vorname = vorname;
		this.gbetrag = df.format(gbetrag);
		this.anfangsdatum = anfangsdatum;
		this.enddatum = enddatum;
		this.raumnr = raumnr;
		this.email = email;
		this.rabatt = rabatt;
		this.filename = "Rechnung " + bunr + ""; 
		
		try {
			erstellePDF(filename);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}
	
	/**
	 * Die Methode erstellePDF() �bernimmt den filename aus der getRechnungsDaten() Methode und erzeugt ein neues document
	 * vom Typ Document im A4 Format. Der PdfWriter �bernimmt das document und erzeugt einen neuen FileOutputStream um 
	 * in das documet zu schreiben. Die fertige Datei wird im Projektorder in Rechnungen\ gespeichert.
	 * @param filename
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public void erstellePDF(String filename) throws DocumentException, MalformedURLException, IOException {
		//Obejkt von Typ Dokument erzeugen
		Document document = new Document(PageSize.A4, 40, 20, 0, 0);
		
		//Pr�fen ob Ordner vorhanden ist und wenn nicht erzeugen
				Path ordner = Paths.get("Rechnungen");
				if(!Files.exists(ordner))
				{	
					Files.createDirectory(ordner);
				}
		
		//Erzeugen der PDFDatei und Ausgabe lokal
		PdfWriter.getInstance(document, new FileOutputStream("Rechnungen\\" + filename + ".pdf"));
		document.open();
  
		//PdfTabelle mit drei Spalten f�r Anschrift, �berschrift und Datum erzeugen und Darstellung definieren
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setSpacingBefore(150f);
		table.setWidths(new int[] { 2, 1, 2 });
		//Es wird eine neue PdfCell und Phrase erzeugt. String, Font und darstellung definiert und dem table hinzugef�gt-
		PdfPCell cell = new PdfPCell(new Phrase("Firma\nEinstein Denkfabrik\nInteraktion 1\nD 33619 Bielefeld\n", font1));
		cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
		cell.setBorder(0);
		table.addCell(cell);

		PdfPCell cellrg = new PdfPCell(new Phrase("Rechnung", font2));
		cellrg.setBorder(0);
		cellrg.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
		cellrg.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		table.addCell(cellrg);

		PdfPCell cell2 = new PdfPCell();
		cell2.setBorder(0);
		cell2.setHorizontalAlignment(PdfPCell.RIGHT);
		table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);
		document.add(table);

		// Datum mit SDF erzeugen und platzieren
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		StringBuilder dateBuilder = new StringBuilder(dateFormat.format(date));
		Paragraph rightDate = new Paragraph();
		rightDate.setAlignment(Element.ALIGN_RIGHT);
		rightDate.setFont(font1);
		rightDate.add("" + dateBuilder);

		document.add(new LineSeparator());

		document.add(rightDate);
		document.add(new Paragraph(""));

		// Tabelle mit zwei Spalten f�r Kundendaten erzeugen
		PdfPTable adressTable = new PdfPTable(2);
		adressTable.setHorizontalAlignment(PdfPTable.ALIGN_RIGHT);
		adressTable.setSpacingBefore(40f);
		adressTable.setWidthPercentage(96f);
		cell = new PdfPCell(
				new Phrase(vorname + "\n" + name + "\n", font2));
		cell.setBorder(0);
		adressTable.addCell(cell);
		Chunk chunk = new Chunk(
				"Rechnungsnummer: " +bunr+ "\n" + "Kundennummer: " + kuid + "\n" + "Rechnungsdatum: " + dateBuilder, font1);
		cell = new PdfPCell(new Phrase(chunk));
		cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		cell.setBorder(0);
		adressTable.addCell(cell);
		document.add(adressTable);

		// Tabelle mit einer Spalte f�r Text erzeugen
		PdfPTable textTable = new PdfPTable(1);
		textTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
		textTable.setSpacingBefore(40f);
		textTable.setWidthPercentage(96f);
		cell = new PdfPCell(new Phrase("Rechnung " +bunr+ "", font2));
		cell.setBorder(0);
		textTable.addCell(cell);
		cell2 = new PdfPCell(new Phrase(
				"Sehr geehrter " +anrede+ " " +name+", \nwir berechnen Ihnen f�r Ihren Auftrag " + bunr + " folgendes:",
				font3));
		cell2.setBorder(0);
		textTable.addCell(cell2);
		document.add(textTable);

		// Tabelle mit zwei Spalten f�r Rechnungsparameter erzeugen
		PdfPTable contentTable = new PdfPTable(2);
		contentTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
		contentTable.setSpacingBefore(40f);
		contentTable.setWidthPercentage(96f);
		contentTable.addCell(new Phrase("Buchungsnr " + bunr + "", font3));
		contentTable.addCell(new Phrase("Datum: " + anfangsdatum + " - " + enddatum, font3));
		contentTable.addCell(new Phrase("Raumnr: " +raumnr, font3));
		contentTable.addCell(new Phrase("Summe " + gbetrag + "�" + "  Rabatt: " + rabatt, font3));
		document.add(contentTable);

		document.close();
		// filename wird an die Methode sendRechnung() im MailController �bergeben.
		sendpdf.sendRechnung("Rechnungen\\" + filename + ".pdf", email,anrede,name,bunr);
		
		// Erzeugung von OptionPane zur Info �ber erfolgreich angelegte und versendete PDF
		JOptionPane.showMessageDialog(null, "Die Rechnung wurde erfolgreich im Projektordner hinterlegt und per E-Mail versandt");
	}
}
