package view.mitarbeiterview;

/**
 * @author Hendrik Veips, Alexander Stavski
 */

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.mitarbeitercontroller.MitarbeiterÄndernEntfernenController;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.Mitarbeiter;
import model.tablemodels.MitarbeiterTableModel;
import view.StartansichtGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MitarbeiterÄndernEntfernenGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private JScrollPane scrollV;
	private JButton cancel = new JButton("Abbrechen");
	private JButton change = new JButton("Ändern");
	private JButton delete = new JButton("Entfernen");
	private JDialog dialog;

	private StartansichtGUI gui;

	private Border cueBorder;
	private Border buttonBorder;

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow1 = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);

	private JTable table;
	private MitarbeiterTableModel tableModel;
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private MitarbeiterÄndernEntfernenController maController;
	private TableRowSorter<TableModel> sorter;
	private char rolleEingelogt;

	/**
	 * Der Konstruktor erzeugt die MitarbeiterÄndernEntfernenGUI über mehrere Methoden und übergibt gegebenfalls den Parameter entfernen oder den Parameter rolle.
	 * @param gui
	 * @param rolle
	 * @param entfernen
	 */
	public MitarbeiterÄndernEntfernenGUI(StartansichtGUI gui, char rolle, boolean entfernen)
	{
		this.gui = gui;
		this.maController = new MitarbeiterÄndernEntfernenController(this);
		erzeugeDialog(entfernen);
		Image image1 = new ImageIcon(this.getClass().getResource("/MitarbeiterIcon.jpeg")).getImage();
		dialog.setIconImage(image1);
		setStichwortSuche();
		setTable(rolle);
		setButtons(entfernen);
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Größe, Location, BorderLayout, usw.).
	 * Ist der übergebene Parameter entfernen = true, wird der Titel "Mitarbeiter entfernen" gesetzt; falls entfernen = false ist, wird der Titel "Mitarbeiter ändern" gesetzt.
	 * @param entfernen
	 */
	public void erzeugeDialog(boolean entfernen)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		if(entfernen == true)
		{
			dialog.setTitle("Mitarbeiter entfernen");
		}
		else
		{
			dialog.setTitle("Mitarbeiter ändern");
		}
		dialog.setPreferredSize(new Dimension(900, 590));
		dialog.setLocation(510, 245);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode übergibt der Tabelle das MitarbeiterTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach übergibt sie der Tabelle noch den sorter, damit der Nutzer die Möglichkeit hat die Einträge zu sortieren und sie übergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst möglich macht.
	 * Außerdem wird über den übergebenen Parameter rolle und dem MitarbeiterÄndernEntfernenController entschieden welche Funktionen der jeweilige Nutzer hat.
	 * @param rolle
	 */
	private void setTable(char rolle)
	{
		tableModel = new MitarbeiterTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);
		this.rolleEingelogt = rolle;
		maController.rollenUnterscheidung();
		tabellenwerteZentrieren();
		tabellenbreiteFestlegen();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortSuche()
	{
		panel1.setPreferredSize(new Dimension(740, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		panel1.setLayout(flow1);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(540, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}

	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 * Über den übergebenen Parameter entfernen entscheidet sie zusätzlich, ob der Button delete oder der Button change gesetzt wird.
	 * @param entfernen
	 */
	private void setButtons(boolean entfernen)
	{
		panel2.setPreferredSize(new Dimension(740, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		if(entfernen == true)
		{
			delete.setPreferredSize(new Dimension(160, 40));
			delete.setFont(schriftart1);
			delete.setBackground(Color.WHITE);
			delete.addActionListener(this);
			panel2.add(delete);
			Image image2 = new ImageIcon(this.getClass().getResource("/LöschenIcon.jpeg")).getImage();
			delete.setIcon(new ImageIcon(image2));
		}
		else
		{
			change.setPreferredSize(new Dimension(160, 40));
			change.setFont(schriftart1);
			change.setBackground(Color.WHITE);
			change.addActionListener(this);
			panel2.add(change);
			Image image1 = new ImageIcon(this.getClass().getResource("/BearbeitenIcon.jpeg")).getImage();
			change.setIcon(new ImageIcon(image1));
		}
		cancel.setPreferredSize(new Dimension(160, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image3 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image3));
	}

	
	/**
	 * @author Alexander Stavski
	 */
	private void tabellenwerteZentrieren()
	{
		// Tabellenwerte zentrieren
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++)
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}

	/**
	 * @author Alexander Stavski
	 */
	private void tabellenbreiteFestlegen()
	{
		// Tabellenspalten Breite festlegen
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(45);
		table.getColumnModel().getColumn(5).setPreferredWidth(140);
		table.getColumnModel().getColumn(6).setPreferredWidth(40);
		table.getColumnModel().getColumn(7).setPreferredWidth(220);
		table.getColumnModel().getColumn(8).setPreferredWidth(20);
		table.getColumnModel().getColumn(8).setResizable(false);
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getCueInput()
	{
		return this.cueInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTable getTable()
	{
		return this.table;
	}

	/**
	 * @author Alexander Stavski
	 */
	public TableRowSorter<TableModel> getTableRowSorter()
	{
		return this.sorter;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JButton getDeleteButton()
	{
		return delete;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JButton getChangeButton()
	{
		return change;
	}

	/**
	 * @author Alexander Stavski
	 */
	public char getRolleEingelogt()
	{
		return rolleEingelogt;
	}

	/**
	 * @author Alexander Stavski
	 */
	public String getAnredeRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Mitarbeiter ma = tableModel.getData(row);
		String anrede = ma.getAnrede();
		return anrede;
	}

	/**
	 * @author Alexander Stavski
	 */
	public String getVornameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());;
		Mitarbeiter ma = tableModel.getData(row);
		String vorname = ma.getVorname();
		return vorname;
	}

	/**
	 * @author Alexander Stavski
	 */
	public String getNachnameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Mitarbeiter ma = tableModel.getData(row);
		String nachname = ma.getName();
		return nachname;
	}

	/**
	 * @author Alexander Stavski
	 */
	public Mitarbeiter getMitarbeiterRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Mitarbeiter ma = tableModel.getData(row);
		return ma;
	}

	/**
	 * @author Alexander Stavski
	 */
	public String getRolleRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Mitarbeiter ma = tableModel.getData(row);
		String rolle = ma.getRolle();
		return rolle;
	}

	/**
	 * @author Alexander Stavski
	 */
	public TableModel getUpdatedTableModel()
	{
		tableModel.databaseUpdated();
		return tableModel;
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == this.change)
		{
			maController.aendernButtonAction();
		}
		if (e.getSource() == this.delete)
		{
			maController.loeschenButtonAction();
		}
		if (e.getSource() == this.cancel)
		{
			dialog.dispose();
		}
	}
}