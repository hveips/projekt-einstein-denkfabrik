package view.mitarbeiterview;

/**
 * @author Christoph Hoppe, Alexander Stavski
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.IntegerOnlyDocument;
import controller.documentcontroller.LimitedDocument;
import controller.documentcontroller.LimitedMailDocument;
import controller.documentcontroller.StringOnlyDocument;
import controller.mitarbeitercontroller.MitarbeiterHinzufügenController;
import view.StartansichtGUI;

public class MitarbeiterHinzufügenGUI extends JFrame implements ActionListener {
	private JPanel eingabePanel = new JPanel();
	private JPanel idPanel = new JPanel();
	private JPanel anredePanel = new JPanel();
	private JPanel vornamePanel = new JPanel();
	private JPanel nachnamePanel = new JPanel();
	private JPanel plzPanel = new JPanel();
	private JPanel ortPanel = new JPanel();
	private JPanel strassePanel = new JPanel();
	private JPanel hnrPanel = new JPanel();
	private JPanel eMailPanel = new JPanel();
	private JPanel rollePanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JLabel id = new JLabel("Mitarbeiter-ID");
	private JLabel anrede = new JLabel("Anrede");
	private JLabel vorname = new JLabel("Vorname");
	private JLabel nachname = new JLabel("Nachname");
	private JLabel plz = new JLabel("Postleitzahl");
	private JLabel ort = new JLabel("Ort");
	private JLabel strasse = new JLabel("Straße");
	private JLabel hnr = new JLabel("Hausnummer");
	private JLabel eMail = new JLabel("E-Mail");
	private JLabel rolle = new JLabel("Rolle");
	private JTextField idInput = new JTextField();
	private JComboBox anredeInput = new JComboBox();
	private JTextField vornameInput = new JTextField();
	private JTextField nachnameInput = new JTextField();
	private JTextField plzInput = new JTextField();
	private JTextField ortInput = new JTextField();
	private JTextField strasseInput = new JTextField();
	private JTextField hnrInput = new JTextField();
	private JTextField eMailInput = new JTextField();
	private JComboBox rolleInput = new JComboBox();
	private JButton info = new JButton();
	private JButton cancel = new JButton("Abbrechen");
	private JButton hinzufügenbtn = new JButton("Hinzufügen");
	private JDialog dialog;

	private StartansichtGUI gui;

	private Border eingabeBorder;
	private Border buttonBorder;

	private String anreden[] = { "Herr", "Frau" };
	private String role[] = { "M", "G", "A" };

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow3 = new FlowLayout(FlowLayout.RIGHT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private MitarbeiterHinzufügenController maController;

	/**
	 * Der Konstruktor erzeugt die MitarbeiterHinzufügenGUI und übernimmt als
	 * Parameter die Startansicht und rolle
	 * 
	 * @param gui
	 * @param rolle
	 */
	public MitarbeiterHinzufügenGUI(StartansichtGUI gui, char rolle) {
		this.gui = gui;
		this.maController = new MitarbeiterHinzufügenController(this);
		erzeugeDialog();
		Image image1 = new ImageIcon(this.getClass().getResource("/MitarbeiterIcon.jpeg")).getImage();
		dialog.setIconImage(image1);
		setEingabePanel();
		setEingabeID();
		setEingabeAnrede();
		setEingabeVorname();
		setEingabeNachname();
		setEingabePLZ();
		setEingabeOrt();
		setEingabeStrasse();
		setEingabeHnr();
		setEingabeEMail();
		setEingabeRolle(rolle);
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Die Methode erzeugt einen neuen JDialog und definiert dessen Eigenschaften
	 */
	public void erzeugeDialog() {
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Mitarbeiter hinzufügen");
		dialog.setPreferredSize(new Dimension(700, 540));
		dialog.setLocation(610, 270);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode definiert das Panel für die verschiedenen Eingaben und weist u.a
	 * das Flowlayout zu. Auf diesem Panel werden später alle Panel für die Eingaben
	 * hinzugefügt.
	 */
	private void setEingabePanel() {
		eingabePanel.setPreferredSize(new Dimension(650, 450));
		eingabePanel.setLayout(flow);
		eingabePanel.setBackground(Color.lightGray);
		eingabeBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		eingabePanel.setBorder(eingabeBorder);
		add(eingabePanel, border.CENTER);
	}

	/**
	 * Die Methode definiert das idLabel,Textfeld und idPanel, welche dem
	 * eingabePanel zugeordnet werden. Das Textfeld darf nicht editiert werden,damit
	 * keine Inkonsistenzen in der KundenDatenbank entstehen.
	 */
	public void setEingabeID() {
		idPanel.setPreferredSize(new Dimension(630, 40));
		idPanel.setLayout(flow2);
		idPanel.setBackground(Color.lightGray);
		eingabePanel.add(idPanel);
		id.setPreferredSize(new Dimension(150, 30));
		id.setFont(schriftart1);
		idPanel.add(id);
		idInput.setPreferredSize(new Dimension(460, 30));
		idInput.setEditable(false);
		idInput.setFont(schriftart1);
		idPanel.add(idInput);
		maController.setNewMitarbeiterID();
	}

	/**
	 * Die Methode definiert das anredeLabel,ComboBox und anredePanel, welches auf
	 * dem eingabePanel platziert wird.
	 * 
	 */
	public void setEingabeAnrede() {
		anredePanel.setPreferredSize(new Dimension(630, 40));
		anredePanel.setLayout(flow2);
		anredePanel.setBackground(Color.lightGray);
		eingabePanel.add(anredePanel);
		anrede.setPreferredSize(new Dimension(150, 30));
		anrede.setFont(schriftart1);
		anredePanel.add(anrede);
		anredeInput.setPreferredSize(new Dimension(460, 30));
		anredeInput.setFont(schriftart1);
		for (int i = 0; i < anreden.length; i++) {
			anredeInput.addItem(anreden[i]);
		}
		anredePanel.add(anredeInput);
	}

	/**
	 * Die Methode definiert das vornameLabel,TextField und vornamePanel, welches
	 * auf dem eingabePanel platziert wird. Das TextField nimmt nur String Werte mit
	 * max. 35 Zeichen entgegen.
	 */
	public void setEingabeVorname() {
		vornamePanel.setPreferredSize(new Dimension(630, 40));
		vornamePanel.setLayout(flow2);
		vornamePanel.setBackground(Color.lightGray);
		eingabePanel.add(vornamePanel);
		vorname.setPreferredSize(new Dimension(150, 30));
		vorname.setFont(schriftart1);
		vornamePanel.add(vorname);
		vornameInput.setPreferredSize(new Dimension(460, 30));
		vornameInput.setFont(schriftart1);
		vornamePanel.add(vornameInput);
		vornameInput.setDocument(new StringOnlyDocument(35));
	}

	/**
	 * Die Methode definiert das nachnameLabel,TextField und nachnamePanel, welches
	 * auf dem eingabePanel platziert wird. Das TextField nimmt nur String Werte mit
	 * max. 35 Zeichen entgegen.
	 */
	public void setEingabeNachname() {
		nachnamePanel.setPreferredSize(new Dimension(630, 40));
		nachnamePanel.setLayout(flow2);
		nachnamePanel.setBackground(Color.lightGray);
		eingabePanel.add(nachnamePanel);
		nachname.setPreferredSize(new Dimension(150, 30));
		nachname.setFont(schriftart1);
		nachnamePanel.add(nachname);
		nachnameInput.setPreferredSize(new Dimension(460, 30));
		nachnameInput.setFont(schriftart1);
		nachnamePanel.add(nachnameInput);
		nachnameInput.setDocument(new StringOnlyDocument(35));
	}

	/**
	 * Die Methode definiert das Plzlabel,TextField und Plzpanel, welches auf dem
	 * eingabePanel platziert wird. Das TextField nimmt nur Integer Werte mit max. 5
	 * Zeichen entgegen.
	 */
	public void setEingabePLZ() {
		plzPanel.setPreferredSize(new Dimension(630, 40));
		plzPanel.setLayout(flow2);
		plzPanel.setBackground(Color.lightGray);
		eingabePanel.add(plzPanel);
		plz.setPreferredSize(new Dimension(150, 30));
		plz.setFont(schriftart1);
		plzPanel.add(plz);
		plzInput.setPreferredSize(new Dimension(460, 30));
		plzInput.setFont(schriftart1);
		plzPanel.add(plzInput);
		plzInput.setDocument(new IntegerOnlyDocument(5));
	}

	/**
	 * Die Methode definiert das Ortlabel,TextField und ortPanel, welches auf dem
	 * eingabePanel platziert wird. Das TextField nimmt nur String Werte mit max. 25
	 * Zeichen entgegen.
	 */
	public void setEingabeOrt() {
		ortPanel.setPreferredSize(new Dimension(630, 40));
		ortPanel.setLayout(flow2);
		ortPanel.setBackground(Color.lightGray);
		eingabePanel.add(ortPanel);
		ort.setPreferredSize(new Dimension(150, 30));
		ort.setFont(schriftart1);
		ortPanel.add(ort);
		ortInput.setPreferredSize(new Dimension(460, 30));
		ortInput.setFont(schriftart1);
		ortPanel.add(ortInput);
		ortInput.setDocument(new StringOnlyDocument(25));
	}

	/**
	 * Die Methode definiert das Strasselabel,TextField und StrassePanel, welchse
	 * auf dem eingabePanel platziert wird. Das TextField nimmt nur String Werte mit
	 * max. 30 Zeichen entgegen.
	 */
	public void setEingabeStrasse() {
		strassePanel.setPreferredSize(new Dimension(630, 40));
		strassePanel.setLayout(flow2);
		strassePanel.setBackground(Color.lightGray);
		eingabePanel.add(strassePanel);
		strasse.setPreferredSize(new Dimension(150, 30));
		strasse.setFont(schriftart1);
		strassePanel.add(strasse);
		strasseInput.setPreferredSize(new Dimension(460, 30));
		strasseInput.setFont(schriftart1);
		strassePanel.add(strasseInput);
		strasseInput.setDocument(new StringOnlyDocument(30));
	}

	/**
	 * Die Methode definiert das HnrLabel,TextField und HnrPanel, welches auf dem
	 * eingabePanel platziert wird.
	 */
	public void setEingabeHnr() {
		hnrPanel.setPreferredSize(new Dimension(630, 40));
		hnrPanel.setLayout(flow2);
		hnrPanel.setBackground(Color.lightGray);
		eingabePanel.add(hnrPanel);
		hnr.setPreferredSize(new Dimension(150, 30));
		hnr.setFont(schriftart1);
		hnrPanel.add(hnr);
		hnrInput.setPreferredSize(new Dimension(460, 30));
		hnrInput.setFont(schriftart1);
		hnrPanel.add(hnrInput);
		hnrInput.setDocument(new LimitedDocument(4));
	}

	/**
	 * Die Methode definiert das eMaillabel,TextField und emailPanel, welches auf
	 * dem eingabePanel platziert wird. Das TextField nimmt max. 35 Zeichen
	 * entgegen.
	 */
	public void setEingabeEMail() {
		eMailPanel.setPreferredSize(new Dimension(630, 40));
		eMailPanel.setLayout(flow2);
		eMailPanel.setBackground(Color.lightGray);
		eingabePanel.add(eMailPanel);
		eMail.setPreferredSize(new Dimension(150, 30));
		eMail.setFont(schriftart1);
		eMailPanel.add(eMail);
		eMailInput.setPreferredSize(new Dimension(460, 30));
		eMailInput.setFont(schriftart1);
		eMailPanel.add(eMailInput);
		eMailInput.setDocument(new LimitedMailDocument(35));
	}

	/**
	 * Die Methode definiert rollePanel, Label und fügt sie dem eingabePanel hinzu
	 * 
	 * @param roll
	 */
	public void setEingabeRolle(char roll) {
		rollePanel.setPreferredSize(new Dimension(630, 40));
		rollePanel.setLayout(flow2);
		rollePanel.setBackground(Color.lightGray);
		eingabePanel.add(rollePanel);
		rolle.setPreferredSize(new Dimension(150, 30));
		rolle.setFont(schriftart1);
		rollePanel.add(rolle);
		rolleInput.setPreferredSize(new Dimension(425, 30));
		rolleInput.setFont(schriftart1);
		// Wählt Rolle aus String Array aus
		for (int i = 0; i < role.length; i++) {
			rolleInput.addItem(role[i]);
		}
		rollePanel.add(rolleInput);
		info.setPreferredSize(new Dimension(30, 30));
		info.setBackground(Color.WHITE);
		info.addActionListener(this);
		rollePanel.add(info);
		Image image1 = new ImageIcon(this.getClass().getResource("/InfoIcon.jpeg")).getImage();
		info.setIcon(new ImageIcon(image1));
		
		/** 
		 * 
		 *  Geschäftsführer dürfen keine Admins anlegen
		 *  @author Alexander Stavski
		 */
		if (roll == 'G') 
		{
			rolleInput.removeItemAt(2);
		}
	}

	/**
	 * Die Methode definiert das ButtonPanel und fügt zwei Buttons auf ihm hinzu
	 */
	public void setButtons() {
		buttonPanel.setPreferredSize(new Dimension(650, 50));
		buttonPanel.setLayout(flow3);
		buttonPanel.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 25, 30, Color.lightGray);
		buttonPanel.setBorder(buttonBorder);
		add(buttonPanel, border.SOUTH);
		hinzufügenbtn.setPreferredSize(new Dimension(170, 40));
		hinzufügenbtn.setFont(schriftart1);
		hinzufügenbtn.setBackground(Color.WHITE);
		hinzufügenbtn.addActionListener(this);
		buttonPanel.add(hinzufügenbtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/HinzufügenIcon.jpeg")).getImage();
		hinzufügenbtn.setIcon(new ImageIcon(image1));
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		buttonPanel.add(cancel);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image2));
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getIDInput() 
	{
		return idInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JComboBox getAnredeInput() 
	{
		return anredeInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getVornameInput() 
	{
		return vornameInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getNachnameInput() 
	{
		return nachnameInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JComboBox getRolleInput() 
	{
		return rolleInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getPlzInput() 
	{
		return plzInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getOrtInput() 
	{
		return ortInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getStrasseInput() 
	{
		return strasseInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getHnrInput() 
	{
		return hnrInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField geteMailInput() 
	{
		return eMailInput;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.cancel) {
			dialog.dispose();
		}
		if (e.getSource() == this.hinzufügenbtn) 
		{
			maController.mitarbeiterHinzufügenAction();
		}
		if (e.getSource() == this.info) {
			JOptionPane.showMessageDialog(null, "M - Mitarbeiter" + "\n" + "G - Geschäftsführer" + "\n" + "A - Admin",
					"Information", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}