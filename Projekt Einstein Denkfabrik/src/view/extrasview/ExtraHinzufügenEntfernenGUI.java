package view.extrasview;

/**
 * @author Hendrik Veips
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.extrascontroller.ExtraHinzufügenEntfernenController;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.tablemodels.ExtraTableModel;
import view.StartansichtGUI;

public class ExtraHinzufügenEntfernenGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private JTable table;
	private ExtraTableModel tableModel;
	private JScrollPane scrollV;
	private JButton add = new JButton("Hinzufügen");
	private JButton delete = new JButton("Entfernen");
	private JButton cancel = new JButton("Abbrechen");
	private JDialog dialog = new JDialog();
	
	private StartansichtGUI gui;
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;	
	
	private ExtraHinzufügenEntfernenController extraHinzufügenEntfernenController;
	private int[] anzahlEssenGetraenke;
	private ArrayList<String> bezeichnungen = new ArrayList<String>();
	private boolean löschen = false;
	
	/**
	 * Der Konstruktor erzeugt die ExtraHinzufügenEntfernenGUI über mehrere Methoden und übergibt gegebenfalls den Parameter hinzufügen.
	 * @param gui
	 * @param hinzufügen
	 */
	public ExtraHinzufügenEntfernenGUI(StartansichtGUI gui, boolean hinzufügen)
	{
		this.gui = gui;
		extraHinzufügenEntfernenController = new ExtraHinzufügenEntfernenController(this);
		erzeugeDialog(hinzufügen);
		Image image4 = new ImageIcon(this.getClass().getResource("/ExtraIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();
		setTable();
		setButtons(hinzufügen);
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Größe, Location, BorderLayout, usw.).
	 * Ist der übergebene Parameter hinzufügen = true, wird der Titel "Extras hinzufügen" gesetzt; falls buchen = false ist, wird der Titel "Extras entfernen" gesetzt.
	 * @param hinzufügen
	 */
	public void erzeugeDialog(boolean hinzufügen)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		if(hinzufügen == true)
		{
			dialog.setTitle("Extras hinzufügen");
		}
		else
		{
			dialog.setTitle("Extras entfernen");
		}
		dialog.setPreferredSize(new Dimension(600, 400));
		dialog.setLocation(660, 340);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	public void setStichwortsuche()
	{
		panel1.setPreferredSize(new Dimension(540, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(140, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(380, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}
	
	/**
	 * Die Methode übergibt der Tabelle das ExtraTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach übergibt sie der Tabelle noch den sorter, damit der Nutzer die Möglichkeit hat die Einträge zu sortieren und sie übergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst möglich macht.
	 */
	public void setTable()
	{
		tableModel = new ExtraTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 300));
		add(scrollV, border.CENTER);
		tabellenwerteZentrieren();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 * Über den übergebenen Parameter hinzufügen entscheidet sie zusätzlich, ob der Button add oder der Button delete gesetzt wird.
	 * @param hinzufügen
	 */
	public void setButtons(boolean hinzufügen)
	{
		panel2.setPreferredSize(new Dimension(540, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		if(hinzufügen == true)
		{
			add.setPreferredSize(new Dimension(170, 40));
			add.setFont(schriftart1);
			add.setBackground(Color.WHITE);
			add.addActionListener(this);
			panel2.add(add);
			Image image1 = new ImageIcon(this.getClass().getResource("/HinzufügenIcon.jpeg")).getImage();
			add.setIcon(new ImageIcon(image1));
			add.setEnabled(extraHinzufügenEntfernenController.hinzufügenEntscheidung());
		}
		else
		{
			delete.setPreferredSize(new Dimension(170, 40));
			delete.setFont(schriftart1);
			delete.setBackground(Color.WHITE);
			delete.addActionListener(this);
			panel2.add(delete);
			Image image2 = new ImageIcon(this.getClass().getResource("/LöschenIcon.jpeg")).getImage();
			delete.setIcon(new ImageIcon(image2));
		}
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image3 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image3));
	}
	
	/**
	 * Die Methode zentriert die Tabellenwerte.
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	public JTable getTable() 
	{
		return this.table;
	}
	
	public ExtraTableModel getTableModel() 
	{
		return tableModel;
	}
	
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	public TableModel getUpdatedTableModel()
	{
		tableModel.databaseUpdated();
		return tableModel;
	}
	
	public JButton getDeleteButton()
	{
		return delete;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		/**
		 * Das System übergibt dem Konstruktor der ExtraHinzufügenGUI das int-Array anzahlEssenGetraenke und erzuegt diese.
		 * Zusätzlich wird nach dem Schließen der ExtraHinzufügenGUI überprüft, ob noch weitere Extras hinzugefügt werden können
		 * und gegebenfalls wird der Button add gesperrt.
		 */
		if(e.getSource() == this.add)
		{
			anzahlEssenGetraenke = extraHinzufügenEntfernenController.getAnzahlEssenGetraenke();
			new ExtraHinzufügenGUI(this, anzahlEssenGetraenke);
			add.setEnabled(extraHinzufügenEntfernenController.hinzufügenEntscheidung());
		}
		if(e.getSource() == this.delete) 
		{
			/**
			 * Das System prüft zuerst, ob eine Zeile ausgewählt ist. Wenn ja, prüft es, ob das ausgewahlte Extra in einer Buchung oder Reservierung auftaucht.
			 * Ist dies auch der Fall, kann das Extra nicht gelöscht werden, andernfalls wird das Extra gelöscht, was vom Nutzer bestätigt werden muss.
			 * Ist keine Zeile ausgewählt, wird dies dem Nutzer ebenfalls mitgeteilt.
			 */
			if(table.getSelectedRow() == -1)
			{
				JOptionPane.showMessageDialog(null, "Kein Extra ausgewählt.", "Fehler", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				String bezeichnung = (String) table.getValueAt(table.getSelectedRow(), 0);
				bezeichnungen = extraHinzufügenEntfernenController.istExtraInBuexResex();
				for(int i = 0;i < bezeichnungen.size();i++)
				{
					if(bezeichnung.equals(bezeichnungen.get(i)))
					{
						löschen = false;
						i = bezeichnungen.size();
					}
					else
					{
						löschen = true;
					}
				}
				if(löschen == true)
				{
					extraHinzufügenEntfernenController.deleteExtra(bezeichnung);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Das ausgewählte Extra ist in einer Buchung enthalten!" + "\n" + "Das Extra kann nicht gelöscht werden!", "Fehler", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
	}
}