package view.extrasview;

/**
 * @author Christoph Hoppe, Niklas Nebeling
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import controller.buchungcontroller.BuchenReservierenController;
import controller.documentcontroller.IntegerOnlyDocument;
import controller.extrascontroller.BearbeitenController;
import controller.raumcontroller.RaumController;
import view.buchung_reservierungview.BucheReserviereRaumWeiterGUI;

public class ExtrasGUI extends JFrame implements ActionListener 
{
	private JPanel mainPanel = new JPanel();
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JPanel panel4 = new JPanel();
	private JPanel panel5 = new JPanel();
	private JPanel panel6 = new JPanel();
	private JPanel panel7 = new JPanel();
	private JPanel panel8 = new JPanel();
	private JPanel panel9 = new JPanel();
	private JPanel panel10 = new JPanel();
	private JPanel buttonpnl = new JPanel();
	private JCheckBox chk1 = new JCheckBox();
	private JCheckBox chk2 = new JCheckBox();
	private JCheckBox chk3 = new JCheckBox();
	private JCheckBox chk4 = new JCheckBox();
	private JCheckBox chk5 = new JCheckBox();
	private JCheckBox chk6 = new JCheckBox();
	private JCheckBox chk7 = new JCheckBox();
	private JCheckBox chk8 = new JCheckBox();
	private JCheckBox chk9 = new JCheckBox();
	private JCheckBox chk10 = new JCheckBox();
	private JLabel essenlbl = new JLabel("Essen");
	private JLabel getr�nkelbl = new JLabel("Getr�nke");
	private JLabel sonstlbl = new JLabel("Sonstiges");
	private JLabel personenZahl = new JLabel("Anzahl Personen");
	private JTextField sonsttxt = new JTextField();
	private JTextField personenZahlInput = new JTextField();
	private JButton abbrechenbtn = new JButton("Abbrechen");
	private JButton speichernbtn = new JButton();
	private JDialog dialog = new JDialog();
	
	private BucheReserviereRaumWeiterGUI gui;
	
	private Border border1;
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private JCheckBox essenCheckboxen[] = {chk1, chk2, chk3, chk4, chk5, chk6};
	private JCheckBox getraenkeCheckboxen[] = {chk7, chk8, chk9, chk10};
	private JPanel essenPanels[] = {panel2, panel3, panel4};
	private JPanel getraenkePanels[] = {panel6, panel7};
	
	private BuchenReservierenController controller;
	private BearbeitenController controller2 = new BearbeitenController();
	private RaumController raumcontroller;
	private ArrayList<String> getraenke = new ArrayList<String>();
	private ArrayList<String> essen = new ArrayList<String>();
	private int anzahlGetraenke;
	private int anzahlEssen;
	private int rnr;
	/**
	 * Der Konstruktor erzeugt die ExtrasGUI und �bernimmt dazu verschiedene Parameter
	 * @param buchen
	 * @param controller
	 * @param rnr
	 * @param name
	 * @param vorname
	 * @param kundenID
	 * @param anfangsdatum
	 * @param enddatum
	 */
	public ExtrasGUI(boolean buchen, BuchenReservierenController controller, int rnr)
	{
		this.controller = controller;
		this.rnr = rnr;
		raumcontroller = new RaumController();
		getraenke = controller2.ladeExtraGetraenkeBezeichnungen();
		essen = controller2.ladeExtraEssenBezeichnungen();
		anzahlGetraenke = controller2.getAnzahlGetraenke();
		anzahlEssen = controller2.getAnzahlEssen();
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/ExtraIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setPanelsEssen();
		setEssen();
		setPanelsGetr�nke();
		setGetr�nke();
		setAnzahlPersonen();
		setSonstiges();
		setButtons(buchen);
		dialog.pack();
		dialog.setVisible(true);
	}
	/**
	 * Die Methode erzeugt einen neuen JDialog und definiert neben dem Titel auch Gr��e und Ausrichtung individuell.
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Extras");
		if((anzahlGetraenke + anzahlEssen) <= 5)
		{
			int h�he = 290 + ((anzahlGetraenke + anzahlEssen) * 50);
			dialog.setPreferredSize(new Dimension(450, h�he));
			dialog.setLocation(735, ((1080 - h�he) / 2));
		}
		else
		{
			dialog.setPreferredSize(new Dimension(450, 540));
			dialog.setLocation(735, 295);
		}
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}
	/**
	 * Die Methode definiert das mainPanel und platziert die essenPanels auf diesem
	 */
	public void setPanelsEssen()
	{
		mainPanel.setPreferredSize(new Dimension(400, 455));
		mainPanel.setBackground(Color.lightGray);
		mainPanel.setLayout(flow2);
		border1 = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		mainPanel.setBorder(border1);
		add(mainPanel, border.CENTER);
		panel1.setPreferredSize(new Dimension(380, 40));
		panel1.setBackground(Color.lightGray);
		panel1.setLayout(flow);
		mainPanel.add(panel1);
		essenlbl.setPreferredSize(new Dimension(120, 30));
		essenlbl.setFont(schriftart1);
		panel1.add(essenlbl);
		if(anzahlEssen >= 3)
		{
			for(int i = 0;i < essenPanels.length;i++)
			{
				essenPanels[i].setPreferredSize(new Dimension(350, 40));
				essenPanels[i].setBackground(Color.lightGray);
				essenPanels[i].setLayout(flow);
				mainPanel.add(essenPanels[i]);
			}
		}
		else
		{
			for(int i = 0;i < anzahlEssen;i++)
			{
				essenPanels[i].setPreferredSize(new Dimension(350, 40));
				essenPanels[i].setBackground(Color.lightGray);
				essenPanels[i].setLayout(flow);
				mainPanel.add(essenPanels[i]);
			}
		}
	}
	/**
	 * Die Methode platziert die essenCheckboxen auf dem Panel
	 */
	public void setEssen()
	{
		int j = 0;
		for(int i = 0;i < anzahlEssen;i++)
		{
			essenCheckboxen[i].setPreferredSize(new Dimension(150, 30));
			essenCheckboxen[i].setBackground(Color.lightGray);
			essenCheckboxen[i].setFont(schriftart1);
			essenCheckboxen[i].setText(essen.get(i));
			essenPanels[j].add(essenCheckboxen[i]);
			j++;
			if(j == 3)
			{
				j = 0;
			}
		}
	}
	/**
	 * Die Methode platziert die getr�nkePanel auf dem Mainpanel
	 */
	public void setPanelsGetr�nke()
	{
		panel5.setPreferredSize(new Dimension(380, 40));
		panel5.setBackground(Color.lightGray);
		panel5.setLayout(flow);
		mainPanel.add(panel5);
		getr�nkelbl.setPreferredSize(new Dimension(120, 30));
		getr�nkelbl.setFont(schriftart1);
		panel5.add(getr�nkelbl);
		if(anzahlGetraenke >= 2)
		{
			for(int i = 0;i < getraenkePanels.length;i++)
			{
				getraenkePanels[i].setPreferredSize(new Dimension(350, 40));
				getraenkePanels[i].setBackground(Color.lightGray);
				getraenkePanels[i].setLayout(flow);
				mainPanel.add(getraenkePanels[i]);
			}
		}
		else
		{
			for(int i = 0;i < anzahlGetraenke;i++)
			{
				getraenkePanels[i].setPreferredSize(new Dimension(350, 40));
				getraenkePanels[i].setBackground(Color.lightGray);
				getraenkePanels[i].setLayout(flow);
				mainPanel.add(getraenkePanels[i]);
			}
		}
	}
	/**
	 * Die Methode platziert die getr�nkeCheckboxen auf dem Panel
	 */
	public void setGetr�nke()
	{
		int j = 0;
		for(int i = 0;i < anzahlGetraenke;i++)
		{
			getraenkeCheckboxen[i].setPreferredSize(new Dimension(150, 30));
			getraenkeCheckboxen[i].setBackground(Color.lightGray);
			getraenkeCheckboxen[i].setFont(schriftart1);
			getraenkeCheckboxen[i].setText(getraenke.get(i));
			getraenkePanels[j].add(getraenkeCheckboxen[i]);
			j++;
			if(j == 2)
			{
				j = 0;
			}
		}
	}
	/**
	 * Panel und Textfeld f�r die Eingabe der Personenanzahl werden auf dem mainPanel angeordnet,
	 * es wird definiertm dass nur Integer Werte im Textfeld eingegeben werden d�rfne.
	 */
	public void setAnzahlPersonen()
	{
		panel10.setPreferredSize(new Dimension(380, 40));
		panel10.setBackground(Color.lightGray);
		panel10.setLayout(flow);
		mainPanel.add(panel10);
		personenZahl.setPreferredSize(new Dimension(160, 30));
		personenZahl.setFont(schriftart1);
		panel10.add(personenZahl);
		personenZahlInput.setPreferredSize(new Dimension(50, 30));
		personenZahlInput.setFont(schriftart1);
		personenZahlInput.setDocument(new IntegerOnlyDocument(3));
		panel10.add(personenZahlInput);
	}
	/**
	 * Panel,Label und Textfeld f�r Sonstiges werden auf dem MainPanel angeordnet.
	 */
	public void setSonstiges()
	{
		panel8.setPreferredSize(new Dimension(380, 40));
		panel8.setBackground(Color.lightGray);
		panel8.setLayout(flow);
		mainPanel.add(panel8);
		sonstlbl.setPreferredSize(new Dimension(120, 30));
		sonstlbl.setFont(schriftart1);
		panel8.add(sonstlbl);
		panel9.setPreferredSize(new Dimension(350, 40));
		panel9.setBackground(Color.lightGray);
		panel9.setLayout(flow);
		mainPanel.add(panel9);
		sonsttxt.setPreferredSize(new Dimension(320, 30));
		sonsttxt.setFont(schriftart1);
		panel9.add(sonsttxt);
	}
	/**
	 * Buttonpanel wird definiert und Button darauf platziert.
	 */
	public void setButtons(boolean buchen)
	{
		buttonpnl.setPreferredSize(new Dimension(380, 50));
		buttonpnl.setLayout(flow2);
		buttonpnl.setBackground(Color.lightGray);
		add(buttonpnl, border.SOUTH);
		if(buchen == true)
		{
			speichernbtn.setText("Buchen");
		}
		else
		{
			speichernbtn.setText("Reservieren");
		}
		speichernbtn.setPreferredSize(new Dimension(170, 40));
		speichernbtn.setFont(schriftart1);
		speichernbtn.setBackground(Color.WHITE);
		speichernbtn.addActionListener(this);
		buttonpnl.add(speichernbtn);
		Image image2 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		speichernbtn.setIcon(new ImageIcon(image2));
		abbrechenbtn.setPreferredSize(new Dimension(170, 40));
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.addActionListener(this);
		buttonpnl.add(abbrechenbtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image1));
	}

	/**
	 * @author Niklas Nebeling
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.abbrechenbtn) 
		{
			dialog.dispose();
		}

		if (e.getSource() == this.speichernbtn) 
		{
			if(!personenZahlInput.getText().equalsIgnoreCase("")) 
			{
				int sitzpl�tze = raumcontroller.getSitzpl�tze(rnr);
				if(sitzpl�tze < Integer.parseInt(personenZahlInput.getText()))
				{
					JOptionPane.showMessageDialog(null, "Maximale Personenanzahl f�r diesen Raum:" + sitzpl�tze);
				}
				else 
				{
					controller.�bergebePersonenAnzahl(Integer.parseInt(personenZahlInput.getText()));
					extrasSpeichern();
					dialog.dispose();
				}
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte eine Personenanzahl eingeben.");
			}
		}
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	private void extrasSpeichern()
	{
		if(chk1.isSelected())
			controller.erzeugeExtra(chk1.getText());
		if(chk2.isSelected())
			controller.erzeugeExtra(chk2.getText());
		if(chk3.isSelected())
			controller.erzeugeExtra(chk3.getText());
		if(chk4.isSelected())
			controller.erzeugeExtra(chk4.getText());
		if(chk5.isSelected())
			controller.erzeugeExtra(chk5.getText());
		if(chk6.isSelected())
			controller.erzeugeExtra(chk6.getText());
		if(chk7.isSelected())
			controller.erzeugeExtra(chk7.getText());
		if(chk8.isSelected())
			controller.erzeugeExtra(chk8.getText());
		if(chk9.isSelected())
			controller.erzeugeExtra(chk9.getText());
		if(chk10.isSelected())
			controller.erzeugeExtra(chk10.getText());
		if(!sonsttxt.getText().equals(""))
			controller.sonderExtra(sonsttxt.getText());
	}
}