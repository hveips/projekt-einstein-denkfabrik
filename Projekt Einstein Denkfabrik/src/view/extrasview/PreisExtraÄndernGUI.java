package view.extrasview;

/**
 * @author Hendrik Veips
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.LimitedDocument;
import controller.extrascontroller.PreisExtra�ndernController;

public class PreisExtra�ndernGUI extends JFrame implements ActionListener
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JLabel bezeichnungLabel = new JLabel("Bezeichnung");
	private JLabel preisLabel = new JLabel("Preis/Person");
	private JLabel euroLabel = new JLabel("�");
	private JTextField bezeichnungInput = new JTextField();
	private JTextField preisInput = new JTextField();
	private JButton save = new JButton("Speichern");
	private JButton cancel = new JButton("Abbrechen");
	private JButton info = new JButton();
	private JDialog dialog = new JDialog();
	
	private PreisExtras�ndernGUI gui;
	
	private Border buttonBorder;
	private Border border1;
	private Border border2;
	
	private Border border3 = BorderFactory.createLineBorder(Color.RED, 3);
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private String bezeichnung;
	private String preis;
	
	private PreisExtra�ndernController preisExtra�ndernController;
	private boolean success = false;
	
	/**
	 * Der Konstruktor erzeugt die PreisExtra�ndernGUI �ber mehrere Methoden und �bergibt gegebenfalls die Parameter bezeichnung und preis.
	 * @param gui
	 * @param bezeichnung
	 * @param preis
	 */
	public PreisExtra�ndernGUI(PreisExtras�ndernGUI gui, String bezeichnung, String preis)
	{
		this.gui = gui;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		preisExtra�ndernController = new PreisExtra�ndernController(this);
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/PreisIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setEingabe(bezeichnung, preis);
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Gr��e, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Extra-Preis �ndern");
		dialog.setPreferredSize(new Dimension(500, 160));
		dialog.setLocation(710, 460);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld f�r die Eingabe der Bezeichnung.
	 * Zudem wird das Textfeld bezeichnungInput auf nicht-editierbar gesetzt und der �bergebene Parameter bezeichnung gesetzt.
	 * Au�erdem setzt sie ein Panel auf das CENTER-Feld des BorderLayouts und drauf das Label und das Textfeld f�r die Eingabe des Preises und den Button info.
	 * Auch hier wird in das Textfeld preisInput der �bergebene Parameter preis gesetzt.
	 * @param bezeichnung
	 * @param preis
	 */
	public void setEingabe(String bezeichnung, String preis)
	{
		panel1.setPreferredSize(new Dimension(450, 40));
		panel1.setLayout(flow);
		panel1.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel1.setBorder(border1);
		add(panel1, border.NORTH);
		bezeichnungLabel.setPreferredSize(new Dimension(150, 30));
		bezeichnungLabel.setFont(schriftart1);
		panel1.add(bezeichnungLabel);
		bezeichnungInput.setPreferredSize(new Dimension(180, 30));
		bezeichnungInput.setFont(schriftart1);
		bezeichnungInput.setEditable(false);
		panel1.add(bezeichnungInput);	
		bezeichnungInput.setText(bezeichnung);
		panel2.setPreferredSize(new Dimension(450, 40));
		panel2.setLayout(flow);
		panel2.setBackground(Color.lightGray);
		border2 = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel2.setBorder(border2);
		add(panel2, border.CENTER);
		preisLabel.setPreferredSize(new Dimension(150, 30));
		preisLabel.setFont(schriftart1);
		panel2.add(preisLabel);
		preisInput.setPreferredSize(new Dimension(80, 30));
		preisInput.setFont(schriftart1);
		preisInput.setDocument(new LimitedDocument(5));
		panel2.add(preisInput);
		String prize = preis.replace(".", ",");
		preisInput.setText(prize);
		euroLabel.setPreferredSize(new Dimension(15, 30));
		euroLabel.setFont(schriftart1);
		panel2.add(euroLabel);
		info.setPreferredSize(new Dimension(30, 30));
		info.setBackground(Color.WHITE);
		info.addActionListener(this);
		panel2.add(info);
		Image image1 = new ImageIcon(this.getClass().getResource("/InfoIcon.jpeg")).getImage();
		info.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 */
	public void setButtons()
	{
		panel3.setPreferredSize(new Dimension(450, 50));
		panel3.setLayout(flow2);
		panel3.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel3.setBorder(buttonBorder);
		add(panel3, border.SOUTH);
		save.setPreferredSize(new Dimension(170, 40));
		save.setFont(schriftart1);
		save.setBackground(Color.WHITE);
		save.addActionListener(this);
		panel3.add(save);
		Image image1 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		save.setIcon(new ImageIcon(image1));
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel3.add(cancel);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image2));
	}
	
	public String getBezeichnungInput()
	{
		return bezeichnungInput.getText();
	}
	
	public String getPreisInput()
	{
		return preisInput.getText();
	}
	
	/**
	 * Die Methode erzeugt einen roten Rand um die Eingabefelder und f�ngt so die Fehler bei der Eingabe ab und gibt dies anstatt einer Fehlermeldung aus.
	 */
	public void setRedBorder()
	{
		preisInput.setBorder(border3);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		/**
		 * Das System versucht mit Hilfe des PreisExtra�ndernController die �nderung zu speichern; schl�gt dies fehl, wird die Methode setRedBorder aufgerufen, 
		 * ansonsten wird dem Nutzer die erfolgreiche Speicherung gemeldet, die Tabelle wird aktualisiert und die PreisExtra�ndernGUI wird geschlossen.
		 */
		if(e.getSource() == this.save)
		{
			success = preisExtra�ndernController.change();
			if(success == true)
			{
				JOptionPane.showMessageDialog(null, "Daten wurden ge�ndert und gespeichert");
				gui.getUpdatedTableModel();
				dialog.dispose();
			}
			else
			{
				setRedBorder();
			}
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
		if(e.getSource() == this.info)
		{
			JOptionPane.showMessageDialog(null, "Euro und Cent werden mit einem Komma getrennt!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}