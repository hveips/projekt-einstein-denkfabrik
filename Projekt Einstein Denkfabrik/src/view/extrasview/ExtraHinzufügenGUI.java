package view.extrasview;

/**
 * @author Hendrik Veips
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.LimitedDocument;
import controller.documentcontroller.StringOnlyDocument;
import controller.extrascontroller.ExtraHinzufügenEntfernenController;

import java.awt.*;

public class ExtraHinzufügenGUI extends JFrame implements ActionListener
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JPanel panel4 = new JPanel();
	private JPanel panel5 = new JPanel();
	private JLabel bezeichnungLabel = new JLabel("Bezeichnung");
	private JLabel preisLabel = new JLabel("Preis/Person");
	private JLabel euroLabel = new JLabel("€");
	private JLabel artLabel = new JLabel("Art");
	private JTextField bezeichnungInput = new JTextField();
	private JTextField preisInput = new JTextField();
	private JComboBox artInput = new JComboBox();
	private JButton add = new JButton("Hinzufügen");
	private JButton cancel = new JButton("Abbrechen");
	private JButton info = new JButton();
	private JDialog dialog = new JDialog();
	
	private ExtraHinzufügenEntfernenGUI gui;
	
	private Border buttonBorder;
	private Border border1;
	
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private ExtraHinzufügenEntfernenController extraHinzufügenEntfernenController;
	private int[] anzahlEssenGetraenke;
	private boolean success = false;
	
	/**
	 * Der Konstruktor erzeugt die ExtraHinzufügenGUI über mehrere Methoden und übergibt gegebenfalls den Parameter anzahlEssenGetraenke.
	 * @param gui
	 * @param anzahlEssenGetraenke
	 */
	public ExtraHinzufügenGUI(ExtraHinzufügenEntfernenGUI gui, int[] anzahlEssenGetraenke)
	{
		this.gui = gui;
		this.anzahlEssenGetraenke = anzahlEssenGetraenke;
		extraHinzufügenEntfernenController = new ExtraHinzufügenEntfernenController(gui);
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/ExtraIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setEingabe(anzahlEssenGetraenke);
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Größe, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Extra hinzufügen");
		dialog.setPreferredSize(new Dimension(500, 220));
		dialog.setLocation(710, 430);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das CENTER-Feld des BorderLayouts und setzt darauf die Labels und Texfelder bzw. Comboboxen für die Eingaben.
	 * Beim Setzen der ComboBox wird über den Parameter anzahlEssenGetraenke entschieden, ob der Nutzer nur noch Getränke, oder Essen oder beides hinzufügen kann
	 * und dementsprechend die passenden Items hinzugefügt. Die maximale Größe des int-Arrays beträgt {6, 4}.
	 * @param anzahlEssenGetraenke
	 */
	public void setEingabe(int[] anzahlEssenGetraenke)
	{
		panel1.setPreferredSize(new Dimension(450, 170));
		panel1.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel1.setBorder(border1);
		add(panel1, border.CENTER);
		panel2.setPreferredSize(new Dimension(450, 40));
		panel2.setLayout(flow);
		panel2.setBackground(Color.lightGray);
		panel1.add(panel2);
		bezeichnungLabel.setPreferredSize(new Dimension(150, 30));
		bezeichnungLabel.setFont(schriftart1);
		panel2.add(bezeichnungLabel);
		bezeichnungInput.setPreferredSize(new Dimension(150, 30));
		bezeichnungInput.setFont(schriftart1);
		bezeichnungInput.setDocument(new StringOnlyDocument(15));
		panel2.add(bezeichnungInput);
		panel3.setPreferredSize(new Dimension(450, 40));
		panel3.setLayout(flow);
		panel3.setBackground(Color.lightGray);
		panel1.add(panel3);
		preisLabel.setPreferredSize(new Dimension(150, 30));
		preisLabel.setFont(schriftart1);
		panel3.add(preisLabel);
		preisInput.setPreferredSize(new Dimension(80, 30));
		preisInput.setFont(schriftart1);
		preisInput.setDocument(new LimitedDocument(5));
		panel3.add(preisInput);
		euroLabel.setPreferredSize(new Dimension(15, 30));
		euroLabel.setFont(schriftart1);
		panel3.add(euroLabel);
		info.setPreferredSize(new Dimension(30, 30));
		info.setBackground(Color.WHITE);
		info.addActionListener(this);
		panel3.add(info);
		Image image1 = new ImageIcon(this.getClass().getResource("/InfoIcon.jpeg")).getImage();
		info.setIcon(new ImageIcon(image1));
		panel4.setPreferredSize(new Dimension(450, 40));
		panel4.setLayout(flow);
		panel4.setBackground(Color.lightGray);
		panel1.add(panel4);
		artLabel.setPreferredSize(new Dimension(150, 30));
		artLabel.setFont(schriftart1);
		panel4.add(artLabel);
		artInput.setPreferredSize(new Dimension(100, 30));
		artInput.setFont(schriftart1);
		if(anzahlEssenGetraenke[0] == 6 && anzahlEssenGetraenke[1] < 4)
		{
			artInput.addItem("Getränk");
		}
		else if(anzahlEssenGetraenke[1] == 4 && anzahlEssenGetraenke[0] < 6)
		{
			artInput.addItem("Essen");
		}
		else
		{
			artInput.addItem("Essen");
			artInput.addItem("Getränk");
		}
		panel4.add(artInput);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 */
	public void setButtons()
	{
		panel5.setPreferredSize(new Dimension(450, 50));
		panel5.setLayout(flow2);
		panel5.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel5.setBorder(buttonBorder);
		add(panel5, border.SOUTH);
		add.setPreferredSize(new Dimension(170, 40));
		add.setFont(schriftart1);
		add.setBackground(Color.WHITE);
		add.addActionListener(this);
		panel5.add(add);
		Image image1 = new ImageIcon(this.getClass().getResource("/HinzufügenIcon.jpeg")).getImage();
		add.setIcon(new ImageIcon(image1));
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel5.add(cancel);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image2));
	}
	
	/**
	 * Die Methode erzeugt einen roten Rand um die Eingabefelder und fängt so die Fehler bei der Eingabe ab und gibt dies anstatt einer Fehlermeldung aus.
	 */
	public void setRedBorder()
	{
		if(bezeichnungInput.getText().equals(""))
		{
			bezeichnungInput.setBorder(border2);
		}
		else
		{
			bezeichnungInput.setBorder(border3);
		}
		if(preisInput.getText().equals(""))
		{
			preisInput.setBorder(border2);
		}
		else
		{
			preisInput.setBorder(border3);
		}
		if(bezeichnungInput.getBorder() == border3 && preisInput.getBorder() == border3)
		{
			bezeichnungInput.setBorder(border2);
			preisInput.setBorder(border2);
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		/**
		 * Das System holt sich die Eingaben aus den Textfeldern bzw. der Combobox und versucht über den ExtraHinzufügenEntfernenController das Extra hinzuzufügen.
		 * Ist das Speichern erfolgreich, wird das Fenster geschlossen und die Tabelle aktualisiert, ansonsten wird die Methode setRedBorder() aufgerufen.
		 */
		if(e.getSource() == this.add)
		{
			String bezeichnung = bezeichnungInput.getText();
			String preis = preisInput.getText();
			String art = (String) artInput.getSelectedItem(); 
			success = extraHinzufügenEntfernenController.addExtra(bezeichnung, preis, art);
			if(success == true)
			{
				JOptionPane.showMessageDialog(null, "Daten wurden geändert und gespeichert");
				gui.getUpdatedTableModel();
				dialog.dispose();
			}
			else
			{
				setRedBorder();
			}
		}
		if(e.getSource() == this.cancel)
		{
			dialog.dispose();
		}
		if(e.getSource() == this.info)
		{
			JOptionPane.showMessageDialog(null, "Euro und Cent werden mit einem Komma getrennt!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}