package view.kundenview;

/**
 * @author Christoph Hoppe, Marvin Plepis
 */

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import controller.kundencontroller.Kunde�ndernController;
import controller.documentcontroller.IntegerOnlyDocument;
import controller.documentcontroller.LimitedDocument;
import controller.documentcontroller.StringOnlyDocument;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Kunde�ndernGUI extends JFrame implements ActionListener {
	private JPanel eingabePanel = new JPanel();
	private JPanel idPanel = new JPanel();
	private JPanel anredePanel = new JPanel();
	private JPanel vornamePanel = new JPanel();
	private JPanel nachnamePanel = new JPanel();
	private JPanel plzPanel = new JPanel();
	private JPanel ortPanel = new JPanel();
	private JPanel strassePanel = new JPanel();
	private JPanel hnrPanel = new JPanel();
	private JPanel eMailPanel = new JPanel();
	private JPanel arbeitPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JLabel id = new JLabel("Kunden-ID");
	private JLabel anrede = new JLabel("Anrede");
	private JLabel vorname = new JLabel("Vorname");
	private JLabel nachname = new JLabel("Nachname");
	private JLabel plz = new JLabel("Postleitzahl");
	private JLabel ort = new JLabel("Ort");
	private JLabel strasse = new JLabel("Stra�e");
	private JLabel hnr = new JLabel("Hausnummer");
	private JLabel eMail = new JLabel("E-Mail");
	private JLabel arbeit = new JLabel("Arbeitsplatz");
	private JTextField idInput = new JTextField();
	private JComboBox anredeInput = new JComboBox();
	private JTextField vornameInput = new JTextField();
	private JTextField nachnameInput = new JTextField();
	private JTextField plzInput = new JTextField();
	private JTextField ortInput = new JTextField();
	private JTextField strasseInput = new JTextField();
	private JTextField hnrInput = new JTextField();
	private JTextField eMailInput = new JTextField();
	private JTextField arbeitInput = new JTextField();
	private JButton cancel = new JButton("Abbrechen");
	private JButton save = new JButton("Speichern");
	private JDialog dialog;

	private Kunde�ndernEntfernenStatistikGUI gui;

	private Border eingabeBorder;
	private Border buttonBorder;

	private String title[] = { "Herr", "Frau" };

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow3 = new FlowLayout(FlowLayout.RIGHT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);

	private Kunde�ndernController kuController;

	/**
	 * Der Konstruktor erzeugt die Kunde�ndernGUI und �bernimmt Parmeter
	 * 
	 * @param gui
	 */
	public Kunde�ndernGUI(Kunde�ndernEntfernenStatistikGUI gui) {
		this.gui = gui;
		this.kuController = new Kunde�ndernController(this, gui);
		erzeugeDialog();
		Image image1 = new ImageIcon(this.getClass().getResource("/KundenIcon.jpeg")).getImage();
		dialog.setIconImage(image1);
		setEingabePanel();
		setEingabeID();
		setEingabeTitel();
		setEingabeVorname();
		setEingabeNachname();
		setEingabePLZ();
		setEingabeOrt();
		setEingabeStrasse();
		setEingabeHnr();
		setEingabeEMail();
		setEingabeArbeit();
		kuController.ladeDatenVonAusgew�hlterZeile();
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Die Methode erzeugt einen neuen JDialog und definiert deren Eigenschaften
	 */
	public void erzeugeDialog() {
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Kunde �ndern");
		dialog.setPreferredSize(new Dimension(700, 550));
		dialog.setLocation(610, 265);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}

	/**
	 * Die Methode definiert das Panel f�r die verschiedenen Eingaben und weist u.a
	 * das Flowlayout zu. Auf diesem Panel werden sp�ter alle Panel f�r die
	 * Eingaben hinzugef�gt.
	 */
	private void setEingabePanel() {
		eingabePanel.setPreferredSize(new Dimension(650, 450));
		eingabePanel.setLayout(flow);
		eingabePanel.setBackground(Color.lightGray);
		eingabeBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		eingabePanel.setBorder(eingabeBorder);
		add(eingabePanel, border.CENTER);
	}

	/**
	 * Die Methode definiert das idLabel,Textfeld und idPanel, welche dem
	 * eingabePanel zugeordnet werden. Das Textfeld darf nicht editiert werden,damit
	 * keine Inkonsistenzen in der KundenDatenbank entstehen.
	 */
	public void setEingabeID() {
		idPanel.setPreferredSize(new Dimension(630, 40));
		idPanel.setLayout(flow2);
		idPanel.setBackground(Color.lightGray);
		eingabePanel.add(idPanel);
		id.setPreferredSize(new Dimension(150, 30));
		id.setFont(schriftart1);
		idPanel.add(id);
		idInput.setPreferredSize(new Dimension(460, 30));
		idInput.setEditable(false);
		idInput.setFont(schriftart1);
		idPanel.add(idInput);
	}

	/**
	 * Die Methode definiert das anredeLabel,ComboBox und anredePanel, welches auf
	 * dem eingabePanel platziert wird.
	 * 
	 */
	public void setEingabeTitel() {
		anredePanel.setPreferredSize(new Dimension(630, 40));
		anredePanel.setLayout(flow2);
		anredePanel.setBackground(Color.lightGray);
		eingabePanel.add(anredePanel);
		anrede.setPreferredSize(new Dimension(150, 30));
		anrede.setFont(schriftart1);
		anredePanel.add(anrede);
		anredeInput.setPreferredSize(new Dimension(460, 30));
		anredeInput.setFont(schriftart1);
		// for Schleife durchl�uft das Array vom Typ String und f�gt den Output
		// anredeInput zu.
		for (int i = 0; i < title.length; i++) {
			anredeInput.addItem(title[i]);
		}
		anredePanel.add(anredeInput);
	}

	/**
	 * Die Methode definiert das vornameLabel,TextField und vornamePanel, welches
	 * auf dem eingabePanel platziert wird. Das TextField nimmt nur String Werte mit
	 * max. 35 Zeichen entgegen.
	 */
	public void setEingabeVorname() {
		vornamePanel.setPreferredSize(new Dimension(630, 40));
		vornamePanel.setLayout(flow2);
		vornamePanel.setBackground(Color.lightGray);
		eingabePanel.add(vornamePanel);
		vorname.setPreferredSize(new Dimension(150, 30));
		vorname.setFont(schriftart1);
		vornamePanel.add(vorname);
		vornameInput.setPreferredSize(new Dimension(460, 30));
		vornameInput.setFont(schriftart1);
		vornamePanel.add(vornameInput);
		vornameInput.setDocument(new StringOnlyDocument(35));
	}

	/**
	 * Die Methode definiert das nachnameLabel,TextField und nachnamePanel, welches
	 * auf dem eingabePanel platziert wird. Das TextField nimmt nur String Werte
	 * mit max. 35 Zeichen entgegen.
	 */
	public void setEingabeNachname() {
		nachnamePanel.setPreferredSize(new Dimension(630, 40));
		nachnamePanel.setLayout(flow2);
		nachnamePanel.setBackground(Color.lightGray);
		eingabePanel.add(nachnamePanel);
		nachname.setPreferredSize(new Dimension(150, 30));
		nachname.setFont(schriftart1);
		nachnamePanel.add(nachname);
		nachnameInput.setPreferredSize(new Dimension(460, 30));
		nachnameInput.setFont(schriftart1);
		nachnamePanel.add(nachnameInput);
		nachnameInput.setDocument(new StringOnlyDocument(35));
	}

	/**
	 * Die Methode definiert das Plzlabel,TextField und Plzpanel, welches auf dem
	 * eingabePanel platziert wird. Das TextField nimmt nur Integer Werte mit max. 5
	 * Zeichen entgegen.
	 */
	public void setEingabePLZ() {
		plzPanel.setPreferredSize(new Dimension(630, 40));
		plzPanel.setLayout(flow2);
		plzPanel.setBackground(Color.lightGray);
		eingabePanel.add(plzPanel);
		plz.setPreferredSize(new Dimension(150, 30));
		plz.setFont(schriftart1);
		plzPanel.add(plz);
		plzInput.setPreferredSize(new Dimension(460, 30));
		plzInput.setFont(schriftart1);
		plzPanel.add(plzInput);
		plzInput.setDocument(new IntegerOnlyDocument(5));
	}

	/**
	 * Die Methode definiert das Ortlabel,TextField und ortPanel, welches auf dem
	 * eingabePanel platziert wird. Das TextField nimmt nur String Werte mit max. 25
	 * Zeichen entgegen.
	 */
	public void setEingabeOrt() {
		ortPanel.setPreferredSize(new Dimension(630, 40));
		ortPanel.setLayout(flow2);
		ortPanel.setBackground(Color.lightGray);
		eingabePanel.add(ortPanel);
		ort.setPreferredSize(new Dimension(150, 30));
		ort.setFont(schriftart1);
		ortPanel.add(ort);
		ortInput.setPreferredSize(new Dimension(460, 30));
		ortInput.setFont(schriftart1);
		ortPanel.add(ortInput);
		ortInput.setDocument(new StringOnlyDocument(25));
	}

	/**
	 * Die Methode definiert das Strasselabel,TextField und StrassePanel, welchse
	 * auf dem eingabePanel platziert wird. Das TextField nimmt nur String Werte mit
	 * max. 30 Zeichen entgegen.
	 */
	public void setEingabeStrasse() {
		strassePanel.setPreferredSize(new Dimension(630, 40));
		strassePanel.setLayout(flow2);
		strassePanel.setBackground(Color.lightGray);
		eingabePanel.add(strassePanel);
		strasse.setPreferredSize(new Dimension(150, 30));
		strasse.setFont(schriftart1);
		strassePanel.add(strasse);
		strasseInput.setPreferredSize(new Dimension(460, 30));
		strasseInput.setFont(schriftart1);
		strassePanel.add(strasseInput);
		strasseInput.setDocument(new StringOnlyDocument(30));
	}

	/**
	 * Die Methode definiert das HnrLabel,TextField und HnrPanel, welches auf dem
	 * eingabePanel platziert wird.
	 */
	public void setEingabeHnr() {
		hnrPanel.setPreferredSize(new Dimension(630, 40));
		hnrPanel.setLayout(flow2);
		hnrPanel.setBackground(Color.lightGray);
		eingabePanel.add(hnrPanel);
		hnr.setPreferredSize(new Dimension(150, 30));
		hnr.setFont(schriftart1);
		hnrPanel.add(hnr);
		hnrInput.setPreferredSize(new Dimension(460, 30));
		hnrInput.setFont(schriftart1);
		hnrPanel.add(hnrInput);
	}

	/**
	 * Die Methode definiert das eMaillabel,TextField und emailPanel, welches auf
	 * dem eingabePanel platziert wird. Das TextField nimmt max. 35 Zeichen
	 * entgegen.
	 */
	public void setEingabeEMail() {
		eMailPanel.setPreferredSize(new Dimension(630, 40));
		eMailPanel.setLayout(flow2);
		eMailPanel.setBackground(Color.lightGray);
		eingabePanel.add(eMailPanel);
		eMail.setPreferredSize(new Dimension(150, 30));
		eMail.setFont(schriftart1);
		eMailPanel.add(eMail);
		eMailInput.setPreferredSize(new Dimension(460, 30));
		eMailInput.setFont(schriftart1);
		eMailPanel.add(eMailInput);
		eMailInput.setDocument(new LimitedDocument(35));
	}

	/**
	 * Die Methode definiert das arbeitLabel,TextField und arbeitPanel, welches auf
	 * dem eingabePanel platziert wird. Das TextField nimmt max. 30 Zeichen
	 * entgegen.
	 */
	public void setEingabeArbeit() {
		arbeitPanel.setPreferredSize(new Dimension(630, 40));
		arbeitPanel.setLayout(flow2);
		arbeitPanel.setBackground(Color.lightGray);
		eingabePanel.add(arbeitPanel);
		arbeit.setPreferredSize(new Dimension(150, 30));
		arbeit.setFont(schriftart1);
		arbeitPanel.add(arbeit);
		arbeitInput.setPreferredSize(new Dimension(460, 30));
		arbeitInput.setFont(schriftart1);
		arbeitPanel.add(arbeitInput);
		arbeitInput.setDocument(new LimitedDocument(30));
	}

	/**
	 * Die Methode definiert das buttonpanel und ordnet ihm das FlowLayout.RIGHT,
	 * sowie Border,size,font etc. zu. Es werden weiterhin zwei Button definiert und
	 * auf dem buttonpanel platziert.
	 */
	public void setButtons() {
		buttonPanel.setPreferredSize(new Dimension(650, 50));
		buttonPanel.setLayout(flow3);
		buttonPanel.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 25, 30, Color.lightGray);
		buttonPanel.setBorder(buttonBorder);
		add(buttonPanel, border.SOUTH);
		save.setPreferredSize(new Dimension(170, 40));
		save.setFont(schriftart1);
		save.setBackground(Color.WHITE);
		save.addActionListener(this);
		buttonPanel.add(save);
		Image image1 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		save.setIcon(new ImageIcon(image1));
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		buttonPanel.add(cancel);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image2));
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld Kunden ID zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getIDInput() {
		return idInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Combobox Anrede zur�ck
	 * @author Marvin Plepis
	 */
	public JComboBox getAnredeInput() {
		return anredeInput;
	}

	/**
	 *
	 * @return gibt Inhalt aus Textfeld Vorname zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getVornameInput() {
		return vornameInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld Nachname zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getNachnameInput() {
		return nachnameInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld PLZ zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getPlzInput() {
		return plzInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld Ort zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getOrtInput() {
		return ortInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld Stra�e zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getStrasseInput() {
		return strasseInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld Hausnummer zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getHnrInput() {
		return hnrInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld eMail zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField geteMailInput() {
		return eMailInput;
	}

	/**
	 * 
	 * @return gibt Inhalt aus Textfeld Arbeitsplatz zur�ck
	 * @author Marvin Plepis
	 */
	public JTextField getArbeitsplatzInput() {
		return arbeitInput;
	}

	/**
	 * 
	 * @return gibt den gesamten Dialog zur�ck
	 * @author Marvin Plepis
	 */
	public JDialog getJDialog() {
		return dialog;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.cancel) {
			dialog.dispose();
		}
		if (e.getSource() == this.save) {
			kuController.changeButtonAction();
		}
	}
}