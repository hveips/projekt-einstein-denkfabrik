package view.kundenview;

/**
 * @author Hendrik Veips, Marvin Plepis
 */
  
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.kundencontroller.KundeController;
import controller.kundencontroller.Kunde�ndernEntfernenStatistikController;
import controller.pdfcontroller.StatistikPDF;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.Kunde;
import model.tablemodels.KundenTableModel;
import view.StartansichtGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Kunde�ndernEntfernenStatistikGUI extends JFrame implements ActionListener,ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private JScrollPane scrollV;
	private JButton cancel = new JButton("Abbrechen");
	private JButton change = new JButton("�ndern");
	private JButton delete = new JButton("Entfernen");
	private JButton create = new JButton("Erstellen");
	private JDialog dialog;
	
	private StartansichtGUI gui;
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private StatistikPDF statistik = new StatistikPDF();
	
	private JTable table;
	private KundenTableModel tableModel;
	private KundeController db = new KundeController();
	private TableRowSorter<TableModel> sorter;
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	
	private Kunde�ndernEntfernenStatistikController kuController;
	
	/**
	 * Der Konstruktor erzeugt die Raum�ndernAktivierenDeaktivierenGUI �ber mehrere Methoden und �bergibt gegebenfalls den Parameter a.
	 * @param gui
	 * @param a
	 */
	public Kunde�ndernEntfernenStatistikGUI(StartansichtGUI gui, int a)
	{
		this.gui = gui;
		this.kuController = new Kunde�ndernEntfernenStatistikController(this);
		erzeugeDialog(a);
		Image image1 = new ImageIcon(this.getClass().getResource("/KundenIcon.jpeg")).getImage();
		dialog.setIconImage(image1);
		setStichwortsuche();
		setTable();
		setButtons(a);
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Gr��e, Location, BorderLayout, usw.).
	 * Ist der �bergebene Parameter a = 0, wird der Titel "Kunde �ndern" gesetzt; falls a = 1 ist, wird der Titel "Kunde entfernen" gesetzt;
	 * falls a = 2 ist, wird der Titel "Erstelle Kunden-Statistik" gesetzt.
	 * @param a
	 */
	public void erzeugeDialog(int a)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		if(a == 0)
		{
			dialog.setTitle("Kunde �ndern");
		}
		else if(a == 1)
		{
			dialog.setTitle("Kunde entfernen");
		}
		else
		{
			dialog.setTitle("Erstelle Kunden-Statistik");
		}
		dialog.setPreferredSize(new Dimension(1000, 650));
		dialog.setLocation(460, 215);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode �bergibt der Tabelle das KundenTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach �bergibt sie der Tabelle noch den sorter, damit der Nutzer die M�glichkeit hat die Eintr�ge zu sortieren und sie �bergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst m�glich macht.
	 */
	private void setTable() 
	{
		tableModel = new KundenTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);
		tabellenwerteZentrieren();
		tabellenbreiteFestlegen();
		DocumentListener search = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(search);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche() 
	{
		panel1.setPreferredSize(new Dimension(940, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(680, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}

	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 * �ber den �bergebenen Parameter a entscheidet sie zus�tzlich, ob der Button change, der Button delete oder der Button create gesetzt wird.
	 * @param a
	 */
	private void setButtons(int a) 
	{
		panel2.setPreferredSize(new Dimension(940, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		if(a == 0)
		{
			change.setPreferredSize(new Dimension(160, 40));
			change.setFont(schriftart1);
			change.setBackground(Color.WHITE);
			change.addActionListener(this);
			panel2.add(change);
			Image image2 = new ImageIcon(this.getClass().getResource("/BearbeitenIcon.jpeg")).getImage();
			change.setIcon(new ImageIcon(image2));
		}
		else if(a == 1)
		{
			delete.setPreferredSize(new Dimension(160, 40));
			delete.setFont(schriftart1);
			delete.setBackground(Color.WHITE);
			delete.addActionListener(this);
			panel2.add(delete);
			Image image3 = new ImageIcon(this.getClass().getResource("/L�schenIcon.jpeg")).getImage();
			delete.setIcon(new ImageIcon(image3));
		}
		else
		{
			create.setPreferredSize(new Dimension(160, 40));
			create.setBackground(Color.WHITE);
			create.setFont(schriftart1);
			create.addActionListener(this);
			panel2.add(create);
			Image image1 = new ImageIcon(this.getClass().getResource("/StatistikIcon.jpeg")).getImage();
			create.setIcon(new ImageIcon(image1));
		}
		cancel.setPreferredSize(new Dimension(160, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image4 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image4));
	}

	/**
	 * Zentriert den Inhalt der einzelnden Zellen.
	 * @author Marvin Plepis
	 */
	private void tabellenwerteZentrieren() 
	{
		// Tabellenwerte zentrieren
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	/**
	 * Legt die Breite der Zeilen fest
	 * @author Marvin Plepis
	 */
	private void tabellenbreiteFestlegen() 
	{
		// Tabellenspalten Breite festlegen
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(45);
		table.getColumnModel().getColumn(5).setPreferredWidth(140);
		table.getColumnModel().getColumn(6).setPreferredWidth(40);
		table.getColumnModel().getColumn(7).setPreferredWidth(220);
		table.getColumnModel().getColumn(8).setPreferredWidth(140);
	}
	
	/**
	 * Methode l�dt die Kunden Tabelle neu
	 * @author Marvin Plepis
	 * @return gibt die aktualisierte Kunden Tabelle zur�ck
	 */
	public TableModel loadTable() 
	{
		tableModel.loadTable();
		return tableModel;
	}
	
	/**
	 * @author Marvin Plepis
	 * @return Gibt Inhalt des Suchfelds zur�ck
	 */
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}

	/**
	 * @author Marvin Plepis
	 * @return gibt Kunden Tabelle zur�ck
	 */
	public JTable getTable() 
	{
		return this.table;
	}

	/**
	 * @author Marvin Plepis
	 */
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes Anrede zur�ck
	 */
	public String getAnredeRow() 
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde kd = tableModel.getRow(row);
		String anrede = kd.getAnrede();
		return anrede;
	}
	
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes Vorname zur�ck
	 */
	public String getVornameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		String vorname = ku.getVorname();
		return vorname;
	}

	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes Nachname zur�ck
	 */
	public String getNachnameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		String nachname = ku.getName();
		return nachname;
	}

	/**
	 * @author Marvin Plepis
	 * @return gibt den ausgew�hlten Kunden zur�ck
	 */
	public Kunde getKundeRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		return ku;
	}
	
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes Kunden ID zur�ck
	 */
	public int getKundenIdRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		int kuid = ku.getKundenID();
		return kuid;
	}
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes Stra�e zur�ck
	 */
	public String getStra�eRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		String strasse = ku.getStra�e();
		return strasse;
	}
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes HausNr zur�ck
	 */
	public String getHausNrRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		String hausNr = ku.getHausNr();
		return hausNr;
	}
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes PLZ zur�ck
	 */
	public int getPlzRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		int plz = ku.getPlz();
		return plz;
	}
	/**
	 * @author Marvin Plepis
	 * @return gibt Inhalt der ausgew�hlten Reihe des Textfeldes Ort zur�ck
	 */
	public String getOrtRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Kunde ku = tableModel.getData(row);
		String ort = ku.getOrt();
		return ort;
	}
	
	/**
	 * Buttons der jeweiligen Views
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.change) 
		{
			kuController.aendernButtonAction();
		}
		if (e.getSource() == this.delete) 
		{
			kuController.loeschenButtonAction();
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
		/**
		 * Speichert Daten in Variablen, wenn Kunde ausgew�hlt wurde
		 * Kein Kunde ausgew�hlt - Fehlermeldung
		 * @author Marvin Plepis
		 */
		// Bei Bet�tigung des Knopfes Statistik erstellen, wird eine PDF generiert und der Inhalt des ausgew�hlten Kunden (Reihe/Row) an die StatistikPDF Klasse �bergeben
		if(e.getSource() == this.create) 
		{
			int selectedRow = getTable().getSelectedRow();
			if (selectedRow == -1)
			{
				JOptionPane.showMessageDialog(gui, "Bitte w�hlen Sie einen Kunden aus, dessen Statistik erstellt werden soll.", "Fehler", JOptionPane.ERROR_MESSAGE);
			} else
			{
				int kuid = getKundenIdRow();
				String anrede = getAnredeRow();
				String name = getNachnameRow();
				String vorname = getVornameRow();
				String strasse = getStra�eRow();
				String hausNr = getHausNrRow();
				int plz = getPlzRow();
				String ort = getOrtRow();
				statistik.getPdfContent(kuid, anrede, name, vorname, strasse, hausNr , plz, ort);
				
			}
		}
	}
}