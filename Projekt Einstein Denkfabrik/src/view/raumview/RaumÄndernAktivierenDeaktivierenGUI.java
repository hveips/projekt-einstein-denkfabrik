package view.raumview;

/**
 * @author Hendrik Veips, Niklas Nebeling
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.raumcontroller.RaumÄndernAktivierenDeaktivierenController;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.tablemodels.RaumTableModel;
import view.StartansichtGUI;

public class RaumÄndernAktivierenDeaktivierenGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTable table;
	private RaumTableModel tableModel;
	private JTextField cueInput = new JTextField();
	private JScrollPane scrollV;
	private JButton cancel = new JButton("Abbrechen");
	private JButton change = new JButton("Ändern");
	private JButton active = new JButton("Aktivieren");
	private JButton deactive = new JButton("Deaktivieren");
	private JDialog dialog = new JDialog();
	
	private StartansichtGUI gui;
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;
	
	private RaumÄndernAktivierenDeaktivierenController raumÄndernAktivierenDeaktivierenController;
	
	/**
	 * Der Konstruktor erzeugt die RaumÄndernAktivierenDeaktivierenGUI über mehrere Methoden und übergibt gegebenfalls den Parameter a.
	 * @param gui
	 * @param a
	 */
	public RaumÄndernAktivierenDeaktivierenGUI(StartansichtGUI gui, boolean aendern)
	{
		this.gui = gui;
		erzeugeDialog(aendern);
		Image image4 = new ImageIcon(this.getClass().getResource("/RaumIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		raumÄndernAktivierenDeaktivierenController = new RaumÄndernAktivierenDeaktivierenController(this);
		setStichwortsuche();
		setTable();
		setButtons(aendern);
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Größe, Location, BorderLayout, usw.).
	 * Ist der übergebene Parameter a = 0, wird der Titel "Raum ändern" gesetzt; falls a = 1 ist, wird der Titel "Raum aktivieren" gesetzt;
	 * falls a = 2 ist, wird der Titel "Raum deaktivieren" gesetzt.
	 * @param a
	 */
	public void erzeugeDialog(boolean aendern)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		if(aendern == true)
		{
			dialog.setTitle("Raum ändern");
		}
		else
		{
			dialog.setTitle("Raum de/aktivieren");
		}
		dialog.setPreferredSize(new Dimension(800, 590));
		dialog.setLocation(560, 245);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 * Über den übergebenen Parameter a entscheidet sie zusätzlich, ob der Button change, der Button active oder der Button deactive gesetzt wird.
	 * @param a
	 */
	private void setButtons(boolean aendern) 
	{
		panel2.setPreferredSize(new Dimension(740, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		if(aendern == true)
		{
			change.setPreferredSize(new Dimension(180, 40));
			change.setFont(schriftart1);
			change.setBackground(Color.WHITE);
			change.addActionListener(this);
			panel2.add(change);
			Image image2 = new ImageIcon(this.getClass().getResource("/VorwärtsGroßIcon.jpeg")).getImage();
			change.setIcon(new ImageIcon(image2));
		}
		else
		{
			active.setPreferredSize(new Dimension(180, 40));
			active.setFont(schriftart1);
			active.setBackground(Color.WHITE);
			active.addActionListener(this);
			panel2.add(active);
			Image image4 = new ImageIcon(this.getClass().getResource("/AktivierenIcon.jpeg")).getImage();
			active.setIcon(new ImageIcon(image4));
			deactive.setPreferredSize(new Dimension(180, 40));
			deactive.setFont(schriftart1);
			deactive.setBackground(Color.WHITE);
			deactive.addActionListener(this);
			panel2.add(deactive);
			Image image3 = new ImageIcon(this.getClass().getResource("/DeaktivierenIcon.jpeg")).getImage();
			deactive.setIcon(new ImageIcon(image3));
		}
		cancel.setPreferredSize(new Dimension(180, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}

	/**
	 * Die Methode übergibt der Tabelle das RaumTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach übergibt sie der Tabelle noch den sorter, damit der Nutzer die Möglichkeit hat die Einträge zu sortieren und sie übergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst möglich macht.
	 */
	private void setTable() 
	{
		tableModel = new RaumTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);
		raumÄndernAktivierenDeaktivierenController.aktivierenDeaktivierenUnterscheidung();
		tabellenwerteZentrieren();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche() 
	{
		panel1.setPreferredSize(new Dimension(740, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(480, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}
	
	public JButton getActiveButton()
	{
		return active;
	}
	
	public JButton getDeactiveButton()
	{
		return deactive;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public JTable getTable() 
	{
		return this.table;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public RaumTableModel getTableModel() 
	{
		return tableModel;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	/**
	 * Diese Methode bestimmt die Aktionen die ausgeführt werden wenn Buttons gedrückt werden.
	 * Prüft ob eine Tabellenzeile ausgewählt ist und führt dann controller-Methode aus,
	 * sonst Fehlermeldung.
	 * @author Niklas Nebeling, Hendrik Veips
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.change)    
		{
			if(table.getSelectedRow() != -1) 							//Es muss eine Zeile ausgewählt sein (Sonst ist SelectedRow -1)
			{
				int row = table.convertRowIndexToModel(table.getSelectedRow());
				raumÄndernAktivierenDeaktivierenController.übergebeReihe(row);		//ausgewählte Zeile wird übergeben
				raumÄndernAktivierenDeaktivierenController.erzeugeRaumÄndernGUI();
			} 
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einen Raum auswählen.");
			}
			
		}
		/**
		 * Das System aktiviert/deaktiviert einen ausgewählten Raum mit Hilfe des RaumÄndernAktivierenDeaktivierenControllers und aktualisiert die Tabelle.
		 * @author Hendrik Veips
		 */
		if(e.getSource() == this.active)
		{
			if(table.getSelectedRow() != -1)
			{
				raumÄndernAktivierenDeaktivierenController.übergebeReihe(table.convertRowIndexToModel(table.getSelectedRow()));
				raumÄndernAktivierenDeaktivierenController.raumAktivieren();
				raumÄndernAktivierenDeaktivierenController.update();
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einen Raum auswählen.");
			}
		}
		if (e.getSource() == this.deactive) 
		{
			if(table.getSelectedRow() != -1) 							
			{
				raumÄndernAktivierenDeaktivierenController.übergebeReihe(table.convertRowIndexToModel(table.getSelectedRow()));		
				raumÄndernAktivierenDeaktivierenController.raumDeaktivieren();
				raumÄndernAktivierenDeaktivierenController.update();
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einen Raum auswählen.");
			}
			
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
	}
}