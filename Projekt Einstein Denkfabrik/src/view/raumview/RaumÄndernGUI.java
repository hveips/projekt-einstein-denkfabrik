package view.raumview;

/**
 * @author Hendrik Veips, Niklas Nebeling
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.IntegerOnlyDocument;
import controller.raumcontroller.Raum�ndernAktivierenDeaktivierenController;

public class Raum�ndernGUI extends JFrame implements ActionListener 
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel buttonpnl1 = new JPanel();
	private JPanel buttonpnl2 = new JPanel();
	private JPanel buttonpnl3 = new JPanel();
	private JLabel sitzpl�tzelbl = new JLabel("Anzahl Sitzpl�tze:");
	private JTextField sitzpl�tzetxt = new JTextField();
	private JButton hinzuf�genbtn = new JButton("Ausstattung ver�ndern");
	private JButton abbrechenbtn = new JButton("Abbrechen");
	private JButton speichernbtn = new JButton("Speichern");
	private JDialog dialog = new JDialog();
	
	private Border border1;
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	
	private Raum�ndernAktivierenDeaktivierenGUI gui;
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private Raum�ndernAktivierenDeaktivierenController controller;

	/**
	 * Der Konstruktor erzeugt die Raum�ndernGUI �ber mehrere Methoden und �bergibt gegebenfalls den Parameter sitzpl�tze.
	 * @param gui
	 * @param controller
	 * @param sitzpl�tze
	 */
	public Raum�ndernGUI(Raum�ndernAktivierenDeaktivierenGUI gui, Raum�ndernAktivierenDeaktivierenController controller, int sitzpl�tze) 
	{
		this.gui = gui;
		this.controller = controller; 
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/RaumIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setEingabeSitzpl�tze();
		sitzpl�tzetxt.setText(Integer.toString(sitzpl�tze));
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Gr��e, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Weiter");
		dialog.setPreferredSize(new Dimension(500, 265));
		dialog.setLocation(710, 405);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das CENTER-Feld des BorderLayouts und setzt darauf ein Panel mit dem Label und dem Textfeld f�r die Eingabe der Sitzpl�tze.
	 */
	public void setEingabeSitzpl�tze()
	{
		panel1.setPreferredSize(new Dimension(450, 215));
		panel1.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(border1);
		add(panel1, border.CENTER);
		panel2.setPreferredSize(new Dimension(430, 40));
		panel2.setLayout(flow);
		panel2.setBackground(Color.lightGray);
		panel1.add(panel2);
		sitzpl�tzelbl.setPreferredSize(new Dimension(150, 30));
		sitzpl�tzelbl.setFont(schriftart1);
		panel2.add(sitzpl�tzelbl);
		sitzpl�tzetxt.setPreferredSize(new Dimension(100, 30));
		sitzpl�tzetxt.setFont(schriftart1);
		sitzpl�tzetxt.setDocument(new IntegerOnlyDocument(4));
		panel2.add(sitzpl�tzetxt);
	}
	
	/**
	 * Die Methode setzt 3 Panels � 1 Button auf das Panel vom CENTER-Feld des BorderLayouts.
	 */
	public void setButtons()
	{
		buttonpnl1.setPreferredSize(new Dimension(430, 50));
		buttonpnl1.setLayout(flow2);
		buttonpnl1.setBackground(Color.lightGray);
		panel1.add(buttonpnl1);
		hinzuf�genbtn.setPreferredSize(new Dimension(260, 40));
		hinzuf�genbtn.setFont(schriftart1);
		hinzuf�genbtn.setBackground(Color.WHITE);
		hinzuf�genbtn.addActionListener(this);
		buttonpnl1.add(hinzuf�genbtn);
		Image image3 = new ImageIcon(this.getClass().getResource("/AusstattungIcon.jpeg")).getImage();
		hinzuf�genbtn.setIcon(new ImageIcon(image3));
		buttonpnl2.setPreferredSize(new Dimension(430, 50));
		buttonpnl2.setLayout(flow2);
		buttonpnl2.setBackground(Color.lightGray);
		panel1.add(buttonpnl2);
		speichernbtn.setPreferredSize(new Dimension(160, 40));
		speichernbtn.setFont(schriftart1);
		speichernbtn.setBackground(Color.WHITE);
		speichernbtn.addActionListener(this);
		buttonpnl2.add(speichernbtn);
		Image image2 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		speichernbtn.setIcon(new ImageIcon(image2));
		buttonpnl3.setPreferredSize(new Dimension(430, 50));
		buttonpnl3.setLayout(flow2);
		buttonpnl3.setBackground(Color.lightGray);
		panel1.add(buttonpnl3);
		abbrechenbtn.setPreferredSize(new Dimension(160, 40));
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.addActionListener(this);
		buttonpnl3.add(abbrechenbtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image1));
	}

	/**
	 * Diese Methode bestimmt die Aktionen die ausgef�hrt werden wenn Buttons gedr�ckt werden.
	 * Pr�ft ob Eingaben vollst�ndig und korrekt sind und f�hrt dann controller-Methoden aus,
	 * sonst rote Markierung des Fehlers.
	 * @author Niklas Nebeling
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.abbrechenbtn) 
		{
			dialog.dispose();
		}
		if (e.getSource() == this.speichernbtn) 
		{
			try 
			{
				int sitzpl�tze = Integer.parseInt(sitzpl�tzetxt.getText());
				sitzpl�tzetxt.setBorder(border3);
				controller.raum�ndern(sitzpl�tze);
				controller.update();
				dialog.dispose();
			}  
			catch (NumberFormatException ex) 
			{
				sitzpl�tzetxt.setBorder(border2);
			}
		}
		if (e.getSource() == this.hinzuf�genbtn) 
		{
			controller.erzeugeAusstattungVer�ndernGUI();
		}
	}
}