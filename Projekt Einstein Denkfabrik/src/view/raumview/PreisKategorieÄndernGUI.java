package view.raumview;

/**
 * @author Hendrik Veips
 */

import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.IntegerOnlyDocument;
import controller.raumcontroller.PreisKategorieÄndernController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PreisKategorieÄndernGUI extends JFrame implements ActionListener
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JPanel panel4 = new JPanel();
	private JPanel panel5 = new JPanel();
	private JLabel kategorieLabel = new JLabel("Kategorie");
	private JLabel preis_hLabel = new JLabel("Preis/Stunde");
	private JLabel tagespreisLabel = new JLabel("Tagespreis");
	private JTextField kategorieInput = new JTextField();
	private JTextField preis_hInput = new JTextField();
	private JTextField tagespreisInput = new JTextField();
	private JButton save = new JButton("Speichern");
	private JButton cancel = new JButton("Abbrechen");
	private JDialog dialog = new JDialog();
	
	private PreisKategorienÄndernGUI gui;
	
	private Border buttonBorder;
	private Border border1;
	
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private String kategorie;
	private int preis_h;
	private int tagespreis;
	
	private PreisKategorieÄndernController preisKategorieÄndernController;
	private boolean success = false;
	
	/**
	 * Der Konstruktor erzeugt die PreisKategorieÄndernGUI über mehrere Methoden und übergibt gegebenfalls die Parameter kategorie, preis_h und tagespreis.
	 * @param gui
	 * @param kategorie
	 * @param preis_h
	 * @param tagespreis
	 */
	public PreisKategorieÄndernGUI(PreisKategorienÄndernGUI gui, String kategorie, int preis_h, int tagespreis)
	{
		this.gui = gui;
		this.kategorie = kategorie;
		this.preis_h = preis_h;
		this.tagespreis = tagespreis;
		preisKategorieÄndernController = new PreisKategorieÄndernController(this);
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/PreisIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setEingabe(kategorie, preis_h, tagespreis);
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Größe, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Kategorie-Preis ändern");
		dialog.setPreferredSize(new Dimension(500, 220));
		dialog.setLocation(710, 430);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das CENTER-Feld des BorderLayouts und setzt darauf drei Panels für die drei Eingaben.
	 * Auf jedes Panel wird ein Label und ein Textfeld für Eingaben der Kategorie, des Preises pro Stunde und des Tagespreises gesetzt.
	 * Zuletzt werden die übergebenen Parameter kategorie, preis_h und tagespreis in die passenden Eimgabefelder gesetzt und das Eingabefeld kategorieInput wird auf nicht-editierbar gesetzt.
	 * @param kategorie
	 * @param preis_h
	 * @param tagespreis
	 */
	public void setEingabe(String kategorie, int preis_h, int tagespreis)
	{
		panel1.setPreferredSize(new Dimension(450, 170));
		panel1.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel1.setBorder(border1);
		add(panel1, border.CENTER);
		panel2.setPreferredSize(new Dimension(450, 40));
		panel2.setLayout(flow);
		panel2.setBackground(Color.lightGray);
		panel1.add(panel2);
		kategorieLabel.setPreferredSize(new Dimension(150, 30));
		kategorieLabel.setFont(schriftart1);
		panel2.add(kategorieLabel);
		kategorieInput.setPreferredSize(new Dimension(50, 30));
		kategorieInput.setFont(schriftart1);
		kategorieInput.setEditable(false);
		panel2.add(kategorieInput);
		kategorieInput.setText(kategorie);
		panel3.setPreferredSize(new Dimension(450, 40));
		panel3.setLayout(flow);
		panel3.setBackground(Color.lightGray);
		panel1.add(panel3);
		preis_hLabel.setPreferredSize(new Dimension(150, 30));
		preis_hLabel.setFont(schriftart1);
		panel3.add(preis_hLabel);
		preis_hInput.setPreferredSize(new Dimension(80, 30));
		preis_hInput.setFont(schriftart1);
		preis_hInput.setDocument(new IntegerOnlyDocument(3));
		panel3.add(preis_hInput);
		preis_hInput.setText(Integer.toString(preis_h));
		panel4.setPreferredSize(new Dimension(450, 40));
		panel4.setLayout(flow);
		panel4.setBackground(Color.lightGray);
		panel1.add(panel4);
		tagespreisLabel.setPreferredSize(new Dimension(150, 30));
		tagespreisLabel.setFont(schriftart1);
		panel4.add(tagespreisLabel);
		tagespreisInput.setPreferredSize(new Dimension(80, 30));
		tagespreisInput.setFont(schriftart1);
		tagespreisInput.setDocument(new IntegerOnlyDocument(5));
		panel4.add(tagespreisInput);
		tagespreisInput.setText(Integer.toString(tagespreis));
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 */
	public void setButtons()
	{
		panel5.setPreferredSize(new Dimension(450, 50));
		panel5.setLayout(flow2);
		panel5.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel5.setBorder(buttonBorder);
		add(panel5, border.SOUTH);
		save.setPreferredSize(new Dimension(170, 40));
		save.setFont(schriftart1);
		save.setBackground(Color.WHITE);
		save.addActionListener(this);
		panel5.add(save);
		Image image1 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		save.setIcon(new ImageIcon(image1));
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel5.add(cancel);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image2));
	}
	
	public String getKategorieInput()
	{
		return kategorieInput.getText();
	}
	
	public String getPreis_HInput()
	{
		return preis_hInput.getText();
	}
	
	public String getTagespreisInput()
	{
		return tagespreisInput.getText();
	}
	
	/**
	 * Die Methode erzeugt einen roten Rand um die Eingabefelder und fängt so die Fehler bei der Eingabe ab und gibt dies anstatt einer Fehlermeldung aus.
	 */
	public void setRedBorder()
	{
		if(preis_hInput.getText().equals(""))
		{
			preis_hInput.setBorder(border2);
		}
		else
		{
			preis_hInput.setBorder(border3);
		}
		if(tagespreisInput.getText().equals(""))
		{
			tagespreisInput.setBorder(border2);
		}
		else
		{
			tagespreisInput.setBorder(border3);
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		/**
		 * Das System versucht mit Hilfe des PreisKategorieÄndernController die Änderung zu speichern; schlägt dies fehl, wird die Methode setRedBorder aufgerufen, 
		 * ansonsten wird dem Nutzer die erfolgreiche Speicherung gemeldet, die Tabelle wird aktualisiert und die PreisKategorieÄndernGUI wird geschlossen.
		 */
		if(e.getSource() == this.save)
		{
			success = preisKategorieÄndernController.change();
			if(success == true)
			{
				JOptionPane.showMessageDialog(null, "Daten wurden geändert und gespeichert");
				gui.getUpdatedTableModel();
				dialog.dispose();
			}
			else if(success == false)
			{
				setRedBorder();
			}
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
	}
}