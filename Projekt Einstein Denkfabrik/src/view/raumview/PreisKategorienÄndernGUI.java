package view.raumview;

/**
 * @author Hendrik Veips
 */

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.tablemodels.RaumKategorieTableModel;
import view.StartansichtGUI;

public class PreisKategorien�ndernGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private JTable table;
	private RaumKategorieTableModel tableModel;
	private JScrollPane scrollV;
	private JButton change = new JButton("�ndern");
	private JButton cancel = new JButton("Abbrechen");
	private JDialog dialog = new JDialog();
	
	private StartansichtGUI gui;
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;
	
	/**
	 * Der Konstruktor erzeugt die PreisKategorien�ndernGUI �ber mehrere Methoden.
	 * @param gui
	 */
	public PreisKategorien�ndernGUI(StartansichtGUI gui)
	{
		this.gui = gui;
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/PreisIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();
		setTable();
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Gr��e, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Raum-Preis �ndern");
		dialog.setPreferredSize(new Dimension(700, 400));
		dialog.setLocation(610, 340);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	public void setStichwortsuche()
	{
		panel1.setPreferredSize(new Dimension(640, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(190, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(430, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}
	
	/**
	 * Die Methode �bergibt der Tabelle das RaumKategorieTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach �bergibt sie der Tabelle noch den sorter, damit der Nutzer die M�glichkeit hat die Eintr�ge zu sortieren und sie �bergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst m�glich macht.
	 */
	public void setTable()
	{
		tableModel = new RaumKategorieTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 300));
		add(scrollV, border.CENTER);
		tabellenwerteZentrieren();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 */
	public void setButtons()
	{
		panel2.setPreferredSize(new Dimension(640, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		change.setPreferredSize(new Dimension(170, 40));
		change.setFont(schriftart1);
		change.setBackground(Color.WHITE);
		change.addActionListener(this);
		panel2.add(change);
		Image image2 = new ImageIcon(this.getClass().getResource("/BearbeitenIcon.jpeg")).getImage();
		change.setIcon(new ImageIcon(image2));
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * Die Methode zentriert die Tabellenwerte.
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	public JTable getTable() 
	{
		return this.table;
	}
	
	public RaumKategorieTableModel getTableModel() 
	{
		return tableModel;
	}
	
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	public TableModel getUpdatedTableModel()
	{
		tableModel.databaseUpdated();
		return tableModel;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		/**
		 * Das System pr�ft, ob eine Zeile ausgew�hlt ist oder nicht. Ist eine Zeile ausgew�hlt, werden die Attribute kategorie, preis_h und tagespreis aus der Tabelle ausgelesen,
		 * diese werden an den Konstruktor der PreisKategorie�ndernGUI �bergeben und diese wird erzeugt. Falls keine Zeile ausgew�hlt ist, wird dies dem Nutzer mitgeteilt.
		 */
		if(e.getSource() == this.change) 
		{
			if(table.getSelectedRow() == -1)
			{
				JOptionPane.showMessageDialog(null, "Keine Kategorie ausgew�hlt.", "Fehler", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				String kategorie = (String) table.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0);
				int preis_h = (int) table.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 1);
				int tagespreis = (int) table.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 2);
				new PreisKategorie�ndernGUI(this, kategorie, preis_h, tagespreis);
			}
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
	}
}