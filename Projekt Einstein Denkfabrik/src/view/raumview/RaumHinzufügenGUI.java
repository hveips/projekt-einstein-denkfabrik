package view.raumview;

/**
 * @author Christoph Hoppe, Niklas Nebeling
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.IntegerOnlyDocument;
import controller.raumcontroller.RaumHinzuf�genController;
import view.StartansichtGUI;

public class RaumHinzuf�genGUI extends JFrame implements ActionListener 
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JPanel buttonpnl1 = new JPanel();
	private JPanel buttonpnl2 = new JPanel();
	private JPanel buttonpnl3 = new JPanel();
	private JLabel raumnummerlbl = new JLabel("Raumnummer");
	private JTextField raumnummertxt = new JTextField();
	private JLabel sitzpl�tzelbl = new JLabel("Anzahl Sitzpl�tze");
	private JTextField sitzpl�tzetxt = new JTextField();
	private JButton hinzuf�genbtn = new JButton("Ausstattung hinzuf�gen");
	private JButton abbrechenbtn = new JButton("Abbrechen");
	private JButton speichernbtn = new JButton("Speichern");
	private JDialog dialog = new JDialog();
	
	private StartansichtGUI gui;
	
	private Border border1;
	private Border border2 = BorderFactory.createLineBorder(Color.RED, 3);
	private Border border3 = BorderFactory.createLineBorder(Color.lightGray);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private RaumHinzuf�genController controller = new RaumHinzuf�genController();

	public RaumHinzuf�genGUI(StartansichtGUI gui) 
	{
		this.gui = gui;
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/RaumIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		panel1.setPreferredSize(new Dimension(450, 255));
		panel1.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(border1);
		add(panel1, border.CENTER);
		setEingabeRaumnr();
		setEingabeSitzpl�tze();
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Raum hinzuf�gen");
		dialog.setPreferredSize(new Dimension(500, 305));
		dialog.setLocation(710, 385);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}
	
	public void setEingabeRaumnr()
	{
		panel2.setPreferredSize(new Dimension(430, 40));
		panel2.setLayout(flow);
		panel2.setBackground(Color.lightGray);
		panel1.add(panel2);
		raumnummerlbl.setPreferredSize(new Dimension(150, 30));
		raumnummerlbl.setFont(schriftart1);
		panel2.add(raumnummerlbl);
		raumnummertxt.setPreferredSize(new Dimension(100, 30));
		raumnummertxt.setFont(schriftart1);
		raumnummertxt.setDocument(new IntegerOnlyDocument(3));
		panel2.add(raumnummertxt);
	}
	
	public void setEingabeSitzpl�tze()
	{
		panel3.setPreferredSize(new Dimension(430, 40));
		panel3.setLayout(flow);
		panel3.setBackground(Color.lightGray);
		panel1.add(panel3);
		sitzpl�tzelbl.setPreferredSize(new Dimension(150, 30));
		sitzpl�tzelbl.setFont(schriftart1);
		panel3.add(sitzpl�tzelbl);
		sitzpl�tzetxt.setPreferredSize(new Dimension(100, 30));
		sitzpl�tzetxt.setFont(schriftart1);
		sitzpl�tzetxt.setDocument(new IntegerOnlyDocument(4));
		panel3.add(sitzpl�tzetxt);
	}
	
	public void setButtons()
	{
		buttonpnl1.setPreferredSize(new Dimension(430, 50));
		buttonpnl1.setLayout(flow2);
		buttonpnl1.setBackground(Color.lightGray);
		panel1.add(buttonpnl1);
		hinzuf�genbtn.setPreferredSize(new Dimension(260, 40));
		hinzuf�genbtn.setFont(schriftart1);
		hinzuf�genbtn.setBackground(Color.WHITE);
		hinzuf�genbtn.addActionListener(this);
		buttonpnl1.add(hinzuf�genbtn);
		Image image3 = new ImageIcon(this.getClass().getResource("/AusstattungIcon.jpeg")).getImage();
		hinzuf�genbtn.setIcon(new ImageIcon(image3));
		buttonpnl2.setPreferredSize(new Dimension(430, 50));
		buttonpnl2.setLayout(flow2);
		buttonpnl2.setBackground(Color.lightGray);
		panel1.add(buttonpnl2);
		speichernbtn.setPreferredSize(new Dimension(160, 40));
		speichernbtn.setFont(schriftart1);
		speichernbtn.setBackground(Color.WHITE);
		speichernbtn.addActionListener(this);
		buttonpnl2.add(speichernbtn);
		Image image2 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		speichernbtn.setIcon(new ImageIcon(image2));
		buttonpnl3.setPreferredSize(new Dimension(430, 50));
		buttonpnl3.setLayout(flow2);
		buttonpnl3.setBackground(Color.lightGray);
		panel1.add(buttonpnl3);
		abbrechenbtn.setPreferredSize(new Dimension(160, 40));
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.addActionListener(this);
		buttonpnl3.add(abbrechenbtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image1));
	}

	/**
	 * Diese Methode bestimmt die Aktionen die ausgef�hrt werden wenn Buttons gedr�ckt werden.
	 * Pr�ft ob Eingaben vollst�ndig und korrekt sind und f�hrt dann controller-Methoden aus,
	 * sonst rote Markierung des Fehlers.
	 * @author Niklas Nebeling
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.abbrechenbtn) 
		{
			dialog.dispose();
		}
		if (e.getSource() == this.speichernbtn) 
		{
			try 
			{
				speichern();
				clearFields();
				raumnummertxt.setBorder(border3);
				sitzpl�tzetxt.setBorder(border3);
			} 
			catch (NumberFormatException ex) 
			{
				inputError();
			}
		}
		if (e.getSource() == this.hinzuf�genbtn) 
		{   
			new AusstattungHinzuf�genGUI(this, controller);
		}
	}

	/**
	 * Rote Markierung von fehlenden oder fehlerhaften Eingaben.
	 */
	private void inputError() {
		if(raumnummertxt.getText().equals("") && sitzpl�tzetxt.getText().equals(""))
		{
			raumnummertxt.setBorder(border2);
			sitzpl�tzetxt.setBorder(border2);					
		}
		else if(raumnummertxt.getText().equals(""))
		{
			raumnummertxt.setBorder(border2);
			sitzpl�tzetxt.setBorder(border3);
		}
		else if(sitzpl�tzetxt.getText().equals(""))
		{
			sitzpl�tzetxt.setBorder(border2);
			raumnummertxt.setBorder(border3);
		}
	}
	
	/**
	 * �bergebt Controller Raumnummer und Sitzpl�tze und ruft raumAnlegen auf.
	 * @author Niklas Nebeling
	 */
	public void speichern() throws NumberFormatException
	{
		int rnr = Integer.parseInt(raumnummertxt.getText());
		int sitzpl�tze = Integer.parseInt(sitzpl�tzetxt.getText());
		controller.raumAnlegen(rnr, sitzpl�tze);
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public void clearFields() 
	{
		raumnummertxt.setText("");
		sitzpl�tzetxt.setText("");
	}
}