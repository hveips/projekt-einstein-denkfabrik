package view.raumview;


/**
 * @author Christoph Hoppe, Niklas Nebeling
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.Border;
import controller.documentcontroller.IntegerOnlyDocument;
import controller.raumcontroller.RaumÄndernAktivierenDeaktivierenController;
import model.Ausstattung;
import view.buchung_reservierungview.BucheReserviereRaumWeiterGUI;
import view.raumview.RaumÄndernGUI;

public class AusstattungAnzeigenVerändernGUI extends JFrame implements ActionListener {
	private JPanel panel1 = new JPanel();
	private JPanel buttonpnl = new JPanel();
	private JPanel beamerPanel = new JPanel();
	private JPanel internetPanel = new JPanel();
	private JPanel wlanPanel = new JPanel();
	private JPanel tafelPanel = new JPanel();
	private JPanel whiteboardPanel = new JPanel();
	private JPanel smartboardPanel = new JPanel();
	private JPanel flipchartPanel = new JPanel();
	private JPanel cateringPanel = new JPanel();
	private JCheckBox beamerchk = new JCheckBox("Beamer");
	private JCheckBox internetchk = new JCheckBox("Internetanschluss");
	private JCheckBox wlanchk = new JCheckBox("W-Lan");
	private JCheckBox tafelchk = new JCheckBox("Tafel");
	private JCheckBox whiteboardchk = new JCheckBox("Whiteboard");
	private JCheckBox smartboardchk = new JCheckBox("Smartboard");
	private JCheckBox flipchartchk = new JCheckBox("Flipchart");
	private JCheckBox cateringchk = new JCheckBox("Cateringbereich");
	private JLabel beamerLabel = new JLabel("Anzahl");
	private JLabel internetLabel = new JLabel("Anzahl");
	private JLabel cateringLabel = new JLabel("Anzahl");
	private JTextField beamerInput = new JTextField();
	private JTextField internetInput = new JTextField();
	private JTextField cateringInput = new JTextField();
	private JButton abbrechenbtn = new JButton();
	private JButton speichernbtn = new JButton("Speichern");
	private JDialog dialog = new JDialog();

	private RaumÄndernGUI gui;
	private BucheReserviereRaumWeiterGUI gui2;

	private Border border1;
	private Border buttonBorder;

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.RIGHT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.LEFT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);

	private ArrayList<Ausstattung> ausstattung; // Leider nicht ohne unnötig hohen Aufwand auslagerbar in Controller
												// Klasse, da diese benötigt zum setzen der Checkboxen

	private RaumÄndernAktivierenDeaktivierenController controller;
	private boolean change;

	/**
	 *  Der Konstruktor erzeugt die AusstattungAnzeigenVerändernGUI und übernimmt parameter
	 * @param controller
	 * @param ausstattung
	 * @param change
	 */
	public AusstattungAnzeigenVerändernGUI(RaumÄndernAktivierenDeaktivierenController controller,ArrayList<Ausstattung> ausstattung, boolean change) {
		this.change = change;
		erzeugeDialog(change);
		Image image4 = new ImageIcon(this.getClass().getResource("/AusstattungIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		this.controller = controller;
		this.ausstattung = ausstattung;
		setBeamerCheckbox();
		setInternetCheckbox();
		setWlanCheckbox();
		setTafelCheckbox();
		setWhiteboardCheckbox();
		setSmartboardCheckbox();
		setFlipchartCheckbox();
		setCateringCheckbox();
		if (change == false) {
			setTextfelderUnabled();
		}
		setButtons(change);
		setAusstattung();
		dialog.pack();
		dialog.setVisible(true);
	}
	/**
	 * Die Methode erzeugt einen neuen JDialog und setzt den Titel individuell nach Anwendungsfall
	 * @param change
	 */
	public void erzeugeDialog(boolean change) {
		if (change == true) {
			dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
			dialog.setContentPane(this.getContentPane());
			dialog.setTitle("Ausstattung verändern");
		} else {
			dialog = new JDialog(gui2, Dialog.ModalityType.DOCUMENT_MODAL);
			dialog.setContentPane(this.getContentPane());
			dialog.setTitle("Ausstattung anzeigen");
		}
		dialog.setPreferredSize(new Dimension(450, 460));
		dialog.setLocation(735, 310);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}
	
	/**
	 *Die Methode ordnert die Checkbox und das Textfeld für den Beamer auf dem Panel an.
	 */
	public void setBeamerCheckbox() {
		panel1.setPreferredSize(new Dimension(400, 245));
		panel1.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(5, 20, 0, 25, Color.lightGray);
		panel1.setBorder(border1);
		panel1.setLayout(new GridLayout(0, 1, 6, 3));
		add(panel1, border.CENTER);
		beamerPanel.setPreferredSize(new Dimension(380, 40));
		beamerPanel.setBackground(Color.lightGray);
		beamerPanel.setLayout(flow2);
		panel1.add(beamerPanel);
		beamerchk.setPreferredSize(new Dimension(200, 30));
		beamerchk.setBackground(Color.lightGray);
		beamerchk.setFont(schriftart1);
		beamerPanel.add(beamerchk);
		beamerLabel.setPreferredSize(new Dimension(60, 30));
		beamerLabel.setFont(schriftart1);
		beamerPanel.add(beamerLabel);
		beamerInput.setPreferredSize(new Dimension(40, 30));
		beamerInput.setFont(schriftart1);
		beamerInput.setDocument(new IntegerOnlyDocument(2));
		beamerPanel.add(beamerInput);
	}

	public void setInternetCheckbox() {
		internetPanel.setPreferredSize(new Dimension(380, 40));
		internetPanel.setBackground(Color.lightGray);
		internetPanel.setLayout(flow2);
		panel1.add(internetPanel);
		internetchk.setPreferredSize(new Dimension(200, 30));
		internetchk.setBackground(Color.lightGray);
		internetchk.setFont(schriftart1);
		internetPanel.add(internetchk);
		internetLabel.setPreferredSize(new Dimension(60, 30));
		internetLabel.setFont(schriftart1);
		internetPanel.add(internetLabel);
		internetInput.setPreferredSize(new Dimension(40, 30));
		internetInput.setFont(schriftart1);
		internetInput.setDocument(new IntegerOnlyDocument(2));
		internetPanel.add(internetInput);
	}

	public void setWlanCheckbox() {
		wlanPanel.setPreferredSize(new Dimension(380, 40));
		wlanPanel.setBackground(Color.lightGray);
		wlanPanel.setLayout(flow2);
		panel1.add(wlanPanel);
		wlanchk.setPreferredSize(new Dimension(380, 30));
		wlanchk.setBackground(Color.lightGray);
		wlanchk.setFont(schriftart1);
		wlanPanel.add(wlanchk);
	}

	public void setTafelCheckbox() {
		tafelPanel.setPreferredSize(new Dimension(380, 40));
		tafelPanel.setBackground(Color.lightGray);
		tafelPanel.setLayout(flow2);
		panel1.add(tafelPanel);
		tafelchk.setPreferredSize(new Dimension(380, 30));
		tafelchk.setBackground(Color.lightGray);
		tafelchk.setFont(schriftart1);
		tafelPanel.add(tafelchk);
	}

	public void setWhiteboardCheckbox() {
		whiteboardPanel.setPreferredSize(new Dimension(380, 40));
		whiteboardPanel.setBackground(Color.lightGray);
		whiteboardPanel.setLayout(flow2);
		panel1.add(whiteboardPanel);
		whiteboardchk.setPreferredSize(new Dimension(380, 30));
		whiteboardchk.setBackground(Color.lightGray);
		whiteboardchk.setFont(schriftart1);
		whiteboardPanel.add(whiteboardchk);
	}

	public void setSmartboardCheckbox() {
		smartboardPanel.setPreferredSize(new Dimension(380, 40));
		smartboardPanel.setBackground(Color.lightGray);
		smartboardPanel.setLayout(flow2);
		panel1.add(smartboardPanel);
		smartboardchk.setPreferredSize(new Dimension(380, 30));
		smartboardchk.setBackground(Color.lightGray);
		smartboardchk.setFont(schriftart1);
		smartboardPanel.add(smartboardchk);
	}

	public void setFlipchartCheckbox() {
		flipchartPanel.setPreferredSize(new Dimension(380, 40));
		flipchartPanel.setBackground(Color.lightGray);
		flipchartPanel.setLayout(flow2);
		panel1.add(flipchartPanel);
		flipchartchk.setPreferredSize(new Dimension(380, 30));
		flipchartchk.setBackground(Color.lightGray);
		flipchartchk.setFont(schriftart1);
		flipchartPanel.add(flipchartchk);
	}

	public void setCateringCheckbox() {
		cateringPanel.setPreferredSize(new Dimension(380, 40));
		cateringPanel.setBackground(Color.lightGray);
		cateringPanel.setLayout(flow2);
		panel1.add(cateringPanel);
		cateringchk.setPreferredSize(new Dimension(200, 30));
		cateringchk.setBackground(Color.lightGray);
		cateringchk.setFont(schriftart1);
		cateringPanel.add(cateringchk);
		cateringLabel.setPreferredSize(new Dimension(60, 30));
		cateringLabel.setFont(schriftart1);
		cateringPanel.add(cateringLabel);
		cateringInput.setPreferredSize(new Dimension(40, 30));
		cateringInput.setFont(schriftart1);
		cateringInput.setDocument(new IntegerOnlyDocument(2));
		cateringPanel.add(cateringInput);
	}
	/**
	 * Die Methode definiert das Buttonpanel und ordnet die Button je nach Anwendungsfall an.
	 * @param change
	 */
	public void setButtons(boolean change) {
		buttonpnl.setPreferredSize(new Dimension(400, 50));
		buttonpnl.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 25, 20, Color.lightGray);
		buttonpnl.setBorder(buttonBorder);
		buttonpnl.setLayout(flow);
		add(buttonpnl, border.SOUTH);
		if (change == true) {
			speichernbtn.setPreferredSize(new Dimension(160, 40));
			speichernbtn.setFont(schriftart1);
			speichernbtn.setBackground(Color.WHITE);
			speichernbtn.addActionListener(this);
			buttonpnl.add(speichernbtn);
			Image image2 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
			speichernbtn.setIcon(new ImageIcon(image2));
			abbrechenbtn.setText("Abbrechen");
		} else {
			abbrechenbtn.setText("Schließen");
		}
		abbrechenbtn.setPreferredSize(new Dimension(160, 40));
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.addActionListener(this);
		buttonpnl.add(abbrechenbtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image1));
	}

	public void setTextfelderUnabled() {
		beamerInput.setEditable(false);
		internetInput.setEditable(false);
		cateringInput.setEditable(false);
	}

	/**
	 * Diese Methode bestimmt die Aktionen die ausgeführt werden wenn Buttons gedrückt werden.
	 * Gibt Fehlermeldung aus wenn Ausstattung angekreuzt ist ohne Anzahl.
	 * @author Niklas Nebeling
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.speichernbtn) {
			controller.clearAusstattung();
			try {
				ausstattungSpeichern();
				dialog.dispose();
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Bitte eine Anzahl eingeben neben den ausgewählten Checkboxen.",
						"Fehler!", JOptionPane.ERROR_MESSAGE);
			}
		}
		if (e.getSource() == this.abbrechenbtn) {
			dialog.dispose();
		}
	}

	/**
	 * Setzt die Ausstattungen und ggf. die Anzahlen dieser.
	 * @author Niklas Nebeling
	 */
	// Zeigt die Ausstattungskonfiguration des (vorher) gewählten Raumes an
	private void setAusstattung() {
		for (int i = 0; i < ausstattung.size(); i++) {
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Beamer")) { // Wenn die Bezeichung des i-ten
																					// Elemtents der ausstattungsliste
																					// des gewählten Raumes mit "X"
																					// übereinstimmt
				beamerchk.setSelected(true); // Dann setze die checkbox auf selected
				beamerInput.setText("" + ausstattung.get(i).getMenge()); // Und setze (ggf.) die entsprechende Anzahl in
																			// das Textfeld
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Internetanschluss")) {
				internetchk.setSelected(true);
				internetInput.setText("" + ausstattung.get(i).getMenge());
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Cateringbereich")) {
				cateringchk.setSelected(true);
				cateringInput.setText("" + ausstattung.get(i).getMenge());
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("W-Lan")) {
				wlanchk.setSelected(true);
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Tafel")) {
				tafelchk.setSelected(true);
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Whiteboard")) {
				whiteboardchk.setSelected(true);
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Smartboard")) {
				smartboardchk.setSelected(true);
			}
			if (ausstattung.get(i).getBezeichnung().equalsIgnoreCase("Flipchart")) {
				flipchartchk.setSelected(true);
			}
		}
	}

	/**
	 * Speichert die gewählten Ausstattungen und ggf. die Anzahlen dieser im Controller.
	 * @author Niklas Nebeling
	 */
	public void ausstattungSpeichern() throws NumberFormatException {
		if (beamerchk.isSelected()) {
			controller.ausstattungErzeugen("Beamer", Integer.parseInt(beamerInput.getText())); // Name und Menge des
																								// Ausstattungsobjektes
																								// werden an controller
																								// übergeben
		}
		if (internetchk.isSelected()) {
			controller.ausstattungErzeugen("Internetanschluss", Integer.parseInt(internetInput.getText()));
		}
		if (wlanchk.isSelected()) {
			controller.ausstattungErzeugen("W-Lan", 1); // Hier wird nur Menge 1 übergeben da es diese Objekte pro Raum
														// nur einmal gibt
		}
		if (tafelchk.isSelected()) {
			controller.ausstattungErzeugen("Tafel", 1);
		}
		if (whiteboardchk.isSelected()) {
			controller.ausstattungErzeugen("Whiteboard", 1);
		}
		if (smartboardchk.isSelected()) {
			controller.ausstattungErzeugen("Smartboard", 1);
		}
		if (flipchartchk.isSelected()) {
			controller.ausstattungErzeugen("Flipchart", 1);
		}
		if (cateringchk.isSelected()) {
			controller.ausstattungErzeugen("Cateringbereich", Integer.parseInt(cateringInput.getText()));
		}
	}
}