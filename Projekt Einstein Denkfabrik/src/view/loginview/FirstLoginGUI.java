package view.loginview;

/**
 * @author Hendrik Veips
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;
import controller.LoginController;
import controller.documentcontroller.LimitedDocument;
import view.StartansichtGUI;

public class FirstLoginGUI extends JFrame implements ActionListener
{
	private JPanel textpnl = new JPanel();
	private JPanel buttonpnl = new JPanel();
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel pw = new JLabel("Neues Passwort");
	private JPasswordField pwTextField = new JPasswordField();
	private JLabel pwW = new JLabel("Passwort wiederholen");
	private JPasswordField pwWTextField = new JPasswordField();
	private JButton savebtn = new JButton("Passwort speichern");
	private JButton info = new JButton();
	
	private LoginGUI gui;
	
	private Border border1;
	private Border buttonBorder;

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.RIGHT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private LoginController loginController = new LoginController();
	private char rolle;
	private String benutzername;
	private boolean samePassword;
	private boolean save;
	
	/**
	 * Der Konstruktor erzeugt die FirstLoginGUI �ber mehrere Methoden.
	 * @param gui
	 * @param rolle
	 * @param benutzername
	 */
	FirstLoginGUI(LoginGUI gui, char rolle, String benutzername) 
	{
		setPreferredSize(new Dimension(550, 185));
		setTitle("First Login");
		setLayout(border);
		setLocation(680, 440);
		setResizable(false);
		Image image4 = new ImageIcon(this.getClass().getResource("/LoginFensterIcon.jpeg")).getImage();
		setIconImage(image4);
		this.gui = gui;
		this.rolle = rolle;
		this.benutzername = benutzername;
		setEingabe();
		setButtons();
		setVisible(true);
		pack();
	}
		
	/**
	 * Die Methode setzt ein Panel auf das CENTER-Feld des BorderLayouts und setzt darauf die Labels und Textfelder der Eingaben.
	 * Au�erdem setzt sie den Button info auf das Panel.
	 */
	public void setEingabe()
	{
		textpnl.setPreferredSize(new Dimension(500, 90));
		textpnl.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		textpnl.setBorder(border1);
		add(textpnl, BorderLayout.CENTER);
		panel1.setPreferredSize(new Dimension(480, 40));
		panel1.setBackground(Color.lightGray);
		textpnl.add(panel1);
		pw.setPreferredSize(new Dimension(200, 30));
		pw.setFont(schriftart1);
		panel1.add(pw);
		pwTextField.setPreferredSize(new Dimension(225, 30));
		pwTextField.setFont(schriftart1);
		pwTextField.setDocument(new LimitedDocument(10));
		panel1.add(pwTextField);
		info.setPreferredSize(new Dimension(30, 30));
		info.setBackground(Color.WHITE);
		info.addActionListener(this);
		panel1.add(info);
		Image image1 = new ImageIcon(this.getClass().getResource("/InfoIcon.jpeg")).getImage();
		info.setIcon(new ImageIcon(image1));
		panel2.setPreferredSize(new Dimension(480, 40));
		panel2.setBackground(Color.lightGray);
		textpnl.add(panel2);
		pwW.setPreferredSize(new Dimension(200, 30));
		pwW.setFont(schriftart1);
		panel2.add(pwW);
		pwWTextField.setPreferredSize(new Dimension(260, 30));
		pwWTextField.setFont(schriftart1);
		pwWTextField.setDocument(new LimitedDocument(10));
		panel2.add(pwWTextField);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt darauf den Button.
	 */
	public void setButtons()
	{
		buttonpnl.setPreferredSize(new Dimension(500, 50));
		buttonpnl.setBackground(Color.lightGray);
		buttonpnl.setLayout(flow);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 35, Color.lightGray);
		buttonpnl.setBorder(buttonBorder);
		add(buttonpnl, BorderLayout.SOUTH);
		savebtn.setPreferredSize(new Dimension(240, 40));
		savebtn.setBackground(Color.WHITE);
		savebtn.setFont(schriftart1);
		savebtn.addActionListener(this);
		buttonpnl.add(savebtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		savebtn.setIcon(new ImageIcon(image1));
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		/**
		 * Das System liest die beiden Eingaben der Passw�rter aus und vergleicht beide mit Hilfe des LoginControllers. 
		 * Sind die Passw�rter nicht gleich, wird dies dem Nutzer gemeldet, ansonsten wird versucht das Passwort zu speichern.
		 * Falls dies fehlschl�gt, wird dies dem Nutzer gemeldet, ansonsten wird der Nutzer gefragt ob er fortfahren will oder nicht.
		 * Auf der einen Seite wird die Startansicht erzeugt, wobei �ber den �bergebenen Parameter rolle entschieden wird, welche Funktionen dem Nutzer freigeschaltet werden,
		 * und die FirstLoginGUI geschlossen, ansonsten wird das Programm beendet.
		 */
		if (e.getSource() == this.savebtn)
		{
			char[] passwort = pwTextField.getPassword();
			char[] passwort2 = pwWTextField.getPassword();
			String savePassword = String.valueOf(passwort);
			samePassword = loginController.pruefePasswoerter(passwort, passwort2);
			if(samePassword == true)
			{
				save = loginController.speicherePasswort(savePassword, benutzername);
				if(save == true)
				{
					int option = JOptionPane.showConfirmDialog(this, "Passwort wurde erfolgreich gespeichert. Wollen Sie das System starten?", "Systemstart?", JOptionPane.YES_NO_OPTION);
					if (option == JOptionPane.YES_OPTION)
					{
						if(rolle == 'A') 
						{
							new StartansichtGUI('A', benutzername);
						} 
						else if(rolle == 'G') 
						{
							new StartansichtGUI('G', benutzername);
						} 
						else 
						{
							new StartansichtGUI('M',benutzername);
						}
						dispose();
					}
					else
					{
						dispose();
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Verbindung mit der Datenbank fehlgeschlagen. �berpr�fen Sie ihre Verbindung und versuchen Sie es erneut.");
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Passwort-Eingabe nicht korrekt!");
				pwTextField.setText("");
				pwWTextField.setText("");
			}
		}
		if(e.getSource() == this.info)
		{
			JOptionPane.showMessageDialog(null, "Die Passwort-L�nge betr�gt maximal 10 Zeichen!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}