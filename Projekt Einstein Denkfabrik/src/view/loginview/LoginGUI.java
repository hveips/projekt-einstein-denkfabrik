package view.loginview;

/**
 * @author Christoph Hoppe, Hendrik Veips
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import javax.swing.*;
import javax.swing.border.Border;

import controller.LoginController;
import controller.MailController;
import controller.documentcontroller.LimitedDocument;
import controller.documentcontroller.LimitedMailDocument;
import view.StartansichtGUI;
import view.passwortview.PasswortVergessenGUI;
import controller.DatumController;

public class LoginGUI extends JFrame implements ActionListener {
	private JPanel textpnl = new JPanel();
	private JPanel buttonpnl = new JPanel();
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel name = new JLabel("Benutzername");
	private JTextField nameTextField = new JTextField();
	private JLabel pw = new JLabel("Passwort");
	private JPasswordField passwordField = new JPasswordField();
	private JButton anmeldenbtn = new JButton("Anmelden");
	private JButton abbrechenbtn = new JButton("Abbrechen");
	private JButton pwVergessenbtn = new JButton("Passwort vergessen");

	private Border border1;
	private Border buttonBorder;

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.RIGHT);

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);

	private GregorianCalendar calendar = new GregorianCalendar();
	private int month = calendar.get(GregorianCalendar.MONTH);
	private int year = calendar.get(GregorianCalendar.YEAR);
	private int day = calendar.get(GregorianCalendar.DAY_OF_MONTH);

	private LoginController loginController = new LoginController();
	private boolean validLogin = false;
	private char rolle;
	private String anrede;
	private String nam;
	private int eingelogt;

	private MailController mailController = new MailController();
	private String security;

	private DatumController datumController = new DatumController();
	private String tag;
	private String monat;
	private String jahr;

	public static void main(String[] args) {
		new LoginGUI();
	}

	/**
	 * Die Methode definiert die grundlegenden Eigenschaften der LoginGUI() wie z.B
	 * Gr��e, Location etc. und weist das BorderLayout zu
	 */
	public LoginGUI() {
		setPreferredSize(new Dimension(660, 185));
		setTitle("Login Screen");
		setLayout(border);
		setLocation(630, 440);
		setResizable(false);
		Image image4 = new ImageIcon(this.getClass().getResource("/LoginFensterIcon.jpeg")).getImage();
		setIconImage(image4);
		setEingabe();
		setButtons();
		setVisible(true);
		pack();
	}

	/**
	 * Die Methode definiert die Label,Panel und Textfelder f�r Name und
	 * Passworteingabe und f�gt diese auf den JPanels zu.
	 */
	public void setEingabe() {
		textpnl.setPreferredSize(new Dimension(600, 90));
		textpnl.setBackground(Color.lightGray);
		border1 = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		textpnl.setBorder(border1);
		add(textpnl, BorderLayout.CENTER);
		panel1.setPreferredSize(new Dimension(580, 40));
		panel1.setBackground(Color.lightGray);
		textpnl.add(panel1);
		name.setPreferredSize(new Dimension(160, 30));
		name.setFont(schriftart1);
		panel1.add(name);
		nameTextField.setPreferredSize(new Dimension(400, 30));
		nameTextField.setFont(schriftart1);
		nameTextField.setDocument(new LimitedMailDocument(35));
		panel1.add(nameTextField);
		panel2.setPreferredSize(new Dimension(580, 40));
		panel2.setBackground(Color.lightGray);
		textpnl.add(panel2);
		pw.setPreferredSize(new Dimension(160, 30));
		pw.setFont(schriftart1);
		panel2.add(pw);
		passwordField.setPreferredSize(new Dimension(400, 30));
		passwordField.setFont(schriftart1);
		passwordField.setDocument(new LimitedDocument(10));
		panel2.add(passwordField);
	}

	/**
	 * Die Methode definiert das buttonpanel und f�gt drei Button hinzu
	 */
	public void setButtons() {
		buttonpnl.setPreferredSize(new Dimension(600, 50));
		buttonpnl.setBackground(Color.lightGray);
		buttonpnl.setLayout(flow);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 35, Color.lightGray);
		buttonpnl.setBorder(buttonBorder);
		add(buttonpnl, BorderLayout.SOUTH);
		abbrechenbtn.setPreferredSize(new Dimension(160, 40));
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.addActionListener(this);
		buttonpnl.add(abbrechenbtn);
		Image image3 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image3));
		pwVergessenbtn.setPreferredSize(new Dimension(240, 40));
		pwVergessenbtn.setBackground(Color.WHITE);
		pwVergessenbtn.addActionListener(this);
		pwVergessenbtn.setFont(schriftart1);
		buttonpnl.add(pwVergessenbtn);
		Image image2 = new ImageIcon(this.getClass().getResource("/FragezeichenIcon.jpeg")).getImage();
		pwVergessenbtn.setIcon(new ImageIcon(image2));
		anmeldenbtn.setPreferredSize(new Dimension(160, 40));
		anmeldenbtn.setBackground(Color.WHITE);
		anmeldenbtn.setFont(schriftart1);
		anmeldenbtn.addActionListener(this);
		buttonpnl.add(anmeldenbtn);
		Image image1 = new ImageIcon(this.getClass().getResource("/LoginIcon.jpeg")).getImage();
		anmeldenbtn.setIcon(new ImageIcon(image1));
	}

	/**
	 * @author Hendrik Veips
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.abbrechenbtn) {
			dispose();
		}
		/**
		 * Das System pr�ft, ob ein Benutzername eingegeben worden ist. Ist dies der
		 * Fall und findet sich der Benutzername in der Datenbank, erzeugt der
		 * LoginController eine Zufallsreihe aus Zahlen und Buchstaben, sendet diese per
		 * Mail an den Mitarbeiter, versichert sich, dass das Passwort zur�ckgesetzt
		 * werden soll und �ffnet gegebenenfalls das Fenster PasswortVergessenGUI.
		 * Sollte das Eingabefeld f�r den Benutzernamen leer sein oder der Benutzername
		 * nicht in der Datenbank vorhanden sein, wird dem Mitarbeiter gemeldet, dass
		 * entweder kein Eingabe erfolgt ist, oder seine Daten in der Datenbank nicht
		 * vorliegen.
		 */
		if (e.getSource() == this.pwVergessenbtn) {
			String benutzername = nameTextField.getText();
			rolle = loginController.getRolle(benutzername);
			nam = loginController.getName(benutzername);
			anrede = loginController.getAnrede(benutzername);

			if (nameTextField.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Benutzername nicht eingegeben!");
			} else {
				int a = loginController.pruefeUsername(benutzername);
				if (a == 1) {
					security = loginController.setZufallsSicherheitsAbfrage();
					mailController.sendMailSecurityQuery(anrede, nam, benutzername, security);
					int option = JOptionPane.showConfirmDialog(this,
							"Sie haben eine E-Mail mit einer Sicherheitsabfrage erhalten!" + "\n"
									+ "Bitte best�tigen Sie Ihre Identit�t im n�chsten Fenster!" + "\n"
									+ "Bitte beenden Sie nicht das Programm!",
							"Passwort zur�cksetzen?", JOptionPane.OK_CANCEL_OPTION);
					if (option == JOptionPane.OK_OPTION) {
						new PasswortVergessenGUI(this, security, benutzername, rolle);
						dispose();
					}
				} else {
					JOptionPane.showMessageDialog(null, "Ihre Daten sind nicht in der Datenbank vorhanden!");
				}
			}
		}
		/**
		 * Das System pr�ft zuerst �ber den LoginController, ob die Eingaben des Logins
		 * korrekt sind. Ist dies der Fall, wandelt das System zuerst �ber den
		 * DatumController und den LoginController, Reservierungen, die in 3 oder
		 * weniger Tagen beginnen, in Buchungen um. Danach kontrolliert es das Attribut
		 * "eingelogt", ob es den Wert 1 oder 0 gespeichert hat; sollte eingelogt = 0
		 * gelten, wird das Fenster FirstLoginGUI ge�ffnet und das Fenster LoginGUI
		 * geschlossen. Im anderen Fall wird das Fenster StartansichtGUI ge�ffnet und
		 * das Fenster LoginGUI geschlossen; hierbei wird allerdings noch eine
		 * Rollenunterscheidung vorgenommen (A - Admin, G - Gesch�ftsf�hrer, M -
		 * Mitarbeiter) und zusammen mit dem Benutzernamen an den Konstruktor �bergeben.
		 * Sollten die Eingabefelder des Logins leer sein oder die Login-Daten nicht
		 * korrekt eingegeben worden sein, meldet das System, dass die Eingaben nicht
		 * korrekt sind und leert die Eingabe des Passwortfeldes.
		 */
		if (e.getSource() == this.anmeldenbtn) 
		{
			 String benutzername = nameTextField.getText();
			 char[] password = passwordField.getPassword();
			 validLogin = loginController.pruefeLoginDaten(benutzername, password);
			 if (validLogin == true) 
			 {
				 month = month + 1;
				 for(int i = 0;i <= 3;i++) 
				 {
					 tag = datumController.getNaechstenTag(day, month, year);
					 day = Integer.parseInt(tag);
					 monat = datumController.getNaechstenMonat(tag, month);
					 month = Integer.parseInt(monat);
					 jahr = datumController.getNaechstesJahr(day, month, year);
					 year = Integer.parseInt(jahr);
				 }
				 loginController.wandleReservierungInBuchung(tag, monat, jahr);
				 eingelogt = loginController.getEingelogt(benutzername);
				 rolle = loginController.getRolle(benutzername);
				 if (eingelogt == 0) 
				 {
					 new FirstLoginGUI(this, rolle, benutzername);
					 dispose();
				 }
				 else 
				 {
					 if(rolle == 'A') 
					 {
						 new StartansichtGUI('A', benutzername);
					 }
					 else if(rolle == 'G') 
					 {
						 new StartansichtGUI('G', benutzername);
					 }
					 else 
					 {
						 new StartansichtGUI('M', benutzername);
					 }
					 dispose();
				 }
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null, "Benutzername oder Kennwort falsch!");
				 passwordField.setText("");
			 }
		}
	}
}