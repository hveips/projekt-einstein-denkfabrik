package view.buchung_reservierungview;

/**
 * @author Hendrik Veips
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.buchungcontroller.BuchungBearbeitenController;
import controller.buchungcontroller.BuchungStornierenController;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.Buchung;
import model.tablemodels.BuchungBearbeitenTableModel;
import view.StartansichtGUI;
import view.extrasview.BearbeitenGUI;

public class BuchungBearbeitenStornierenGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JTable table;
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private BuchungBearbeitenTableModel tableModel;
	private JScrollPane scrollV;
	private JButton cancel = new JButton("Abbrechen");
	private JButton storno = new JButton("Stornieren");
	private JButton edit = new JButton("Bearbeiten");
	private JDialog dialog = new JDialog();
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private StartansichtGUI gui;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;
	
	private BuchungStornierenController storniereBuchungenController;
	private BuchungBearbeitenController bearbeiteBuchungController;
	
	/**
	 * Der Konstruktor erzeugt die BuchungBearbeitenStornierenGUI �ber mehrere Methoden und �bergibt gegebenfalls den Parameter bearbeiten.
	 * @param gui
	 * @param bearbeiten
	 */
	public BuchungBearbeitenStornierenGUI(StartansichtGUI gui, boolean bearbeiten)
	{
		this.gui = gui;
		this.storniereBuchungenController = new BuchungStornierenController(this);
		this.bearbeiteBuchungController = new BuchungBearbeitenController();
		erzeugeDialog(bearbeiten);
		Image image4 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();
		setTable();
		setButtons(bearbeiten);
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Gr��e, Location, BorderLayout, usw.).
	 * Ist der �bergebene Parameter bearbeiten = true, wird der Titel "Bearbeite Buchung" gesetzt; falls buchen = false ist, wird der Titel "Storniere Buchung" gesetzt.
	 * @param bearbeiten
	 */
	public void erzeugeDialog(boolean bearbeiten)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		if(bearbeiten == true)
		{
			dialog.setTitle("Bearbeite Buchung");
		}
		else
		{
			dialog.setTitle("Storniere Buchung");
		}
		dialog.setPreferredSize(new Dimension(900, 640));
		dialog.setLocation(510, 220);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche() 
	{
		panel2.setPreferredSize(new Dimension(840, 40));
		panel2.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel2.setBorder(cueBorder);
		add(panel2, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel2.add(cue);
		cueInput.setPreferredSize(new Dimension(580, 30));
		cueInput.setFont(schriftart1);
		panel2.add(cueInput);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 * �ber den �bergebenen Parameter bearbeiten entscheidet sie zus�tzlich, ob der Button edit oder der Button storno gesetzt wird.
	 * @param bearbeiten
	 */
	private void setButtons(boolean bearbeiten) 
	{
		panel1.setPreferredSize(new Dimension(840, 50));
		panel1.setLayout(flow2);
		panel1.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel1.setBorder(buttonBorder);
		add(panel1, border.SOUTH);
		if(bearbeiten == true)
		{
			edit.setPreferredSize(new Dimension(160, 40));
			edit.setFont(schriftart1);
			edit.setBackground(Color.WHITE);
			edit.addActionListener(this);
			panel1.add(edit);
			Image image3 = new ImageIcon(this.getClass().getResource("/BearbeitenIcon.jpeg")).getImage();
			edit.setIcon(new ImageIcon(image3));
		}
		else
		{		
			storno.setPreferredSize(new Dimension(160, 40));
			storno.setFont(schriftart1);
			storno.setBackground(Color.WHITE);
			storno.addActionListener(this);
			panel1.add(storno);
			Image image2 = new ImageIcon(this.getClass().getResource("/StornierenIcon.jpeg")).getImage();
			storno.setIcon(new ImageIcon(image2));
		}
		cancel.setPreferredSize(new Dimension(160, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel1.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}

	/**
	 * Die Methode �bergibt der Tabelle das RaumBuchenTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach �bergibt sie der Tabelle noch den sorter, damit der Nutzer die M�glichkeit hat die Eintr�ge zu sortieren und sie �bergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst m�glich macht.
	 */
	private void setTable() 
	{
		tableModel = new BuchungBearbeitenTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);
		tabellenwerteZentrieren();
		tabellenbreiteFestlegen();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}
	
	/**
	 * Die Methode legt die Spaltenbreite der einzelnen Spalten der Tabelle fest.
	 */
	private void tabellenbreiteFestlegen() 
	{
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(20);
		table.getColumnModel().getColumn(2).setPreferredWidth(30);
		table.getColumnModel().getColumn(5).setPreferredWidth(100);
		table.getColumnModel().getColumn(6).setPreferredWidth(100);
		table.getColumnModel().getColumn(7).setPreferredWidth(70);
		table.getColumnModel().getColumn(8).setPreferredWidth(20);
	}

	/**
	 * Die Methode zentriert die Tabellenwerte.
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	public JTable getTable()
	{
		return this.table;
	}
	
	public Buchung getBuchungRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row);
	}
	/**
	 * @author Christoph Hoppe
	 */
	public String getNameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getName();
	}
	/**
	 * @author Christoph Hoppe
	 */
	public String getEmailRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getEmail();
	}
	/**
	 * @author Christoph Hoppe
	 */
	public int getBuchungsNrRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getBuchungsNr();
	}
	
	public void databaseUpdated()
	{
		tableModel.databaseUpdated();
	}
	
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		/**
		 * Das System pr�ft ob eine Zeile ausgew�hlt ist oder nicht. Ist eine Zeile ausgew�hlt, wird die ausgew�hlte Zeile an den Konstrukter der BearbeitenGUI �bergeben und diese erzeugt;
		 * falls keine Zeile ausgew�hlt wurde, macht das System den Nutzer darauf aufmerksam.
		 */
		if (e.getSource() == this.edit) 
		{
			if(table.getSelectedRow() != -1)
			{
				int row = table.convertRowIndexToModel(table.getSelectedRow());
				new BearbeitenGUI(this, bearbeiteBuchungController, tableModel.getData(row));
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Bitte eine Zeile ausw�hlen.");
			}
		}
		if (e.getSource() == this.storno) 
		{			
			storniereBuchungenController.loeschenButtonAction();
			}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
	}
}