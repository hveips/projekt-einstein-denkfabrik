package view.buchung_reservierungview;

/**
 * @author Hendrik Veips, Christoph Hoppe
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import controller.buchungcontroller.BuchungBearbeitenController;
import controller.documentcontroller.IntegerOnlyDocument;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.pdfcontroller.RechnungPDF;
import controller.tablesearchcontroller.TableSearch;
import model.tablemodels.RechnungErstellenTableModel;
import view.StartansichtGUI;

public class ErstelleRechnungGUI extends JFrame implements ActionListener, ITabellenContainer 
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JTable table;
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private RechnungErstellenTableModel tableModel;
	private JScrollPane scrollV;
	private JButton cancel = new JButton("Abbrechen");
	private JButton bill = new JButton("Rechnung");
	private JLabel discount = new JLabel("Rabatt %");
	private JTextField discountInput = new JTextField();
	private JDialog dialog = new JDialog();
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private StartansichtGUI gui;

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private RechnungPDF rechnung = new RechnungPDF();
	private BuchungBearbeitenController controller;

	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;

	/**
	 * Der Konstruktor erzeugt die ErstelleRechnungGUI �ber mehrere Methoden.
	 * @param gui
	 */
	public ErstelleRechnungGUI(StartansichtGUI gui) 
	{
		this.gui = gui;
		erzeugeDialog();
		controller = new BuchungBearbeitenController();
		Image image4 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();
		setTable();
		setRabattInput();
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Die Methode �bergibt der Tabelle das BuchenBearbeitenTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach �bergibt sie der Tabelle noch den sorter, damit der Nutzer die M�glichkeit hat die Eintr�ge zu sortieren und sie �bergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst m�glich macht.
	 */
	private void setTable() 
	{
		tableModel = new RechnungErstellenTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);
		tabellenwerteZentrieren();
		tabellenbreiteFestlegen();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Gr��e, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Bearbeite Buchungen");
		dialog.setPreferredSize(new Dimension(900, 640));
		dialog.setLocation(510, 220);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche() 
	{
		panel3.setPreferredSize(new Dimension(840, 40));
		panel3.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel3.setBorder(cueBorder);
		add(panel3, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel3.add(cue);
		cueInput.setPreferredSize(new Dimension(580, 30));
		cueInput.setFont(schriftart1);
		panel3.add(cueInput);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Rabatt-Eingabe.
	 */
	private void setRabattInput() 
	{
		panel1.setPreferredSize(new Dimension(740, 50));
		panel1.setLayout(flow2);
		panel1.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel1.setBorder(buttonBorder);
		add(panel1, border.SOUTH);
		discount.setPreferredSize(new Dimension(80, 40));
		discount.setFont(schriftart1);
		panel1.add(discount);
		discountInput.setPreferredSize(new Dimension(100, 40));
		discountInput.setDocument(new IntegerOnlyDocument(2));
		discountInput.setFont(schriftart1);
		panel1.add(discountInput);
	}

	/**
	 * Die Methode setzt auf das zuvor gesetzte Panel ein Panel als Abstandshalter und danach die Buttons.
	 */
	private void setButtons() 
	{
		panel2.setPreferredSize(new Dimension(330, 40));
		panel2.setBackground(Color.lightGray);
		panel1.add(panel2);
		bill.setPreferredSize(new Dimension(160, 40));
		bill.setFont(schriftart1);
		bill.setBackground(Color.WHITE);
		bill.addActionListener(this);
		panel1.add(bill);
		Image image3 = new ImageIcon(this.getClass().getResource("/RechnungIcon.jpeg")).getImage();
		bill.setIcon(new ImageIcon(image3));
		cancel.setPreferredSize(new Dimension(160, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel1.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * Die Methode legt die Spaltenbreite der einzelnen Spalten der Tabelle fest.
	 */
	private void tabellenbreiteFestlegen() 
	{
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(20);
		table.getColumnModel().getColumn(2).setPreferredWidth(30);
		table.getColumnModel().getColumn(5).setPreferredWidth(100);
		table.getColumnModel().getColumn(6).setPreferredWidth(100);
		table.getColumnModel().getColumn(7).setPreferredWidth(70);
		table.getColumnModel().getColumn(8).setPreferredWidth(20);
	}

	/**
	 * Die Methode zentriert die Tabellenwerte.
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	public JTable getTable()
	{
		return this.table;
	}
	
	/**
	 * Die Methode liest aus der angew�hlt Zeile im BuchungTableModel den Wert der Email aus und gibt diesen als String zur�ck.
	 * @author Christoph Hoppe
	 */
	public String getEmailRow() 
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());;
		return tableModel.getData(row).getEmail();
	}

	public String getAnredeRow() 
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getAnrede();
	}

	public String getNameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getName();
	}
	
	public String getVornameRow() {
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getVorname();
	}
	
	public int getBuchungsNrRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getBuchungsNr();
	}
	
	public String getAnfangsDatumRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getAnfangsdatum();
	}
	
	public String getEndDatumRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getEnddatum();
	}
	
	public int getKundenIdRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getKuID();
	}
	
	public int getRaumNrRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getRaumNr();
	}
	
	public double getGesamtbetragRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getGesamtbetrag();
	}
	
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}

		/**
		 * Wenn der Button bill bet�tigt wird, erzeugt die Methode Variablen, welche als Parameter die Werte der Getter-methoden annehmen
		 * und �bergibt diese an die Methode getRechnungsDaten() in der Klasse RechnungPDF.
		 * @author Christoph Hoppe
		 */
		if (e.getSource() == this.bill)
		{
			int index = table.convertRowIndexToModel(table.getSelectedRow());
			if(discountInput.getText().equals(""))
			{
				controller.erstelleRechnung(tableModel.getData(index), 0);
			}
			else 
			{
				controller.erstelleRechnung(tableModel.getData(index), Integer.parseInt(discountInput.getText()));
			}
			createPDF();
			tableModel.databaseUpdated();
			discountInput.setText("");
		}
	}

	private void createPDF() {
		//  Parameter�bergabe an RechnungPDF
		double rabattfraction;
		String rabatt;
		if(discountInput.getText().equals(""))
		{
			rabattfraction = 1.0;
			rabatt = "0%";
		}
		else
		{
			rabattfraction = (double)(100-Integer.parseInt(discountInput.getText()))/100;
			rabatt = discountInput.getText() + "%";
		}
		int bunr = getBuchungsNrRow();
		String anfangsdatum = getAnfangsDatumRow();
		String enddatum = getEndDatumRow();
		String anrede = getAnredeRow();
		String name = getNameRow();
		String vorname = getVornameRow();
		String email = getEmailRow();
		int kuid = getKundenIdRow();
		int raumnr = getRaumNrRow();
		double gbetrag = getGesamtbetragRow()*rabattfraction;
		rechnung.getRechnungsDaten(bunr, anrede, name, vorname, gbetrag, kuid, anfangsdatum, enddatum, raumnr, email, rabatt);
	}
}