package view.buchung_reservierungview;

/**
 * @author Hendrik Veips, Niklas Nebeling
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JDateChooser;

import controller.buchungcontroller.BuchenReservierenController;
import controller.documentcontroller.UhrzeitDocument;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.tablemodels.KundenTableModel;
import view.StartansichtGUI;

public class BucheReserviereRaumGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JPanel panel4 = new JPanel();
	private JPanel panel5 = new JPanel();
	private JPanel panel6 = new JPanel();
	private JPanel panel7 = new JPanel();
	private JPanel panel8 = new JPanel();
	private JPanel panel9 = new JPanel();
	private JPanel panel10 = new JPanel();
	private JPanel panel11 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JLabel datum = new JLabel("Datum");
	private JLabel von = new JLabel("von");
	private JLabel bis = new JLabel("bis");
	private JLabel uhrzeit = new JLabel("Uhrzeit");
	private JLabel von2 = new JLabel("von");
	private JLabel bis2 = new JLabel("bis");
	private JDateChooser datumVon = new JDateChooser();
	private JTextField uhrzeitVon = new JTextField();
	private JDateChooser datumBis = new JDateChooser();
	private JTextField uhrzeitBis = new JTextField();
	private JTable table;
	private KundenTableModel tableModel;
	private JTextField cueInput = new JTextField();
	private JScrollPane scrollV;
	private JRadioButton eint�gig = new JRadioButton("Eint�gig");
	private JRadioButton mehrt�gig = new JRadioButton("Mehrt�gig");
	private ButtonGroup group = new ButtonGroup();
	private JButton cancel = new JButton("Abbrechen");
	private JButton forward = new JButton("Weiter");
	private JDialog dialog = new JDialog();
	
	private StartansichtGUI gui;
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;
	
	private BuchenReservierenController buchenReservierenController;
	
	private boolean buchen;
	
	/**
	 * Der Konstruktor erzeugt die BucheReserviereRaumGUI �ber mehrere Methoden und �bergibt gegebenfalls den Parameter buchen.
	 * @param gui
	 * @param buchen
	 */
	public BucheReserviereRaumGUI(StartansichtGUI gui, boolean buchen)
	{
		this.gui = gui;
		this.buchen = buchen;
		this.buchenReservierenController = new BuchenReservierenController(this);
		erzeugeDialog(buchen);
		Image image4 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();	
		setTable();
		setDatumUhrzeitVon();
		setDatumUhrzeitBis();
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Gr��e, Location, BorderLayout, usw.).
	 * Ist der �bergebene Parameter buchen = true, wird der Titel "Buche Raum" gesetzt; falls buchen = false ist, wird der Titel "Reserviere Raum" gesetzt.
	 * @param buchen
	 */
	public void erzeugeDialog(boolean buchen)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		if(buchen == true)
		{
			dialog.setTitle("Buche Raum");
		}
		else
		{
			dialog.setTitle("Reserviere Raum");
		}
		dialog.setPreferredSize(new Dimension(1000, 735));
		dialog.setLocation(460, 170);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche() 
	{
		panel1.setPreferredSize(new Dimension(940, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(680, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}

	/**
	 * Die Methode �bergibt der Tabelle das KundenTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach �bergibt sie der Tabelle noch den sorter, damit der Nutzer die M�glichkeit hat die Eintr�ge zu sortieren und sie �bergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst m�glich macht.
	 */
	private void setTable() 
	{
		tableModel = new KundenTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);	
		tabellenwerteZentrieren();
		tabellenbreiteFestlegen();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}
	
	/**
	 * Die Methode setzt Labels und die Textfelder bei der Uhrzeit bzw. DateChooser beim Datum f�r die Eingabe des Startpunktes einer Buchung/Reservierung.
	 */
	public void setDatumUhrzeitVon()
	{
		panel3.setPreferredSize(new Dimension(720, 40));
		panel3.setBackground(Color.lightGray);
		panel3.setLayout(flow);
		panel2.add(panel3);
		datum.setPreferredSize(new Dimension(70, 30));
		datum.setFont(schriftart1);
		panel3.add(datum);
		von.setPreferredSize(new Dimension(30, 30));
		von.setFont(schriftart1);
		panel3.add(von);
		datumVon.setPreferredSize(new Dimension(120, 30));
		datumVon.setFont(schriftart1);
		panel3.add(datumVon);
		panel4.setPreferredSize(new Dimension(50, 30));
		panel4.setBackground(Color.lightGray);
		panel3.add(panel4);
		uhrzeit.setPreferredSize(new Dimension(70, 30));
		uhrzeit.setFont(schriftart1);
		panel3.add(uhrzeit);
		von2.setPreferredSize(new Dimension(30, 30));
		von2.setFont(schriftart1);
		panel3.add(von2);
		uhrzeitVon.setPreferredSize(new Dimension(100, 30));
		uhrzeitVon.setFont(schriftart1);
		uhrzeitVon.setToolTipText("'HH:mm'");
		uhrzeitVon.setDocument(new UhrzeitDocument());
		panel3.add(uhrzeitVon);
		panel10.setPreferredSize(new Dimension(60, 30));
		panel10.setBackground(Color.lightGray);
		panel3.add(panel10);
		eint�gig.setPreferredSize(new Dimension(120, 30));
		eint�gig.setFont(schriftart1);
		eint�gig.setSelected(true);
		eint�gig.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e) 
		    {
		    	eint�gigAusgew�hlt();
		    }
		});
		group.add(eint�gig);
		panel3.add(eint�gig);
	}
	
	/**
	 * Die Methode setzt Labels und die Textfelder bei der Uhrzeit bzw. DateChooser beim Datum f�r die Eingabe des Endpunktes einer Buchung/Reservierung.
	 */
	public void setDatumUhrzeitBis()
	{
		panel5.setPreferredSize(new Dimension(720, 40));
		panel5.setBackground(Color.lightGray);
		panel5.setLayout(flow);
		panel2.add(panel5);
		panel6.setPreferredSize(new Dimension(70, 30));
		panel6.setBackground(Color.lightGray);
		panel5.add(panel6);
		bis.setPreferredSize(new Dimension(30, 30));
		bis.setFont(schriftart1);
		panel5.add(bis);
		datumBis.setPreferredSize(new Dimension(120, 30));
		datumBis.setFont(schriftart1);
		datumBis.setEnabled(false);
		panel5.add(datumBis);
		panel7.setPreferredSize(new Dimension(50, 30));
		panel7.setBackground(Color.lightGray);
		panel5.add(panel7);
		panel8.setPreferredSize(new Dimension(70, 30));
		panel8.setBackground(Color.lightGray);
		panel5.add(panel8);
		bis2.setPreferredSize(new Dimension(30, 30));
		bis2.setFont(schriftart1);
		panel5.add(bis2);
		uhrzeitBis.setPreferredSize(new Dimension(100, 30));
		uhrzeitBis.setFont(schriftart1);
		uhrzeitBis.setToolTipText("'HH:mm'");
		uhrzeitBis.setDocument(new UhrzeitDocument());
		panel5.add(uhrzeitBis);
		panel11.setPreferredSize(new Dimension(60, 30));
		panel11.setBackground(Color.lightGray);
		panel5.add(panel11);
		mehrt�gig.setPreferredSize(new Dimension(120, 30));
		mehrt�gig.setFont(schriftart1);
		mehrt�gig.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e) 
		    {
		    	mehrt�gigAusgew�hlt();
		    }
		});
		group.add(mehrt�gig);
		panel5.add(mehrt�gig);
	}

	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 */
	private void setButtons() 
	{
		panel2.setPreferredSize(new Dimension(940, 145));
		panel2.setLayout(flow);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		panel9.setPreferredSize(new Dimension(940, 50));
		panel9.setBackground(Color.lightGray);
		panel9.setLayout(flow2);
		panel2.add(panel9);
		forward.setPreferredSize(new Dimension(160, 40));
		forward.setFont(schriftart1);
		forward.setBackground(Color.WHITE);
		forward.addActionListener(this);
		panel9.add(forward);
		Image image2 = new ImageIcon(this.getClass().getResource("/Vorw�rtsGro�Icon.jpeg")).getImage();
		forward.setIcon(new ImageIcon(image2));
		cancel.setPreferredSize(new Dimension(160, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel9.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	/**
	 * Legt die Breiten der einzelnen Spalten fest
	 * @author Niklas Nebeling
	 */
	private void tabellenbreiteFestlegen() 
	{
		// Tabellenspalten Breite festlegen
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(45);
		table.getColumnModel().getColumn(5).setPreferredWidth(140);
		table.getColumnModel().getColumn(6).setPreferredWidth(40);
		table.getColumnModel().getColumn(7).setPreferredWidth(220);
		table.getColumnModel().getColumn(8).setPreferredWidth(140);
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public Date getDatumVonInput() 
	{
		return datumVon.getDate();
	}

	/**
	 * @author Niklas Nebeling
	 */
	public String getUhrzeitVonInput() 
	{
		return uhrzeitVon.getText();
	}

	/**
	 * @author Niklas Nebeling
	 */
	public Date getDatumBisInput() 
	{
		return datumBis.getDate();
	}


	/**
	 * @author Niklas Nebeling
	 */
	public String getUhrzeitBisInput() 
	{
		return uhrzeitBis.getText();
	}

	/**
	 * get-Methode, um die E-Mail des Kunden auszulesen, damit eine Buchungs-/Reservierungsbest�tigung verschickt
	 * werden kann
	 * @author Alexander Stavski
	 */
	public String getEmailRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getEmail();
	}
	
	/**
	 * @author Alexander Stavski
	 */
	public String getNameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getName();
	}
	
	/**
	 * @author Alexander Stavski
	 */
	public String getVornameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getVorname();
	}
	
	/**
	 * @author Alexander Stavski
	 */
	public int getKundenIDRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		return tableModel.getData(row).getKundenID();
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public JTable getTable() 
	{
		return this.table;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public KundenTableModel getTableModel() 
	{
		return tableModel;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	/**
	 * Diese Methode bestimmt die Aktionen die ausgef�hrt werden wenn Buttons gedr�ckt werden.
	 * Pr�ft ob eine Tabellenzeile ausgew�hlt ist und f�hrt dann controller-Methode aus,
	 * sonst Fehlermeldung.
	 * @author Niklas Nebeling
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.forward) 
		{
			if(table.getSelectedRow() != -1) 							
			{
				buchenReservierenController.weiter(eint�gig.isSelected(), buchen);
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einen Kunden ausw�hlen.\n");
			}
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
	}
	
	/**Wenn der Radio Button Mehrt�gig ausgew�hlt wird der zweite Datechooser enabled, die Uhrzeiten
	 * auf 06:00 - 21:00 gesetzt und disabled.
	 * @author Niklas Nebeling
	 */
	private void mehrt�gigAusgew�hlt() 
	{
		datumBis.setEnabled(true);
		uhrzeitVon.setText("06:00");
		uhrzeitVon.setEditable(false);
		uhrzeitBis.setText("21:00");
		uhrzeitBis.setEditable(false);
	}

	/**Wenn der Radio Button eint�gig ausgew�hlt wird der zweite Datechooser disabled, die Uhrzeiten
	 * auf "" gesetzt und enabled.
	 * @author Niklas Nebeling
	 */
	private void eint�gigAusgew�hlt() 
	{
		uhrzeitVon.setEditable(true);
		uhrzeitVon.setText("");
		uhrzeitBis.setEditable(true);
		uhrzeitBis.setText("");
		datumBis.setEnabled(false);
	}
	
}