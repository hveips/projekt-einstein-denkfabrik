package view.buchung_reservierungview;

/**
 * @author Hendrik Veips, Niklas Nebeling
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.buchungcontroller.BuchenReservierenController;
import controller.raumcontroller.RaumController;
import controller.raumcontroller.RaumÄndernAktivierenDeaktivierenController;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.Ausstattung;
import model.tablemodels.RaumBuchenTableModel;
import view.extrasview.ExtrasGUI;
import view.raumview.AusstattungAnzeigenVerändernGUI;

public class BucheReserviereRaumWeiterGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTable table;
	private RaumBuchenTableModel tableModel;
	private JTextField cueInput = new JTextField();
	private JScrollPane scrollV;
	private JButton extra = new JButton("Extras");
	private JButton cancel = new JButton("Abbrechen");
	private JButton book = new JButton("Buchen");
	private JButton reserve = new JButton("Reservieren");
	private JButton ausstattung = new JButton("Ausstattung");
	private JDialog dialog = new JDialog();
	
	private BucheReserviereRaumGUI gui;
	
	private Border cueBorder;
	private Border buttonBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;
	
	private BuchenReservierenController buchenReservierenController;
	private boolean buchen;
	
	private RaumController raumController = new RaumController();
	private RaumÄndernAktivierenDeaktivierenController controller;
	
	/**
	 * Der Konstruktor erzeugt die BucheReserviereRaumWeiterGUI über mehrere Methoden und übergibt gegebenfalls den Parameter buchen
	 * oder die Parameter anfangsdatum, enddatum und eintägig.
	 * @param gui
	 * @param buchen
	 * @param controller
	 * @param anfangsdatum
	 * @param enddatum
	 * @param eintägig
	 */
	public BucheReserviereRaumWeiterGUI(BucheReserviereRaumGUI gui, boolean buchen, BuchenReservierenController controller, String anfangsdatum, String enddatum, boolean eintägig)
	{
		this.buchen = buchen;
		this.buchenReservierenController = controller;
		this.gui = gui;
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();
		setTable(anfangsdatum, enddatum, eintägig);
		setButtons(buchen);
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Größe, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Weiter");
		dialog.setPreferredSize(new Dimension(800, 650));
		dialog.setLocation(560, 215);
		dialog.setLayout(border);
		dialog.setResizable(false);
		dialog.setModal(true);
	}

	/**
	 * Die Methode übergibt der Tabelle das RaumBuchenTableModel als TableModel, setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des BorderLayouts.
	 * Danach übergibt sie der Tabelle noch den sorter, damit der Nutzer die Möglichkeit hat die Einträge zu sortieren und sie übergibt dem Textfeld der Stichwortsuche mySearch,
	 * was dem Nutzer die Stichwortsuche erst möglich macht.
	 */
	private void setTable(String anfangsdatum, String enddatum, boolean eintägig) 
	{
		tableModel = new RaumBuchenTableModel(anfangsdatum, enddatum, eintägig);
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		add(scrollV, border.CENTER);
		scrollV.setPreferredSize(new Dimension(500, 500));
		tabellenwerteZentrieren();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche() 
	{
		panel1.setPreferredSize(new Dimension(740, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(480, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 * Über den übergebenen Parameter buchen entscheidet sie zusätzlich, ob der Button book oder der Button reserve gesetzt wird.
	 * @param buchen
	 */
	public void setButtons(boolean buchen)
	{
		panel2.setPreferredSize(new Dimension(740, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		ausstattung.setPreferredSize(new Dimension(170, 40));
		ausstattung.setFont(schriftart1);
		ausstattung.setBackground(Color.WHITE);
		ausstattung.addActionListener(this);
		panel2.add(ausstattung);
		Image image4 = new ImageIcon(this.getClass().getResource("/AusstattungIcon.jpeg")).getImage();
		ausstattung.setIcon(new ImageIcon(image4));
		extra.setPreferredSize(new Dimension(170, 40));
		extra.setFont(schriftart1);
		extra.setBackground(Color.WHITE);
		extra.addActionListener(this);
		panel2.add(extra);
		Image image3 = new ImageIcon(this.getClass().getResource("/ExtraIcon.jpeg")).getImage();
		extra.setIcon(new ImageIcon(image3));
		if(buchen == true)
		{
			book.setPreferredSize(new Dimension(170, 40));
			book.setFont(schriftart1);
			book.setBackground(Color.WHITE);
			book.addActionListener(this);
			panel2.add(book);
			Image image2 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
			book.setIcon(new ImageIcon(image2));
		}
		else
		{
			reserve.setPreferredSize(new Dimension(170, 40));
			reserve.setFont(schriftart1);
			reserve.setBackground(Color.WHITE);
			reserve.addActionListener(this);
			panel2.add(reserve);
			Image image2 = new ImageIcon(this.getClass().getResource("/ReservierenIcon.jpeg")).getImage();
			reserve.setIcon(new ImageIcon(image2));
		}
		cancel.setPreferredSize(new Dimension(170, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	private void tabellenwerteZentrieren() 
	{
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++) 
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public JTextField getCueInput() 
	{
		return this.cueInput;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public JTable getTable() 
	{
		return this.table;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public RaumBuchenTableModel getTableModel() 
	{
		return tableModel;
	}
	
	/**
	 * @author Niklas Nebeling
	 */
	public TableRowSorter<TableModel> getTableRowSorter() 
	{
		return this.sorter;
	}
	
	/**
	 * Diese Methode bestimmt die Aktionen die ausgeführt werden wenn Buttons gedrückt werden.
	 * Prüft ob eine Tabellenzeile ausgewählt ist und führt dann controller-Methoden aus,
	 * sonst Fehlermeldung.
	 * @author Niklas Nebeling
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.book) 
		{
			if(table.getSelectedRow() != -1) 							
			{
				int row = table.convertRowIndexToModel(table.getSelectedRow());
				buchenReservierenController.übergebeRaum(tableModel.getData(row));
				buchenReservierenController.buchen();
				dialog.dispose();
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einen Raum auswählen.");
			}
		}
		if(e.getSource() == this.reserve) 
		{
			if(table.getSelectedRow() != -1) 							
			{
				int row = table.convertRowIndexToModel(table.getSelectedRow());
				buchenReservierenController.übergebeRaum(tableModel.getData(row));
				buchenReservierenController.reservieren();
				dialog.dispose();
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einen Raum auswählen.");
			}
		}
		if(e.getSource() == this.cancel) 
		{
			dialog.dispose();
		}
		if(e.getSource() == this.extra)
		{
			if(table.getSelectedRow() != -1) 							
			{
				int row = table.convertRowIndexToModel(table.getSelectedRow());
				new ExtrasGUI(buchen, buchenReservierenController, tableModel.getData(row).getRaumNr());
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Bitte einn Raum auswählen.");
			}
		}
		if(e.getSource() == this.ausstattung)
		{
			if(table.getSelectedRow() != -1)
			{
				int row = table.convertRowIndexToModel(table.getSelectedRow());
				int rnr = tableModel.getData(row).getRaumNr();
				try
				{
					ArrayList<Ausstattung> ausstattungsListe = raumController.ladeAusstattung(rnr);
					new AusstattungAnzeigenVerändernGUI(controller, ausstattungsListe, false);
				}
				catch(SQLException ex)
				{
					JOptionPane.showMessageDialog(null, "Fehler bei der Datenbank Verbindung!");
				}
			}
		}
	}
}