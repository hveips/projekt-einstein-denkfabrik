package view.buchung_reservierungview;

/**
 * @author Hendrik Veips, Alexander Stavski
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import controller.reservierungcontroller.ReservierungStornierenController;
import controller.tablesearchcontroller.ITabellenContainer;
import controller.tablesearchcontroller.TableSearch;
import model.Reservierung;
import model.tablemodels.ReservierungTableModel;
import view.StartansichtGUI;

public class ReservierungStornierenGUI extends JFrame implements ActionListener, ITabellenContainer
{
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JScrollPane scrollV;
	private JButton cancel = new JButton("Abbrechen");
	private JButton delete = new JButton("Stornieren");
	private Border buttonBorder;
	private Border cueBorder;
	private JLabel cue = new JLabel("Stichwortsuche");
	private JTextField cueInput = new JTextField();
	private JDialog dialog;

	private StartansichtGUI gui;

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.PLAIN, 16);

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow1 = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);

	private JTable table;
	private ReservierungTableModel tableModel;
	private DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	private TableRowSorter<TableModel> sorter;

	private ReservierungStornierenController controller;

	/**
	 * Der Konstruktor erzeugt die RaumÄndernAktivierenDeaktivierenGUI
	 * über mehrere Methoden.
	 * @param gui
	 */
	public ReservierungStornierenGUI(StartansichtGUI gui)
	{
		this.gui = gui;
		this.controller = new ReservierungStornierenController(this);
		erzeugeDialog();
		Image image4 = new ImageIcon(this.getClass().getResource("/ReservierenIcon.jpeg")).getImage();
		dialog.setIconImage(image4);
		setStichwortsuche();
		setTable();
		setButtons();
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende
	 * Konfiguration (Titel, Größe, Location, BorderLayout, usw.).
	 */
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Storniere Reservierungen");
		dialog.setPreferredSize(new Dimension(900, 640));
		dialog.setLocation(510, 220);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode setzt ein Panel auf das NORTH-Feld des BorderLayouts und setzt
	 * darauf das Label und das Textfeld der Stichwortsuche.
	 */
	private void setStichwortsuche()
	{
		panel1.setPreferredSize(new Dimension(840, 40));
		panel1.setBackground(Color.lightGray);
		cueBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		panel1.setBorder(cueBorder);
		panel1.setLayout(flow1);
		add(panel1, border.NORTH);
		cue.setPreferredSize(new Dimension(240, 30));
		cue.setFont(schriftart1);
		panel1.add(cue);
		cueInput.setPreferredSize(new Dimension(580, 30));
		cueInput.setFont(schriftart1);
		panel1.add(cueInput);
	}

	/**
	 * Die Methode übergibt der Tabelle das ReservierungTableModel als TableModel,
	 * setzt eine ScrollPane und setzt die Tabelle auf das CENTER-Feld des
	 * BorderLayouts. Danach übergibt sie der Tabelle noch den sorter, damit der
	 * Nutzer die Möglichkeit hat die Einträge zu sortieren und sie übergibt dem
	 * Textfeld der Stichwortsuche mySearch, was dem Nutzer die Stichwortsuche erst
	 * möglich macht.
	 */
	private void setTable()
	{
		tableModel = new ReservierungTableModel();
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFont(schriftart2);
		table.getTableHeader().setFont(schriftart2);
		scrollV = new JScrollPane(table);
		scrollV.setPreferredSize(new Dimension(500, 500));
		add(scrollV, border.CENTER);
		tabellenwerteZentrieren();
		tabellenbreiteFestlegen();
		DocumentListener mySearch = new TableSearch(this);
		sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		cueInput.getDocument().addDocumentListener(mySearch);
	}

	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt
	 * die Buttons auf dieses Panel.
	 */
	private void setButtons()
	{
		panel2.setPreferredSize(new Dimension(840, 50));
		panel2.setLayout(flow2);
		panel2.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 0, 25, Color.lightGray);
		panel2.setBorder(buttonBorder);
		add(panel2, border.SOUTH);
		delete.setPreferredSize(new Dimension(160, 40));
		delete.setFont(schriftart1);
		delete.setBackground(Color.WHITE);
		delete.addActionListener(this);
		panel2.add(delete);
		Image image2 = new ImageIcon(this.getClass().getResource("/StornierenIcon.jpeg")).getImage();
		delete.setIcon(new ImageIcon(image2));
		cancel.setPreferredSize(new Dimension(160, 40));
		cancel.setFont(schriftart1);
		cancel.setBackground(Color.WHITE);
		cancel.addActionListener(this);
		panel2.add(cancel);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		cancel.setIcon(new ImageIcon(image1));
	}

	/**
	 * Die Methode legt die Spaltenbreite der einzelnen Spalten der Tabelle fest.
	 */
	private void tabellenbreiteFestlegen()
	{
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(20);
		table.getColumnModel().getColumn(2).setPreferredWidth(30);
		table.getColumnModel().getColumn(5).setPreferredWidth(100);
		table.getColumnModel().getColumn(6).setPreferredWidth(100);
		table.getColumnModel().getColumn(7).setPreferredWidth(70);
		table.getColumnModel().getColumn(8).setPreferredWidth(20);
	}

	/**
	 * @author Alexander Stavski
	 */
	private void tabellenwerteZentrieren()
	{
		// Tabellenwerte zentrieren
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < tableModel.getColumnCount(); i++)
		{
			table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
		}
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTextField getCueInput()
	{
		return this.cueInput;
	}

	/**
	 * @author Alexander Stavski
	 */
	public JTable getTable()
	{
		return this.table;
	}

	/**
	 * @author Alexander Stavski
	 */
	public TableRowSorter<TableModel> getTableRowSorter()
	{
		return this.sorter;
	}

	/**
	 * @author Alexander Stavski
	 */
	public ReservierungTableModel getDatabaseUpdated()
	{
		tableModel.databaseUpdated();
		return tableModel;
	}

	/**
	 * @author Alexander Stavski
	 */
	public Reservierung getReservierungRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Reservierung rs = tableModel.getDataReservierung(row);
		return rs;
	}

	/**
	 * @author Christoph Hoppe
	 */
	public int getResnrRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Reservierung rs = tableModel.getDataReservierung(row);
		int resnr = rs.getReservierungsNr();
		return resnr;
	}

	/**
	 * @author Christoph Hoppe
	 */
	public String getEmailRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Reservierung rs = tableModel.getDataReservierung(row);
		String email = rs.getEmail();
		return email;
	}

	/**
	 * @author Christoph Hoppe
	 */
	public String getNameRow()
	{
		int row = table.convertRowIndexToModel(table.getSelectedRow());
		Reservierung rs = tableModel.getDataReservierung(row);
		String name = rs.getName();
		return name;
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == this.cancel)
		{
			dialog.dispose();
		}
		if (e.getSource() == this.delete)
		{
			controller.loeschenButtonAction();
		}
	}
}