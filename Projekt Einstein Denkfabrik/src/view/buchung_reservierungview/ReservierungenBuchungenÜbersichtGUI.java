package view.buchung_reservierungview;

/**
 * @author Hendrik Veips
 */

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import controller.CalendarController;
import view.StartansichtGUI;

public class ReservierungenBuchungen�bersichtGUI extends JFrame implements ActionListener
{
	private JTextArea area = new JTextArea();
	private JPanel areaPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JButton close = new JButton("Schlie�en");
	private JScrollPane scrollV;
	private JDialog dialog;
	
	private StartansichtGUI gui;
	
	private Border areaBorder;
	private Border buttonBorder;
	
	private CalendarController calendarController = new CalendarController();
	private String tag;
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout(FlowLayout.RIGHT);
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private DecimalFormat format1 = new DecimalFormat("00");
	
	private ArrayList<String> buchungen = new ArrayList<String>();
	private ArrayList<String> buchungen2 = new ArrayList<String>();
	private ArrayList<String> reservierungen = new ArrayList<String>();
	private ArrayList<String> reservierungen2 = new ArrayList<String>();
	
	/**
	 * Der Konstruktor erzeugt die ReservierungenBuchungen�bersichtGUI �ber mehrere Methoden und �bergibt gegebenfalls die Parameter day, month und tag.
	 * @param gui
	 * @param day
	 * @param month
	 * @param year
	 */
	public ReservierungenBuchungen�bersichtGUI(StartansichtGUI gui, int day, int month, int year)
	{
		month = month + 1;
		tag = format1.format(day);
		this.gui = gui;
		erzeugeDialog(tag, month, year);
		setArea();
		setReservierungen(day, month, year);
		setBuchungen(day, month, year);
		setScrollPane();
		setButton();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	/**
	 * Die Methode erzeugt das Fenster als Dialog und setzt die grundlegende Konfiguration (Titel, Gr��e, Location, BorderLayout, usw.).
	 * Dem Titel werden die Parameter tag, month und year �bergeben.
	 * @param tag
	 * @param month
	 * @param year
	 */
	public void erzeugeDialog(String tag, int month, int year)
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Reservierungen & Buchungen am " + tag + "." + month + "." + year);
		dialog.setPreferredSize(new Dimension(650, 390));
		dialog.setLocation(635, 345);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	/**
	 * Die Methode setzt ein ScrollPane auf das Panel f�r die Textarea und bindet es an diese.
	 */
	private void setScrollPane() 
	{
		scrollV = new JScrollPane(area);
		scrollV.setPreferredSize(new Dimension(590, 300));
		scrollV.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		areaPanel.add(scrollV);
	}

	/**
	 * Die Methode �bergibt die Parameter day, month und year an den CalendarController und l�sst sich einmal die Buchungen von heute zur�ckgeben
	 * und einmal die Buchungen zur�ckgeben, bei denen der heutige Tag in deren Zeitspanne liegt.
	 * Danach werden die vorhandenen Buchungen in die Textarea eingef�gt.
	 * @param day
	 * @param month
	 * @param year
	 */
	private void setBuchungen(int day, int month, int year) 
	{
		area.append("\n");
		area.append("Buchung/en:");
		area.append("\n");
		area.append("\n");
		area.append("  ");
		buchungen = calendarController.getBuchungen(day, month, year);
		buchungen2 = calendarController.getBuchungen2(day, month, year);
		if(buchungen.size() == 0 && buchungen2.size() == 0)
		{
			area.append("Es liegen noch keine Buchungen an diesem Datum vor!");
		}
		else
		{
			int i = 0;
			while(i < buchungen.size())
			{	
				area.append((i + 1) + ". " + buchungen.get(i));
				area.append("\n");
				area.append("  ");
				i++;
			}
			if(buchungen2.size() > 0 && buchungen.size() > 0)
			{
				area.append("\n");
				area.append("  ");
			}
			for(int j = 0;j < buchungen2.size();j++)
			{	
				area.append((i + 1) + ". " + buchungen2.get(j));
				area.append("\n");
				area.append("  ");
				i++;
			}
		}
	}

	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt darauf den Button close.
	 */
	private void setButton() 
	{
		buttonPanel.setPreferredSize(new Dimension(560, 50));
		buttonPanel.setBackground(Color.lightGray);
		buttonPanel.setLayout(flow);
		buttonBorder = BorderFactory.createMatteBorder(0, 10, 0, 10, Color.lightGray);
		buttonPanel.setBorder(buttonBorder);
		add(buttonPanel, border.SOUTH);
		close.setPreferredSize(new Dimension(160, 40));
		close.setBackground(Color.WHITE);
		close.setFont(schriftart1);
		close.addActionListener(this);
		buttonPanel.add(close);
		Image image1 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		close.setIcon(new ImageIcon(image1));
	}

	/**
	 * Die Methode �bergibt die Parameter day, month und year an den CalendarController und l�sst sich einmal die Reservierungen von heute zur�ckgeben
	 * und einmal die Reservierungen zur�ckgeben, bei denen der heutige Tag in deren Zeitspanne liegt.
	 * Danach werden die vorhandenen Reservierungen in die Textarea eingef�gt.
	 * @param day
	 * @param month
	 * @param year
	 */
	private void setReservierungen(int day, int month, int year) 
	{
		area.append("Reservierung/en:");
		area.append("\n");
		area.append("\n");
		area.append("  ");
		reservierungen = calendarController.getReservierungen(day, month, year);
		reservierungen2 = calendarController.getReservierungen2(day, month, year);
		if(reservierungen.size() == 0 && reservierungen2.size() == 0)
		{
			area.append("Es liegen noch keine Reservierungen an diesem Datum vor!");
		}
		else
		{
			int i = 0;
			while(i < reservierungen.size())
			{	
				area.append((i + 1) + ". " + reservierungen.get(i));
				area.append("\n");
				area.append("  ");
				i++;	
			}
			if(reservierungen2.size() > 0 && reservierungen.size() > 0)
			{
				area.append("\n");
				area.append("  ");
			}
			for(int j = 0;j < reservierungen2.size();j++)
			{	
				area.append((i + 1) + ". " + reservierungen2.get(j));
				area.append("\n");
				area.append("  ");
				i++;
			}
		}
		area.append("\n");
	}

	/**
	 * Die Methode setzt ein Panel f�r die Textarea auf das CENTER-Feld des BorderLayouts und konfiguriert noch die Textarea.
	 */
	private void setArea() 
	{
		areaPanel.setPreferredSize(new Dimension(560, 340));
		areaPanel.setBackground(Color.lightGray);
		areaBorder = BorderFactory.createMatteBorder(10, 10, 0, 10, Color.lightGray);
		areaPanel.setBorder(areaBorder);
		area.setFont(schriftart1);
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		area.setEditable(false);
		add(areaPanel, border.CENTER);
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == this.close) 
		{
			dialog.dispose();
		}
	}
}