package view;

/**
 * @author Hendrik Veips
 */

import java.awt.*;
import java.text.*;
import javax.swing.*;
import javax.swing.border.Border;

import view.buchung_reservierungview.BucheReserviereRaumGUI;
import view.buchung_reservierungview.BuchungBearbeitenStornierenGUI;
import view.buchung_reservierungview.ErstelleRechnungGUI;
import view.buchung_reservierungview.ReservierungStornierenGUI;
import view.buchung_reservierungview.ReservierungenBuchungenÜbersichtGUI;
import view.extrasview.ExtraHinzufügenEntfernenGUI;
import view.extrasview.PreisExtrasÄndernGUI;
import view.kundenview.KundeHinzufügenGUI;
import view.kundenview.KundeÄndernEntfernenStatistikGUI;
import view.loginview.LoginGUI;
import view.mitarbeiterview.MitarbeiterHinzufügenGUI;
import view.mitarbeiterview.MitarbeiterÄndernEntfernenGUI;
import view.passwortview.PasswortÄndernGUI;
import view.raumview.PreisKategorienÄndernGUI;
import view.raumview.RaumHinzufügenGUI;
import view.raumview.RaumÄndernAktivierenDeaktivierenGUI;

import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartansichtGUI extends JFrame implements ActionListener
{
	private JButton cancelRoom = new JButton("  Storniere Buchung");
	private JButton reserveRoom = new JButton("  Reserviere Raum");
	private JButton bookRoom = new JButton("  Buche Raum      ");
	private JButton logout = new JButton("Logout");
	private JButton pwÄndernButton = new JButton("Passwort ändern");
	private JButton forward = new JButton();
	private JButton back = new JButton();
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private JPanel panel4 = new JPanel();
	private JPanel panel5 = new JPanel();
	private JPanel panel6 = new JPanel();
	private JTextArea area = new JTextArea();
	private JLabel image = new JLabel();
	private JTextArea area2 = new JTextArea();
	private JMenuBar bar = new JMenuBar();
	private JMenu kunde = new JMenu("Kunde");
	private JMenuItem kunde1 = new JMenuItem("Kunde hinzufügen...");
	private JMenuItem kunde2 = new JMenuItem("Kunde ändern...");
	private JMenuItem kunde3 = new JMenuItem("Kunde entfernen...");
	private JMenuItem kunde4 = new JMenuItem("Statistik erstellen...");
	private JMenu mitarbeiter = new JMenu("Mitarbeiter");
	private JMenuItem mitarbeiter1 = new JMenuItem("Mitarbeiter hinzufügen...");
	private JMenuItem mitarbeiter2 = new JMenuItem("Mitarbeiter ändern...");
	private JMenuItem mitarbeiter3 = new JMenuItem("Mitarbeiter entfernen...");
	private JMenu raum = new JMenu("Raum");
	private JMenuItem raum1 = new JMenuItem("Raum hinzufügen...");
	private JMenuItem raum2 = new JMenuItem("Raum ändern...");
	private JMenuItem raum3 = new JMenuItem("Raum de/aktivieren...");
	private JMenu buchung = new JMenu("Buchung");
	private JMenuItem buchung1 = new JMenuItem("Bearbeite Buchungen...");
	private JMenuItem buchung2 = new JMenuItem("Erstelle Rechnung...");
	private JMenu reservierung = new JMenu("Reservierung");
	private JMenuItem reservierung1 = new JMenuItem("Storniere Reservierungen...");
	private JMenu preis = new JMenu("Preis");
	private JMenuItem preis1 = new JMenuItem("Raum-Preise ändern...");
	private JMenuItem preis2 = new JMenuItem("Extra-Preise ändern...");
	private JMenu extra = new JMenu("Extra");
	private JMenuItem extra1 = new JMenuItem("Extra hinzufügen...");
	private JMenuItem extra2 = new JMenuItem("Extra entfernen...");
	private JMenuBar barWoche1 = new JMenuBar();
	private JMenuBar barWoche2 = new JMenuBar();
	private JMenuBar barWoche3 = new JMenuBar();
	private JMenuBar barWoche4 = new JMenuBar();
	private JMenuBar barWoche5 = new JMenuBar();
	private JMenuBar barWoche6 = new JMenuBar();
	private JPanel abstand1 = new JPanel();
	private JPanel abstand2 = new JPanel();
	private JPanel abstand3 = new JPanel();
	private JPanel abstand4 = new JPanel();
	private JPanel abstand5 = new JPanel();
	private JPanel abstand6 = new JPanel();
	private JPanel abstand7 = new JPanel();
	private JPanel abstand8 = new JPanel();
	private JPanel abstand9 = new JPanel();
	private JPanel abstand10 = new JPanel();
	private JPanel abstand11 = new JPanel();
	private JPanel abstand12 = new JPanel();
	private JMenuItem tag1 = new JMenuItem();
	private JMenuItem tag2 = new JMenuItem();
	private JMenuItem tag3 = new JMenuItem();
	private JMenuItem tag4 = new JMenuItem();
	private JMenuItem tag5 = new JMenuItem();
	private JMenuItem tag6 = new JMenuItem();
	private JMenuItem tag7 = new JMenuItem();
	private JMenuItem tag8 = new JMenuItem();
	private JMenuItem tag9 = new JMenuItem();
	private JMenuItem tag10 = new JMenuItem();
	private JMenuItem tag11 = new JMenuItem();
	private JMenuItem tag12 = new JMenuItem();
	private JMenuItem tag13 = new JMenuItem();
	private JMenuItem tag14 = new JMenuItem();
	private JMenuItem tag15 = new JMenuItem();
	private JMenuItem tag16 = new JMenuItem();
	private JMenuItem tag17 = new JMenuItem();
	private JMenuItem tag18 = new JMenuItem();
	private JMenuItem tag19 = new JMenuItem();
	private JMenuItem tag20 = new JMenuItem();
	private JMenuItem tag21 = new JMenuItem();
	private JMenuItem tag22 = new JMenuItem();
	private JMenuItem tag23 = new JMenuItem();
	private JMenuItem tag24 = new JMenuItem();
	private JMenuItem tag25 = new JMenuItem();
	private JMenuItem tag26 = new JMenuItem();
	private JMenuItem tag27 = new JMenuItem();
	private JMenuItem tag28 = new JMenuItem();
	private JMenuItem tag29 = new JMenuItem();
	private JMenuItem tag30 = new JMenuItem();
	private JMenuItem tag31 = new JMenuItem();
	private JMenuItem tag32 = new JMenuItem();
	private JMenuItem tag33 = new JMenuItem();
	private JMenuItem tag34 = new JMenuItem();
	private JMenuItem tag35 = new JMenuItem();
	private JMenuItem tag36 = new JMenuItem();
	private JMenuItem tag37 = new JMenuItem();
	private JMenuItem tag38 = new JMenuItem();
	private JMenuItem tag39 = new JMenuItem();
	private JMenuItem tag40 = new JMenuItem();
	private JMenuItem tag41 = new JMenuItem();
	private JMenuItem tag42 = new JMenuItem();
	
	private Border imageBorder;
	private Border logoutBorder;
	private Border rightBorder;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	private Font schriftart2 = new Font("Arial", Font.BOLD, 18);
	private DecimalFormat format1 = new DecimalFormat("00");
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.LEFT);
	
	private JMenuBar menubars[] = {barWoche1, barWoche2, barWoche3, barWoche4, barWoche5, barWoche6}; 
	private JPanel panels[] = {abstand1, abstand2, abstand3, abstand4, abstand5, abstand6};
	private JPanel panels2[] = {abstand7, abstand8, abstand9, abstand10, abstand11, abstand12};
	private JMenuItem menus[] = {tag1, tag2, tag3, tag4, tag5, tag6, tag7, tag8, tag9, tag10, tag11, tag12, tag13, tag14, tag15, tag16, tag17, tag18, tag19, tag20, tag21, tag22, tag23, tag24, tag25, tag26, tag27, tag28, tag29, tag30, tag31, tag32, tag33, tag34, tag35, tag36, tag37, tag38, tag39, tag40, tag41, tag42};
	
	private GregorianCalendar calendar = new GregorianCalendar();
	private int heutigerMonat = calendar.get(GregorianCalendar.MONTH);
	private int month = heutigerMonat;
	private int heutigesJahr = calendar.get(GregorianCalendar.YEAR);
	private int year = heutigesJahr;
	private int heutigerTag = calendar.get(GregorianCalendar.DAY_OF_MONTH);
	private String wochenTage[] = { "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So" };
	private String monate[] = {"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};
	private int tag;
	
	private char rolle;
	private String benutzername;
	private boolean buchen;
	
	/**
	 * Der Konstruktor setzt zuerst die Grundkonfiguration des Fensters (Titel, Größe, BorderLayout, usw.) und setzt dann über den Aufruf von Methoden die einzelnen Elemente der 
	 * Startansicht zusammen. Bei einer nötigen Rollenunterscheidung wird der Methode noch der Parameter "rolle" übergeben, bei der Kalendarerzeugung werden gegebenenfalls der Methode
	 * die Parameter "heutigesJahr" und "heutigerMonat" übergeben.
	 * @param rolle
	 * @param benutzername
	 */
	public StartansichtGUI(char rolle, String benutzername)
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Einstein Denkfabrik");
		setPreferredSize(new Dimension(1920, 1080));
		setFont(schriftart1);
		setLayout(border);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.rolle = rolle;
		this.benutzername = benutzername;
		Image image4 = new ImageIcon(this.getClass().getResource("/StartbildschirmIcon.jpeg")).getImage();
		setIconImage(image4);
		setBar();
		setKundeMenu();
		setMitarbeiterMenu(rolle);
		setRaumMenu();
		setExtraMenu(rolle);
		setPreisMenu(rolle);
		setBuchungMenu();
		setReservierungMenu();
		setImage();
		setPanelRight();
		setBackButton();
		setCalendarArea(heutigesJahr, heutigerMonat);
		setForwardButton();
		setCalendarMenus();
		setCalendarDays(heutigesJahr, heutigerMonat);
		setCalendarButtom();
		setButtonsRight();
		setLogoutButton();
		setPWButton();
		pack();
		setVisible(true);
	}
	
	/**
	 * Die Methode setzt die Menüleiste auf das NORTH-Feld des BorderLayouts.
	 */
	public void setBar()
	{
		bar.setPreferredSize(new Dimension(1920, 32));
		bar.setOpaque(true);
		bar.setBackground(Color.WHITE);
		add(bar, border.NORTH);
	}
	
	/**
	 * Die Methode setzt das Kundenmenü auf die Menüleiste.
	 */
	public void setKundeMenu()
	{
		kunde.setPreferredSize(new Dimension(200, 32));
		kunde.setFont(schriftart1);
		kunde.setOpaque(true);
		kunde.setBackground(Color.WHITE);
		bar.add(kunde);
		Image image1 = new ImageIcon(this.getClass().getResource("/KundenIcon.jpeg")).getImage();
		kunde.setIcon(new ImageIcon(image1));
		kunde1.setPreferredSize(new Dimension(200, 25));
		kunde1.setFont(schriftart1);
		kunde1.addActionListener(this);
		kunde.add(kunde1);
		kunde2.setPreferredSize(new Dimension(200, 25));
		kunde2.setFont(schriftart1);
		kunde2.addActionListener(this);
		kunde.add(kunde2);
		kunde3.setPreferredSize(new Dimension(200, 25));
		kunde3.setFont(schriftart1);
		kunde3.addActionListener(this);
		kunde.add(kunde3);
		kunde4.setPreferredSize(new Dimension(200, 25));
		kunde4.setFont(schriftart1);
		kunde4.addActionListener(this);
		kunde.add(kunde4);
	}
	
	/**
	 * Die Methode setzt das Mitarbeitermenü auf die Menüleiste. Falls die der übergebene Parameter rolle = 'M' ist, wird dieses Menü gesperrt.
	 * @param rolle
	 */
	public void setMitarbeiterMenu(char rolle)
	{
		mitarbeiter.setPreferredSize(new Dimension(235, 32));
		mitarbeiter.setFont(schriftart1);
		mitarbeiter.setOpaque(true);
		mitarbeiter.setBackground(Color.WHITE);
		bar.add(mitarbeiter);
		Image image1 = new ImageIcon(this.getClass().getResource("/MitarbeiterIcon.jpeg")).getImage();
		mitarbeiter.setIcon(new ImageIcon(image1));
		mitarbeiter1.setPreferredSize(new Dimension(235, 25));
		mitarbeiter1.setFont(schriftart1);
		mitarbeiter1.addActionListener(this);
		mitarbeiter.add(mitarbeiter1);
		mitarbeiter2.setPreferredSize(new Dimension(235, 25));
		mitarbeiter2.setFont(schriftart1);
		mitarbeiter2.addActionListener(this);
		mitarbeiter.add(mitarbeiter2);
		mitarbeiter3.setPreferredSize(new Dimension(235, 25));
		mitarbeiter3.setFont(schriftart1);
		mitarbeiter3.addActionListener(this);
		mitarbeiter.add(mitarbeiter3);
		if(rolle == 'M')
		{
			mitarbeiter.setEnabled(false);
		}
	}
	
	/**
	 * Die Methode setzt das Raummenü auf die Menüleiste.
	 */
	public void setRaumMenu()
	{
		raum.setPreferredSize(new Dimension(210, 32));
		raum.setFont(schriftart1);
		raum.setOpaque(true);
		raum.setBackground(Color.WHITE);
		bar.add(raum);
		Image image1 = new ImageIcon(this.getClass().getResource("/RaumIcon.jpeg")).getImage();
		raum.setIcon(new ImageIcon(image1));
		raum1.setPreferredSize(new Dimension(210, 25));
		raum1.setFont(schriftart1);
		raum1.addActionListener(this);
		raum.add(raum1);
		raum2.setPreferredSize(new Dimension(210, 25));
		raum2.setFont(schriftart1);
		raum2.addActionListener(this);
		raum.add(raum2);
		raum3.setPreferredSize(new Dimension(210, 25));
		raum3.setFont(schriftart1);
		raum3.addActionListener(this);
		raum.add(raum3);
	}
	
	/**
	 * Die Methode setzt das Extramenü auf die Menüleiste. Falls die der übergebene Parameter rolle = 'M' ist, wird dieses Menü gesperrt.
	 * @param rolle
	 */
	public void setExtraMenu(char rolle)
	{
		extra.setPreferredSize(new Dimension(195, 32));
		extra.setFont(schriftart1);
		extra.setOpaque(true);
		extra.setBackground(Color.WHITE);
		bar.add(extra);
		Image image1 = new ImageIcon(this.getClass().getResource("/ExtraIcon.jpeg")).getImage();
		extra.setIcon(new ImageIcon(image1));
		extra1.setPreferredSize(new Dimension(195, 25));
		extra1.setFont(schriftart1);
		extra1.addActionListener(this);
		extra.add(extra1);
		extra2.setPreferredSize(new Dimension(195, 25));
		extra2.setFont(schriftart1);
		extra2.addActionListener(this);
		extra.add(extra2);
		if(rolle == 'M')
		{
			extra.setEnabled(false);
		}
	}
	
	/**
	 * Die Methode setzt das Preismenü auf die Menüleiste. Falls die der übergebene Parameter rolle = 'M' ist, wird dieses Menü gesperrt.
	 * @param rolle
	 */
	public void setPreisMenu(char rolle)
	{
		preis.setPreferredSize(new Dimension(225, 32));
		preis.setFont(schriftart1);
		preis.setOpaque(true);
		preis.setBackground(Color.WHITE);
		bar.add(preis);
		Image image1 = new ImageIcon(this.getClass().getResource("/PreisIcon.jpeg")).getImage();
		preis.setIcon(new ImageIcon(image1));
		preis1.setPreferredSize(new Dimension(225, 25));
		preis1.setFont(schriftart1);
		preis1.addActionListener(this);
		preis.add(preis1);
		preis2.setPreferredSize(new Dimension(225, 25));
		preis2.setFont(schriftart1);
		preis2.addActionListener(this);
		preis.add(preis2);
		if(rolle == 'M')
		{
			preis.setEnabled(false);
		}
	}
	
	/**
	 * Die Methode setzt das Buchungsmenü auf die Menüleiste.
	 */
	public void setBuchungMenu()
	{
		buchung.setPreferredSize(new Dimension(230, 32));
		buchung.setFont(schriftart1);
		buchung.setOpaque(true);
		buchung.setBackground(Color.WHITE);
		bar.add(buchung);
		Image image1 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
		buchung.setIcon(new ImageIcon(image1));
		buchung1.setPreferredSize(new Dimension(230, 25));
		buchung1.setFont(schriftart1);
		buchung1.addActionListener(this);
		buchung.add(buchung1);
		buchung2.setPreferredSize(new Dimension(230, 25));
		buchung2.setFont(schriftart1);
		buchung2.addActionListener(this);
		buchung.add(buchung2);
	}
	
	/**
	 * Die Methode setzt das Reservierungsmenü auf die Menüleiste.
	 */
	public void setReservierungMenu()
	{
		reservierung.setPreferredSize(new Dimension(260, 32));
		reservierung.setFont(schriftart1);
		reservierung.setOpaque(true);
		reservierung.setBackground(Color.WHITE);
		bar.add(reservierung);
		Image image1 = new ImageIcon(this.getClass().getResource("/ReservierenIcon.jpeg")).getImage();
		reservierung.setIcon(new ImageIcon(image1));
		reservierung1.setPreferredSize(new Dimension(260, 25));
		reservierung1.setFont(schriftart1);
		reservierung1.addActionListener(this);
		reservierung.add(reservierung1);
	}
	
	/**
	 * Die Methode setzt das Logo der Einstein Denkfabrik auf das CENTER-Feld des BorderLayouts.
	 */
	public void setImage()
	{
		image.setPreferredSize(new Dimension(1148, 666));
		image.setOpaque(true);
		image.setBackground(Color.lightGray);
		imageBorder = BorderFactory.createMatteBorder(130, 25, 0, 50, Color.lightGray);
		image.setBorder(imageBorder);
		add(image, border.CENTER);
		Image image1 = new ImageIcon(this.getClass().getResource("/Einstein Denkfabrik Logo.jpeg")).getImage();
		image.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * Die Methode setzt ein Panel auf das LINE_END-Feld des BorderLayouts.
	 */
	public void setPanelRight()
	{
		panel1.setPreferredSize(new Dimension(415, 676));
		rightBorder = BorderFactory.createMatteBorder(180, 0, 20, 40, Color.lightGray);
		panel1.setBorder(rightBorder);
		panel1.setBackground(Color.lightGray);
		add(panel1, border.LINE_END);
	}
	
	/**
	 * Die Methode setzt das Panel für den Kalendar auf das rechte Panel, setzt auf das eben hinzugefügte Panel den Zurück-Button
	 * und konfiguriert grundlegend die erste Textarea für den Kalendar.
	 */
	public void setBackButton()
	{
		panel2.setPreferredSize(new Dimension(360, 320));
		panel2.setBackground(Color.WHITE);
		panel1.add(panel2);
		back.setPreferredSize(new Dimension(15, 110));
		back.setBackground(Color.WHITE);
		back.addActionListener(this);
		panel2.add(back);
		Image image2 = new ImageIcon(this.getClass().getResource("/RückwärtsIcon.jpeg")).getImage();
		back.setIcon(new ImageIcon(image2));
		area.setFont(schriftart1);
		area.setPreferredSize(new Dimension(310, 110));
	}
	
	/**
	 * Die Methode setzt den Monatsnamen, das Jahr und die Abkürzungen für die Wochentage auf die erste Textarea, dabei wird auch auf die Länge des Monatsnamen geachtet 
	 * und passend eingerückt.
	 * @param jahr
	 * @param monat
	 */
	public void setCalendarArea(int jahr, int monat) 
	{
		area.append("\n");
		int b = 0;
		if(monate[monat].length() >= 8)
		{
			b = 18;
		}
		else if(monate[monat].length() >= 6)
		{
			b = 20;
		}
		else if(monate[monat].length() >= 4)
		{
			b = 22;
		}
		else
		{
			b = 23;
		}
		for(int i = 0;i < b;i++)
		{
			area.append(" ");
		}
		area.append(monate[monat] + " " + jahr);
		area.append("\n");
		area.append("\n");
		area.append(" ");
		for(int i=0; i < wochenTage.length; i++)
		{
			area.append(wochenTage[i]);
			area.append("     ");
		}
	}
	
	/**
	 * Die Methode setzt die erste, nicht editierbare Textarea und den Vorwärts-Button auf das zuvor gesetzte Panel
	 */
	public void setForwardButton()
	{
		area.setEditable(false);
		panel2.add(area);
		forward.setPreferredSize(new Dimension(15, 110));
		forward.setBackground(Color.WHITE);
		forward.addActionListener(this);
		panel2.add(forward);
		Image image1 = new ImageIcon(this.getClass().getResource("/VorwärtsIcon.jpeg")).getImage();
		forward.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * Die Methode setzt 6 Menüs à 7 Menüitems auf das zuvor gesetzte Panel für 6 möglichen Wochen mit 7 Tagen.
	 */
	public void setCalendarMenus()
	{
		int k = 0;
		for(int i = 0;i < menubars.length;i++)
		{
			menubars[i].setPreferredSize(new Dimension(350, 25));
			menubars[i].setBackground(Color.WHITE);
			panel2.add(menubars[i]);
			panels[i].setPreferredSize(new Dimension(17, 25));
			panels[i].setBackground(Color.WHITE);
			menubars[i].add(panels[i]);
			for(int j = 0;j < 7;j++)
			{
				menus[k].setPreferredSize(new Dimension(45, 25));
				menus[k].setFont(schriftart1);
				menus[k].setBackground(Color.WHITE);
				menus[k].addActionListener(this);		
				menubars[i].add(menus[k]);		
				k++;
			}
			panels2[i].setPreferredSize(new Dimension(17, 25));
			panels2[i].setBackground(Color.WHITE);
			menubars[i].add(panels2[i]);
		}
	}
	
	/**
	 * Die Methode holt sich zuerst über die Calendar-Klasse die benötigten Parameter für das Setzen der Tage auf die einzelnen Menüitems. 
	 * Danach werden die möglichen Tage vor dem 1. des heutigen Moants gesetzt und die Menüitems gesperrt. Dann folgen die Tage des heutigen Monats; 
	 * hierbei wird noch der heutige Tage unterschieden, bei dem die Schriftart auf fett und blau gesetzt wird,
	 * ansonsten ist die Schriftart normal und schwarz. Als Letztes folgen die möglichen Tage des nächsten Monats, die den möglichen Rest der Menüitems besetzen
	 * und auch diese Menüitems werden gesperrt.
	 * @param jahr
	 * @param monat
	 */
	public void setCalendarDays(int jahr, int monat)
	{
		calendar.set(GregorianCalendar.MONTH, monat - 1);
		int anzahlTageImMonatVorher = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		calendar.set(GregorianCalendar.MONTH, monat);
		calendar.set(GregorianCalendar.YEAR, jahr);
		calendar.set(GregorianCalendar.DAY_OF_MONTH, 1);
		int wochenTagDesErsten = calendar.get(GregorianCalendar.DAY_OF_WEEK);
		int anzahlTageImMonat = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		wochenTagDesErsten = (wochenTagDesErsten+5)%7;
		int spalte;
		int a = 1;
		for(spalte = 0;spalte < wochenTagDesErsten;spalte++)
		{
			menus[spalte].setText(" " + format1.format(anzahlTageImMonatVorher - wochenTagDesErsten + spalte + 1));
			menus[spalte].setEnabled(false);
		}
		for(int tag = 1;tag <= anzahlTageImMonat;tag++) 
		{
			if(monat == heutigerMonat && tag == heutigerTag && jahr == heutigesJahr)
			{
				menus[spalte].setFont(schriftart2);
				menus[spalte].setForeground(Color.BLUE);
				menus[spalte].setText(" " + format1.format(tag));
			}
			else
			{
				menus[spalte].setFont(schriftart1);
				menus[spalte].setForeground(Color.BLACK);
				menus[spalte].setText(" " + format1.format(tag));
			}
			++spalte;
		}
		while(spalte < 42)
		{
			menus[spalte].setText(" " + format1.format(a));
			menus[spalte].setEnabled(false);
			++a;
			++spalte;
		}
	}
	
	/**
	 * Die Methode setzt zum Abschluss des Kalendars eine leere, nicht editierbare Textarea unten drunter in dasselbe Panel.
	 */
	public void setCalendarButtom()
	{
		area2.setPreferredSize(new Dimension(350, 22));
		area2.setEditable(false);
		panel2.add(area2);
	}

	/**
	 * Die Methode setzt dreimal im Wechsel ein Panel als Anstandshalter und einen Button unter den Kalendar auf dasselbe Panel.
	 */
	public void setButtonsRight()
	{
		panel3.setPreferredSize(new Dimension(340, 50));
		panel3.setBackground(Color.lightGray);
		panel1.add(panel3);
		cancelRoom.setPreferredSize(new Dimension(360, 80));
		cancelRoom.setBackground(Color.WHITE);
		cancelRoom.setFont(schriftart1);
		cancelRoom.addActionListener(this);
		panel1.add(cancelRoom);
		Image image1 = new ImageIcon(this.getClass().getResource("/StornierenIcon.jpeg")).getImage();
		cancelRoom.setIcon(new ImageIcon(image1));
		panel4.setPreferredSize(new Dimension(340, 50));
		panel4.setBackground(Color.lightGray);
		panel1.add(panel4);
		reserveRoom.setPreferredSize(new Dimension(360, 80));
		reserveRoom.setBackground(Color.WHITE);
		reserveRoom.setFont(schriftart1);
		reserveRoom.addActionListener(this);
		panel1.add(reserveRoom);
		Image image3 = new ImageIcon(this.getClass().getResource("/ReservierenIcon.jpeg")).getImage();
		reserveRoom.setIcon(new ImageIcon(image3));
		panel5.setPreferredSize(new Dimension(340, 50));
		panel5.setBackground(Color.lightGray);
		panel1.add(panel5);
		bookRoom.setPreferredSize(new Dimension(360, 80));
		bookRoom.setBackground(Color.WHITE);
		bookRoom.setFont(schriftart1);
		bookRoom.addActionListener(this);
		panel1.add(bookRoom);
		Image image2 = new ImageIcon(this.getClass().getResource("/BuchenIcon.jpeg")).getImage();
		bookRoom.setIcon(new ImageIcon(image2));
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt den Logout Button auf dieses Panel.
	 */
	public void setLogoutButton()
	{
		panel6.setPreferredSize(new Dimension(1280, 50));
		panel6.setLayout(flow2);
		logoutBorder = BorderFactory.createMatteBorder(0, 20, 10, 40, Color.lightGray);
		panel6.setBorder(logoutBorder);
		panel6.setBackground(Color.lightGray);
		add(panel6, border.SOUTH);
		logout.setPreferredSize(new Dimension(130, 40));
		logout.setBackground(Color.WHITE);
		logout.setFont(schriftart1);
		logout.addActionListener(this);
		panel6.add(logout);
		Image image3 = new ImageIcon(this.getClass().getResource("/LogoutIcon.jpeg")).getImage();
		logout.setIcon(new ImageIcon(image3));
	}
	
	/**
	 * Die Methode setzt den Button zum Passwort ändern auf das zuvor gesetzte Panel.
	 */
	public void setPWButton()
	{
		pwÄndernButton.setPreferredSize(new Dimension(210, 40));
		pwÄndernButton.setBackground(Color.WHITE);
		pwÄndernButton.setFont(schriftart1);
		pwÄndernButton.addActionListener(this);
		panel6.add(pwÄndernButton);
		Image image3 = new ImageIcon(this.getClass().getResource("/BearbeitenIcon.jpeg")).getImage();
		pwÄndernButton.setIcon(new ImageIcon(image3));
	}
	
	/**
	 * Die Methode liest aus dem übergebenen Menüitem den Tag als String aus, löscht das erste Leerzeichen vor der Zahl und parsed den String in eine Integer-Zahl.
	 * @param item
	 * @return day
	 */
	public int getTagJMenuItem(JMenuItem item)
	{
		String tag = item.getText();
		String tagNeu = tag.replace(Character.toString(tag.charAt(0)), "");
		int day = Integer.parseInt(tagNeu);
		return day;
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == this.logout) 
		{
			dispose();
			new LoginGUI();
		}
		if(e.getSource() == this.pwÄndernButton) 
		{
			new PasswortÄndernGUI(this, benutzername);
		}
		if(e.getSource() == this.cancelRoom) 
		{
			new BuchungBearbeitenStornierenGUI(this, false);
		}
		if(e.getSource() == this.reserveRoom) 
		{
			buchen = false;
			new BucheReserviereRaumGUI(this, buchen);
		}
		if(e.getSource() == this.bookRoom) 
		{
			buchen = true;
			new BucheReserviereRaumGUI(this, buchen);
		}
		if(e.getSource() == this.kunde1) 
		{
			new KundeHinzufügenGUI(this);
		}
		if(e.getSource() == this.kunde2) 
		{
			new KundeÄndernEntfernenStatistikGUI(this, 0);
		}
		if(e.getSource() == this.kunde3)
		{
			new KundeÄndernEntfernenStatistikGUI(this, 1);
		}
		if(e.getSource() == this.kunde4)
		{
			new KundeÄndernEntfernenStatistikGUI(this, 2);
		}
		if(e.getSource() == this.mitarbeiter1) 
		{
			new MitarbeiterHinzufügenGUI(this, rolle);
		}
		if(e.getSource() == this.mitarbeiter2) 
		{
			new MitarbeiterÄndernEntfernenGUI(this, rolle, false);
		}
		if(e.getSource() == this.mitarbeiter3)
		{
			new MitarbeiterÄndernEntfernenGUI(this, rolle, true);
		}
		if(e.getSource() == this.raum1) 
		{
			new RaumHinzufügenGUI(this);
		}
		if(e.getSource() == this.raum2) 
		{
			new RaumÄndernAktivierenDeaktivierenGUI(this, true);
		}
		if(e.getSource() == this.raum3)
		{
			new RaumÄndernAktivierenDeaktivierenGUI(this, false);
		}
		if(e.getSource() == this.extra1)
		{
			new ExtraHinzufügenEntfernenGUI(this, true);
		}
		if(e.getSource() == this.extra2)
		{
			new ExtraHinzufügenEntfernenGUI(this, false);
		}
		if(e.getSource() == this.preis1)
		{
			new PreisKategorienÄndernGUI(this);
		}
		if(e.getSource() == this.preis2)
		{
			new PreisExtrasÄndernGUI(this);
		}
		if(e.getSource() == this.buchung1) 
		{
			new BuchungBearbeitenStornierenGUI(this, true);
		}
		if(e.getSource() == this.buchung2)
		{
			new ErstelleRechnungGUI(this);
		}
		if(e.getSource() == this.reservierung1)
		{
			new ReservierungStornierenGUI(this);
		}
		/**
		 * Das System setzt den Inhalt der ersten Textarea auf null und entsperrt dann alle Menüitems vom Kalendar und setzt den Inhalt auf null.
		 * Wenn der heutige Monat zwischen Januar und November liegt (0 >= month <= 10), wird der Monat um einen erhöht; falls der heutige Monat Dezember heißt (month = 11),
		 * wird das Jahr um einen erhöht und der Monat auf Januar (0) gesetzt. In beiden Fällen werden die beiden Methoden setCalendarArea(year, month) und setCalendarDays(year, month) aufgerufen. 
		 */
		if(e.getSource() == this.forward)
		{
			area.setText("");
			for(int i = 0;i < menus.length;i++)
			{
				menus[i].setEnabled(true);
				menus[i].setText("");
			}
			if(month >= 0 && month <= 10)
			{
				month++;
				setCalendarArea(year, month);
				setCalendarDays(year, month);
			}
			else
			{
				year = year + 1;
				month = 0;
				setCalendarArea(year, month);
				setCalendarDays(year, month);
			}
		}
		/**
		 * Das System setzt den Inhalt der ersten Textarea auf null und entsperrt dann alle Menüitems vom Kalendar und setzt den Inhalt auf null.
		 * Wenn der heutige Monat zwischen Februar und Dezember liegt (1 >= month <= 11), wird der Monat um einen verringert; falls der heutige Monat Januar heißt (month = 0),
		 * wird das Jahr um einen verringert und der Monat auf Dezember (11) gesetzt. In beiden Fällen werden die beiden Methoden setCalendarArea(year, month) und setCalendarDays(year, month) aufgerufen. 
		 */
		if(e.getSource() == this.back)
		{
			area.setText("");
			for(int i = 0;i < menus.length;i++)
			{
				menus[i].setEnabled(true);
				menus[i].setText("");
			}
			if(month >= 1 && month <= 11)
			{
				month--;
				setCalendarArea(year, month);
				setCalendarDays(year, month);
			}
			else
			{
				year = year - 1;
				month = 11;
				setCalendarArea(year, month);
				setCalendarDays(year, month);
			}
		}
		if(e.getSource() == this.tag1)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag1), month, year);
		}
		if(e.getSource() == this.tag2)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag2), month, year);
		}
		if(e.getSource() == this.tag3)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag3), month, year);
		}
		if(e.getSource() == this.tag4)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag4), month, year);
		}
		if(e.getSource() == this.tag5)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag5), month, year);
		}
		if(e.getSource() == this.tag6)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag6), month, year);
		}
		if(e.getSource() == this.tag7)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag7), month, year);
		}
		if(e.getSource() == this.tag8)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag8), month, year);
		}
		if(e.getSource() == this.tag9)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag9), month, year);
		}
		if(e.getSource() == this.tag10)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag10), month, year);
		}
		if(e.getSource() == this.tag11)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag11), month, year);
		}
		if(e.getSource() == this.tag12)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag12), month, year);
		}
		if(e.getSource() == this.tag13)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag13), month, year);
		}
		if(e.getSource() == this.tag14)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag14), month, year);
		}
		if(e.getSource() == this.tag15)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag15), month, year);
		}
		if(e.getSource() == this.tag16)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag16), month, year);
		}
		if(e.getSource() == this.tag17)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag17), month, year);
		}
		if(e.getSource() == this.tag18)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag18), month, year);
		}
		if(e.getSource() == this.tag19)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag19), month, year);
		}
		if(e.getSource() == this.tag20)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag20), month, year);
		}
		if(e.getSource() == this.tag21)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag21), month, year);
		}
		if(e.getSource() == this.tag22)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag22), month, year);
		}
		if(e.getSource() == this.tag23)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag23), month, year);
		}
		if(e.getSource() == this.tag24)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag24), month, year);
		}
		if(e.getSource() == this.tag25)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag25), month, year);
		}
		if(e.getSource() == this.tag26)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag26), month, year);
		}
		if(e.getSource() == this.tag27)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag27), month, year);
		}
		if(e.getSource() == this.tag28)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag28), month, year);
		}
		if(e.getSource() == this.tag29)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag29), month, year);
		}
		if(e.getSource() == this.tag30)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag30), month, year);
		}
		if(e.getSource() == this.tag31)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag31), month, year);
		}
		if(e.getSource() == this.tag32)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag32), month, year);
		}
		if(e.getSource() == this.tag33)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag33), month, year);
		}
		if(e.getSource() == this.tag34)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag34), month, year);
		}
		if(e.getSource() == this.tag35)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag35), month, year);
		}
		if(e.getSource() == this.tag36)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag36), month, year);
		}
		if(e.getSource() == this.tag37)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag37), month, year);
		}
		if(e.getSource() == this.tag38)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag38), month, year);
		}
		if(e.getSource() == this.tag39)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag39), month, year);
		}
		if(e.getSource() == this.tag40)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag40), month, year);
		}
		if(e.getSource() == this.tag41)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag41), month, year);
		}
		if(e.getSource() == this.tag42)
		{
			new ReservierungenBuchungenÜbersichtGUI(this, tag = getTagJMenuItem(tag42), month, year);
		}
	}
}