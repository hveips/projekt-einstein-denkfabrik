package view.passwortview;

/**
 * @author Hendrik Veips
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.LimitedDocument;
import controller.passwortcontroller.PasswortVergessenController;
import view.StartansichtGUI;
import view.loginview.LoginGUI;
import controller.LoginController;

public class PasswortVergessenGUI extends JFrame implements ActionListener
{
	private JLabel security = new JLabel("Sicherheitsabfrage");
	private JLabel newPassword = new JLabel("Neues Passwort"); 
	private JLabel repeatPassword = new JLabel("Passwort wiederholen");
	private JTextField securityInput = new JTextField("");
	private JPasswordField newPasswordInput = new JPasswordField("");
	private JPasswordField repeatPasswordInput = new JPasswordField("");
	private JButton abbrechenbtn = new JButton("Abbrechen");
	private JButton save = new JButton("Speichern");
	private JButton info = new JButton();
	private JPanel eingabePanel = new JPanel();
	private JPanel securityPanel = new JPanel();
	private JPanel passwordPanel = new JPanel();
	private JPanel repeatPasswordPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private Border eingabeBorder;
	private Border buttonBorder;
	private LoginGUI gui;
	
	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);
	
	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.LEFT);
	private FlowLayout flow3 = new FlowLayout(FlowLayout.RIGHT);
	
	private PasswortVergessenController passwortVergessenController = new PasswortVergessenController();
	private String secur; 
	private boolean securityQuery;
	private boolean samePassword;
	
	private LoginController loginController = new LoginController();
	private String benutzername;
	private boolean save2;
	private char rolle;

	/**
	 * Der Konstruktor erzeugt die PasswortVergessenGUI �ber mehrere Methoden.
	 * @param gui
	 * @param secur
	 * @param benutzername
	 * @param rolle
	 */
	public PasswortVergessenGUI(LoginGUI gui, String secur, String benutzername, char rolle)
	{
		setTitle("Passwort vergessen...");
		setPreferredSize(new Dimension(700, 220));
		setLocation(610, 420);
		setFont(schriftart1);
		setLayout(border);
		setResizable(false);
		Image image1 = new ImageIcon(this.getClass().getResource("/FragezeichenIcon.jpeg")).getImage();
		setIconImage(image1);
		this.gui = gui;
		this.secur = secur;
		this.benutzername = benutzername;
		this.rolle = rolle;
		setAbfrage();
		setPasswordEingabe();
		setPasswordWiederholungEingabe();
		setButton();
		pack();
		setVisible(true);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das CENTER-Feld des BorderLayouts und setzt wiederum darauf ein Panel.
	 * Auf dieses Panel wird das Label und das Textfeld f�r die Eingabe der Sicherheitsfrage gesetzt.
	 */
	public void setAbfrage()
	{
		eingabePanel.setPreferredSize(new Dimension(650, 135));
		eingabePanel.setLayout(flow);
		eingabePanel.setBackground(Color.lightGray);
		eingabeBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		eingabePanel.setBorder(eingabeBorder);
		add(eingabePanel, border.CENTER);
		securityPanel.setPreferredSize(new Dimension(630, 40));
		securityPanel.setBackground(Color.lightGray);
		securityPanel.setLayout(flow2);
		eingabePanel.add(securityPanel);
		security.setFont(schriftart1);
		security.setPreferredSize(new Dimension(200, 30));
		securityPanel.add(security);
		securityInput.setPreferredSize(new Dimension(410, 30));
		securityInput.setFont(schriftart1);
		securityInput.setDocument(new LimitedDocument(5));
		securityPanel.add(securityInput);
	}
	
	/**
	 * Die Methode setzt ein Panel auf das Panel im CENTER und setzt darauf das Label und das Textfeld f�r die Eingabe des Passwortes.
	 */
	public void setPasswordEingabe()
	{
		passwordPanel.setPreferredSize(new Dimension(630, 40));
		passwordPanel.setBackground(Color.lightGray);
		passwordPanel.setLayout(flow2);
		eingabePanel.add(passwordPanel);
		newPassword.setFont(schriftart1);
		newPassword.setPreferredSize(new Dimension(200, 30));
		passwordPanel.add(newPassword);
		newPasswordInput.setPreferredSize(new Dimension(375, 30));
		newPasswordInput.setFont(schriftart1);
		newPasswordInput.setDocument(new LimitedDocument(10));
		passwordPanel.add(newPasswordInput);
		info.setPreferredSize(new Dimension(30, 30));
		info.setBackground(Color.WHITE);
		info.addActionListener(this);
		passwordPanel.add(info);
		Image image1 = new ImageIcon(this.getClass().getResource("/InfoIcon.jpeg")).getImage();
		info.setIcon(new ImageIcon(image1));
	}
	
	/**
	 * Die Methode setzt ein Panel auf das Panel im CENTER und setzt darauf das Label und das Textfeld f�r die Eingabe des wiederholten Passwortes.
	 */
	public void setPasswordWiederholungEingabe()
	{
		repeatPasswordPanel.setPreferredSize(new Dimension(630, 40));
		repeatPasswordPanel.setBackground(Color.lightGray);
		repeatPasswordPanel.setLayout(flow2);
		eingabePanel.add(repeatPasswordPanel);
		repeatPassword.setFont(schriftart1);
		repeatPassword.setPreferredSize(new Dimension(200, 30));
		repeatPasswordPanel.add(repeatPassword);
		repeatPasswordInput.setPreferredSize(new Dimension(410, 30));
		repeatPasswordInput.setFont(schriftart1);
		repeatPasswordInput.setDocument(new LimitedDocument(10));
		repeatPasswordPanel.add(repeatPasswordInput);	
	}
	
	/**
	 * Die Methode setzt ein Panel auf das SOUTH-Feld des BorderLayouts und setzt die Buttons auf dieses Panel.
	 */
	public void setButton()
	{
		buttonPanel.setPreferredSize(new Dimension(650, 50));
		buttonPanel.setLayout(flow3);
		buttonPanel.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 25, 35, Color.lightGray);
		buttonPanel.setBorder(buttonBorder);
		add(buttonPanel, border.SOUTH);
		abbrechenbtn.setPreferredSize(new Dimension(160, 40));
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.addActionListener(this);
		buttonPanel.add(abbrechenbtn);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image2));
		save.setPreferredSize(new Dimension(160, 40));
		save.setBackground(Color.WHITE);
		save.setFont(schriftart1);
		save.addActionListener(this);
		buttonPanel.add(save);
		Image image3 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		save.setIcon(new ImageIcon(image3));
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == this.abbrechenbtn)
		{
			new LoginGUI();
			dispose();
		}
		/**
		 * Das System pr�ft zuerst die Gleichheit der Sicherheitsabfrage mit Hilfe des PasswortVergessenControllers; falls nicht, wird dies dem Nutzer gemeldet.
		 * Wenn doch, pr�ft das System die Gleichheit der beiden Passw�rter; falls nicht, wird das dem Nutzer gemeldet.
		 * Wenn doch, versucht das System das Passwort zu speichern; schl�gt dies fehl, wird dies dem Nutzer gemeldet, ansonsten wird der Nutzer gefragt, ob er fortfahren will oder nicht.
		 * Auf der einen Seite wird die Startansicht erzeugt, wobei �ber den �bergebenen Parameter rolle entschieden wird, welche Funktionen dem Nutzer freigeschaltet werden,
		 * und die PasswortVergessenGUI wird geschlossen, ansonsten wird das Programm beendet.
		 */
		if(e.getSource() == this.save) 
		{
			String eingabeAbfrage = securityInput.getText();
			char[] eingabeAbfrage2 = eingabeAbfrage.toCharArray();
			char[] secur2 = secur.toCharArray();
			if(eingabeAbfrage.length() == 5)
			{
				securityQuery = passwortVergessenController.pruefeGleichheit(secur2, eingabeAbfrage2);
				if(securityQuery == true)
				{
					char[] eingabePasswort = newPasswordInput.getPassword();
					char[] eingabeWiederholePasswort = repeatPasswordInput.getPassword();
					if(eingabePasswort.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Kein Passwort gesetzt!");
					}
					else
					{
						samePassword = passwortVergessenController.pruefeGleichheit(eingabePasswort, eingabeWiederholePasswort);
						if(samePassword == true)
						{
							String passwort = String.valueOf(eingabePasswort);
							save2 = loginController.speicherePasswort(passwort, benutzername);
							if(save2 == true)
							{
								int option = JOptionPane.showConfirmDialog(this, "Passwort wurde erfolgreich gespeichert. Wollen Sie das System starten?", "Systemstart?", JOptionPane.YES_NO_OPTION);
								if (option == JOptionPane.YES_OPTION)
								{
									if(rolle == 'A') 
									{
										new StartansichtGUI('A', benutzername);
									} 
									else if(rolle == 'G') 
									{
										new StartansichtGUI('G', benutzername);
									} 
									else 
									{
										new StartansichtGUI('M', benutzername);
									}
									dispose();
								}
								else
								{
									dispose();
								}
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Verbindung mit der Datenbank fehlgeschlagen. �berpr�fen Sie ihre Verbindung und versuchen Sie es erneut.");
							}
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Passw�rter stimmen nicht �berein!");
							newPasswordInput.setText("");
							repeatPasswordInput.setText("");	
						}
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Sicherheitsabfrage ist nicht korrekt!");
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Sicherheitsabfrage ist nicht korrekt!");
			}
		}
		if(e.getSource() == this.info)
		{
			JOptionPane.showMessageDialog(null, "Die Passwort-L�nge betr�gt maximal 10 Zeichen!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}