package view.passwortview;

/**
 * @author Alexander Stavski
 * Diese Klasse erzeugt die View für das Ändern des Passwortes
 * auf der Startseite wenn man schon eingelogt ist
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;

import controller.documentcontroller.LimitedDocument;
import controller.passwortcontroller.PasswortÄndernController;
import view.StartansichtGUI;

public class PasswortÄndernGUI extends JFrame implements ActionListener
{
	private JLabel newPassword = new JLabel("Neues Passwort");
	private JLabel repeatPassword = new JLabel("Passwort wiederholen");
	private JPasswordField newPasswordInput = new JPasswordField("");
	private JPasswordField repeatPasswordInput = new JPasswordField("");
	private JButton abbrechenbtn = new JButton("Abbrechen");
	private JButton save = new JButton("Speichern");
	private JButton info = new JButton();
	private JPanel eingabePanel = new JPanel();
	private JPanel passwordPanel = new JPanel();
	private JPanel repeatPasswordPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JDialog dialog = new JDialog();
	
	private Border eingabeBorder;
	private Border buttonBorder;
	
	private StartansichtGUI gui;

	private Font schriftart1 = new Font("Arial", Font.PLAIN, 18);

	private BorderLayout border = new BorderLayout();
	private FlowLayout flow = new FlowLayout();
	private FlowLayout flow2 = new FlowLayout(FlowLayout.RIGHT);
	
	private PasswortÄndernController pwController;
	private String benutzername;

	public PasswortÄndernGUI(StartansichtGUI gui, String benutzername)
	{
		this.gui = gui;
		this.benutzername = benutzername;
		this.pwController = new PasswortÄndernController(this);
		erzeugeDialog();
		Image image1 = new ImageIcon(this.getClass().getResource("/FragezeichenIcon.jpeg")).getImage();
		dialog.setIconImage(image1);
		setPasswordEingabe();
		setPasswordWiederholungEingabe();
		setButton();
		dialog.pack();
		dialog.setVisible(true);
	}
	
	public void erzeugeDialog()
	{
		dialog = new JDialog(gui, Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setContentPane(this.getContentPane());
		dialog.setTitle("Passwort ändern");
		dialog.setPreferredSize(new Dimension(700, 180));
		dialog.setLocation(610, 420);
		dialog.setLayout(border);
		dialog.setResizable(false);
	}

	public void setPasswordEingabe()
	{
		eingabePanel.setPreferredSize(new Dimension(650, 135));
		eingabePanel.setLayout(flow);
		eingabePanel.setBackground(Color.lightGray);
		eingabeBorder = BorderFactory.createMatteBorder(0, 25, 25, 25, Color.lightGray);
		eingabePanel.setBorder(eingabeBorder);
		add(eingabePanel, border.CENTER);
		passwordPanel.setPreferredSize(new Dimension(630, 40));
		passwordPanel.setBackground(Color.lightGray);
		eingabePanel.add(passwordPanel);
		newPassword.setFont(schriftart1);
		newPassword.setPreferredSize(new Dimension(200, 30));
		passwordPanel.add(newPassword);
		newPasswordInput.setPreferredSize(new Dimension(375, 30));
		newPasswordInput.setFont(schriftart1);
		newPasswordInput.setDocument(new LimitedDocument(10));
		passwordPanel.add(newPasswordInput);
		info.setPreferredSize(new Dimension(30, 30));
		info.setBackground(Color.WHITE);
		info.addActionListener(this);
		passwordPanel.add(info);
		Image image1 = new ImageIcon(this.getClass().getResource("/InfoIcon.jpeg")).getImage();
		info.setIcon(new ImageIcon(image1));
	}

	public void setPasswordWiederholungEingabe()
	{
		repeatPasswordPanel.setPreferredSize(new Dimension(630, 40));
		repeatPasswordPanel.setBackground(Color.lightGray);
		eingabePanel.add(repeatPasswordPanel);
		repeatPassword.setFont(schriftart1);
		repeatPassword.setPreferredSize(new Dimension(200, 30));
		repeatPasswordPanel.add(repeatPassword);
		repeatPasswordInput.setPreferredSize(new Dimension(410, 30));
		repeatPasswordInput.setFont(schriftart1);
		repeatPasswordInput.setDocument(new LimitedDocument(10));
		repeatPasswordPanel.add(repeatPasswordInput);
	}

	public void setButton()
	{
		buttonPanel.setPreferredSize(new Dimension(650, 50));
		buttonPanel.setLayout(flow2);
		buttonPanel.setBackground(Color.lightGray);
		buttonBorder = BorderFactory.createMatteBorder(0, 25, 25, 35, Color.lightGray);
		buttonPanel.setBorder(buttonBorder);
		add(buttonPanel, border.SOUTH);
		abbrechenbtn.setPreferredSize(new Dimension(160, 40));
		abbrechenbtn.setBackground(Color.WHITE);
		abbrechenbtn.setFont(schriftart1);
		abbrechenbtn.addActionListener(this);
		buttonPanel.add(abbrechenbtn);
		Image image2 = new ImageIcon(this.getClass().getResource("/AbbrechenIcon.jpeg")).getImage();
		abbrechenbtn.setIcon(new ImageIcon(image2));
		save.setPreferredSize(new Dimension(160, 40));
		save.setBackground(Color.WHITE);
		save.setFont(schriftart1);
		save.addActionListener(this);
		buttonPanel.add(save);
		Image image3 = new ImageIcon(this.getClass().getResource("/SpeichernIcon.jpeg")).getImage();
		save.setIcon(new ImageIcon(image3));
	}

	public JPasswordField getNewPassword() 
	{
		return this.newPasswordInput;
	}
	
	public JPasswordField getRepeatPassword()
	{
		return this.repeatPasswordInput;
	}
	
	public String getBenutzername() 
	{
		return this.benutzername;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == this.save)
		{
			if(pwController.passwortÄndern() == true)
			{
				dialog.dispose();
			}		
		}
		if(e.getSource() == this.info)
		{
			JOptionPane.showMessageDialog(null, "Die Passwort-Länge beträgt maximal 10 Zeichen!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
		if (e.getSource() == this.abbrechenbtn)
		{
			dialog.dispose();
		}
	}
}