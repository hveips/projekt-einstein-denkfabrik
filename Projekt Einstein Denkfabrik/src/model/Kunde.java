package model;  
/**
 * 
 * @author Marvin Plepis
 *
 */
public class Kunde extends Person
{
	private int kundenID;
	private String arbeitsplatz;
	/**
	 * Erzeugt Kunden Objekt
	 * @param kdid
	 * @param anrede
	 * @param vorname
	 * @param name
	 * @param plz
	 * @param ort
	 * @param stra�e
	 * @param hausNr
	 * @param email
	 * @param arbeitsplatz
	 */
	public Kunde(int kdid, String anrede, String vorname, String name, int plz, String ort, String stra�e, String hausNr, String email, String arbeitsplatz) 
	{
		super(anrede, vorname, name, plz, ort, stra�e, hausNr, email);
		this.kundenID = kdid;
		this.arbeitsplatz = arbeitsplatz;
	}

	public int getKundenID() 
	{
		return kundenID;
	}

	public void setKundenID(int kundenID) 
	{
		this.kundenID = kundenID;
	}

	public String getArbeitsplatz() 
	{
		return arbeitsplatz;
	}

	public void setArbeitsplatz(String arbeitsplatz) 
	{
		this.arbeitsplatz = arbeitsplatz;
	}	
}