package model;

/**
 * @author Alexander Stavski
 */

import java.security.SecureRandom;

public class Mitarbeiter extends Person
{
	private int mitarbeiterID;
	private String rolle;
	private String passwort;
	private int eingelogt;
	
	public Mitarbeiter(int maid, String anrede, String vorname, String name, int plz, String ort,String stra�e, String hausNr, String email, String rolle) 
	{
		super(anrede, vorname, name, plz, ort, stra�e, hausNr, email);
		this.mitarbeiterID = maid;
		this.rolle = rolle;
		this.eingelogt = 0;
		this.passwort = setZufallsPasswort();
	}
	
	public void setMitarbeiterID(int maid) 
	{
		this.mitarbeiterID = maid;
	}
	
	public int getMitarbeiterID() 
	{
		return mitarbeiterID;
	}

	public void setRolle(String rolle) 
	{
		this.rolle = rolle;
	}

	public String getRolle() 
	{
		return rolle;
	}

	/**
	 * Methode generiert ein zuf�lliges Passwort, aus den gew�hlten Zeichen und der festgelegten L�nge
	 * @return zuf�llig-generiertes Passwort
	 */
	public String setZufallsPasswort() 
	{
		int laenge = 5; // L�nge des PW festgelegt
		String zeichen = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@*^";
		StringBuilder passwort = new StringBuilder(laenge);
		SecureRandom random = new SecureRandom();
		for (int i = 1; i <= laenge; i++) 
		{
			passwort.append(zeichen.charAt(random.nextInt(zeichen.length())));
		}
		return passwort.toString();
	}

	public String getPasswort() 
	{
		return passwort;
	}
	
	public int getEingelogt() 
	{
		return eingelogt;
	}
}