package model;

/**
 * @author Niklas Nebeling, Hendrik Veips
 */

import java.util.ArrayList;

public class Raum 
{	
	private int sitzpl�tze;
	private int raumNr;
	private String k�rzel;      //Kategorie K�rzel  A, B, C...
	private String inBenutzung;
	private ArrayList<Ausstattung> ausstattung = new ArrayList<Ausstattung>();
		
	public Raum(int raumNr, int sitzpl�tze,  String k�rzel, ArrayList<Ausstattung> ausstattungen) 
	{
		this.raumNr = raumNr;
		this.sitzpl�tze = sitzpl�tze;
		this.k�rzel = k�rzel;
		this.ausstattung = ausstattungen;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public Raum(int raumNr, int sitzpl�tze, String k�rzel, String inBenutzung)
	{
		this.raumNr = raumNr;
		this.sitzpl�tze = sitzpl�tze;
		this.k�rzel = k�rzel;
		this.inBenutzung = inBenutzung;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public String getInBenutzung()
	{
		return inBenutzung;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public void setInBenutzung(String inBenutzung)
	{
		this.inBenutzung = inBenutzung;
	}
	
	public ArrayList<Ausstattung> getAustattung() 
	{
		return ausstattung;
	}
	
	public void ausstattungHinzufuegen(Ausstattung ausstattung) 
	{
		this.ausstattung.add(ausstattung);
	}
	
	public String getK�rzel() 
	{
		return k�rzel;
	}

	public void setK�rzel(String k�rzel) 
	{
		this.k�rzel = k�rzel;
	}

	public ArrayList<Ausstattung> getAusstattung() 
	{
		return ausstattung;
	}
	

	public void setAusstattung(ArrayList<Ausstattung> ausstattungen) 
	{
		this.ausstattung = ausstattungen;
	}

	public int getRaumNr() 
	{
		return raumNr;
	}
	
	public void setRaumNr(int raumNr) 
	{
		this.raumNr = raumNr;
	}
	
	public int getSitzpl�tze() 
	{
		return sitzpl�tze;
	}
	
	public void setSitzpl�tze(int sitzplaetze) 
	{
		this.sitzpl�tze = sitzplaetze;
	}
	
	public String toString() 
	{
		return "rnr:" + raumNr + "k�rzel: "+ k�rzel;
	}
}