package model;

/**
 * @author Niklas Nebeling, Hendrik Veips
 */

public class Extra 
{	
	private String bezeichnung;
	private String preis_pro_person;
	private String art;
	
	public Extra(String bezeichnung) 
	{
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public Extra(String bezeichnung, String preis_pro_person)
	{
		this.bezeichnung = bezeichnung;
		this.preis_pro_person = preis_pro_person;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public Extra(String bezeichnung, String preis_pro_person, String art)
	{
		this.bezeichnung = bezeichnung;
		this.preis_pro_person = preis_pro_person;
		this.art = art;
	}
	
	public String getBezeichnung() 
	{
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) 
	{
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public String getPreisProPerson()
	{
		return preis_pro_person;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public void setPreisProPerson(String preis_pro_person)
	{
		this.preis_pro_person = preis_pro_person;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public String getArt()
	{
		return art;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public void setArt(String art)
	{
		this.art = art;
	}
}