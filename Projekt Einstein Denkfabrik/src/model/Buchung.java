package model;

/**
 * @author Niklas Nebeling, Hendrik Veips
 */

import java.util.ArrayList;

public class Buchung 
{	
	private int buchungsNr;
	private String anfangsdatum;
	private String enddatum;
	private int anzTage;
	private int stunden;
	private int raumNr;
	private int kuID;
	private int personenanzahl;
	private boolean storniert = false;
	private double gesamtbetrag = 0;
	private ArrayList<Extra> extras = new ArrayList<Extra>();
	
	private String name, vorname, email, anrede;
	
	//F�r leeres Objekt im BuchenReservierenController
	public Buchung() {
	}
	
	public Buchung(int bnr, String anfangsdatum, int anzTage,int stunden, int raumNr, int kuID, ArrayList<Extra> extras, String enddatum) 
	{
		this.buchungsNr = bnr;
		this.anfangsdatum = anfangsdatum;
		this.anzTage = anzTage;
		this.stunden = stunden;
		this.raumNr = raumNr;
		this.kuID = kuID;
		this.extras = extras;
		this.enddatum = enddatum;
	}
	
	//Zum Tabellen Laden mit bereits vorhandenem Gesamtbetrag
	public Buchung(int bnr, String anfangsdatum, int anzTage, int stunden, int raumNr, int kuID, String name, String vorname, String email, String anrede, double gesamtbetrag, String enddatum, int personenanzahl) 
	{
		this.buchungsNr = bnr;
		this.anfangsdatum = anfangsdatum;
		this.anzTage = anzTage;
		this.stunden = stunden;
		this.raumNr = raumNr;
		this.kuID = kuID;
		this.name = name;
		this.vorname = vorname;
		this.email = email;
		this.anrede = anrede;
		this.gesamtbetrag = gesamtbetrag;
		this.enddatum = enddatum;
		this.personenanzahl = personenanzahl;
	}
	
	/**
	 * @author Hendrik Veips
	 */
	public Buchung(int bnr, String anfangsdatum, String enddatum, int anzTage, int stunden, int raumNr, int kuID, ArrayList<Extra> extras, int personenanzahl, double gesamtbetrag)
	{
		this.buchungsNr = bnr;
		this.raumNr = raumNr;
		this.kuID = kuID;
		this.extras = extras;
		this.anfangsdatum = anfangsdatum;
		this.enddatum = enddatum;
		this.anzTage = anzTage;
		this.stunden = stunden;
		this.personenanzahl = personenanzahl;
		this.gesamtbetrag = gesamtbetrag;
	}
	
	
	
	public String getAnrede()
	{
		return anrede;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getVorname()
	{
		return vorname;
	}
	
	public String getEmail()
	{
		return email;
	}

	public void stornieren() 
	{
		this.storniert = true;
	}
	
	public int getBuchungsNr() 
	{
		return buchungsNr;
	}
	
	public void setBuchungsNr(int buchungsNr) {
		this.buchungsNr = buchungsNr;
	}

	public String getDatumString()
	{
		return anfangsdatum;
	}

	public int getRaumNr()
	{
		return raumNr;
	}

	public void setRaumNr(int raumNr) 
	{
		this.raumNr = raumNr;
	}
	
	public void setDauer(int anzTage) 
	{
		this.anzTage = anzTage;
	}

	public boolean istStorniert() 
	{
		return storniert;
	}

	public double getGesamtbetrag() 
	{
		return gesamtbetrag;
	}

	public void setGesamtbetrag(double gesamtbetrag) 
	{
		this.gesamtbetrag = gesamtbetrag;
	}

	public ArrayList<Extra> getExtras() 
	{
		return extras;
	}

	public void setExtras(ArrayList<Extra> extras) {
		this.extras = extras;
	}

	public void extraHinzuf�gen(Extra extra) {
		extras.add(extra);
	}
	
	public void clearExtras() {
		extras.clear();
	}

	public int getStunden() 
	{
		return stunden;
	}

	public void setStunden(int stunden) 
	{
		this.stunden = stunden;
	}

	public int getKuID() 
	{
		return kuID;
	}

	public void setKuID(int kuID) 
	{
		this.kuID = kuID;
	}

	public int getAnzTage() 
	{
		return anzTage;
	}

	public void setAnzTage(int anzTage) 
	{
		this.anzTage = anzTage;
	}
	
	public String getAnfangsdatum()
	{
		return anfangsdatum;
	}
	
	public void setAnfangsdatum(String anfangsdatum)
	{
		this.anfangsdatum = anfangsdatum;
	}
	
	public String getEnddatum() 
	{
		return enddatum;
	}
	
	public void setEnddatum(String enddatum)
	{
		this.enddatum = enddatum;
	}

	public int getPersonenanzahl() {
		return personenanzahl;
	}

	public void setPersonenanzahl(int personenanzahl) {
		this.personenanzahl = personenanzahl;
	}
}