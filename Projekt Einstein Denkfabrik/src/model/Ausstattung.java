package model;

/**
 * @author Niklas Nebeling
 */

public class Ausstattung 
{	
	private String bezeichnung;
	private int menge;

	public Ausstattung(String bezeichnung, int anzahl) 
	{
		this.bezeichnung = bezeichnung;
		this.menge = anzahl;
	}

	public String getBezeichnung() 
	{
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) 
	{
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() 
	{
		return menge;
	}

	public void setMenge(int anzahl) 
	{
		this.menge = anzahl;
	}
}