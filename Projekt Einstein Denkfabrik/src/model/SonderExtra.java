package model;

/**
 * @author Niklas Nebeling
 */

public class SonderExtra extends Extra 
{
	public SonderExtra(String bezeichnung) 
	{
		super(bezeichnung);
	}
}