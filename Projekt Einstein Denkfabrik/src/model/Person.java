package model;

/**
 * @author Alexander Stavski
 * Superklasse f�r Kunde/Mitarbeiter
 */

public class Person
{
	private String anrede;
	private String vorname;
	private String name;
	private int plz;
	private String ort;
	private String stra�e;
	private String hausNr;
	private String email;
	
	public Person(String anrede, String vorname, String name, int plz, String ort, String stra�e, String hausNr, String email) 
	{
		this.anrede = anrede;
		this.vorname = vorname;
		this.name = name;
		this.plz = plz;
		this.ort = ort;
		this.stra�e = stra�e;
		this.hausNr = hausNr;
		this.email = email;
	}
	
	public void setAnrede(String anrede) 
	{
		this.anrede = anrede;
	}
	
	public String getAnrede() 
	{
		return anrede;
	}
	
	public void setVorname(String vorname) 
	{
		this.vorname = vorname;
	}

	public String getVorname() 
	{
		return vorname;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getName() 
	{
		return name;
	}

	public void setPlz(int plz) 
	{
		this.plz = plz;
	}

	public int getPlz() 
	{
		return plz;
	}

	public void setOrt(String ort) 
	{
		this.ort = ort;
	}

	public String getOrt() 
	{
		return ort;
	}

	public void setStra�e(String stra�e) 
	{
		this.stra�e = stra�e;
	}
	
	public String getStra�e() 
	{
		return stra�e;
	}
	
	public void setHausNr(String hausNr) 
	{
		this.hausNr = hausNr;
	}

	public String getHausNr() 
	{
		return hausNr;
	}

	public void setEmail(String email) 
	{
		this.email = email;
	}

	public String getEmail() 
	{
		return email;
	}
}