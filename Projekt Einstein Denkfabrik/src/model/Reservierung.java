package model;

/**
 * @author Niklas Nebeling, Alexander Stavski
 */

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Reservierung 
{
	private int resNr;
	private String anfangsdatum;
	private String enddatum;
	private int anzTage;													
	private int raumNr;
	private int kdID;
	private int stunden;
	private int personenanzahl;
	private boolean storniert = false;
	private double gesamtbetrag;
	private ArrayList<Extra> extras = new ArrayList<Extra>();
	
	private String name, vorname, email;
	
	public Reservierung(int resNr, String anfangsdatum, double gesamtbetrag, int kdID, int anzTage, int stunden, int raumNr, String enddatum, ArrayList<Extra> extras) 
	{
		this.resNr = resNr;
		this.anfangsdatum = anfangsdatum;
		this.gesamtbetrag = gesamtbetrag;
		this.kdID = kdID;
		this.anzTage = anzTage;
		this.stunden = stunden;
		this.raumNr = raumNr;
		this.enddatum = enddatum;
		this.extras = extras;
	}
	
	/**
	 * @author Alexander Stavski
	 */
	public Reservierung(int resNr, String anfangsdatum, int raumNr, int kdID, String name, String vorname, String email, int anzTage, String enddatum, int personenanzahl, double gesamtbetrag) 
	{
		this.resNr = resNr;
		this.anfangsdatum = anfangsdatum;
		this.raumNr = raumNr;
		this.kdID = kdID;
		this.name = name;
		this.vorname = vorname;
		this.email = email;
		this.anzTage = anzTage;
		this.enddatum = enddatum;
		this.personenanzahl = personenanzahl;
		this.gesamtbetrag = gesamtbetrag;
	}
	
	public Reservierung(int resnr, String anfangsdatum, String enddatum, int anzTage, int stunden, int raumNr, int kuID, ArrayList<Extra> extras, int personenanzahl, double gesamtbetrag)
	{
		this.resNr = resnr;
		this.raumNr = raumNr;
		this.kdID = kuID;
		this.extras = extras;
		this.anfangsdatum = anfangsdatum;
		this.enddatum = enddatum;
		this.anzTage = anzTage;
		this.stunden = stunden;
		this.personenanzahl = personenanzahl;
		this.gesamtbetrag = gesamtbetrag;
	}
	
	public void stornieren() 
	{
		this.storniert = true;
	}

	public String getAnfangsdatum() 
	{
		return anfangsdatum;
	}

	public void setAnfangsdatum(String anfangsdatum) 
	{
		this.anfangsdatum = anfangsdatum;
	}

	public int getAnzTage() 
	{
		return anzTage;
	}

	public void setAnzTage(int anzTage) 
	{
		this.anzTage = anzTage;
	}

	public int getRaumNr() 
	{
		return raumNr;
	}

	public void setRaumNr(int raumNr) 
	{
		this.raumNr = raumNr;
	}

	public boolean istStorniert() 
	{
		return storniert;
	}

	public double getGesamtbetrag() 
	{
		return gesamtbetrag;
	}

	public void setGesamtbetrag(double gesamtbetrag) 
	{
		this.gesamtbetrag = gesamtbetrag;
	}

	public ArrayList<Extra> getExtras() 
	{
		return extras;
	}

	public void setExtras(ArrayList<Extra> extras) 
	{
		this.extras = extras;
	}

	public int getReservierungsNr() 
	{
		return resNr;
	}

	public int getResNr() 
	{
		return resNr;
	}

	public void setResNr(int resNr) 
	{
		this.resNr = resNr;
	}

	public int getStunden() 
	{
		return stunden;
	}

	public void setStunden(int stunden) 
	{
		this.stunden = stunden;
	}

	public int getKdID() 
	{
		return kdID;
	}

	public void setKdID(int kdID) 
	{
		this.kdID = kdID;
	}

	public boolean isStorniert() 
	{
		return storniert;
	}

	public String getName() 
	{
		return name;
	}

	public String getVorname() 
	{
		return vorname;
	}

	public String getEmail() 
	{
		return email;
	}

	public String getEnddatum() 
	{
		return enddatum;
	}
	
	public void setEnddatum(String enddatum)
	{
		this.enddatum = enddatum;
	}

	public int getPersonenanzahl() {
		return personenanzahl;
	}

	public void setPersonenanzahl(int personenanzahl) {
		this.personenanzahl = personenanzahl;
	}
}