package model;

/**
 * @author Niklas Nebeling
 */

public class RaumKategorie 
{
	private String kuerzel;				//Kategorie A, B ,C ...
	private int preis_h;
	private int tagespreis;
	

	public RaumKategorie(String kuerzel, int preis_h, int tagespreis) 
	{
		this.kuerzel = kuerzel;
		this.preis_h = preis_h;
		this.tagespreis = tagespreis;
	}

	public String getKuerzel() 
	{
		return kuerzel;
	}

	public void setKuerzel(String kuerzel) 
	{
		this.kuerzel = kuerzel;
	}

	public int getPreis_h() 
	{
		return preis_h;
	}

	public void setPreis_h(int preis_h) 
	{
		this.preis_h = preis_h;
	}

	public int getTagespreis() 
	{
		return tagespreis;
	}

	public void setTagespreis(int tagespreis) 
	{
		this.tagespreis = tagespreis;
	}	
}