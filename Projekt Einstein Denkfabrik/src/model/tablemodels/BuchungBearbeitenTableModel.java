package model.tablemodels;

/**
 * @author Niklas Nebeling
 */

import javax.swing.JOptionPane;

public class BuchungBearbeitenTableModel extends BuchungTableModel{
	
	/**
	 * Im Konstruktor wird die Buchungsliste aus der Datenbank geladen.
	 */
	public BuchungBearbeitenTableModel() 
	{
		try 
		{
			buchungen = newOp.ladeBuchungenZumBearbeiten();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
	
	/**
	 * Die Daten in der Datenbank wurden ver�ndert und somit ist ein erneutes Laden der Buchungen n�tig.
	 */
	public void databaseUpdated() 
	{
		try 
		{
			buchungen = newOp.ladeBuchungenZumBearbeiten();
			this.fireTableDataChanged();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
}
