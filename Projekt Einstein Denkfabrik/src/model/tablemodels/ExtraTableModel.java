package model.tablemodels;

/**
 * @author Hendrik Veips
 */

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.extrascontroller.ExtraController;
import model.Extra;

public class ExtraTableModel extends AbstractTableModel
{
	private ArrayList<Extra> extras;
	private String[] columnNames = {"Bezeichnung", "Preis/Person (in �)", "Art"};
	private ExtraController op = new ExtraController();
	
	/**
	 * Der Konstruktor erstellt das TableModel und l�dt die Daten rein.
	 */
	public ExtraTableModel()
	{
		try
		{
			extras = op.ladeExtras();
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
	
	/**
	 * Die Methode gibt die Anzahl der Spalten zur�ck.
	 * @return int
	 */
	public int getColumnCount() 
	{
		return this.columnNames.length;
	}
	
	/**
	 * Die Methode gibt die Anzahl der Zeilen zur�ck.
	 * @return int
	 */
	public int getRowCount() 
	{
		return this.extras.size();
	}
	
	/**
	 * Die Methode gibt den Spaltennamen zur�ck.
	 * @param col
	 * @return String 
	 */
	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}
	
	/**
	 * Die Methode gibt den Wert an der Stelle der �bergebenen Parameter
	 * @param row
	 * @param col
	 * @return Object
	 */
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return this.extras.get(row).getBezeichnung();
			case 1:
				return this.extras.get(row).getPreisProPerson();
			case 2:
				return this.extras.get(row).getArt();
			default:
				return null;
		}
	}
	
	/**
	 * Die Methode l�scht eine Zeile, wenn eine oder mehrere Zeilen existieren.
	 * @param row
	 */
	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.extras.remove(row);
			this.fireTableRowsDeleted(row, this.extras.size() - 1);
		}
	}
	
	/**
	 * Die Methode gibt den Inhalt einer Zeile zur�ck, wenn eine oder mehrere Zeilen existieren.
	 * @param row
	 * @return Extra
	 */
	public Extra getDataExtra(int row) 
	{
		if (row >= 0)
		{
			return this.extras.get(row);
		}
		return null;
	}
	
	/**
	 * Die Methode aktualisiert die Tabelle.
	 */
	public void databaseUpdated() 
	{
		try 
		{
			extras = op.ladeExtras();
			this.fireTableDataChanged();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
}