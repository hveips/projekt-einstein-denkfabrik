package model.tablemodels;

/**
 * @author Hendrik Veips
 */

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.raumcontroller.KategorieController;
import model.RaumKategorie;

public class RaumKategorieTableModel extends AbstractTableModel
{
	private ArrayList<RaumKategorie> raumKateg;
	private String[] columnNames = {"Kategorie", "Preis/Stunde (in �)", "Tagespreis (in �)"};
	private KategorieController op = new KategorieController();
	
	/**
	 * Der Konstruktor erstellt das TableModel und l�dt die Daten rein.
	 */
	public RaumKategorieTableModel() 
	{
		try 
		{
			raumKateg = op.ladeKategorie();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
	
	/**
	 * Die Methode gibt die Anzahl der Spalten zur�ck.
	 * @return int
	 */
	public int getColumnCount() 
	{
		return this.columnNames.length;
	}
	
	/**
	 * Die Methode gibt die Anzahl der Zeilen zur�ck.
	 * @return int
	 */
	public int getRowCount() 
	{
		return this.raumKateg.size();
	}
	
	/**
	 * Die Methode gibt den Spaltennamen zur�ck.
	 * @param col
	 * @return String 
	 */
	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}
	
	/**
	 * Die Methode gibt den Wert an der Stelle der �bergebenen Parameter
	 * @param row
	 * @param col
	 * @return Object
	 */
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return this.raumKateg.get(row).getKuerzel();
			case 1:
				return this.raumKateg.get(row).getPreis_h();
			case 2:
				return this.raumKateg.get(row).getTagespreis();
			case 3:
			default:
				return null;
		}
	}
	
	/**
	 * Die Methode l�scht eine Zeile, wenn eine oder mehrere Zeilen existieren.
	 * @param row
	 */
	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.raumKateg.remove(row);
			this.fireTableRowsDeleted(row, this.raumKateg.size() - 1);
		}
	}
	
	/**
	 * Die Methode gibt den Inhalt einer Zeile zur�ck, wenn eine oder mehrere Zeilen existieren.
	 * @param row
	 * @return RaumKategorie
	 */
	public RaumKategorie getDataRaumKategorie(int row) 
	{
		if (row >= 0)
		{
			return this.raumKateg.get(row);
		}
		return null;
	}
	
	/**
	 * Die Methode aktualisiert die Tabelle.
	 */
	public void databaseUpdated() 
	{
		try 
		{
			raumKateg = op.ladeKategorie();
			this.fireTableDataChanged();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
}