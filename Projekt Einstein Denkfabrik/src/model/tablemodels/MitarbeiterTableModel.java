package model.tablemodels;

/**
 * @author Alexander Stavski
 */

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.mitarbeitercontroller.MitarbeiterController;
import model.Mitarbeiter;

public class MitarbeiterTableModel extends AbstractTableModel 
{
	private List<Mitarbeiter> mlist;
	private String[] columnNames = { "MaID", "Name", "Vorname", "PLZ", "Ort", "Stra�e", "HausNr", "E-Mail", "Rolle" };
	private MitarbeiterController newOp = new MitarbeiterController();

	/**
	 * Laden der Mitarbeitertabelle aus der Datenbank
	 */
	public MitarbeiterTableModel() 
	{
        try 
        {
            mlist = newOp.mitarbeiterTabelleLaden();
        } 
        catch (Exception e) 
        {
        	//JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
        }
    }
	
	//Spaltenanzahl
	@Override
	public int getColumnCount() 
	{
		return this.columnNames.length;
	}

	//Zeilenanzahl
	@Override
	public int getRowCount() 
	{
		return this.mlist.size();
	}
	
	/**
	 * @param col
	 * Gibt den Spaltennamen zur�ck
	 * @return String 
	 */
	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}

	
	@Override
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return this.mlist.get(row).getMitarbeiterID();
			case 1:
				return this.mlist.get(row).getName();
			case 2:
				return this.mlist.get(row).getVorname();
			case 3:
				return this.mlist.get(row).getPlz();
			case 4:
				return this.mlist.get(row).getOrt();
			case 5:
				return this.mlist.get(row).getStra�e();
			case 6:
				return this.mlist.get(row).getHausNr();
			case 7:
				return this.mlist.get(row).getEmail();
			case 8:
				return this.mlist.get(row).getRolle();
			default:
				return null;
		}
	}

//	public Class getColumnClass(int c) 
//	{
//		return getValueAt(0, c).getClass();
//	}
	
	/**
	 * Datens�tze lassen sich nicht in der Tabelle direkt bearbeiten
	 */
	public boolean isCellEditable(int row, int col) 
	{
		if (col == 0)
		{
			return false;
		}
		return false;
	}

	//Derzeit keine Verwendung
	public void setValueAt(Object value, int row, int col) 
	{
		Mitarbeiter ma = mlist.get(row);

		switch (col) 
		{
			case 0:
				ma.setName((String) value);
				break;
			case 1:
				ma.setVorname((String) value);
				break;
			case 2:
				ma.setPlz((int) value);
				break;
			case 3:
				ma.setOrt((String) value);
				break;
			case 4:
				ma.setStra�e((String) value);
				break;
			case 5:
				ma.setHausNr((String) value);
				break;
			case 6:
				ma.setEmail((String) value);
				break;
			case 7:
				ma.setRolle((String) value);
				break;
		}
		this.fireTableCellUpdated(row, col);
		this.fireTableRowsUpdated(row, col);
	}

	public void addRow(Mitarbeiter ma) 
	{
		this.mlist.add(ma);
		this.fireTableRowsInserted(this.mlist.size() - 1, this.mlist.size() - 1);
	}

	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.mlist.remove(row);
			this.fireTableRowsDeleted(row, this.mlist.size() - 1);
		}
	}
	
	/**
	 * @param row - die Zeile
	 * @return Mitarbeiter-Objekt einer bestimmte Zeile
	 */
	public Mitarbeiter getData(int row) 
	{
		if (row >= 0)
		{
			return this.mlist.get(row);
		}
		return null;
	}

	public void setList(List<Mitarbeiter> mlist) 
	{
		this.mlist = mlist;
	}
	
	/**
	 * Aktualisiert die Tabelle
	 */
	public void databaseUpdated() 
	{
        try 
        {
            mlist = newOp.mitarbeiterTabelleLaden();
            this.fireTableDataChanged();
        } 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
        }        
    }    
}