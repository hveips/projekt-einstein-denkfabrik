package model.tablemodels;

/**
 * @author Niklas Nebeling
 */

import javax.swing.JOptionPane;

public class RechnungErstellenTableModel extends BuchungTableModel {

	/**
	 * Im Konstruktor wird die Buchungsliste aus der Datenbank geladen.
	 */
	public RechnungErstellenTableModel() 
	{
		try 
		{
			buchungen = newOp.ladeBuchungenZumRechnungErstellen();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
	
	/**
	 * Die Daten in der Datenbank wurden ver�ndert und somit ist ein erneutes Laden der Buchungen n�tig.
	 */
	public void databaseUpdated() 
	{
		try 
		{
			buchungen = newOp.ladeBuchungenZumRechnungErstellen();
			this.fireTableDataChanged();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}

}
