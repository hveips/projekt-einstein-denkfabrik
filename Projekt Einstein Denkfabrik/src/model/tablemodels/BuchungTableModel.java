package model.tablemodels;

/**
 * @author Niklas Nebeling
 */

import java.text.DecimalFormat;

/**
 * @author Hendrik Veips
 */

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.buchungcontroller.BuchungController;
import model.Buchung;

public class BuchungTableModel extends AbstractTableModel 
{
	protected ArrayList<Buchung> buchungen;
	protected String[] columnNames = { "BuNr", "RaumNr", "KuID", "Name", "Vorname", "von", "bis", "Gesamtbetrag", "Pax" };
	protected BuchungController newOp = new BuchungController();
	private DecimalFormat dfRaum = new DecimalFormat("000");
	private DecimalFormat dfBetrag = new DecimalFormat(".00");

	/**
	 * Im Konstruktor wird die Buchungsliste aus der Datenbank geladen.
	 */
	public BuchungTableModel() 
	{
		try 
		{
			buchungen = newOp.ladeTeildatenAusBuchungenUndKunde();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}

	// Spaltenanzahl
	@Override
	public int getColumnCount() 
	{
		return this.columnNames.length;
	}

	// Zeilenanzahl
	@Override
	public int getRowCount() 
	{
		return this.buchungen.size();
	}

	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return this.buchungen.get(row).getBuchungsNr();
			case 1:
				return dfRaum.format(this.buchungen.get(row).getRaumNr());
			case 2:
				return this.buchungen.get(row).getKuID();
			case 3:
				return this.buchungen.get(row).getName();
			case 4:
				return this.buchungen.get(row).getVorname();
			case 5:
				return this.buchungen.get(row).getDatumString();
			case 6:
				return this.buchungen.get(row).getEnddatum();
			case 7:
				return dfBetrag.format(this.buchungen.get(row).getGesamtbetrag()) + "�";
			case 8:
				return this.buchungen.get(row).getPersonenanzahl();
			default:
				return null;
		}
	}

	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.buchungen.remove(row);
			this.fireTableRowsDeleted(row, this.buchungen.size() - 1);
		}
	}

	public boolean isCellEditable(int row, int col) 
	{
		if (col == 0)
		{
			return false;
		}
		return false;
	}
	
	/**
	 * Gibt das Buchungsobjekt aus der Liste mit dem Index row zur�ck.
	 * @param row
	 * @return
	 */
	public Buchung getData(int row) 
	{
		if (row >= 0)
		{
			return this.buchungen.get(row);
		}
		return null;
	}

	/**
	 * Die Daten in der Datenbank wurden ver�ndert und somit ist ein erneutes Laden der Buchungen n�tig.
	 */
	public void databaseUpdated() 
	{
		try 
		{
			buchungen = newOp.ladeTeildatenAusBuchungenUndKunde();
			this.fireTableDataChanged();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
}