package model.tablemodels;

/**
 * @author Niklas Nebeling
 */

import java.text.DecimalFormat;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.raumcontroller.RaumController;
import model.Raum;

public class RaumBuchenTableModel extends AbstractTableModel 
{	
	private List<Raum> rlist;
	private String[] columnNames = { "Raumnummer", "Sitzpl�tze", "Kategorie"};
	private RaumController db = new RaumController();
	DecimalFormat df = new DecimalFormat("000");
	
	/**
	 * Im Konstruktor wird die Raumsliste aus der Datenbank geladen.
	 */
	public RaumBuchenTableModel(String anfangsdatum, String enddatum, boolean eint�gig) 
	{
		try 
		{
			rlist = db.ladeRaumListeNachDatum(anfangsdatum, enddatum, eint�gig);
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}

	public int getColumnCount() 
	{
		return columnNames.length;
	}
	
	public int getRowCount() 
	{
		return rlist.size();
	}
	
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return df.format(this.rlist.get(row).getRaumNr());
			case 1:
				return this.rlist.get(row).getSitzpl�tze();
			case 2:
				return this.rlist.get(row).getK�rzel();
			default:
				return null;
		}
	}
	
	public Class getColumnClass(int c) 
	{
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) 
	{
		if (col == 0)
			return false;
		return false;
	}
	
	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}
	
	public void setValueAt(Object value, int row, int col) 
	{
		Raum raum = rlist.get(row);

		switch (col) 
		{
			case 0:
				raum.setSitzpl�tze((int) value);
				break;
			case 1:
				raum.setK�rzel((String) value);
				break;
		}
		this.fireTableCellUpdated(row, col);
		this.fireTableRowsUpdated(row, col);
	}
	
	public void addRow(Raum raum) 
	{
		this.rlist.add(raum);
		this.fireTableRowsInserted(this.rlist.size() - 1, this.rlist.size() - 1);
	}

	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.rlist.remove(row);
			this.fireTableRowsDeleted(row, this.rlist.size() - 1);
		}
	}

	/**
	 * Gibt das Raumobjekt aus der Liste mit dem Index row zur�ck.
	 * @param row
	 * @return
	 */
	public Raum getData(int row) 
	{
		if (row >= 0)
			return this.rlist.get(row);
		return null;
	}
	
	/**
	 * Setzt die Liste.
	 * @param rlist
	 */
	public void setList(List<Raum> rlist) 
	{
		this.rlist = rlist;
	}
}