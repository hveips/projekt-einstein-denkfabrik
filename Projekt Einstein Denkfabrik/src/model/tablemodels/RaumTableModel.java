package model.tablemodels;

/**
 * @author Niklas Nebeling
 */

import java.text.DecimalFormat;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.raumcontroller.RaumController;
import model.Raum;

public class RaumTableModel extends AbstractTableModel 
{	
	private List<Raum> rlist;
	private String[] columnNames = { "Raumnummer", "Sitzpl�tze", "Kategorie", "In Benutzung"};
	private RaumController db = new RaumController();
	private DecimalFormat df = new DecimalFormat("000");
	
	/**
	 * Im Konstruktor wird die Raumsliste aus der Datenbank geladen.
	 */
	public RaumTableModel() 
	{
		try 
		{
			rlist = db.ladeRaumListe();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}

	//Spaltenanzahl
	public int getColumnCount() 
	{
		return columnNames.length;
	}

	//Zeilenanzahl
	public int getRowCount() 
	{
		return rlist.size();
	}

	@Override
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return df.format(this.rlist.get(row).getRaumNr());
			case 1:
				return this.rlist.get(row).getSitzpl�tze();
			case 2:
				return this.rlist.get(row).getK�rzel();
			case 3:
				return this.rlist.get(row).getInBenutzung();
			default:
				return null;
		}
	}
	
	public Class getColumnClass(int c) 
	{
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) 
	{
		if (col == 0)
			return false;
		return false;
	}
	
	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}
	
	public void setValueAt(Object value, int row, int col) 
	{
		Raum raum = rlist.get(row);

		switch (col) 
		{
			case 0:
				raum.setSitzpl�tze((int) value);
				break;
			case 1:
				raum.setK�rzel((String) value);
				break;	
			case 2:
				raum.setInBenutzung((String) value);
				break;
		}
		this.fireTableCellUpdated(row, col);
		this.fireTableRowsUpdated(row, col);
	}
	
	public void addRow(Raum raum) 
	{
		this.rlist.add(raum);
		this.fireTableRowsInserted(this.rlist.size() - 1, this.rlist.size() - 1);
	}

	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.rlist.remove(row);
			this.fireTableRowsDeleted(row, this.rlist.size() - 1);
		}
	}
	
	/**
	 * Gibt das Raumobjekt aus der Liste mit dem Index row zur�ck.
	 * @param row
	 * @return
	 */
	public Raum getData(int row) 
	{
		if (row >= 0)
			return this.rlist.get(row);
		return null;
	}

	/**
	 * Setzt die Liste.
	 * @param rlist
	 */
	public void setList(List<Raum> rlist) 
	{
		this.rlist = rlist;
	}
	
	/**
	 * Die Daten in der Datenbank wurden ver�ndert und somit ist ein erneutes Laden der R�ume n�tig.
	 */
	public void databaseUpdated() 
	{
        try 
        {
            rlist = db.ladeRaumListe();
            this.fireTableDataChanged();
        } 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
        }        
    }    
}