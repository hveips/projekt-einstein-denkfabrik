package model.tablemodels;

/**
 * @author Alexander Stavski
 */

import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.reservierungcontroller.ReservierungController;
import model.Reservierung;

public class ReservierungTableModel extends AbstractTableModel 
{
	ArrayList<Reservierung> reservierungen;
	private String[] columnNames = { "ResNr", "RaumNr", "KuID", "Name", "Vorname", "von", "bis", "Gesamtbetrag", "Pax"};
	private ReservierungController newOp = new ReservierungController();
	protected DecimalFormat dfRaum = new DecimalFormat("000");
	private DecimalFormat dfBetrag = new DecimalFormat(".00");

	public ReservierungTableModel() 
	{
		try 
		{
			reservierungen = newOp.ladeTeildatenAusReservierungenUndKunde();
		} 
		catch (Exception e) 
		{
			//JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}

	// Spaltenanzahl
	@Override
	public int getColumnCount() 
	{
		return this.columnNames.length;
	}

	// Zeilenanzahl
	@Override
	public int getRowCount() 
	{
		return this.reservierungen.size();

	}

	public String getColumnName(int col) 
	{
		return this.columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) 
	{
		switch (col) 
		{
			case 0:
				return this.reservierungen.get(row).getResNr();
			case 1:
				return dfRaum.format(this.reservierungen.get(row).getRaumNr());
			case 2:
				return this.reservierungen.get(row).getKdID();
			case 3:
				return this.reservierungen.get(row).getName();
			case 4:
				return this.reservierungen.get(row).getVorname();
			case 5:
				return this.reservierungen.get(row).getAnfangsdatum();
			case 6:
				return this.reservierungen.get(row).getEnddatum();
			case 7:
				return dfBetrag.format(this.reservierungen.get(row).getGesamtbetrag()) +"�";
			case 8:
				return this.reservierungen.get(row).getPersonenanzahl();
			default:
				return null;
		}
	}

	public void removeRow(int row) 
	{
		if (row >= 0) 
		{
			this.reservierungen.remove(row);
			this.fireTableRowsDeleted(row, this.reservierungen.size() - 1);
		}
	}

	public boolean isCellEditable(int row, int col) 
	{
		if (col == 0)
		{
			return false;
		}
		return false;
	}

	public Reservierung getDataReservierung(int row) 
	{
		if (row >= 0)
		{
			return this.reservierungen.get(row);
		}
		return null;
	}

	public void databaseUpdated() 
	{
		try 
		{
			reservierungen = newOp.ladeTeildatenAusReservierungenUndKunde();
			this.fireTableDataChanged();
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}
	}
}