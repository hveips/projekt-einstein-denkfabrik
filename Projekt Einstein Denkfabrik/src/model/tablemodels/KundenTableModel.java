package model.tablemodels;
  
/**
 * @author Marvin Plepis
 */

import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import controller.kundencontroller.KundeController;
import model.Kunde;

public class KundenTableModel extends AbstractTableModel {
	private List<Kunde> kdList;
	private String[] columns = { "KuID", "Name", "Vorname", "PLZ", "Ort", "Stra�e", "HausNr", "E-Mail",
			"Arbeitsplatz" };
	private KundeController db = new KundeController();

	/**
	 * Laden der Kunden Tabelle aus der Datenbank
	 */
	public KundenTableModel() {
		try {
			kdList = db.kundenTabelleLaden();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error" + e.getMessage());
		}
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	/**
	 * Zeilenzahl
	 */
	@Override
	public int getColumnCount() {
		return this.columns.length;
	}

	/**
	 * Spaltenzahl
	 */
	@Override
	public int getRowCount() {
		return this.kdList.size();
	}

	/**
	 * @param col
	 *            Gibt den Namen zur�ck
	 * @return String
	 */
	public String getColumnName(int col) {
		return this.columns[col];
	}

	/**
	 * Gibt Inhalt der jeweilen Spalten zur�ck
	 */
	@Override
	public Object getValueAt(int row, int col) {
		switch (col) {
		case 0:
			return this.kdList.get(row).getKundenID();
		case 1:
			return this.kdList.get(row).getName();
		case 2:
			return this.kdList.get(row).getVorname();
		case 3:
			return this.kdList.get(row).getPlz();
		case 4:
			return this.kdList.get(row).getOrt();
		case 5:
			return this.kdList.get(row).getStra�e();
		case 6:
			return this.kdList.get(row).getHausNr();
		case 7:
			return this.kdList.get(row).getEmail();
		case 8:
			return this.kdList.get(row).getArbeitsplatz();
		default:
			return null;
		}
	}

	/**
	 * Methode, um Inhalt der jeweiligen Zellen zu �ndern
	 */
	public void setValueAt(Object value, int row, int col) {
		Kunde kd = kdList.get(row);

		switch (col) {
		case 0:
			kd.setName((String) value);
			break;
		case 1:
			kd.setVorname((String) value);
			break;
		case 2:
			kd.setPlz((int) value);
			break;
		case 3:
			kd.setOrt((String) value);
			break;
		case 4:
			kd.setStra�e((String) value);
			break;
		case 5:
			kd.setHausNr((String) value);
			break;
		case 6:
			kd.setEmail((String) value);
			break;
		case 7:
			kd.setArbeitsplatz((String) value);
			break;
		}
		this.fireTableCellUpdated(row, col);
		this.fireTableRowsUpdated(row, col);
	}

	/**
	 * @param row
	 * @return - gibt Kunde zur�ck der in der Zeilennummer "row" steht
	 */
	public Kunde getRow(int row)
	{
		return this.kdList.get(row);
	}
	/**
	 * Aktualisiert Kunden Tabelle
	 */
	public void loadTable() {
		try {
			kdList = db.kundenTabelleLaden();
			this.fireTableDataChanged();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Fehler:" + e.getMessage());
		}

	}
/**
 * Verhindert, dass Datens�tze in der View ver�ndert werden k�nnen.
 */
	public boolean isCellEditable(int row, int col) {
		if (col == 0) {
			return false;
		}
		return false;
	}

	public void addRow(Kunde kd) {
		this.kdList.add(kd);
		this.fireTableRowsInserted(this.kdList.size() - 1, this.kdList.size() - 1);
	}

	public void removeRow(int row) {
		if (row >= 0) {
			this.kdList.remove(row);
			this.fireTableRowsDeleted(row, this.kdList.size() - 1);
		}
	}
/**
 * 
 * @param row
 * @return Kunden Objekt einer bestimmten reihe
 */
	public Kunde getData(int row) {
		if (row >= 0) {
			return this.kdList.get(row);
		}
		return null;
	}

	public void setlist(List<Kunde> kdList) {
		this.kdList = kdList;
	}
}